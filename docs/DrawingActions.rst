Drawing Actions
===============


Actions Menu consists of many useful commands that can be applied during your quick modeling.


Region Delete
-------------
This command allows the user to delete some of the existing shapes by making a rectangular selection.



Region Crop
-----------
This command allows the the user to select portion of the drawing to be kept and delete the other non-selected shapes.



 
Total Translation
-----------------
This command allows the user to move the whole drawing from the first clicked location to the second clicked location.



Total Rotation
--------------
The command makes allows the user to rotate the whole drawing with an angles starting from the x-axis.



Roof Exterior Generation
------------------------
The command generates the roof exterior by determining the boundaries of the plan layer and ofsetting it outward to make the exterior. 
The exterior is placed on the roof layer.



Download Current Drawing
------------------------
Takes the the current drawing and downloading it into Dxf file.


Download Whole Drawing
----------------------
Takes the whole layers and inject them into one dxf file with each layer into dxf layer.
