.. HappyArchitect documentation master file, created by
   sphinx-quickstart on Sun May 15 13:11:54 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HappyArchitect's documentation!
==========================================

Contents:


.. toctree::
   :maxdepth: 2

   Introduction
   DrawingCursor
   DrawingModes
   BuildingElements
   Layers
   DrawingActions 

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

