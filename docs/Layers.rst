Layers
======

Happy Architect consists of 5 layers that maps the required building layers in any building.

- Floor Plan Layer
- Roof Plan Layer 
- Front Elevation
- Back Elevation
- Left Elevation
- Right Elevation
    
.. image:: Layers.png

Converting Between these layers requires the user to click on the top left radio button.

Another option is to include the drawing on specific layer into  the view of the selected layer by checking the box at the lower right position.

.. image:: layerselection.png




