Introduction & Overview
=======================

Happy Architect Online is a tool that helps the architect to draft buildings in a quick and efficient way.


Main Screen Features
--------------------
The main screen of the program can be illustrated into different parts as shown in the next figures
    
    
.. image:: MainScreenFeatures.png

.. image:: sample-project.png

.. image:: sample-project-isometry.png


