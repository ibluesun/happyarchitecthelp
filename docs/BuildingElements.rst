Building Elements
=================


Walls
-----

All lines drawn in the lines mode and rectangles mode are considered walls.

.. image:: Room-Walls.PNG

Windows
-------

Drawing line over the wall line generate a window from the start to the end of the overdrawn line

.. image:: Window.PNG

Doors
-----

Doors are drawn by overlapping a line over the window.

.. image:: Door.PNG

Opennings
---------

Opennings are drawn by overlapping a line over the door.

.. image:: Openning.PNG

