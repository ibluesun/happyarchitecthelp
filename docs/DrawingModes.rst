Drawing Modes
=============

There are three drawing modes Lines, Rectangles, and Dimensions mode. These modes can be selected by mouse double click.
Double click the left mouse button will change the current drawing mode.
The current drawing mode can be observed from the top right corner of the drawing circle.

 


Lines
-----
Default drawing mode when openning the website is the lines mode. In this mode the user can select start and end points by using mouse clicks.

.. image:: CursorLinesMode.png



Rectangles
----------
Rectangles drawing serves as a quick drafting tool for making connected rooms.

.. image:: CursorRectanglesMode.png





Dimensions
----------
Dimensions drawing permits the user to draw line dimensions on the drawings in any required confiugrations

.. image:: CursorDimensionsMode.png