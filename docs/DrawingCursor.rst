Smart Drawing Cursor
==============

The drawing cursor is a smart cursor that gives a feed back of the current drawing mode in addition to any snapping option currently applied to the drawn shapes.

Cursor Corners
--------------

- Top Right Corner is used for the feedback of the current drawing mode
- Top Left Corner is used for the feedback of the available snapping option of the drawing.
- Lower Left corner is used distinguish between in and out layers boundaries switching.
