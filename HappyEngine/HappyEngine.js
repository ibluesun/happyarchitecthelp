var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AngleQuadrant;
(function (AngleQuadrant) {
    AngleQuadrant[AngleQuadrant["First"] = 0] = "First";
    AngleQuadrant[AngleQuadrant["Second"] = 1] = "Second";
    AngleQuadrant[AngleQuadrant["Third"] = 2] = "Third";
    AngleQuadrant[AngleQuadrant["Fourth"] = 3] = "Fourth";
})(AngleQuadrant || (AngleQuadrant = {}));
var Angle = (function () {
    function Angle(radians) {
        this._radians = radians;
    }
    Angle.FromDegrees = function (degrees) {
        var r = degrees * Math.PI / 180;
        return new Angle(r);
    };
    Object.defineProperty(Angle.prototype, "Radians", {
        get: function () {
            return this._radians;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "Degrees", {
        get: function () {
            return this._radians * 180 / Math.PI;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "Negate", {
        get: function () {
            return new Angle(-this._radians);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "Revolutions", {
        get: function () {
            return this._radians / (2 * Math.PI);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "AbsoluteAngle", {
        get: function () {
            if (this.Revolutions <= 1) {
                if (this._radians >= 0)
                    return new Angle(this._radians);
                else
                    return new Angle(2 * Math.PI + this._radians);
            }
            else {
                var revs = Math.floor(this.Revolutions);
                var g = this.Revolutions - revs;
                if (g >= 0)
                    return new Angle(g);
                else
                    return new Angle(2 * Math.PI + g);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "RelativeAngle", {
        get: function () {
            if (this.AbsoluteAngle.Degrees > 180) {
                var relative = new Angle(-1 * (Angle.Two_Pi.Radians - this.AbsoluteAngle.Radians));
                return relative;
            }
            return this.AbsoluteAngle;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "Quadrant", {
        get: function () {
            var rad = this.AbsoluteAngle.Radians;
            if (rad <= Math.PI / 2 && rad >= 0)
                return AngleQuadrant.First;
            else if (rad <= Math.PI && rad > Math.PI / 2)
                return AngleQuadrant.Second;
            else if (rad <= 3 / 2 * Math.PI && rad > Math.PI)
                return AngleQuadrant.Third;
            else
                return AngleQuadrant.Fourth;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsClockwise", {
        get: function () {
            return this._radians < 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsCounterClockwise", {
        get: function () {
            return this._radians > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsRight", {
        get: function () {
            return Math.PI / 2 == this._radians;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsStraight", {
        get: function () {
            return Math.PI == this._radians;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsAcute", {
        get: function () {
            return this._radians < Math.PI / 2;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsObtuse", {
        get: function () {
            return (this._radians > Math.PI / 2 && this._radians < Math.PI);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsReflex", {
        get: function () {
            return (this._radians > Math.PI && this._radians < 2 * Math.PI);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "Cos", {
        get: function () {
            return Math.cos(this._radians);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "Sin", {
        get: function () {
            return Math.sin(this._radians);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "Tan", {
        get: function () {
            return Math.tan(this._radians);
        },
        enumerable: true,
        configurable: true
    });
    Angle.prototype.Equals = function (obj) {
        return this._radians == obj._radians;
    };
    Angle.prototype.toString = function () {
        return "Degrees: " + this.Degrees.toString();
    };
    Object.defineProperty(Angle, "Pi", {
        get: function () {
            return new Angle(Math.PI);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle, "Half_Pi", {
        get: function () {
            return new Angle(Math.PI / 2.0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle, "Two_Pi", {
        get: function () {
            return new Angle(Math.PI * 2.0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle, "ThirdTwo_Pi", {
        get: function () {
            return new Angle(Math.PI * 3 / 2);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle, "Zero", {
        get: function () {
            return new Angle(0.0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Angle.prototype, "IsZero", {
        get: function () {
            return this._radians == 0.0;
        },
        enumerable: true,
        configurable: true
    });
    Angle.Subtract = function (left, right) {
        return new Angle(left._radians - right._radians);
    };
    Angle.Add = function (left, right) {
        return new Angle(left._radians + right._radians);
    };
    Angle.GetDeltaAngle = function (firstAngle, secondAngle) {
        var PF = firstAngle;
        var PL = secondAngle;
        var DeltaAngle;
        if (PF.Quadrant == AngleQuadrant.First) {
            if (PL.Quadrant == AngleQuadrant.First) {
                DeltaAngle = Angle.Subtract(PL, PF);
            }
            if (PL.Quadrant == AngleQuadrant.Second) {
                DeltaAngle = Angle.Subtract(PL, PF);
            }
            if (PL.Quadrant == AngleQuadrant.Third) {
                DeltaAngle = Angle.Subtract(PL.AbsoluteAngle, PF);
            }
            if (PL.Quadrant == AngleQuadrant.Fourth) {
                DeltaAngle = Angle.Subtract(PL.AbsoluteAngle, PF);
            }
        }
        if (PF.Quadrant == AngleQuadrant.Second) {
            if (PL.Quadrant == AngleQuadrant.First) {
                DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF, PL));
            }
            if (PL.Quadrant == AngleQuadrant.Second) {
                DeltaAngle = Angle.Subtract(PL, PF);
            }
            if (PL.Quadrant == AngleQuadrant.Third) {
                DeltaAngle = Angle.Subtract(PL.AbsoluteAngle, PF);
            }
            if (PL.Quadrant == AngleQuadrant.Fourth) {
                DeltaAngle = Angle.Subtract(PL.AbsoluteAngle, PF);
            }
        }
        if (PF.Quadrant == AngleQuadrant.Third) {
            if (PL.Quadrant == AngleQuadrant.First) {
                DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.AbsoluteAngle, PL));
            }
            if (PL.Quadrant == AngleQuadrant.Second) {
                DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.AbsoluteAngle, PL));
            }
            if (PL.Quadrant == AngleQuadrant.Third) {
                DeltaAngle = Angle.Subtract(PL.AbsoluteAngle, PF.AbsoluteAngle);
            }
            if (PL.Quadrant == AngleQuadrant.Fourth) {
                DeltaAngle = Angle.Subtract(PL.AbsoluteAngle, PF.AbsoluteAngle);
            }
        }
        if (PF.Quadrant == AngleQuadrant.Fourth) {
            if (PL.Quadrant == AngleQuadrant.First) {
                DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.AbsoluteAngle, PL));
            }
            if (PL.Quadrant == AngleQuadrant.Second) {
                DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.AbsoluteAngle, PL));
            }
            if (PL.Quadrant == AngleQuadrant.Third) {
                DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.AbsoluteAngle, PL.AbsoluteAngle));
            }
            if (PL.Quadrant == AngleQuadrant.Fourth) {
                DeltaAngle = Angle.Subtract(PL.AbsoluteAngle, PF.AbsoluteAngle);
            }
        }
        if (PF.Quadrant == PL.Quadrant) {
            if (DeltaAngle.Radians < 0) {
                DeltaAngle = new Angle(Angle.Two_Pi.Radians - Math.abs(DeltaAngle.Radians));
            }
        }
        return DeltaAngle;
    };
    return Angle;
}());
var ColorByte4 = (function () {
    function ColorByte4(r, g, b, a) {
        this._R = r;
        this._G = g;
        this._B = b;
        this._A = a;
        this._Key = "R" + this._R.toString() + "G" + this._G.toString() + "B" + this._B.toString() + "A" + this._A.toString();
    }
    ColorByte4.prototype.Copy = function () {
        return new ColorByte4(this._R, this._G, this._B, this._A);
    };
    Object.defineProperty(ColorByte4.prototype, "R", {
        get: function () {
            return this._R;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColorByte4.prototype, "G", {
        get: function () {
            return this._G;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColorByte4.prototype, "B", {
        get: function () {
            return this._B;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColorByte4.prototype, "A", {
        get: function () {
            return this._A;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColorByte4.prototype, "Key", {
        get: function () {
            return this._Key;
        },
        enumerable: true,
        configurable: true
    });
    ColorByte4.prototype.Equals = function (o) {
        if (this._A == o.A && this._B == o.B && this._G == o.G && this._R == o.R)
            return true;
        return false;
    };
    ColorByte4.prototype.IncrementOne = function () {
        var r = this._R;
        var g = this._G;
        var b = this._B;
        var a = this._A;
        if (r == 255) {
            r = 0;
            if (g == 255) {
                g = 0;
                if (b == 255) {
                    b = 0;
                    if (a == 255) {
                        throw ("Can't find more than 32 bits for incrementing");
                    }
                    else {
                        a++;
                    }
                }
                else {
                    b++;
                }
            }
            else {
                g++;
            }
        }
        else {
            r++;
        }
        return new ColorByte4(r, g, b, a);
    };
    ColorByte4.prototype.ToFloatArray = function () {
        var fl = new Float32Array([
            this._R / 255, this._G / 255, this._B / 255, this._A / 255
        ]);
        return fl;
    };
    ColorByte4.NullColor = new ColorByte4(255, 255, 255, 255);
    ColorByte4.VoidColor = new ColorByte4(0, 0, 0, 0);
    ColorByte4.ZeroColor = new ColorByte4(0, 0, 0, 255);
    return ColorByte4;
}());
var FloatingMath = (function () {
    function FloatingMath() {
    }
    FloatingMath.FixedEqual = function (first, second) {
        var fixed_first = FloatingMath.FixedNumber(first);
        var fixed_second = FloatingMath.FixedNumber(second);
        return fixed_first == fixed_second;
    };
    FloatingMath.NearlyEqual = function (first, second) {
        var delta = Math.abs(first - second);
        if (delta <= FloatingMath.Epsilon)
            return true;
        return false;
    };
    FloatingMath.Vector2NearlyEqual = function (first, second) {
        if (FloatingMath.NearlyEqual(first.X, second.X) && FloatingMath.NearlyEqual(first.Y, second.Y))
            return true;
        else
            return false;
    };
    FloatingMath.Vector3NearlyEqual = function (first, second) {
        if (FloatingMath.NearlyEqual(first.X, second.X) && FloatingMath.NearlyEqual(first.Y, second.Y) && FloatingMath.NearlyEqual(first.Z, second.Z))
            return true;
        else
            return false;
    };
    FloatingMath.FixedNumber = function (n) {
        return Number(n.toFixed(this.DecimalDigits));
    };
    FloatingMath.sgn = function (x) {
        if (x < 0)
            return -1;
        return 1;
    };
    FloatingMath.MinimumInteger = function (x) {
        if (x > 0)
            return Math.floor(x);
        if (x < 0)
            return -1 * Math.ceil(Math.abs(x));
        return 0;
    };
    FloatingMath.MaximumInteger = function (x) {
        if (x > 0)
            return Math.ceil(x);
        if (x < 0)
            return -1 * Math.floor(Math.abs(x));
        return 0;
    };
    FloatingMath.IsInteger = function (n) {
        return n % 1 === 0;
    };
    FloatingMath.DecimalDigits = 2;
    FloatingMath.Epsilon = 1e-2;
    return FloatingMath;
}());
var WebGL;
(function (WebGL) {
    var Matrix3 = (function () {
        function Matrix3(row0, row1, row2) {
            this.Row0 = row0;
            this.Row1 = row1;
            this.Row2 = row2;
        }
        Matrix3.prototype.ToFloatArray = function () {
            var fl = new Float32Array([
                this.Row0.X, this.Row0.Y, this.Row0.Z,
                this.Row1.X, this.Row1.Y, this.Row1.Z,
                this.Row2.X, this.Row2.Y, this.Row2.Z,
            ]);
            return fl;
        };
        Object.defineProperty(Matrix3, "Zero", {
            get: function () {
                var r0 = new WebGL.Vector3(0, 0, 0);
                var r1 = new WebGL.Vector3(0, 0, 0);
                var r2 = new WebGL.Vector3(0, 0, 0);
                return new Matrix3(r0, r1, r2);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Matrix3, "Identity", {
            get: function () {
                var r0 = new WebGL.Vector3(1, 0, 0);
                var r1 = new WebGL.Vector3(0, 1, 0);
                var r2 = new WebGL.Vector3(0, 0, 1);
                return new Matrix3(r0, r1, r2);
            },
            enumerable: true,
            configurable: true
        });
        Matrix3.Multiply = function (left, right) {
            var result = Matrix3.Zero;
            var lM11 = left.Row0.X;
            var lM12 = left.Row0.Y;
            var lM13 = left.Row0.Z;
            var lM21 = left.Row1.X;
            var lM22 = left.Row1.Y;
            var lM23 = left.Row1.Z;
            var lM31 = left.Row2.X;
            var lM32 = left.Row2.Y;
            var lM33 = left.Row2.Z;
            var rM11 = right.Row0.X;
            var rM12 = right.Row0.Y;
            var rM13 = right.Row0.Z;
            var rM21 = right.Row1.X;
            var rM22 = right.Row1.Y;
            var rM23 = right.Row1.Z;
            var rM31 = right.Row2.X;
            var rM32 = right.Row2.Y;
            var rM33 = right.Row2.Z;
            result.Row0.X = ((lM11 * rM11) + (lM12 * rM21)) + (lM13 * rM31);
            result.Row0.Y = ((lM11 * rM12) + (lM12 * rM22)) + (lM13 * rM32);
            result.Row0.Z = ((lM11 * rM13) + (lM12 * rM23)) + (lM13 * rM33);
            result.Row1.X = ((lM21 * rM11) + (lM22 * rM21)) + (lM23 * rM31);
            result.Row1.Y = ((lM21 * rM12) + (lM22 * rM22)) + (lM23 * rM32);
            result.Row1.Z = ((lM21 * rM13) + (lM22 * rM23)) + (lM23 * rM33);
            result.Row2.X = ((lM31 * rM11) + (lM32 * rM21)) + (lM33 * rM31);
            result.Row2.Y = ((lM31 * rM12) + (lM32 * rM22)) + (lM33 * rM32);
            result.Row2.Z = ((lM31 * rM13) + (lM32 * rM23)) + (lM33 * rM33);
            return result;
        };
        Matrix3.Add = function (left, right) {
            var result;
            result.Row0 = WebGL.Vector3.Add(left.Row0, right.Row0);
            result.Row1 = WebGL.Vector3.Add(left.Row1, right.Row1);
            result.Row2 = WebGL.Vector3.Add(left.Row2, right.Row2);
            return result;
        };
        Matrix3.Subtract = function (left, right) {
            var result;
            result.Row0 = WebGL.Vector3.Subtract(left.Row0, right.Row0);
            result.Row1 = WebGL.Vector3.Subtract(left.Row1, right.Row1);
            result.Row2 = WebGL.Vector3.Subtract(left.Row2, right.Row2);
            return result;
        };
        Matrix3.Invert = function (mat) {
            var colIdx = [0, 0, 0];
            var rowIdx = [0, 0, 0];
            var pivotIdx = [-1, -1, -1];
            var inverse = [
                [mat.Row0.X, mat.Row0.Y, mat.Row0.Z],
                [mat.Row1.X, mat.Row1.Y, mat.Row1.Z],
                [mat.Row2.X, mat.Row2.Y, mat.Row2.Z]
            ];
            var icol = 0;
            var irow = 0;
            var i, j, k;
            for (i = 0; i < 3; i++) {
                var maxPivot = 0.0;
                for (j = 0; j < 3; j++) {
                    if (pivotIdx[j] != 0) {
                        for (k = 0; k < 3; ++k) {
                            if (pivotIdx[k] == -1) {
                                var absVal = Math.abs(inverse[j][k]);
                                if (absVal > maxPivot) {
                                    maxPivot = absVal;
                                    irow = j;
                                    icol = k;
                                }
                            }
                            else if (pivotIdx[k] > 0) {
                                result = mat;
                                return;
                            }
                        }
                    }
                }
                ++(pivotIdx[icol]);
                if (irow != icol) {
                    for (k = 0; k < 3; ++k) {
                        var f = inverse[irow][k];
                        inverse[irow][k] = inverse[icol][k];
                        inverse[icol][k] = f;
                    }
                }
                rowIdx[i] = irow;
                colIdx[i] = icol;
                var pivot = inverse[icol][icol];
                if (pivot == 0.0) {
                    throw "Matrix is singular and cannot be inverted.";
                }
                var oneOverPivot = 1.0 / pivot;
                inverse[icol][icol] = 1.0;
                for (k = 0; k < 3; ++k)
                    inverse[icol][k] *= oneOverPivot;
                for (j = 0; j < 3; ++j) {
                    if (icol != j) {
                        var f = inverse[j][icol];
                        inverse[j][icol] = 0.0;
                        for (k = 0; k < 3; ++k)
                            inverse[j][k] -= inverse[icol][k] * f;
                    }
                }
            }
            for (j = 2; j >= 0; --j) {
                var ir = rowIdx[j];
                var ic = colIdx[j];
                for (k = 0; k < 3; ++k) {
                    var f = inverse[k][ir];
                    inverse[k][ir] = inverse[k][ic];
                    inverse[k][ic] = f;
                }
            }
            var result = Matrix3.Zero;
            result.Row0.X = inverse[0][0];
            result.Row0.Y = inverse[0][1];
            result.Row0.Z = inverse[0][2];
            result.Row1.X = inverse[1][0];
            result.Row1.Y = inverse[1][1];
            result.Row1.Z = inverse[1][2];
            result.Row2.X = inverse[2][0];
            result.Row2.Y = inverse[2][1];
            result.Row2.Z = inverse[2][2];
            return result;
        };
        Matrix3.CreateFromAxisAngle = function (axis, angle) {
            axis.Normalize();
            var axisX = axis.X;
            var axisY = axis.Y;
            var axisZ = axis.Z;
            var cos = Math.cos(-angle);
            var sin = Math.sin(-angle);
            var t = 1.0 - cos;
            var tXX = t * axisX * axisX;
            var tXY = t * axisX * axisY;
            var tXZ = t * axisX * axisZ;
            var tYY = t * axisY * axisY;
            var tYZ = t * axisY * axisZ;
            var tZZ = t * axisZ * axisZ;
            var sinX = sin * axisX;
            var sinY = sin * axisY;
            var sinZ = sin * axisZ;
            var result = Matrix3.Identity;
            result.Row0.X = tXX + cos;
            result.Row0.Y = tXY - sinZ;
            result.Row0.Z = tXZ + sinY;
            result.Row1.X = tXY + sinZ;
            result.Row1.Y = tYY + cos;
            result.Row1.Z = tYZ - sinX;
            result.Row2.X = tXZ - sinY;
            result.Row2.Y = tYZ + sinX;
            result.Row2.Z = tZZ + cos;
            return result;
        };
        Matrix3.CreateFromQuaternion = function (q) {
            return Matrix3.CreateFromAxisAngle(q.AxisAngle.Axis, q.AxisAngle.Angle);
        };
        Matrix3.prototype.Transpose = function () {
            var result = Matrix3.Identity;
            result.Row0.X = this.Row0.X;
            result.Row0.Y = this.Row1.X;
            result.Row0.Z = this.Row2.X;
            result.Row1.X = this.Row0.Y;
            result.Row1.Y = this.Row1.Y;
            result.Row1.Z = this.Row2.Y;
            result.Row2.X = this.Row0.Z;
            result.Row2.Y = this.Row1.Z;
            result.Row2.Z = this.Row2.Z;
            return result;
        };
        Matrix3.CreateScale = function (x, y, z) {
            var result = this.Identity;
            result.Row0.X = x;
            result.Row1.Y = y;
            result.Row2.Z = z;
            return result;
        };
        return Matrix3;
    }());
    WebGL.Matrix3 = Matrix3;
    var Matrix4 = (function () {
        function Matrix4(row0, row1, row2, row3) {
            this.Row0 = row0;
            this.Row1 = row1;
            this.Row2 = row2;
            this.Row3 = row3;
        }
        Matrix4.prototype.ToFloatArray = function () {
            var fl = new Float32Array([
                this.Row0.X, this.Row0.Y, this.Row0.Z, this.Row0.W,
                this.Row1.X, this.Row1.Y, this.Row1.Z, this.Row1.W,
                this.Row2.X, this.Row2.Y, this.Row2.Z, this.Row2.W,
                this.Row3.X, this.Row3.Y, this.Row3.Z, this.Row3.W,
            ]);
            return fl;
        };
        Object.defineProperty(Matrix4, "Zero", {
            get: function () {
                var r0 = new WebGL.Vector4(0, 0, 0, 0);
                var r1 = new WebGL.Vector4(0, 0, 0, 0);
                var r2 = new WebGL.Vector4(0, 0, 0, 0);
                var r3 = new WebGL.Vector4(0, 0, 0, 0);
                return new Matrix4(r0, r1, r2, r3);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Matrix4, "Identity", {
            get: function () {
                var r0 = new WebGL.Vector4(1, 0, 0, 0);
                var r1 = new WebGL.Vector4(0, 1, 0, 0);
                var r2 = new WebGL.Vector4(0, 0, 1, 0);
                var r3 = new WebGL.Vector4(0, 0, 0, 1);
                return new Matrix4(r0, r1, r2, r3);
            },
            enumerable: true,
            configurable: true
        });
        Matrix4.CreateOrthographicOffCenter = function (left, right, bottom, top, zNear, zFar) {
            var result = Matrix4.Identity;
            var invRL = 1.0 / (right - left);
            var invTB = 1.0 / (top - bottom);
            var invFN = 1.0 / (zFar - zNear);
            result.Row0.X = 2 * invRL;
            result.Row1.Y = 2 * invTB;
            result.Row2.Z = -2 * invFN;
            result.Row3.X = -(right + left) * invRL;
            result.Row3.Y = -(top + bottom) * invTB;
            result.Row3.Z = -(zFar + zNear) * invFN;
            return result;
        };
        Matrix4.CreateOrthographic = function (width, height, zNear, zFar) {
            return Matrix4.CreateOrthographicOffCenter(-width / 2, width / 2, -height / 2, height / 2, zNear, zFar);
        };
        Matrix4.CreateTranslation = function (x, y, z) {
            var result = this.Identity;
            result.Row3.X = x;
            result.Row3.Y = y;
            result.Row3.Z = z;
            return result;
        };
        Matrix4.Multiply = function (left, right) {
            var result = Matrix4.Zero;
            var lM11 = left.Row0.X;
            var lM12 = left.Row0.Y;
            var lM13 = left.Row0.Z;
            var lM14 = left.Row0.W;
            var lM21 = left.Row1.X;
            var lM22 = left.Row1.Y;
            var lM23 = left.Row1.Z;
            var lM24 = left.Row1.W;
            var lM31 = left.Row2.X;
            var lM32 = left.Row2.Y;
            var lM33 = left.Row2.Z;
            var lM34 = left.Row2.W;
            var lM41 = left.Row3.X;
            var lM42 = left.Row3.Y;
            var lM43 = left.Row3.Z;
            var lM44 = left.Row3.W;
            var rM11 = right.Row0.X;
            var rM12 = right.Row0.Y;
            var rM13 = right.Row0.Z;
            var rM14 = right.Row0.W;
            var rM21 = right.Row1.X;
            var rM22 = right.Row1.Y;
            var rM23 = right.Row1.Z;
            var rM24 = right.Row1.W;
            var rM31 = right.Row2.X;
            var rM32 = right.Row2.Y;
            var rM33 = right.Row2.Z;
            var rM34 = right.Row2.W;
            var rM41 = right.Row3.X;
            var rM42 = right.Row3.Y;
            var rM43 = right.Row3.Z;
            var rM44 = right.Row3.W;
            result.Row0.X = (((lM11 * rM11) + (lM12 * rM21)) + (lM13 * rM31)) + (lM14 * rM41);
            result.Row0.Y = (((lM11 * rM12) + (lM12 * rM22)) + (lM13 * rM32)) + (lM14 * rM42);
            result.Row0.Z = (((lM11 * rM13) + (lM12 * rM23)) + (lM13 * rM33)) + (lM14 * rM43);
            result.Row0.W = (((lM11 * rM14) + (lM12 * rM24)) + (lM13 * rM34)) + (lM14 * rM44);
            result.Row1.X = (((lM21 * rM11) + (lM22 * rM21)) + (lM23 * rM31)) + (lM24 * rM41);
            result.Row1.Y = (((lM21 * rM12) + (lM22 * rM22)) + (lM23 * rM32)) + (lM24 * rM42);
            result.Row1.Z = (((lM21 * rM13) + (lM22 * rM23)) + (lM23 * rM33)) + (lM24 * rM43);
            result.Row1.W = (((lM21 * rM14) + (lM22 * rM24)) + (lM23 * rM34)) + (lM24 * rM44);
            result.Row2.X = (((lM31 * rM11) + (lM32 * rM21)) + (lM33 * rM31)) + (lM34 * rM41);
            result.Row2.Y = (((lM31 * rM12) + (lM32 * rM22)) + (lM33 * rM32)) + (lM34 * rM42);
            result.Row2.Z = (((lM31 * rM13) + (lM32 * rM23)) + (lM33 * rM33)) + (lM34 * rM43);
            result.Row2.W = (((lM31 * rM14) + (lM32 * rM24)) + (lM33 * rM34)) + (lM34 * rM44);
            result.Row3.X = (((lM41 * rM11) + (lM42 * rM21)) + (lM43 * rM31)) + (lM44 * rM41);
            result.Row3.Y = (((lM41 * rM12) + (lM42 * rM22)) + (lM43 * rM32)) + (lM44 * rM42);
            result.Row3.Z = (((lM41 * rM13) + (lM42 * rM23)) + (lM43 * rM33)) + (lM44 * rM43);
            result.Row3.W = (((lM41 * rM14) + (lM42 * rM24)) + (lM43 * rM34)) + (lM44 * rM44);
            return result;
        };
        Matrix4.Add = function (left, right) {
            var result;
            result.Row0 = WebGL.Vector4.Add(left.Row0, right.Row0);
            result.Row1 = WebGL.Vector4.Add(left.Row1, right.Row1);
            result.Row2 = WebGL.Vector4.Add(left.Row2, right.Row2);
            result.Row3 = WebGL.Vector4.Add(left.Row3, right.Row3);
            return result;
        };
        Matrix4.Subtract = function (left, right) {
            var result;
            result.Row0 = WebGL.Vector4.Subtract(left.Row0, right.Row0);
            result.Row1 = WebGL.Vector4.Subtract(left.Row1, right.Row1);
            result.Row2 = WebGL.Vector4.Subtract(left.Row2, right.Row2);
            result.Row3 = WebGL.Vector4.Subtract(left.Row3, right.Row3);
            return result;
        };
        Matrix4.Invert = function (mat) {
            var colIdx = [0, 0, 0, 0];
            var rowIdx = [0, 0, 0, 0];
            var pivotIdx = [-1, -1, -1, -1];
            var inverse = [
                [mat.Row0.X, mat.Row0.Y, mat.Row0.Z, mat.Row0.W],
                [mat.Row1.X, mat.Row1.Y, mat.Row1.Z, mat.Row1.W],
                [mat.Row2.X, mat.Row2.Y, mat.Row2.Z, mat.Row2.W],
                [mat.Row3.X, mat.Row3.Y, mat.Row3.Z, mat.Row3.W]
            ];
            var icol = 0;
            var irow = 0;
            var i, j, k;
            for (i = 0; i < 4; i++) {
                var maxPivot = 0.0;
                for (j = 0; j < 4; j++) {
                    if (pivotIdx[j] != 0) {
                        for (k = 0; k < 4; ++k) {
                            if (pivotIdx[k] == -1) {
                                var absVal = Math.abs(inverse[j][k]);
                                if (absVal > maxPivot) {
                                    maxPivot = absVal;
                                    irow = j;
                                    icol = k;
                                }
                            }
                            else if (pivotIdx[k] > 0) {
                                result = mat;
                                return;
                            }
                        }
                    }
                }
                ++(pivotIdx[icol]);
                if (irow != icol) {
                    for (k = 0; k < 4; ++k) {
                        var f = inverse[irow][k];
                        inverse[irow][k] = inverse[icol][k];
                        inverse[icol][k] = f;
                    }
                }
                rowIdx[i] = irow;
                colIdx[i] = icol;
                var pivot = inverse[icol][icol];
                if (pivot == 0.0) {
                    throw "Matrix is singular and cannot be inverted.";
                }
                var oneOverPivot = 1.0 / pivot;
                inverse[icol][icol] = 1.0;
                for (k = 0; k < 4; ++k)
                    inverse[icol][k] *= oneOverPivot;
                for (j = 0; j < 4; ++j) {
                    if (icol != j) {
                        var f = inverse[j][icol];
                        inverse[j][icol] = 0.0;
                        for (k = 0; k < 4; ++k)
                            inverse[j][k] -= inverse[icol][k] * f;
                    }
                }
            }
            for (j = 3; j >= 0; --j) {
                var ir = rowIdx[j];
                var ic = colIdx[j];
                for (k = 0; k < 4; ++k) {
                    var f = inverse[k][ir];
                    inverse[k][ir] = inverse[k][ic];
                    inverse[k][ic] = f;
                }
            }
            var result = Matrix4.Zero;
            result.Row0.X = inverse[0][0];
            result.Row0.Y = inverse[0][1];
            result.Row0.Z = inverse[0][2];
            result.Row0.W = inverse[0][3];
            result.Row1.X = inverse[1][0];
            result.Row1.Y = inverse[1][1];
            result.Row1.Z = inverse[1][2];
            result.Row1.W = inverse[1][3];
            result.Row2.X = inverse[2][0];
            result.Row2.Y = inverse[2][1];
            result.Row2.Z = inverse[2][2];
            result.Row2.W = inverse[2][3];
            result.Row3.X = inverse[3][0];
            result.Row3.Y = inverse[3][1];
            result.Row3.Z = inverse[3][2];
            result.Row3.W = inverse[3][3];
            return result;
        };
        Matrix4.CreateScale = function (x, y, z) {
            var result = this.Identity;
            result.Row0.X = x;
            result.Row1.Y = y;
            result.Row2.Z = z;
            return result;
        };
        Matrix4.CreatePerspectiveOffCenter = function (left, right, bottom, top, zNear, zFar) {
            if (zNear <= 0)
                throw ("zNear");
            if (zFar <= 0)
                throw ("zFar");
            if (zNear >= zFar)
                throw ("zNear");
            var x = (2.0 * zNear) / (right - left);
            var y = (2.0 * zNear) / (top - bottom);
            var a = (right + left) / (right - left);
            var b = (top + bottom) / (top - bottom);
            var c = -(zFar + zNear) / (zFar - zNear);
            var d = -(2.0 * zFar * zNear) / (zFar - zNear);
            var result = this.Identity;
            result.Row0.X = x;
            result.Row0.Y = 0;
            result.Row0.Z = 0;
            result.Row0.W = 0;
            result.Row1.X = 0;
            result.Row1.Y = y;
            result.Row1.Z = 0;
            result.Row1.W = 0;
            result.Row2.X = a;
            result.Row2.Y = b;
            result.Row2.Z = c;
            result.Row2.W = -1;
            result.Row3.X = 0;
            result.Row3.Y = 0;
            result.Row3.Z = d;
            result.Row3.W = 0;
            return result;
        };
        Matrix4.CreatePerspectiveFieldOfView = function (fovy, aspect, zNear, zFar) {
            if (fovy <= 0 || fovy > Math.PI)
                throw ("fovy");
            if (aspect <= 0)
                throw ("aspect");
            if (zNear <= 0)
                throw ("zNear");
            if (zFar <= 0)
                throw ("zFar");
            var yMax = zNear * Math.tan(0.5 * fovy);
            var yMin = -yMax;
            var xMin = yMin * aspect;
            var xMax = yMax * aspect;
            return Matrix4.CreatePerspectiveOffCenter(xMin, xMax, yMin, yMax, zNear, zFar);
        };
        Matrix4.LookAt = function (eye, target, up) {
            var z = WebGL.Vector3.Normalize(WebGL.Vector3.Subtract(eye, target));
            var x = WebGL.Vector3.Normalize(WebGL.Vector3.Cross(up, z));
            var y = WebGL.Vector3.Normalize(WebGL.Vector3.Cross(z, x));
            var result = this.Identity;
            result.Row0.X = x.X;
            result.Row0.Y = y.X;
            result.Row0.Z = z.X;
            result.Row0.W = 0;
            result.Row1.X = x.Y;
            result.Row1.Y = y.Y;
            result.Row1.Z = z.Y;
            result.Row1.W = 0;
            result.Row2.X = x.Z;
            result.Row2.Y = y.Z;
            result.Row2.Z = z.Z;
            result.Row2.W = 0;
            result.Row3.X = -((x.X * eye.X) + (x.Y * eye.Y) + (x.Z * eye.Z));
            result.Row3.Y = -((y.X * eye.X) + (y.Y * eye.Y) + (y.Z * eye.Z));
            result.Row3.Z = -((z.X * eye.X) + (z.Y * eye.Y) + (z.Z * eye.Z));
            result.Row3.W = 1;
            return result;
        };
        Matrix4.CreateFromAxisAngle = function (axis, angle) {
            axis.Normalize();
            var axisX = axis.X;
            var axisY = axis.Y;
            var axisZ = axis.Z;
            var cos = Math.cos(-angle);
            var sin = Math.sin(-angle);
            var t = 1.0 - cos;
            var tXX = t * axisX * axisX;
            var tXY = t * axisX * axisY;
            var tXZ = t * axisX * axisZ;
            var tYY = t * axisY * axisY;
            var tYZ = t * axisY * axisZ;
            var tZZ = t * axisZ * axisZ;
            var sinX = sin * axisX;
            var sinY = sin * axisY;
            var sinZ = sin * axisZ;
            var result = Matrix4.Identity;
            result.Row0.X = tXX + cos;
            result.Row0.Y = tXY - sinZ;
            result.Row0.Z = tXZ + sinY;
            result.Row0.W = 0;
            result.Row1.X = tXY + sinZ;
            result.Row1.Y = tYY + cos;
            result.Row1.Z = tYZ - sinX;
            result.Row1.W = 0;
            result.Row2.X = tXZ - sinY;
            result.Row2.Y = tYZ + sinX;
            result.Row2.Z = tZZ + cos;
            result.Row2.W = 0;
            result.Row3 = WebGL.Vector4.UnitW;
            return result;
        };
        Matrix4.CreateFromQuaternion = function (q) {
            var aa = q.AxisAngle;
            var axis = aa.Axis;
            var angle = aa.Angle;
            return Matrix4.CreateFromAxisAngle(axis, angle);
        };
        return Matrix4;
    }());
    WebGL.Matrix4 = Matrix4;
})(WebGL || (WebGL = {}));
var WebGL;
(function (WebGL) {
    var IntegerScalar = (function () {
        function IntegerScalar(value) {
            this.Value = 0;
            this.Value = value;
        }
        return IntegerScalar;
    }());
    WebGL.IntegerScalar = IntegerScalar;
    var FloatingScalar = (function () {
        function FloatingScalar(value) {
            this.Value = 0;
            this.Value = value;
        }
        return FloatingScalar;
    }());
    WebGL.FloatingScalar = FloatingScalar;
    var Vector2 = (function () {
        function Vector2(x, y) {
            this.X = x;
            this.Y = y;
        }
        Object.defineProperty(Vector2.prototype, "Length", {
            get: function () {
                return Math.sqrt(this.X * this.X + this.Y * this.Y);
            },
            enumerable: true,
            configurable: true
        });
        Vector2.prototype.ToVector3 = function (z) {
            if (z === void 0) { z = 0; }
            return new Vector3(this.X, this.Y, z);
        };
        Vector2.prototype.ToFloatArray = function () {
            var fl = new Float32Array([
                this.X, this.Y
            ]);
            return fl;
        };
        Vector2.Add = function (left, right) {
            var result = new Vector2(0, 0);
            result.X = left.X + right.X;
            result.Y = left.Y + right.Y;
            return result;
        };
        Vector2.Subtract = function (left, right) {
            var result = new Vector2(0, 0);
            result.X = left.X - right.X;
            result.Y = left.Y - right.Y;
            return result;
        };
        Vector2.prototype.Normalize = function () {
            var scale = 1.0 / this.Length;
            this.X *= scale;
            this.Y *= scale;
        };
        Object.defineProperty(Vector2.prototype, "Normalized", {
            get: function () {
                var t = new Vector2(this.X, this.Y);
                t.Normalize();
                return t;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector2.prototype, "Key", {
            get: function () {
                return "X" + this.X.toFixed(FloatingMath.DecimalDigits) + "Y" + this.Y.toFixed(FloatingMath.DecimalDigits);
            },
            enumerable: true,
            configurable: true
        });
        Vector2.prototype.ToPolarVector = function () {
            var r = this.Length;
            var theta = new Angle(Math.atan2(this.Y, this.X));
            return new PolarVector(r, theta);
        };
        Vector2.prototype.Round = function () {
            this.X = Math.round(this.X);
            this.Y = Math.round(this.Y);
        };
        Object.defineProperty(Vector2.prototype, "Rounded", {
            get: function () {
                var v = new Vector2(this.X, this.Y);
                v.Round();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        Vector2.prototype.Fix = function () {
            this.X = FloatingMath.FixedNumber(this.X);
            this.Y = FloatingMath.FixedNumber(this.Y);
        };
        Object.defineProperty(Vector2.prototype, "Fixed", {
            get: function () {
                var v = new Vector2(this.X, this.Y);
                v.Fix();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        Vector2.prototype.Equals = function (v) {
            if (v == null)
                return false;
            if (this.X == v.X
                && this.Y == v.Y)
                return true;
            else
                return false;
        };
        Vector2.prototype.NearlyEquals = function (v) {
            if (v == null)
                return false;
            if (FloatingMath.NearlyEqual(this.X, v.X)
                && FloatingMath.NearlyEqual(this.Y, v.Y))
                return true;
            else
                return false;
        };
        Vector2.prototype.FixedEquals = function (v) {
            if (v == null)
                return false;
            if (FloatingMath.FixedEqual(this.X, v.X)
                && FloatingMath.FixedEqual(this.Y, v.Y))
                return true;
            else
                return false;
        };
        Vector2.prototype.ToTupleString = function () {
            return "(" + this.X.toFixed(2) + ", " + this.Y.toFixed(2) + ")";
        };
        Vector2.prototype.Rotate = function (rotationCenter, rotationAngle) {
            var cc = Vector2.Subtract(this, rotationCenter);
            var cp = cc.ToPolarVector();
            var np = cp.AddAngle(rotationAngle);
            return Vector2.Add(np.ToCartesian(), rotationCenter);
        };
        Vector2.Dot = function (left, right) {
            return left.X * right.X + left.Y * right.Y;
        };
        Vector2.prototype.CosineAngle = function (target) {
            return Vector2.Dot(this, target) / this.Length * target.Length;
        };
        Object.defineProperty(Vector2, "UnitX", {
            get: function () {
                return new Vector2(1, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector2, "UnitY", {
            get: function () {
                return new Vector2(0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Vector2.prototype.Extend = function (length) {
            var cos_x = this.CosineAngle(Vector2.UnitX);
            var cos_y = this.CosineAngle(Vector2.UnitY);
            var delta_x = length * cos_x;
            var delta_y = length * cos_y;
            return new Vector2(delta_x, delta_y);
        };
        Vector2.prototype.Inverted = function () {
            return new Vector2(this.X * -1, this.Y * -1);
        };
        return Vector2;
    }());
    WebGL.Vector2 = Vector2;
    var Vector3 = (function () {
        function Vector3(x, y, z) {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }
        Vector3.prototype.ToCoVector = function () {
            var m3 = new WebGL.Matrix3(new Vector3(this.X, 0, 0), new Vector3(0, this.Y, 0), new Vector3(0, 0, this.Z));
            return m3;
        };
        Object.defineProperty(Vector3.prototype, "Xy", {
            get: function () {
                return new Vector2(this.X, this.Y);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector3.prototype, "LengthSquared", {
            get: function () {
                return (this.X * this.X + this.Y * this.Y + this.Z * this.Z);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector3.prototype, "Length", {
            get: function () {
                return Math.sqrt(this.LengthSquared);
            },
            enumerable: true,
            configurable: true
        });
        Vector3.prototype.Normalize = function () {
            var scale = 1.0 / this.Length;
            this.X *= scale;
            this.Y *= scale;
            this.Z *= scale;
        };
        Vector3.Normalize = function (vec) {
            var scale = 1.0 / vec.Length;
            vec.X *= scale;
            vec.Y *= scale;
            vec.Z *= scale;
            return vec;
        };
        Object.defineProperty(Vector3.prototype, "Normalized", {
            get: function () {
                var t = new Vector3(this.X, this.Y, this.Z);
                t.Normalize();
                return t;
            },
            enumerable: true,
            configurable: true
        });
        Vector3.Multiply = function (a, f) {
            return new Vector3(a.X * f, a.Y * f, a.Z * f);
        };
        Vector3.Divide = function (a, f) {
            return new Vector3(a.X / f, a.Y / f, a.Z / f);
        };
        Vector3.Cross = function (left, right) {
            var result = new Vector3(left.Y * right.Z - left.Z * right.Y, left.Z * right.X - left.X * right.Z, left.X * right.Y - left.Y * right.X);
            return result;
        };
        Vector3.Dot = function (left, right) {
            return left.X * right.X + left.Y * right.Y + left.Z * right.Z;
        };
        Vector3.CalculateAngle = function (first, second) {
            var temp = Vector3.Dot(first, second);
            function Clamp(n, min, max) { return Math.max(Math.min(n, max), min); }
            return Math.acos(Clamp(temp / (first.Length * second.Length), -1.0, 1.0));
        };
        Vector3.prototype.ToFloatArray = function () {
            var fl = new Float32Array([
                this.X, this.Y, this.Z
            ]);
            return fl;
        };
        Vector3.prototype.Round = function () {
            this.X = Math.round(this.X);
            this.Y = Math.round(this.Y);
            this.Z = Math.round(this.Z);
        };
        Object.defineProperty(Vector3.prototype, "Rounded", {
            get: function () {
                var v = new Vector3(this.X, this.Y, this.Z);
                v.Round();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        Vector3.prototype.Fix = function () {
            this.X = FloatingMath.FixedNumber(this.X);
            this.Y = FloatingMath.FixedNumber(this.Y);
            this.Z = FloatingMath.FixedNumber(this.Z);
        };
        Object.defineProperty(Vector3.prototype, "Fixed", {
            get: function () {
                var v = new Vector3(this.X, this.Y, this.Z);
                v.Fix();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        Vector3.prototype.Equals = function (v) {
            if (v == null)
                return false;
            if (this.X == v.X
                && this.Y == v.Y
                && this.Z == v.Z)
                return true;
            else
                return false;
        };
        Vector3.prototype.NearlyEqual = function (v) {
            if (v == null)
                return false;
            if (FloatingMath.NearlyEqual(this.X, v.X)
                && FloatingMath.NearlyEqual(this.Y, v.Y)
                && FloatingMath.NearlyEqual(this.Z, v.Z))
                return true;
            else
                return false;
        };
        Vector3.prototype.FixedEquals = function (v) {
            if (v == null)
                return false;
            if (FloatingMath.FixedEqual(this.X, v.X)
                && FloatingMath.FixedEqual(this.Y, v.Y)
                && FloatingMath.FixedEqual(this.Z, v.Z))
                return true;
            else
                return false;
        };
        Vector3.Add = function (left, right) {
            var result = new Vector3(0, 0, 0);
            result.X = left.X + right.X;
            result.Y = left.Y + right.Y;
            result.Z = left.Z + right.Z;
            return result;
        };
        Vector3.Subtract = function (left, right) {
            var result = new Vector3(0, 0, 0);
            result.X = left.X - right.X;
            result.Y = left.Y - right.Y;
            result.Z = left.Z - right.Z;
            return result;
        };
        Vector3.prototype.CosineAngle = function (target) {
            return Vector3.Dot(this, target) / this.Length * target.Length;
        };
        Object.defineProperty(Vector3, "Zero", {
            get: function () {
                return new Vector3(0, 0, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector3, "UnitX", {
            get: function () {
                return new Vector3(1, 0, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector3, "UnitY", {
            get: function () {
                return new Vector3(0, 1, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector3, "UnitZ", {
            get: function () {
                return new Vector3(0, 0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Vector3.prototype.Extend = function (length) {
            var cos_x = this.CosineAngle(Vector3.UnitX);
            var cos_y = this.CosineAngle(Vector3.UnitY);
            var cos_z = this.CosineAngle(Vector3.UnitZ);
            var delta_x = length * cos_x;
            var delta_y = length * cos_y;
            var delta_z = length * cos_z;
            return new Vector3(delta_x, delta_y, delta_z);
        };
        Vector3.prototype.ToTupleString = function () {
            return "(" + this.X.toFixed(2) + ", " + this.Y.toFixed(2) + ", " + this.Z.toFixed(2) + ")";
        };
        Vector3.prototype.ToVector4 = function (w) {
            if (w === void 0) { w = 0; }
            return new Vector4(this.X, this.Y, this.Z, w);
        };
        Vector3.prototype.ToSpherical = function () {
            var sv = new SphericalVector(this.Length, Math.atan2(this.Y, this.X), Math.acos(this.Z / this.Length));
            return sv;
        };
        Vector3.TransformByQuaternion = function (vec, quat) {
            var temp = Vector3.Cross(quat.Xyz, vec);
            var temp2 = Vector3.Multiply(vec, quat.W);
            temp = Vector3.Add(temp, temp2);
            temp = Vector3.Cross(quat.Xyz, temp);
            temp = Vector3.Multiply(temp, 2);
            var result = Vector3.Add(vec, temp);
            return result;
        };
        return Vector3;
    }());
    WebGL.Vector3 = Vector3;
    var Vector4 = (function () {
        function Vector4(x, y, z, w) {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.W = w;
        }
        Object.defineProperty(Vector4.prototype, "Xyz", {
            get: function () {
                return new Vector3(this.X, this.Y, this.Z);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4, "UnitX", {
            get: function () {
                return new Vector4(1, 0, 0, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4, "UnitY", {
            get: function () {
                return new Vector4(0, 1, 0, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4, "UnitZ", {
            get: function () {
                return new Vector4(0, 0, 1, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4, "UnitW", {
            get: function () {
                return new Vector4(0, 0, 0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4, "Zero", {
            get: function () {
                return new Vector4(0, 0, 0, 0);
            },
            enumerable: true,
            configurable: true
        });
        Vector4.prototype.ToFloatArray = function () {
            var fl = new Float32Array([
                this.X, this.Y, this.Z, this.W
            ]);
            return fl;
        };
        Vector4.prototype.Round = function () {
            this.X = Math.round(this.X);
            this.Y = Math.round(this.Y);
            this.Z = Math.round(this.Z);
            this.W = Math.round(this.W);
        };
        Object.defineProperty(Vector4.prototype, "Rounded", {
            get: function () {
                var v = new Vector4(this.X, this.Y, this.Z, this.W);
                v.Round();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        Vector4.prototype.Fix = function () {
            this.X = FloatingMath.FixedNumber(this.X);
            this.Y = FloatingMath.FixedNumber(this.Y);
            this.Z = FloatingMath.FixedNumber(this.Z);
            this.W = FloatingMath.FixedNumber(this.W);
        };
        Object.defineProperty(Vector4.prototype, "Fixed", {
            get: function () {
                var v = new Vector4(this.X, this.Y, this.Z, this.W);
                v.Fix();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        Vector4.prototype.Equals = function (v) {
            if (v == null)
                return false;
            if (this.X == v.X
                && this.Y == v.Y
                && this.Z == v.Z
                && this.W == v.W)
                return true;
            else
                return false;
        };
        Vector4.prototype.NearlyEquals = function (v) {
            if (v == null)
                return false;
            if (FloatingMath.NearlyEqual(this.X, v.X)
                && FloatingMath.NearlyEqual(this.Y, v.Y)
                && FloatingMath.NearlyEqual(this.Z, v.Z)
                && FloatingMath.NearlyEqual(this.W, v.W))
                return true;
            else
                return false;
        };
        Vector4.prototype.FixedEquals = function (v) {
            if (v == null)
                return false;
            if (FloatingMath.FixedEqual(this.X, v.X)
                && FloatingMath.FixedEqual(this.Y, v.Y)
                && FloatingMath.FixedEqual(this.Z, v.Z)
                && FloatingMath.FixedEqual(this.W, v.W))
                return true;
            else
                return false;
        };
        Vector4.Transform = function (vec, mat) {
            var result = new Vector4(vec.X * mat.Row0.X + vec.Y * mat.Row1.X + vec.Z * mat.Row2.X + vec.W * mat.Row3.X, vec.X * mat.Row0.Y + vec.Y * mat.Row1.Y + vec.Z * mat.Row2.Y + vec.W * mat.Row3.Y, vec.X * mat.Row0.Z + vec.Y * mat.Row1.Z + vec.Z * mat.Row2.Z + vec.W * mat.Row3.Z, vec.X * mat.Row0.W + vec.Y * mat.Row1.W + vec.Z * mat.Row2.W + vec.W * mat.Row3.W);
            return result;
        };
        Vector4.Add = function (left, right) {
            var result;
            result.X = left.X + right.X;
            result.Y = left.Y + right.Y;
            result.Z = left.Z + right.Z;
            result.W = left.W + right.W;
            return result;
        };
        Vector4.Subtract = function (left, right) {
            var result;
            result.X = left.X - right.X;
            result.Y = left.Y - right.Y;
            result.Z = left.Z - right.Z;
            result.W = left.W - right.W;
            return result;
        };
        return Vector4;
    }());
    WebGL.Vector4 = Vector4;
    function Vectors2ToVectors3(vectors) {
        var vecs = new Array();
        vectors.forEach(function (x) { return vecs.push(x.ToVector3()); });
        return vecs;
    }
    WebGL.Vectors2ToVectors3 = Vectors2ToVectors3;
    function Vectors2ToArray(vectors) {
        var vecs = new Array();
        vectors.forEach(function (v) {
            vecs.push(v.X);
            vecs.push(v.Y);
        });
        return vecs;
    }
    WebGL.Vectors2ToArray = Vectors2ToArray;
    function Vectors3ToArray(vectors) {
        var vecs = new Array();
        vectors.forEach(function (v) {
            vecs.push(v.X);
            vecs.push(v.Y);
            vecs.push(v.Z);
        });
        return vecs;
    }
    WebGL.Vectors3ToArray = Vectors3ToArray;
    function Vectors4ToArray(vectors) {
        var vecs = new Array();
        vectors.forEach(function (v) {
            vecs.push(v.X);
            vecs.push(v.Y);
            vecs.push(v.Z);
            vecs.push(v.W);
        });
        return vecs;
    }
    WebGL.Vectors4ToArray = Vectors4ToArray;
    var PolarVector = (function () {
        function PolarVector(r, theta) {
            this.Radius = r;
            this.Theta = theta;
        }
        PolarVector.prototype.ToCartesian = function () {
            return new Vector2(this.Radius * Math.cos(this.Theta.Radians), this.Radius * Math.sin(this.Theta.Radians));
        };
        PolarVector.prototype.AddAngle = function (theta) {
            var np = new PolarVector(this.Radius, Angle.Add(this.Theta, theta));
            return np;
        };
        return PolarVector;
    }());
    WebGL.PolarVector = PolarVector;
    var SphericalVector = (function () {
        function SphericalVector(r, theta, phi) {
            this.Radial = r;
            this.Theta = new Angle(theta);
            this.Phi = new Angle(phi);
        }
        SphericalVector.prototype.ToCartesian = function () {
            var x = this.Radial * Math.cos(this.Theta.Radians) * Math.sin(this.Phi.Radians);
            var y = this.Radial * Math.sin(this.Theta.Radians) * Math.sin(this.Phi.Radians);
            var z = this.Radial * Math.cos(this.Phi.Radians);
            return new Vector3(x, y, z);
        };
        return SphericalVector;
    }());
    WebGL.SphericalVector = SphericalVector;
    var Vector4Colors = (function () {
        function Vector4Colors() {
        }
        Vector4Colors.FromRGB = function (r, g, b) {
            return new Vector4(r / 255, g / 255, b / 255, 1);
        };
        Vector4Colors.FromRGBA = function (r, g, b, a) {
            return new Vector4(r / 255, g / 255, b / 255, a / 255);
        };
        Object.defineProperty(Vector4Colors, "Red", {
            get: function () {
                return new Vector4(1, 0, 0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Green", {
            get: function () {
                return new Vector4(0, 1, 0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Blue", {
            get: function () {
                return new Vector4(0, 0, 1, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Black", {
            get: function () {
                return new Vector4(0, 0, 0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "White", {
            get: function () {
                return new Vector4(1, 1, 1, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Grey", {
            get: function () {
                return new Vector4(0.5, 0.5, 0.5, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Yellow", {
            get: function () {
                return new Vector4(1, 1, 0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Brown", {
            get: function () {
                return Vector4Colors.FromRGB(165, 42, 42);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "DarkGreen", {
            get: function () {
                return Vector4Colors.FromRGB(0, 100, 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "GoldenRod", {
            get: function () {
                return Vector4Colors.FromRGB(218, 165, 32);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Wheat", {
            get: function () {
                return Vector4Colors.FromRGB(245, 222, 179);
            },
            enumerable: true,
            configurable: true
        });
        Vector4Colors.HexToRgb = function (hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        };
        Object.defineProperty(Vector4Colors, "Orange", {
            get: function () {
                var rgb = Vector4Colors.HexToRgb("#FFA500");
                return Vector4Colors.FromRGB(rgb.r, rgb.g, rgb.b);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "Moccasin", {
            get: function () {
                var rgb = Vector4Colors.HexToRgb("#FFE4B5");
                return Vector4Colors.FromRGB(rgb.r, rgb.g, rgb.b);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vector4Colors, "NavajoWhite", {
            get: function () {
                var rgb = Vector4Colors.HexToRgb("#FFDEAD");
                return Vector4Colors.FromRGB(rgb.r, rgb.g, rgb.b);
            },
            enumerable: true,
            configurable: true
        });
        return Vector4Colors;
    }());
    WebGL.Vector4Colors = Vector4Colors;
})(WebGL || (WebGL = {}));
var WebGL;
(function (WebGL) {
    var Shaders;
    (function (Shaders) {
        var Shader = (function () {
            function Shader(shadertype, shaderSource) {
                this._ShaderType = shadertype;
                this._ShaderSource = shaderSource;
            }
            Object.defineProperty(Shader.prototype, "ShaderType", {
                get: function () {
                    return this._ShaderType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Shader.prototype, "ShaderId", {
                get: function () {
                    return this._ShaderId;
                },
                enumerable: true,
                configurable: true
            });
            Shader.prototype.IsCompiled = function (gl) {
                var exists = gl.isShader(this._ShaderId);
                if (exists) {
                    var IsCompiled = gl.getShaderParameter(this._ShaderId, WebGLRenderingContext.COMPILE_STATUS);
                    return IsCompiled;
                }
                return false;
            };
            Shader.prototype.Compile = function (gl) {
                this.BeforeCompile(gl);
                this.OnCompile(gl);
                this.AfterCompile(gl);
            };
            Shader.prototype.BeforeCompile = function (gl) {
            };
            Shader.prototype.OnCompile = function (gl) {
                this._ShaderId = gl.createShader(this._ShaderType);
                if (this._ShaderId == 0)
                    alert('Error in Creating Shader');
                gl.shaderSource(this._ShaderId, this._ShaderSource);
                gl.compileShader(this._ShaderId);
                var compile_log = gl.getShaderInfoLog(this._ShaderId);
                if (!this.IsCompiled(gl)) {
                    alert(compile_log);
                    throw compile_log;
                }
            };
            Shader.prototype.AfterCompile = function (gl) {
            };
            return Shader;
        }());
        Shaders.Shader = Shader;
        var VertexShader = (function (_super) {
            __extends(VertexShader, _super);
            function VertexShader(shaderSource) {
                _super.call(this, WebGLRenderingContext.VERTEX_SHADER, shaderSource);
            }
            return VertexShader;
        }(Shader));
        Shaders.VertexShader = VertexShader;
        var FragmentShader = (function (_super) {
            __extends(FragmentShader, _super);
            function FragmentShader(shaderSource) {
                _super.call(this, WebGLRenderingContext.FRAGMENT_SHADER, shaderSource);
            }
            return FragmentShader;
        }(Shader));
        Shaders.FragmentShader = FragmentShader;
        var PipelineProgram = (function () {
            function PipelineProgram(programUniqueName, vs, fs) {
                this.Buffers = new Object();
                this.ArrayVertexAttributes = new Object();
                this.ConstantVertexAttributes = new Object();
                this.Uniforms = new Object();
                this._FragmentShader = fs;
                this._VertexShader = vs;
                if (PipelineProgram.ProgramsNames[programUniqueName] == undefined) {
                    this._ProgramUniqueName = programUniqueName;
                    PipelineProgram.ProgramsNames[programUniqueName] = true;
                }
                else {
                    var error = "A program with the same name [" + programUniqueName + "] has been declared before. Please use a different name for this program";
                    alert(error);
                    throw error;
                }
            }
            Object.defineProperty(PipelineProgram.prototype, "ProgramUniqueName", {
                get: function () {
                    return this._ProgramUniqueName;
                },
                enumerable: true,
                configurable: true
            });
            PipelineProgram.prototype.SetProgramId = function (gl, programId) {
                if (gl.Programs == undefined) {
                    gl.Programs = new Object();
                }
                gl.Programs[this._ProgramUniqueName] = programId;
            };
            PipelineProgram.prototype.GetProgramId = function (gl) {
                if (gl.Programs != undefined) {
                    var id = gl.Programs[this._ProgramUniqueName];
                    return id;
                }
                return undefined;
            };
            Object.defineProperty(PipelineProgram.prototype, "VertexShader", {
                get: function () {
                    return this._VertexShader;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PipelineProgram.prototype, "FragmentShader", {
                get: function () {
                    return this._FragmentShader;
                },
                enumerable: true,
                configurable: true
            });
            PipelineProgram.prototype.IsLinked = function (gl) {
                var glany = gl;
                if (glany.R2Programs == undefined)
                    return false;
                if (glany.R2Programs.hasOwnProperty(this._ProgramUniqueName))
                    return true;
                var prog = this.GetProgramId(gl);
                var exists = prog != undefined;
                if (exists) {
                    if (gl.isProgram(prog)) {
                        var IsLinked = gl.getProgramParameter(prog, WebGLRenderingContext.LINK_STATUS);
                        return IsLinked;
                    }
                    return false;
                }
                return false;
            };
            PipelineProgram.prototype.Link = function (gl) {
                if (!this._VertexShader.IsCompiled(gl))
                    this._VertexShader.Compile(gl);
                if (!this._FragmentShader.IsCompiled(gl))
                    this._FragmentShader.Compile(gl);
                this.SetProgramId(gl, gl.createProgram());
                var programId = this.GetProgramId(gl);
                gl.attachShader(programId, this._VertexShader.ShaderId);
                gl.attachShader(programId, this._FragmentShader.ShaderId);
                gl.linkProgram(programId);
                var linked = gl.getProgramParameter(programId, gl.LINK_STATUS);
                if (!linked) {
                    var link_log = gl.getProgramInfoLog(this.GetProgramId(gl));
                    alert(link_log);
                    throw link_log;
                }
                var glany = gl;
                if (glany.R2Programs == undefined)
                    glany.R2Programs = new Object();
                glany.R2Programs[this._ProgramUniqueName] = true;
            };
            PipelineProgram.prototype.SendProgramData = function (gl) {
                for (var key in this.Uniforms) {
                    if (this.Uniforms.hasOwnProperty(key)) {
                        var value = this.Uniforms[key];
                        var UniformKeyLocation = gl.getUniformLocation(this.GetProgramId(gl), key);
                        if (UniformKeyLocation == null) {
                            alert(key + ' not found in the shader');
                        }
                        else {
                            if (typeof value == 'number') {
                                gl.uniform1f(UniformKeyLocation, value);
                            }
                            else if (value instanceof WebGL.IntegerScalar) {
                                gl.uniform1i(UniformKeyLocation, value.Value);
                            }
                            else if (value instanceof WebGL.FloatingScalar) {
                                gl.uniform1f(UniformKeyLocation, value.Value);
                            }
                            else if (value instanceof WebGL.Vector2) {
                                gl.uniform2f(UniformKeyLocation, value.X, value.Y);
                            }
                            else if (value instanceof WebGL.Vector3) {
                                gl.uniform3f(UniformKeyLocation, value.X, value.Y, value.Z);
                            }
                            else if (value instanceof WebGL.Vector4) {
                                gl.uniform4f(UniformKeyLocation, value.X, value.Y, value.Z, value.W);
                            }
                            else if (value instanceof WebGL.Matrix4) {
                                gl.uniformMatrix4fv(UniformKeyLocation, false, value.ToFloatArray());
                            }
                            else {
                                alert("unknown value type");
                            }
                        }
                    }
                }
                for (var key in this.ConstantVertexAttributes) {
                    if (this.ConstantVertexAttributes.hasOwnProperty(key)) {
                        var value = this.Uniforms[key];
                        var CAKeyLocation = gl.getAttribLocation(this.GetProgramId(gl), key);
                        gl.disableVertexAttribArray(CAKeyLocation);
                        if (typeof value == 'number') {
                            gl.vertexAttrib1f(CAKeyLocation, value);
                        }
                        else if (value instanceof WebGL.FloatingScalar) {
                            gl.vertexAttrib1f(CAKeyLocation, value.Value);
                        }
                        else if (value instanceof WebGL.Vector2) {
                            gl.vertexAttrib2f(CAKeyLocation, value.X, value.Y);
                        }
                        else if (value instanceof WebGL.Vector3) {
                            gl.vertexAttrib3f(CAKeyLocation, value.X, value.Y, value.Z);
                        }
                        else if (value instanceof WebGL.Vector4) {
                            gl.vertexAttrib4f(CAKeyLocation, value.X, value.Y, value.Z, value.W);
                        }
                        else {
                            alert("unknown value type");
                        }
                    }
                }
                for (var key in this.ArrayVertexAttributes) {
                    if (this.ArrayVertexAttributes.hasOwnProperty(key)) {
                        var value = this.ArrayVertexAttributes[key];
                        var ArrayVertexLocation = gl.getAttribLocation(this.GetProgramId(gl), key);
                        gl.enableVertexAttribArray(ArrayVertexLocation);
                        var buffer = gl.createBuffer();
                        this.Buffers[key] = buffer;
                        gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, buffer);
                        gl.bufferData(WebGLRenderingContext.ARRAY_BUFFER, value, WebGLRenderingContext.DYNAMIC_DRAW);
                        gl.vertexAttribPointer(ArrayVertexLocation, this.ArrayVertexSize, WebGLRenderingContext.FLOAT, false, 0, 0);
                    }
                }
            };
            PipelineProgram.prototype.BeginUse = function (gl) {
                gl.useProgram(this.GetProgramId(gl));
                this.SendProgramData(gl);
            };
            PipelineProgram.prototype.EndUse = function (gl) {
                for (var key in this.ArrayVertexAttributes) {
                    if (this.ArrayVertexAttributes.hasOwnProperty(key)) {
                        var value = this.ArrayVertexAttributes[key];
                        var ArrayVertexLocation = gl.getAttribLocation(this.GetProgramId(gl), key);
                        gl.disableVertexAttribArray(ArrayVertexLocation);
                    }
                }
                for (var key in this.Buffers) {
                    if (this.Buffers.hasOwnProperty(key)) {
                        var buffer = this.Buffers[key];
                        gl.deleteBuffer(buffer);
                    }
                }
            };
            PipelineProgram.ProgramsNames = new Object();
            return PipelineProgram;
        }());
        Shaders.PipelineProgram = PipelineProgram;
    })(Shaders = WebGL.Shaders || (WebGL.Shaders = {}));
})(WebGL || (WebGL = {}));
var Sprites;
(function (Sprites) {
    var Sprite = (function () {
        function Sprite(top, left, width, height) {
            this.SpriteTexture = null;
            this.UpdateTexture = true;
            this.RotationAngle = Angle.Zero;
            this._Points = null;
            this.DrawBorder = false;
            this._Top = top;
            this._Left = left;
            this._WidthVector = new WebGL.Vector2(width, 0);
            this._HeightVector = new WebGL.Vector2(0, height);
        }
        Object.defineProperty(Sprite.prototype, "Width", {
            get: function () {
                return this._WidthVector.Length;
            },
            set: function (value) {
                this._WidthVector = this._WidthVector.Normalized.Extend(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sprite.prototype, "Height", {
            get: function () {
                return this._HeightVector.Length;
            },
            set: function (value) {
                this._HeightVector = this._HeightVector.Normalized.Extend(value);
            },
            enumerable: true,
            configurable: true
        });
        Sprite.prototype.OnUpdateTexture = function (gl) {
        };
        Object.defineProperty(Sprite.prototype, "Top", {
            get: function () {
                return this._Top;
            },
            set: function (value) {
                this._Top = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sprite.prototype, "Left", {
            get: function () {
                return this._Left;
            },
            set: function (value) {
                this._Left = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sprite.prototype, "TopLeft", {
            get: function () {
                return new WebGL.Vector2(this._Left, this._Top);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sprite.prototype, "TopRight", {
            get: function () {
                return WebGL.Vector2.Add(this.TopLeft, this._WidthVector);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sprite.prototype, "BottomLeft", {
            get: function () {
                return WebGL.Vector2.Subtract(this.TopLeft, this._HeightVector);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sprite.prototype, "BottomRight", {
            get: function () {
                return WebGL.Vector2.Subtract(this.TopRight, this._HeightVector);
            },
            enumerable: true,
            configurable: true
        });
        Sprite.prototype.Rotate = function (angle) {
            var wp = this._WidthVector.ToPolarVector();
            wp.Theta = Angle.Add(wp.Theta, angle);
            this._WidthVector = wp.ToCartesian();
            var hp = this._HeightVector.ToPolarVector();
            hp.Theta = Angle.Add(hp.Theta, angle);
            this._HeightVector = hp.ToCartesian();
            this.RotationAngle = angle;
        };
        Object.defineProperty(Sprite.prototype, "Points", {
            get: function () {
                {
                    this._Points = new Array();
                    this._Points.push(this.TopLeft, this.BottomRight, this.TopRight, this.TopLeft, this.BottomLeft, this.BottomRight);
                }
                return this._Points;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sprite.prototype, "TextureCoordinates", {
            get: function () {
                var tcoords = new Array();
                tcoords.push(new WebGL.Vector2(0, 0), new WebGL.Vector2(1, -1), new WebGL.Vector2(1, 0), new WebGL.Vector2(0, 0), new WebGL.Vector2(0, -1), new WebGL.Vector2(1, -1));
                return tcoords;
            },
            enumerable: true,
            configurable: true
        });
        Sprite.prototype.RenderRectangle = function (gl) {
            if (!Shapes.R2Line.Program.IsLinked(gl))
                Shapes.R2Line.Program.Link(gl);
            Shapes.R2Line.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            Shapes.R2Line.Program.Uniforms.ShapeColor = new WebGL.Vector4(0, 0, 0, 1);
            Shapes.R2Line.Program.ArrayVertexSize = 3;
            var pps = new Array();
            pps.push(this.TopLeft, this.TopRight, this.BottomRight, this.BottomLeft);
            Shapes.R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(WebGL.Vectors2ToVectors3(pps)));
            Shapes.R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINE_LOOP, 0, 4);
            Shapes.R2Line.Program.EndUse(gl);
        };
        Sprite.prototype.OnRender = function (gl) {
            if (this.DrawBorder)
                this.RenderRectangle(gl);
            if (this.UpdateTexture) {
                this.OnUpdateTexture(gl);
                this.UpdateTexture = false;
            }
            if (!Sprite.Program.IsLinked(gl))
                Sprite.Program.Link(gl);
            Sprite.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            gl.activeTexture(WebGLRenderingContext.TEXTURE10);
            gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, this.SpriteTexture);
            Sprite.Program.Uniforms.SpriteTexture = new WebGL.IntegerScalar(10);
            Sprite.Program.ArrayVertexSize = 3;
            Sprite.Program.ArrayVertexAttributes.vPosition = new Float32Array(WebGL.Vectors3ToArray(WebGL.Vectors2ToVectors3(this.Points)));
            Sprite.Program.ArrayVertexAttributes.a_texCoord = new Float32Array(WebGL.Vectors3ToArray(WebGL.Vectors2ToVectors3(this.TextureCoordinates)));
            gl.enable(WebGLRenderingContext.BLEND);
            gl.blendFunc(WebGLRenderingContext.SRC_ALPHA, WebGLRenderingContext.ONE_MINUS_SRC_ALPHA);
            Sprite.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, this.Points.length);
            Sprite.Program.EndUse(gl);
            gl.disable(WebGLRenderingContext.BLEND);
        };
        Sprite.prototype.Render = function (gl) {
            this.OnRender(gl);
        };
        Sprite.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec2 vPosition;\
                attribute vec2 a_texCoord;\
                varying vec2 v_texCoord;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * vec4(vPosition.x, vPosition.y, 0, 1);\
                        v_texCoord = a_texCoord;\
                    }\
                } ");
        Sprite.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                varying vec2 v_texCoord;\
                uniform sampler2D SpriteTexture;\
			    void main()\
                {\
                    {\
                        vec4 BGRA = texture2D(SpriteTexture, v_texCoord);\
                        gl_FragColor = vec4(BGRA.z, BGRA.y, BGRA.x, BGRA.w);\
                    }\
                } \
            ");
        Sprite.Program = new WebGL.Shaders.PipelineProgram("SpriteProgram", Sprite.VertexShader, Sprite.FragmentShader);
        return Sprite;
    }());
    Sprites.Sprite = Sprite;
})(Sprites || (Sprites = {}));
var Sprites;
(function (Sprites) {
    var TextSprite = (function (_super) {
        __extends(TextSprite, _super);
        function TextSprite(top, left, width, height) {
            _super.call(this, top, left, width, height);
            this.FontSize = 32;
            this.TextCanvas = document.createElement('canvas');
        }
        Object.defineProperty(TextSprite.prototype, "Text", {
            get: function () {
                return this._Text;
            },
            set: function (text) {
                this._Text = text;
                this.UpdateCanvas();
                this.UpdateTexture = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TextSprite.prototype, "TextSize", {
            get: function () {
                return this.FontSize;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TextSprite.prototype, "CanvasWidth", {
            get: function () {
                return this.Width * this.FontSize;
            },
            enumerable: true,
            configurable: true
        });
        TextSprite.prototype.UpdateCanvas = function () {
            var textToWrite = this._Text;
            this.RenderingContext = this.TextCanvas.getContext('2d');
            var ctx = this.RenderingContext;
            ctx.msImageSmoothingEnabled = false;
            ctx.font = this.TextSize.toString() + "px monospace";
            var text = new Array();
            var maxWidth = TextSprite.createMultilineText(ctx, textToWrite, 512 * 2, text);
            var canvasX = TextSprite.getPowerOfTwo(maxWidth);
            var canvasY = TextSprite.getPowerOfTwo(this.TextSize * (text.length + 1));
            this.TextCanvas.width = canvasX;
            this.TextCanvas.height = canvasY;
            ctx.clearRect(0, 0, this.TextCanvas.width, this.TextCanvas.height);
            ctx.fillStyle = "#000000";
            ctx.textAlign = "center";
            ctx.textBaseline = "middle";
            ctx.font = this.TextSize.toString() + "px monospace";
            var textX = canvasX / 2;
            var offset = (canvasY - this.TextSize * (text.length + 1)) * 0.5;
            for (var i = 0; i < text.length; i++) {
                var textY = (i + 1) * this.TextSize + offset;
                ctx.fillText(text[i], textX, textY);
            }
        };
        TextSprite.prototype.OnUpdateTexture = function (gl) {
            if (this.SpriteTexture == null || this.UpdateTexture == true) {
                if (this.SpriteTexture != null) {
                }
                this.UpdateCanvas();
                this.SpriteTexture = gl.createTexture();
                gl.pixelStorei(WebGLRenderingContext.UNPACK_FLIP_Y_WEBGL, 1);
                gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, this.SpriteTexture);
                gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, this.TextCanvas);
                gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.LINEAR);
                gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.LINEAR_MIPMAP_NEAREST);
                gl.generateMipmap(WebGLRenderingContext.TEXTURE_2D);
                gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
            }
        };
        TextSprite.getPowerOfTwo = function (value, pow) {
            if (pow === void 0) { pow = 1; }
            var pow = pow || 1;
            while (pow < value) {
                pow *= 2;
            }
            return pow;
        };
        TextSprite.createMultilineText = function (ctx, textToWrite, maxWidth, text) {
            textToWrite = textToWrite.replace("\n", " ");
            var currentText = textToWrite;
            var futureText;
            var subWidth = 0;
            var maxLineWidth = 0;
            var wordArray = textToWrite.split(" ");
            var wordsInCurrent, wordArrayLength;
            wordsInCurrent = wordArrayLength = wordArray.length;
            while (ctx.measureText(currentText).width > maxWidth && wordsInCurrent > 1) {
                wordsInCurrent--;
                var linebreak = false;
                currentText = futureText = "";
                for (var i = 0; i < wordArrayLength; i++) {
                    if (i < wordsInCurrent) {
                        currentText += wordArray[i];
                        if (i + 1 < wordsInCurrent) {
                            currentText += " ";
                        }
                    }
                    else {
                        futureText += wordArray[i];
                        if (i + 1 < wordArrayLength) {
                            futureText += " ";
                        }
                    }
                }
            }
            text.push(currentText);
            maxLineWidth = ctx.measureText(currentText).width;
            if (futureText) {
                subWidth = TextSprite.createMultilineText(ctx, futureText, maxWidth, text);
                if (subWidth > maxLineWidth) {
                    maxLineWidth = subWidth;
                }
            }
            return maxLineWidth;
        };
        return TextSprite;
    }(Sprites.Sprite));
    Sprites.TextSprite = TextSprite;
})(Sprites || (Sprites = {}));
var Shapes;
(function (Shapes) {
    (function (ShapeLineType) {
        ShapeLineType[ShapeLineType["Dotted"] = 0] = "Dotted";
        ShapeLineType[ShapeLineType["Normal"] = 1] = "Normal";
    })(Shapes.ShapeLineType || (Shapes.ShapeLineType = {}));
    var ShapeLineType = Shapes.ShapeLineType;
    (function (BuildingType) {
        BuildingType[BuildingType["Undefined"] = 0] = "Undefined";
        BuildingType[BuildingType["Wall"] = 1] = "Wall";
        BuildingType[BuildingType["Window"] = 2] = "Window";
        BuildingType[BuildingType["Door"] = 3] = "Door";
        BuildingType[BuildingType["Opening"] = 4] = "Opening";
    })(Shapes.BuildingType || (Shapes.BuildingType = {}));
    var BuildingType = Shapes.BuildingType;
    var R2Shape = (function () {
        function R2Shape() {
            this.LineType = ShapeLineType.Normal;
            this.BuildingType = BuildingType.Wall;
            this._Owners = Object();
            this._Children = Object();
            this._ShapeCenter = new WebGL.Vector3(0, 0, 0);
            this._LastShapeCenter = new WebGL.Vector3(0, 0, 0);
            this._Alpha = 1;
            this._ShapeColor = new WebGL.Vector4(0, 0, 0, 1);
            this._IsDrawing = false;
            this.IsSelected = false;
            this.InnerTextSprite = null;
            this.EditingControls = new Object();
            this._UnderMouseControl = false;
            this.MouseDown = null;
            this.MouseUp = null;
            this.LayerName = "0";
            this._IntersectedShapes = new Object();
            this._InnerSegments = null;
            this.BuildingShapes = new Array();
            this.OwnerBuildingShape = null;
            R2Shape._LastColorID = R2Shape._LastColorID.IncrementOne();
            this._SelectionColorId = R2Shape._LastColorID.Copy();
            R2Shape.ShapesByColor[this._SelectionColorId.Key] = this;
            this.ShapeColor = WebGL.Vector4Colors.Blue;
        }
        Object.defineProperty(R2Shape.prototype, "SelectionColorVector", {
            get: function () {
                return new WebGL.Vector4(this._SelectionColorId.R / 255, this._SelectionColorId.G / 255, this._SelectionColorId.B / 255, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "SelectionColor", {
            get: function () {
                return this._SelectionColorId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "ColorKey", {
            get: function () {
                return this._SelectionColorId.Key;
            },
            enumerable: true,
            configurable: true
        });
        R2Shape.prototype.Equals = function (other) {
            if (other == null)
                return false;
            return this.ColorKey === other.ColorKey;
        };
        R2Shape.prototype.AddChild = function (shape) {
            this._Children[shape.ColorKey] = shape;
            shape._Owners[this.ColorKey] = this;
        };
        R2Shape.prototype.AddOwner = function (shape) {
            shape.AddChild(this);
        };
        R2Shape.prototype.RemoveOwner = function (owner) {
            if (this._Owners.hasOwnProperty(owner.ColorKey)) {
                delete this._Owners[owner.ColorKey];
                delete owner._Children[this.ColorKey];
            }
        };
        R2Shape.prototype.Dispose = function () {
            for (var key in this._Children) {
                var child = this._Children[key];
                child.RemoveOwner(this);
            }
            for (var key in this._Owners) {
                var owner = this._Owners[key];
                this.RemoveOwner(owner);
            }
            delete R2Shape.ShapesByColor[this.ColorKey];
        };
        Object.defineProperty(R2Shape.prototype, "LastShapeCenter", {
            get: function () {
                return this._LastShapeCenter;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "ShapeCenter", {
            get: function () {
                return this._ShapeCenter;
            },
            set: function (value) {
                this._LastShapeCenter = this._ShapeCenter;
                this._ShapeCenter = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "Alpha", {
            get: function () {
                return this._Alpha;
            },
            set: function (alpha) {
                this._Alpha = alpha;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "ShapeColor", {
            get: function () {
                if (this._IsDrawing)
                    return WebGL.Vector4Colors.FromRGB(150, 216, 0);
                else {
                    var cl = this._ShapeColor.Xyz.ToVector4(this._ShapeColor.W * this._Alpha);
                    return cl;
                }
            },
            set: function (value) {
                this._ShapeColor = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "IsDrawing", {
            get: function () {
                return this._IsDrawing;
            },
            set: function (value) {
                this._IsDrawing = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape, "MemoryShapesCount", {
            get: function () {
                return Object.keys(R2Shape.ShapesByColor).length;
            },
            enumerable: true,
            configurable: true
        });
        R2Shape.SelectShapeByColor = function (color) {
            if (color.Equals(ColorByte4.NullColor))
                return null;
            if (color.Equals(ColorByte4.VoidColor))
                return null;
            if (Object.keys(R2Shape.ShapesByColor).length == 0)
                return null;
            if (R2Shape.ShapesByColor.hasOwnProperty(color.Key))
                return R2Shape.ShapesByColor[color.Key];
            else
                throw "The shape color key (" + color.Key + ") doesn't exist.";
        };
        R2Shape.prototype.BeforeRender = function (gl) {
        };
        R2Shape.prototype.OnRender = function (gl) {
        };
        R2Shape.prototype.AfterRender = function (gl) {
        };
        R2Shape.prototype.Render = function (gl) {
            this.BeforeRender(gl);
            this.OnRender(gl);
            this.AfterRender(gl);
        };
        R2Shape.prototype.OnRenderForPicking = function (gl) {
        };
        R2Shape.prototype.RenderForPicking = function (gl) {
            this.BeforeRender(gl);
            this.OnRenderForPicking(gl);
            this.AfterRender(gl);
        };
        Object.defineProperty(R2Shape.prototype, "JsonObject", {
            get: function () {
                var sh = {
                    ShapeName: "R2Shape",
                    LayerName: this.LayerName,
                    ShapeContent: null
                };
                return sh;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "Json", {
            get: function () {
                return JSON.stringify(this.JsonObject);
            },
            enumerable: true,
            configurable: true
        });
        R2Shape.prototype.GetEditingControls = function () {
            var ecs = new Array();
            for (var key in this.EditingControls) {
                var value = this.EditingControls[key];
                if (value instanceof R2Shape) {
                    var sh = value;
                    ecs.push(sh);
                }
            }
            return ecs;
        };
        R2Shape.prototype.RenderEditingControlsForPicking = function (gl) {
            for (var key in this.EditingControls) {
                var value = this.EditingControls[key];
                if (value instanceof R2Shape) {
                    var sh = value;
                    sh.ProjectionMatrix = this.ProjectionMatrix;
                    sh.RenderForPicking(gl);
                }
            }
        };
        R2Shape.prototype.ResetMouseStatus = function () {
            this._UnderMouseControl = false;
        };
        R2Shape.prototype.ExecuteMouseDown = function (args) {
            if (this._UnderMouseControl == false) {
                this._UnderMouseControl = true;
                this.OnMouseDown(args);
            }
        };
        R2Shape.prototype.OnEditingControlChange = function (childShape) {
        };
        R2Shape.prototype.ExecuteMouseMove = function (args) {
            var result;
            for (var key in this.EditingControls) {
                var value = this.EditingControls[key];
                if (value instanceof R2Shape) {
                    var ctrl = value;
                    result = result || ctrl.ExecuteMouseMove(args);
                }
            }
            if (this._UnderMouseControl) {
                this.PreviousShapeCenter = this.ShapeCenter;
                this.OnMouseMove(args);
                for (var key in this._Owners) {
                    var value = this._Owners[key];
                    if (value instanceof R2Shape) {
                        var owner = value;
                        owner.OnEditingControlChange(this);
                    }
                }
                result = true;
            }
            return result;
        };
        R2Shape.prototype.ExecuteMouseUp = function (args) {
            for (var key in this.EditingControls) {
                var value = this.EditingControls[key];
                if (value instanceof R2Shape) {
                    var ctrl = value;
                    ctrl.ExecuteMouseUp(args);
                }
            }
            if (this._UnderMouseControl == true) {
                this.OnMouseUp(args);
                this._UnderMouseControl = false;
            }
        };
        R2Shape.prototype.OnMouseDown = function (args) {
            if (this.MouseDown != null)
                this.MouseDown(this);
        };
        R2Shape.prototype.OnMouseMove = function (args) {
        };
        R2Shape.prototype.OnMouseUp = function (args) {
            if (this.MouseUp != null)
                this.MouseUp(this);
        };
        Object.defineProperty(R2Shape.prototype, "TerminalPoints", {
            get: function () {
                var vv = new Array();
                return vv;
            },
            enumerable: true,
            configurable: true
        });
        R2Shape.prototype.GetIntersectionsWithRay = function (firstPoint, lastPoint) {
            var intersections = new Array();
            if (this instanceof Shapes.R2Line) {
                var line = this;
                var p = line.IntersectionWithRay(firstPoint, lastPoint);
                if (p != null) {
                    if (line.IsPointOnLineSegment(p)) {
                        intersections.push(p);
                    }
                }
            }
            if (this instanceof Shapes.R2Arc) {
                var arc = this;
                var ia = arc.IntersectionWithRay(firstPoint, lastPoint);
                if (ia.FirstPoint != null) {
                    if (arc.IsPointOnArc(ia.FirstPoint)) {
                        intersections.push(ia.FirstPoint);
                    }
                }
                if (ia.SecondPoint != null) {
                    if (arc.IsPointOnArc(ia.SecondPoint)) {
                        intersections.push(ia.SecondPoint);
                    }
                }
            }
            if (this instanceof Shapes.R2Circle) {
                var circle = this;
                var ic = circle.IntersectionWithRay(firstPoint, lastPoint);
                if (ic.FirstPoint != null) {
                    intersections.push(ic.FirstPoint);
                }
                if (ic.SecondPoint != null) {
                    intersections.push(ic.SecondPoint);
                }
            }
            if (this instanceof Shapes.R2Rectangle) {
                var rectangle = this;
                var iis = rectangle.IntersectionWithRay(firstPoint, lastPoint);
                iis.forEach(function (p) { return intersections.push(p); });
            }
            return intersections;
        };
        R2Shape.prototype.GetIntersectionsWith = function (shape) {
            var intersections = new Array();
            return intersections;
        };
        R2Shape.prototype.FindIntersectionsWithAll = function (shapes) {
            var intersections = new Array();
            var ishape;
            for (ishape = 0; ishape < shapes.length; ishape++) {
                var targetShape = shapes[ishape];
                var iis = this.GetIntersectionsWith(targetShape);
                iis.forEach(function (x) { return intersections.push(x); });
            }
            return intersections;
        };
        Object.defineProperty(R2Shape.prototype, "ShapeName", {
            get: function () {
                return "R2Shape";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "IsComposite", {
            get: function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "InnerShapes", {
            get: function () {
                var composites = new Array();
                return composites;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "IntersectedShapes", {
            get: function () {
                return this._IntersectedShapes;
            },
            enumerable: true,
            configurable: true
        });
        R2Shape.prototype.ResetDiscoveredIntersections = function () {
            this._IntersectedShapes = new Object();
        };
        R2Shape.prototype.AddIntersectedShape = function (shape, intersectedPoints) {
            var intersectionPoints;
            if (!this._IntersectedShapes.hasOwnProperty(shape.ColorKey)) {
                intersectionPoints = new Array();
                this._IntersectedShapes[shape.ColorKey] = intersectionPoints;
            }
            else {
                intersectionPoints = this._IntersectedShapes[shape.ColorKey];
            }
            intersectedPoints.forEach(function (p) { return intersectionPoints.push(p); });
        };
        R2Shape.prototype.GetStoredIntersectionsWithShape = function (shape) {
            if (this._IntersectedShapes.hasOwnProperty(shape.ColorKey)) {
                return this._IntersectedShapes[shape.ColorKey];
            }
            return null;
        };
        Object.defineProperty(R2Shape.prototype, "IntersectedPoints", {
            get: function () {
                var vvs = new Array();
                for (var key in this._IntersectedShapes) {
                    var ps = this._IntersectedShapes[key];
                    ps.forEach(function (x) { return vvs.push(x); });
                }
                return vvs;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "InnerSegments", {
            get: function () {
                return this._InnerSegments;
            },
            enumerable: true,
            configurable: true
        });
        R2Shape.prototype.ApplySegmentation = function () {
        };
        R2Shape.prototype.GetNearestPoint = function (point) {
            return null;
        };
        Object.defineProperty(R2Shape.prototype, "MaximumX", {
            get: function () {
                return 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "MinimumX", {
            get: function () {
                return 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "MaximumY", {
            get: function () {
                return 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Shape.prototype, "MinimumY", {
            get: function () {
                return 0;
            },
            enumerable: true,
            configurable: true
        });
        R2Shape.prototype.AddBuildingShape = function (buildingShape) {
            this.BuildingShapes.push(buildingShape);
            buildingShape.OwnerBuildingShape = this;
        };
        R2Shape.prototype.RemoveBuildingShape = function (buildingShape) {
            var idx = this.GetBuildingShapeIndex(buildingShape);
            this.RemoveBuildingShapeByIndex(idx);
            buildingShape.OwnerBuildingShape = null;
        };
        R2Shape.prototype.GetBuildingShapeIndex = function (shape) {
            var ix;
            var selectedIndex = -1;
            for (ix = 0; ix < this.BuildingShapes.length; ix++) {
                if (this.BuildingShapes[ix].SelectionColor.Key == shape.SelectionColor.Key) {
                    selectedIndex = ix;
                    break;
                }
            }
            return selectedIndex;
        };
        R2Shape.prototype.RemoveBuildingShapeByIndex = function (shapeIndex) {
            this.BuildingShapes.splice(shapeIndex, 1);
        };
        R2Shape.prototype.GetOwnersOf = function (shapeName) {
            var oshapes = new Array();
            for (var key in this._Owners) {
                var sh = this._Owners[key];
                if (sh.ShapeName === shapeName) {
                    oshapes.push(sh);
                }
            }
            return oshapes;
        };
        R2Shape.prototype.ToCdtXElement = function () {
            return "<Shape/>";
        };
        R2Shape.prototype.ToSliceXElement = function () {
            return "<Slice/>";
        };
        R2Shape.prototype.Translate = function (translation) {
            this.ShapeCenter = WebGL.Vector3.Add(this.ShapeCenter, translation.ToVector3());
            this.OnTranslation(translation);
        };
        R2Shape.prototype.OnTranslation = function (translation) {
        };
        R2Shape.prototype.Rotate = function (rotationCenter, rotationAngle) {
            this.ShapeCenter = this.ShapeCenter.Xy.Rotate(rotationCenter, rotationAngle).ToVector3();
            this.OnRotation(rotationCenter, rotationAngle);
        };
        R2Shape.prototype.OnRotation = function (rotationCenter, rotationAngle) {
        };
        R2Shape.prototype.RevertToLastLocation = function () {
            this._ShapeCenter = this._LastShapeCenter;
        };
        R2Shape.prototype.UpdateLastLocation = function () {
            this._LastShapeCenter = this._ShapeCenter;
        };
        R2Shape.prototype.Refresh = function () {
        };
        R2Shape._LastColorID = ColorByte4.ZeroColor;
        R2Shape.ShapesByColor = new Object();
        return R2Shape;
    }());
    Shapes.R2Shape = R2Shape;
})(Shapes || (Shapes = {}));
var Geometry;
(function (Geometry) {
    var Line = (function () {
        function Line(firstPoint, lastPoint) {
            this.FirstPoint = firstPoint;
            this.LastPoint = lastPoint;
        }
        Object.defineProperty(Line.prototype, "Vector", {
            get: function () {
                return WebGL.Vector3.Subtract(this.LastPoint, this.FirstPoint);
            },
            enumerable: true,
            configurable: true
        });
        return Line;
    }());
    Geometry.Line = Line;
})(Geometry || (Geometry = {}));
var InvariantShapes;
(function (InvariantShapes) {
    var MultiLinesInvariantGlyph = (function () {
        function MultiLinesInvariantGlyph() {
            this.ShapeColor = new WebGL.Vector4(1, 0, 0, 1);
            this.WindowPercentage = 2 / 100;
            this.ShapeCenter = new WebGL.Vector3(0, 0, 0);
            this.ProjectionAngle = Angle.Zero;
            this.Lines = new Array();
        }
        Object.defineProperty(MultiLinesInvariantGlyph.prototype, "ProjectionRotationAngleDegrees", {
            get: function () {
                return this.ProjectionAngle.Degrees;
            },
            set: function (value) {
                this.ProjectionAngle = Angle.FromDegrees(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MultiLinesInvariantGlyph.prototype, "RotationQuaternion", {
            get: function () {
                var q = WebGL.Quaternion.FromAxisAngle(WebGL.Vector3.UnitZ, -this.ProjectionAngle.Radians);
                return q;
            },
            enumerable: true,
            configurable: true
        });
        MultiLinesInvariantGlyph.prototype.AddLine = function (x1, y1, x2, y2) {
            var firstPoint = new WebGL.Vector3(x1, y1, 0);
            var lastPoint = new WebGL.Vector3(x2, y2, 0);
            this.Lines.push(new Geometry.Line(firstPoint, lastPoint));
        };
        MultiLinesInvariantGlyph.prototype.ClearLines = function () {
            this.Lines = new Array();
        };
        Object.defineProperty(MultiLinesInvariantGlyph.prototype, "LinesPoints", {
            get: function () {
                var pps = new Array();
                this.Lines.forEach(function (l) {
                    pps.push(l.FirstPoint);
                    pps.push(l.LastPoint);
                });
                return pps;
            },
            enumerable: true,
            configurable: true
        });
        MultiLinesInvariantGlyph.prototype.Render = function (gl) {
            if (!MultiLinesInvariantGlyph.Program.IsLinked(gl))
                MultiLinesInvariantGlyph.Program.Link(gl);
            MultiLinesInvariantGlyph.Program.Uniforms.ShapeColor = this.ShapeColor;
            var scaledMatrix = WebGL.Matrix4.CreateScale(this.WindowPercentage * this.ProjectionWidth, this.WindowPercentage * this.ProjectionWidth, 1);
            MultiLinesInvariantGlyph.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            MultiLinesInvariantGlyph.Program.Uniforms.ScaledMatrix = scaledMatrix;
            MultiLinesInvariantGlyph.Program.Uniforms.ShapeCenter = this.ShapeCenter;
            MultiLinesInvariantGlyph.Program.ArrayVertexSize = 3;
            var lpls = this.LinesPoints;
            MultiLinesInvariantGlyph.Program.ArrayVertexAttributes.position = new Float32Array(WebGL.Vectors3ToArray(PointsCloud.RotatePoints(lpls, this.RotationQuaternion)));
            MultiLinesInvariantGlyph.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lpls.length);
            MultiLinesInvariantGlyph.Program.EndUse(gl);
        };
        MultiLinesInvariantGlyph.RoofGlyph = function () {
            var roofglyph = new MultiLinesInvariantGlyph();
            var maxX = 10;
            var maxY = 5;
            var scale = 20;
            maxX = maxX / scale;
            maxY = maxY / scale;
            roofglyph.AddLine(-maxX, maxY, maxX, maxY);
            roofglyph.AddLine(-maxX / 2, 0, maxX / 2, 0);
            roofglyph.AddLine(-maxX, -maxY, maxX, -maxY);
            roofglyph.AddLine(-maxX, -maxY, -maxX, maxY);
            roofglyph.AddLine(maxX, -maxY, maxX, maxY);
            roofglyph.AddLine(maxX / 2, 0, maxX, maxY);
            roofglyph.AddLine(maxX / 2, 0, maxX, -maxY);
            roofglyph.AddLine(-maxX / 2, 0, -maxX, maxY);
            roofglyph.AddLine(-maxX / 2, 0, -maxX, -maxY);
            return roofglyph;
        };
        MultiLinesInvariantGlyph.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 position;\
                uniform mat4 ProjectionMatrix;\
                uniform mat4 ScaledMatrix;\
                uniform vec3 ShapeCenter;\
                void main()\
                {\
                    {\
                        vec4 finalPosition = vec4(ShapeCenter, 0) + (ScaledMatrix * position);\
                        gl_Position = ProjectionMatrix * finalPosition;\
                    }\
                } ");
        MultiLinesInvariantGlyph.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        MultiLinesInvariantGlyph.Program = new WebGL.Shaders.PipelineProgram("MultiLinesInvariantShapeProgram", MultiLinesInvariantGlyph.VertexShader, MultiLinesInvariantGlyph.FragmentShader);
        return MultiLinesInvariantGlyph;
    }());
    InvariantShapes.MultiLinesInvariantGlyph = MultiLinesInvariantGlyph;
})(InvariantShapes || (InvariantShapes = {}));
var DrawingLayer = (function () {
    function DrawingLayer() {
        this.IsBackGroundVisible = false;
        this.ShouldCalculateOffsetBoundary = false;
        this._DrawingShapes = new Array();
        this.AllShapesRemoved = null;
        this.ShapeAdded = null;
        this._CADLayers = null;
        this.ShouldRefreshLayers = true;
        this.ProcessedShapes = new Array();
        this.ShapesCoordinatesIntersections = new Object();
        this.ShapesIntersections = new Object();
        this.ShapesTerminals = new Object();
        this.ShapesProjections = new Object();
        this.TopBorder = new Shapes.R2Line();
        this.LeftBorder = new Shapes.R2Line();
        this.RightBorder = new Shapes.R2Line();
        this.BottomBorder = new Shapes.R2Line();
        this.TopRightDiagonalBorder = new Shapes.R2Line();
        this.TopLeftDiagonalBorder = new Shapes.R2Line();
        this.BottomRightDiagonalBorder = new Shapes.R2Line();
        this.BottomLeftDiagonalBorder = new Shapes.R2Line();
        this.BorderLeftMargin = 6;
        this.BorderRightMargin = 6;
        this.BorderTopMargin = 6;
        this.BorderBottomMargin = 6;
        this.DefaultDrawingColor = null;
        this.ShowBorder = true;
        this.BoundaryLines = null;
        this.TopRightGlyph = InvariantShapes.MultiLinesInvariantGlyph.RoofGlyph();
        this.DiscoveredVerticalLines = new Array();
        this.DiscoveredHorizontalLines = new Array();
        this._Name = "Drawing Layer";
        this._MaximumX = null;
        this._MinimumX = null;
        this._MaximumY = null;
        this._MinimumY = null;
        this._AveragePoint = null;
        this._WholePointsLocations = null;
        DrawingLayer.LastId++;
        this._Id = DrawingLayer.LastId;
    }
    Object.defineProperty(DrawingLayer.prototype, "Id", {
        get: function () {
            return this._Id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "DrawingShapes", {
        get: function () {
            return this._DrawingShapes;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "VisibleShapes", {
        get: function () {
            var _this = this;
            var visishapes = new Array();
            this.DrawingShapes.forEach(function (sh) {
                if (_this.CADLayers[sh.LayerName])
                    visishapes.push(sh);
            });
            return visishapes;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "ShapesCount", {
        get: function () {
            return this._DrawingShapes.length;
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.prototype.AddCADLayer = function (layerName) {
        if (this._CADLayers.hasOwnProperty(layerName) == false) {
            this._CADLayers[layerName] = true;
        }
    };
    DrawingLayer.prototype.RefreshCADLayers = function () {
        var _this = this;
        if (this._CADLayers == null)
            this._CADLayers = new Object();
        var addednames = new Object();
        this.DrawingShapes.forEach(function (sh) {
            _this.AddCADLayer(sh.LayerName);
            if (addednames.hasOwnProperty(sh.LayerName) == false)
                addednames[sh.LayerName] = true;
        });
        for (var nn in this._CADLayers) {
            if (addednames.hasOwnProperty(nn)) {
            }
            else {
                delete this._CADLayers[nn];
            }
        }
    };
    Object.defineProperty(DrawingLayer.prototype, "CADLayers", {
        get: function () {
            if (this.ShouldRefreshLayers) {
                this.RefreshCADLayers();
                this.ShouldRefreshLayers = false;
            }
            return this._CADLayers;
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.prototype.AddShapeWithoutProcessing = function (shape) {
        this.DrawingShapes.push(shape);
        this.ClearCache();
    };
    DrawingLayer.prototype.AddShape = function (shape) {
        this.DrawingShapes.push(shape);
        this.ProcessShape(shape, false);
        if (this.DefaultDrawingColor != null) {
            shape.ShapeColor = this.DefaultDrawingColor;
        }
        if (this.ShapeAdded != null)
            this.ShapeAdded(this, shape);
        this.ClearCache();
    };
    DrawingLayer.prototype.ReplaceShape = function (shapeIndex, newShape) {
        var shape = this.GetShapeByIndex(shapeIndex);
        this.RemoveProcessedShape(shape);
        shape.Dispose();
        this._DrawingShapes.splice(shapeIndex, 1, newShape);
        this.ClearCache();
    };
    DrawingLayer.prototype.RemoveShape = function (shapeIndex) {
        if (shapeIndex >= 0) {
            var shape = this.GetShapeByIndex(shapeIndex);
            this.RemoveProcessedShape(shape);
            shape.Dispose();
            this._DrawingShapes.splice(shapeIndex, 1);
            if (this.ShapesCount == 0 && this.AllShapesRemoved != null) {
                this.AllShapesRemoved(this);
            }
            this.ClearCache();
        }
    };
    DrawingLayer.prototype.ClearShapes = function () {
        this._DrawingShapes = new Array();
        this.ClearCache();
    };
    DrawingLayer.prototype.GetShapeIndex = function (shape) {
        var ix;
        var selectedIndex = -1;
        for (ix = 0; ix < this._DrawingShapes.length; ix++) {
            if (this._DrawingShapes[ix].SelectionColor.Key == shape.SelectionColor.Key) {
                selectedIndex = ix;
                break;
            }
        }
        return selectedIndex;
    };
    DrawingLayer.prototype.GetShapeByIndex = function (index) {
        return this._DrawingShapes[index];
    };
    DrawingLayer.prototype.ResetAllShapesIntersectionInformation = function () {
        var ish;
        for (ish = 0; ish < this.ShapesCount; ish++) {
            var shape = this.DrawingShapes[ish];
            shape.ResetDiscoveredIntersections();
        }
    };
    DrawingLayer.prototype.DetermineTwoShapeIntersections = function (shape, againstShape) {
        var intersections = new Array();
        if (shape.IsComposite == false && againstShape.IsComposite == false) {
            var discoveredIntersections = shape.GetIntersectionsWith(againstShape);
            discoveredIntersections.forEach(function (p) { return intersections.push(p); });
            shape.AddIntersectedShape(againstShape, discoveredIntersections);
            againstShape.AddIntersectedShape(shape, discoveredIntersections);
        }
        if (shape.IsComposite == false && againstShape.IsComposite == true) {
            var againstShapes = againstShape.InnerShapes;
            var iy;
            for (iy = 0; iy < againstShapes.length; iy++) {
                var innerAgainstShape = againstShapes[iy];
                var sects = this.DetermineTwoShapeIntersections(shape, innerAgainstShape);
                sects.forEach(function (p) { return intersections.push(p); });
            }
        }
        if (shape.IsComposite == true && againstShape.IsComposite == false) {
            var innerShapes = shape.InnerShapes;
            var ix;
            for (ix = 0; ix < innerShapes.length; ix++) {
                var innerShape = innerShapes[ix];
                var sects = this.DetermineTwoShapeIntersections(innerShape, againstShape);
                sects.forEach(function (p) { return intersections.push(p); });
            }
        }
        if (shape.IsComposite == true && againstShape.IsComposite == true) {
            var ix;
            var iy;
            var innerShapes = shape.InnerShapes;
            var againstShapes = againstShape.InnerShapes;
            for (ix = 0; ix < innerShapes.length; ix++) {
                var innerShape = innerShapes[ix];
                for (iy = 0; iy < againstShapes.length; iy++) {
                    var innerAgainstShape = againstShapes[iy];
                    var sects = this.DetermineTwoShapeIntersections(innerShape, innerAgainstShape);
                    sects.forEach(function (p) { return intersections.push(p); });
                }
            }
        }
        return intersections;
    };
    DrawingLayer.prototype.DetermineShapesIntersections = function () {
        this.ResetAllShapesIntersectionInformation();
        var TestedShapes = new Array();
        var intersections = new Array();
        var ish;
        for (ish = 0; ish < this.ShapesCount; ish++) {
            var shape = this.DrawingShapes[ish];
            var iagainst;
            for (iagainst = 0; iagainst < this.ShapesCount; iagainst++) {
                var againstShape = this.DrawingShapes[iagainst];
                var ShouldWeTest = true;
                if (ish != iagainst) {
                    var itest;
                    for (itest = 0; itest < TestedShapes.length; itest++) {
                        var x = TestedShapes[itest];
                        if (x.FirstShape.ColorKey == shape.ColorKey) {
                            if (x.SecondShape.ColorKey == againstShape.ColorKey) {
                                ShouldWeTest = false;
                                break;
                            }
                        }
                    }
                }
                else {
                    ShouldWeTest = false;
                }
                if (ShouldWeTest) {
                    var sects = this.DetermineTwoShapeIntersections(shape, againstShape);
                    sects.forEach(function (p) { return intersections.push(p); });
                    TestedShapes.push({ FirstShape: shape, SecondShape: againstShape });
                    TestedShapes.push({ FirstShape: againstShape, SecondShape: shape });
                }
            }
        }
        return intersections;
    };
    DrawingLayer.prototype.SetBorderMargin = function (value) {
        this.BorderLeftMargin = value;
        this.BorderRightMargin = value;
        this.BorderTopMargin = value;
        this.BorderBottomMargin = value;
    };
    Object.defineProperty(DrawingLayer.prototype, "Top", {
        get: function () {
            return this.MaximumY + this.BorderTopMargin;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "Bottom", {
        get: function () {
            return this.MinimumY - this.BorderBottomMargin;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "Right", {
        get: function () {
            return this.MaximumX + this.BorderRightMargin;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "Left", {
        get: function () {
            return this.MinimumX - this.BorderLeftMargin;
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.prototype.BlendingRender = function (gl, alpha) {
        gl.enable(WebGLRenderingContext.BLEND);
        gl.blendFunc(WebGLRenderingContext.SRC_ALPHA, WebGLRenderingContext.ONE_MINUS_SRC_ALPHA);
        var ix;
        for (ix = 0; ix < this.ShapesCount; ix++) {
            var sh = this.DrawingShapes[ix];
            if (sh instanceof Shapes.R2Line) {
                var ll = sh;
                if (ll.IsDimension)
                    continue;
            }
            if (sh instanceof Shapes.R2Rectangle) {
                var rc = sh;
                if (rc.InnerTextSprite != null)
                    continue;
            }
            sh.Alpha = alpha;
            sh.ProjectionMatrix = this.ProjectionMatrix;
            sh.Render(gl);
            sh.Alpha = 1;
        }
        gl.disable(WebGLRenderingContext.BLEND);
    };
    DrawingLayer.prototype.RenderBorder = function (gl) {
        this.TopBorder.FirstPointLocation = new WebGL.Vector3(this.MinimumX - this.BorderLeftMargin, this.MaximumY + this.BorderTopMargin, 0);
        this.TopBorder.LastPointLocation = new WebGL.Vector3(this.MaximumX + this.BorderRightMargin, this.MaximumY + this.BorderTopMargin, 0);
        this.BottomBorder.FirstPointLocation = new WebGL.Vector3(this.MinimumX - this.BorderLeftMargin, this.MinimumY - this.BorderBottomMargin, 0);
        this.BottomBorder.LastPointLocation = new WebGL.Vector3(this.MaximumX + this.BorderRightMargin, this.MinimumY - this.BorderBottomMargin, 0);
        this.RightBorder.FirstPointLocation = new WebGL.Vector3(this.MaximumX + this.BorderRightMargin, this.MaximumY + this.BorderTopMargin, 0);
        this.RightBorder.LastPointLocation = new WebGL.Vector3(this.MaximumX + this.BorderRightMargin, this.MinimumY - this.BorderBottomMargin, 0);
        this.LeftBorder.FirstPointLocation = new WebGL.Vector3(this.MinimumX - this.BorderLeftMargin, this.MaximumY + this.BorderTopMargin, 0);
        this.LeftBorder.LastPointLocation = new WebGL.Vector3(this.MinimumX - this.BorderLeftMargin, this.MinimumY - this.BorderBottomMargin, 0);
        this.TopBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.BottomBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.LeftBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.RightBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.TopBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.BottomBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.LeftBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.RightBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.TopBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.BottomBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.LeftBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.RightBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.TopBorder.DashWidth = 5;
        this.BottomBorder.DashWidth = 5;
        this.LeftBorder.DashWidth = 5;
        this.RightBorder.DashWidth = 5;
        this.TopBorder.Render(gl);
        this.BottomBorder.Render(gl);
        this.LeftBorder.Render(gl);
        this.RightBorder.Render(gl);
        this.TopRightDiagonalBorder.FirstPointLocation = new WebGL.Vector3(this.MaximumX + this.BorderRightMargin, this.MaximumY + this.BorderTopMargin, 0);
        this.TopRightDiagonalBorder.LastPointLocation = WebGL.Vector3.Add(this.TopRightDiagonalBorder.FirstPointLocation, new WebGL.Vector3(1000, 1000, 0));
        this.TopLeftDiagonalBorder.FirstPointLocation = new WebGL.Vector3(this.MinimumX - this.BorderLeftMargin, this.MaximumY + this.BorderTopMargin, 0);
        this.TopLeftDiagonalBorder.LastPointLocation = WebGL.Vector3.Add(this.TopLeftDiagonalBorder.FirstPointLocation, new WebGL.Vector3(-1000, 1000, 0));
        this.BottomRightDiagonalBorder.FirstPointLocation = new WebGL.Vector3(this.MaximumX + this.BorderRightMargin, this.MinimumY - this.BorderBottomMargin, 0);
        this.BottomRightDiagonalBorder.LastPointLocation = WebGL.Vector3.Add(this.BottomRightDiagonalBorder.FirstPointLocation, new WebGL.Vector3(1000, -1000, 0));
        this.BottomLeftDiagonalBorder.FirstPointLocation = new WebGL.Vector3(this.MinimumX - this.BorderLeftMargin, this.MinimumY - this.BorderBottomMargin, 0);
        this.BottomLeftDiagonalBorder.LastPointLocation = WebGL.Vector3.Add(this.BottomLeftDiagonalBorder.FirstPointLocation, new WebGL.Vector3(-1000, -1000, 0));
        this.TopRightDiagonalBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.TopLeftDiagonalBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.BottomRightDiagonalBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.BottomLeftDiagonalBorder.ProjectionMatrix = this.ProjectionMatrix;
        this.TopRightDiagonalBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.TopLeftDiagonalBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.BottomRightDiagonalBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.BottomLeftDiagonalBorder.LineType = Shapes.ShapeLineType.Dotted;
        this.TopRightDiagonalBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.TopLeftDiagonalBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.BottomRightDiagonalBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.BottomLeftDiagonalBorder.ShapeColor = WebGL.Vector4Colors.Orange;
        this.TopRightDiagonalBorder.DashWidth = 5;
        this.TopLeftDiagonalBorder.DashWidth = 5;
        this.BottomRightDiagonalBorder.DashWidth = 5;
        this.BottomLeftDiagonalBorder.DashWidth = 5;
    };
    DrawingLayer.prototype.CalculateOffsetBoundary = function () {
        try {
            if (this.BoundaryLines == null && this.ShapesCount < 80) {
                var ls = this.DiscoverExteriorBoundary();
                if (ls != null) {
                    this.BoundaryLines = ls.LinesOffset(-2);
                    this.BoundaryLines.forEach(function (l) {
                        l.ShapeColor = WebGL.Vector4Colors.Red;
                    });
                }
            }
        }
        catch (ee) {
            console.log(ee);
            if (this.BoundaryLines != null) {
                if (this.BoundaryLines.length > 0) {
                    this.BoundaryLines.forEach(function (l) { return l.Dispose(); });
                }
                this.BoundaryLines = null;
            }
        }
    };
    DrawingLayer.prototype.RenderOffsetBoundary = function (gl) {
        if (this.BoundaryLines != null) {
            gl.enable(WebGLRenderingContext.BLEND);
            gl.blendFunc(WebGLRenderingContext.SRC_ALPHA, WebGLRenderingContext.ONE_MINUS_SRC_ALPHA);
            for (var ix = 0; ix < this.BoundaryLines.length; ix++) {
                var ll = this.BoundaryLines[ix];
                ll.ProjectionMatrix = this.ProjectionMatrix;
                ll.Alpha = 0.8;
                ll.Render(gl);
                ll.Alpha = 1;
            }
            gl.disable(WebGLRenderingContext.BLEND);
        }
    };
    DrawingLayer.prototype.Render = function (gl) {
        var ix;
        for (ix = 0; ix < this.ShapesCount; ix++) {
            var sh = this.DrawingShapes[ix];
            if (this.CADLayers[sh.LayerName]) {
                sh.ProjectionMatrix = this.ProjectionMatrix;
                sh.Render(gl);
                if (sh instanceof Shapes.R2Line) {
                    var ll = sh;
                    if (ll.BuildingType == Shapes.BuildingType.Window)
                        ll.RenderIdentificationCircle(gl);
                    if (ll.BuildingType == Shapes.BuildingType.Door) {
                        ll.RenderIdentificationCircle(gl);
                        ll.RenderDoorControllingShapes(gl);
                    }
                }
            }
        }
        if (this.ShapesCount == 0)
            return;
        if (this.ShowBorder) {
            this.RenderBorder(gl);
        }
        if (this.TopRightGlyph != null) {
            this.TopRightGlyph.ShapeCenter = new WebGL.Vector3(this.Right, this.Top, 0);
            this.TopRightGlyph.ShapeColor = WebGL.Vector4Colors.Black;
            this.TopRightGlyph.ProjectionMatrix = this.ProjectionMatrix;
            this.TopRightGlyph.ProjectionHeight = this.ProjectionHeight;
            this.TopRightGlyph.ProjectionWidth = this.ProjectionWidth;
        }
    };
    DrawingLayer.prototype.FindLineIntersectionsWithCoordinateLines = function (line) {
        var Intersections = new Array();
        var iy;
        for (iy = FloatingMath.MinimumInteger(line.MinimumX) + 1; iy < FloatingMath.MaximumInteger(line.MaximumX); iy++) {
            var VerticalLine = new WebGL.Geometry.Line();
            VerticalLine.FirstPoint = new WebGL.Vector2(iy, FloatingMath.MinimumInteger(line.MinimumY - 1));
            VerticalLine.LastPoint = new WebGL.Vector2(iy, FloatingMath.MaximumInteger(line.MaximumY + 1));
            var lci = VerticalLine.IntersectionWithLineSegment(line.FirstPointLocation.Xy, line.LastPointLocation.Xy);
            if (lci != null) {
                Intersections.push(lci);
            }
        }
        var ix;
        for (ix = FloatingMath.MinimumInteger(line.MinimumY) + 1; ix < FloatingMath.MaximumInteger(line.MaximumY); ix++) {
            var HorizontalLine = new WebGL.Geometry.Line();
            HorizontalLine.FirstPoint = new WebGL.Vector2(FloatingMath.MinimumInteger(line.MinimumX - 1), ix);
            HorizontalLine.LastPoint = new WebGL.Vector2(FloatingMath.MaximumInteger(line.MaximumX + 1), ix);
            var lci = HorizontalLine.IntersectionWithLineSegment(line.FirstPointLocation.Xy, line.LastPointLocation.Xy);
            if (lci != null) {
                Intersections.push(lci);
            }
        }
        var PointsIntersectionsCloud = new PointsCloud();
        PointsIntersectionsCloud.PointsColor = WebGL.Vector4Colors.Red;
        PointsIntersectionsCloud.AddPoints(line.ColorKey, WebGL.Vectors2ToVectors3(Intersections));
        this.ShapesCoordinatesIntersections[line.ColorKey] = PointsIntersectionsCloud;
    };
    DrawingLayer.prototype.DetermineLineIntersectionsAndTerminals = function (line) {
        var _this = this;
        var pcloud = new PointsCloud();
        pcloud.PointsColor = WebGL.Vector4Colors.Black;
        this.ProcessedShapes.forEach(function (shape) {
            var iis = line.GetIntersectionsWith(shape);
            var psc = _this.ShapesIntersections[shape.ColorKey];
            iis.forEach(function (ii) {
                psc.AddPoint(line.ColorKey, ii.ToVector3());
                pcloud.AddPoint(shape.ColorKey, ii.ToVector3());
            });
        });
        this.ShapesIntersections[line.ColorKey] = pcloud;
        var Terminals = new PointsCloud();
        Terminals.PointsColor = WebGL.Vector4Colors.DarkGreen;
        Terminals.AddPoint(line.ColorKey, line.FirstPointLocation);
        Terminals.AddPoint(line.ColorKey, line.LastPointLocation);
        this.ShapesTerminals[line.ColorKey] = Terminals;
    };
    DrawingLayer.prototype.GetHorizontalRay = function (point) {
        var HLine = new WebGL.Geometry.Line();
        HLine.FirstPoint = new WebGL.Vector2(-1000, point.Y);
        HLine.LastPoint = new WebGL.Vector2(1000, point.Y);
        return HLine;
    };
    DrawingLayer.prototype.GetVerticalRay = function (point) {
        var VLine = new WebGL.Geometry.Line();
        VLine.FirstPoint = new WebGL.Vector2(point.X, -1000);
        VLine.LastPoint = new WebGL.Vector2(point.X, 1000);
        return VLine;
    };
    DrawingLayer.prototype.DetermineOtherShapesProjectionsOnMe = function (sourceShape) {
        var ix;
        var psc = new PointsCloud();
        psc.PointsColor = WebGL.Vector4Colors.GoldenRod;
        for (ix = 0; ix < this.ProcessedShapes.length; ix++) {
            var shape = this.ProcessedShapes[ix];
            var ipoints = this.GetShapeIntersectionsPointsCloud(shape);
            var tpoints = this.GetShapeTerminalsPointsCloud(shape);
            var allPoints = new Array();
            if (ipoints != null)
                ipoints.PointsLocations.forEach(function (p) { return allPoints.push(new WebGL.Vector2(p.X, p.Y)); });
            if (tpoints != null)
                tpoints.PointsLocations.forEach(function (p) { return allPoints.push(new WebGL.Vector2(p.X, p.Y)); });
            var iy;
            for (iy = 0; iy < allPoints.length; iy++) {
                var point = allPoints[iy];
                var VLine = this.GetVerticalRay(point);
                var HLine = this.GetHorizontalRay(point);
                var vs = sourceShape.GetIntersectionsWithRay(VLine.FirstPoint, VLine.LastPoint);
                vs.forEach(function (p) { return psc.AddPoint(shape.ColorKey, p.ToVector3()); });
                var hs = sourceShape.GetIntersectionsWithRay(HLine.FirstPoint, HLine.LastPoint);
                hs.forEach(function (p) { return psc.AddPoint(shape.ColorKey, p.ToVector3()); });
            }
        }
        this.ShapesProjections[sourceShape.ColorKey] = psc;
    };
    DrawingLayer.prototype.ProjectMyTerminalsOnOtherShapes = function (sourceShape) {
        var MyTerminalPoints = sourceShape.TerminalPoints;
        var isc = this.GetShapeIntersectionsPointsCloud(sourceShape);
        isc.PointsLocations.forEach(function (p) { return MyTerminalPoints.push(p.Xy); });
        var ix;
        for (ix = 0; ix < this.ProcessedShapes.length; ix++) {
            var shape = this.ProcessedShapes[ix];
            var psc = this.GetShapeProjectionsPointsCloud(shape);
            var iy;
            for (iy = 0; iy < MyTerminalPoints.length; iy++) {
                var point = MyTerminalPoints[iy];
                var VLine = this.GetVerticalRay(point);
                var HLine = this.GetHorizontalRay(point);
                var vs = shape.GetIntersectionsWithRay(VLine.FirstPoint, VLine.LastPoint);
                vs.forEach(function (p) { return psc.AddPoint(sourceShape.ColorKey, p.ToVector3()); });
                var hs = shape.GetIntersectionsWithRay(HLine.FirstPoint, HLine.LastPoint);
                hs.forEach(function (p) { return psc.AddPoint(sourceShape.ColorKey, p.ToVector3()); });
            }
        }
    };
    DrawingLayer.prototype.FindArcIntersectionsWithCoordinateLines = function (arc) {
        var Intersections = new Array();
        var iy;
        for (iy = FloatingMath.MinimumInteger(arc.MinimumX) + 1; iy < FloatingMath.MaximumInteger(arc.MaximumX); iy++) {
            var VerticalLine = new WebGL.Geometry.Line();
            VerticalLine.FirstPoint = new WebGL.Vector2(iy, FloatingMath.MinimumInteger(arc.MinimumY - 1));
            VerticalLine.LastPoint = new WebGL.Vector2(iy, FloatingMath.MaximumInteger(arc.MaximumY + 1));
            var ci = arc.IntersectionWithRay(VerticalLine.FirstPoint, VerticalLine.LastPoint);
            if (ci.FirstPoint != null && arc.IsPointOnArc(ci.FirstPoint) != null) {
                Intersections.push(ci.FirstPoint);
            }
            if (ci.SecondPoint != null && arc.IsPointOnArc(ci.SecondPoint) != null) {
                Intersections.push(ci.SecondPoint);
            }
        }
        var ix;
        for (ix = FloatingMath.MinimumInteger(arc.MinimumY) + 1; ix < FloatingMath.MaximumInteger(arc.MaximumY); ix++) {
            var HorizontalLine = new WebGL.Geometry.Line();
            HorizontalLine.FirstPoint = new WebGL.Vector2(FloatingMath.MinimumInteger(arc.MinimumX - 1), ix);
            HorizontalLine.LastPoint = new WebGL.Vector2(FloatingMath.MaximumInteger(arc.MaximumX + 1), ix);
            var ci = arc.IntersectionWithRay(HorizontalLine.FirstPoint, HorizontalLine.LastPoint);
            if (ci.FirstPoint != null && arc.IsPointOnArc(ci.FirstPoint) != null) {
                Intersections.push(ci.FirstPoint);
            }
            if (ci.SecondPoint != null && arc.IsPointOnArc(ci.SecondPoint) != null) {
                Intersections.push(ci.SecondPoint);
            }
        }
        var PointsIntersectionsCloud = new PointsCloud();
        PointsIntersectionsCloud.PointsColor = WebGL.Vector4Colors.Red;
        PointsIntersectionsCloud.AddPoints(arc.ColorKey, WebGL.Vectors2ToVectors3(Intersections));
        this.ShapesCoordinatesIntersections[arc.ColorKey] = PointsIntersectionsCloud;
    };
    DrawingLayer.prototype.DetermineArcIntersectionsAndTerminals = function (arc) {
        var _this = this;
        var pcloud = new PointsCloud();
        pcloud.PointsColor = WebGL.Vector4Colors.Black;
        this.ProcessedShapes.forEach(function (shape) {
            var iis = arc.GetIntersectionsWith(shape);
            var psc = _this.ShapesIntersections[shape.ColorKey];
            iis.forEach(function (ii) {
                if (psc != undefined) {
                    psc.AddPoint(arc.ColorKey, ii.ToVector3());
                    pcloud.AddPoint(shape.ColorKey, ii.ToVector3());
                }
            });
        });
        this.ShapesIntersections[arc.ColorKey] = pcloud;
        var Terminals = new PointsCloud();
        Terminals.PointsColor = WebGL.Vector4Colors.DarkGreen;
        Terminals.AddPoint(arc.ColorKey, arc.FirstPointLocation);
        Terminals.AddPoint(arc.ColorKey, arc.LastPointLocation);
        this.ShapesTerminals[arc.ColorKey] = Terminals;
    };
    DrawingLayer.prototype.FindCircleIntersectionsWithCoordinateLines = function (circle) {
        var Intersections = new Array();
        var iy;
        for (iy = FloatingMath.MinimumInteger(circle.MinimumX) + 1; iy < FloatingMath.MaximumInteger(circle.MaximumX); iy++) {
            var VerticalLine = new WebGL.Geometry.Line();
            VerticalLine.FirstPoint = new WebGL.Vector2(iy, FloatingMath.MinimumInteger(circle.MinimumY - 1));
            VerticalLine.LastPoint = new WebGL.Vector2(iy, FloatingMath.MaximumInteger(circle.MaximumY + 1));
            var ci = circle.IntersectionWithRay(VerticalLine.FirstPoint, VerticalLine.LastPoint);
            if (ci.FirstPoint != null) {
                Intersections.push(ci.FirstPoint);
            }
            if (ci.SecondPoint != null) {
                Intersections.push(ci.SecondPoint);
            }
        }
        var ix;
        for (ix = FloatingMath.MinimumInteger(circle.MinimumY) + 1; ix < FloatingMath.MaximumInteger(circle.MaximumY); ix++) {
            var HorizontalLine = new WebGL.Geometry.Line();
            HorizontalLine.FirstPoint = new WebGL.Vector2(FloatingMath.MinimumInteger(circle.MinimumX - 1), ix);
            HorizontalLine.LastPoint = new WebGL.Vector2(FloatingMath.MaximumInteger(circle.MaximumX + 1), ix);
            var ci = circle.IntersectionWithRay(HorizontalLine.FirstPoint, HorizontalLine.LastPoint);
            if (ci.FirstPoint != null) {
                Intersections.push(ci.FirstPoint);
            }
            if (ci.SecondPoint != null) {
                Intersections.push(ci.SecondPoint);
            }
        }
        var PointsIntersectionsCloud = new PointsCloud();
        PointsIntersectionsCloud.PointsColor = WebGL.Vector4Colors.Red;
        PointsIntersectionsCloud.AddPoints(circle.ColorKey, WebGL.Vectors2ToVectors3(Intersections));
        this.ShapesCoordinatesIntersections[circle.ColorKey] = PointsIntersectionsCloud;
    };
    DrawingLayer.prototype.DetermineCircleIntersectionsAndTerminals = function (circle) {
        var _this = this;
        var pcloud = new PointsCloud();
        pcloud.PointsColor = WebGL.Vector4Colors.Black;
        this.ProcessedShapes.forEach(function (shape) {
            var iis = circle.GetIntersectionsWith(shape);
            var psc = _this.ShapesIntersections[shape.ColorKey];
            iis.forEach(function (ii) {
                if (psc != undefined) {
                    psc.AddPoint(circle.ColorKey, ii.ToVector3());
                    pcloud.AddPoint(shape.ColorKey, ii.ToVector3());
                }
            });
        });
        this.ShapesIntersections[circle.ColorKey] = pcloud;
        var Terminals = new PointsCloud();
        Terminals.PointsColor = WebGL.Vector4Colors.DarkGreen;
        Terminals.AddPoint(circle.ColorKey, circle.TopPoint.ToVector3());
        Terminals.AddPoint(circle.ColorKey, circle.RightPoint.ToVector3());
        Terminals.AddPoint(circle.ColorKey, circle.BottomPoint.ToVector3());
        Terminals.AddPoint(circle.ColorKey, circle.LeftPoint.ToVector3());
        Terminals.AddPoint(circle.ColorKey, circle.CenterPointLocation);
        this.ShapesTerminals[circle.ColorKey] = Terminals;
    };
    DrawingLayer.prototype.FindRectangleIntersectionsWithCoordinateLines = function (rectangle) {
        var Intersections = new Array();
        var iy;
        for (iy = FloatingMath.MinimumInteger(rectangle.MinimumX) + 1; iy < FloatingMath.MaximumInteger(rectangle.MaximumX); iy++) {
            var VerticalLine = new WebGL.Geometry.Line();
            VerticalLine.FirstPoint = new WebGL.Vector2(iy, FloatingMath.MinimumInteger(rectangle.MinimumY - 1));
            VerticalLine.LastPoint = new WebGL.Vector2(iy, FloatingMath.MaximumInteger(rectangle.MaximumY + 1));
            var ci = rectangle.IntersectionWithLineSegment(VerticalLine.FirstPoint, VerticalLine.LastPoint);
            ci.forEach(function (p) { return Intersections.push(p); });
        }
        var ix;
        for (ix = FloatingMath.MinimumInteger(rectangle.MinimumY) + 1; ix < FloatingMath.MaximumInteger(rectangle.MaximumY); ix++) {
            var HorizontalLine = new WebGL.Geometry.Line();
            HorizontalLine.FirstPoint = new WebGL.Vector2(FloatingMath.MinimumInteger(rectangle.MinimumX - 1), ix);
            HorizontalLine.LastPoint = new WebGL.Vector2(FloatingMath.MaximumInteger(rectangle.MaximumX + 1), ix);
            var ci = rectangle.IntersectionWithLineSegment(HorizontalLine.FirstPoint, HorizontalLine.LastPoint);
            ci.forEach(function (p) { return Intersections.push(p); });
        }
        var PointsIntersectionsCloud = new PointsCloud();
        PointsIntersectionsCloud.PointsColor = WebGL.Vector4Colors.Red;
        PointsIntersectionsCloud.AddPoints(rectangle.ColorKey, WebGL.Vectors2ToVectors3(Intersections));
        this.ShapesCoordinatesIntersections[rectangle.ColorKey] = PointsIntersectionsCloud;
    };
    DrawingLayer.prototype.DetermineRectangleIntersectionsAndTerminals = function (rectangle) {
        var _this = this;
        var pcloud = new PointsCloud();
        pcloud.PointsColor = WebGL.Vector4Colors.Black;
        this.ProcessedShapes.forEach(function (shape) {
            var iis = rectangle.GetIntersectionsWith(shape);
            var psc = _this.ShapesIntersections[shape.ColorKey];
            iis.forEach(function (ii) {
                if (psc != undefined) {
                    psc.AddPoint(rectangle.ColorKey, ii.ToVector3());
                    pcloud.AddPoint(shape.ColorKey, ii.ToVector3());
                }
            });
        });
        this.ShapesIntersections[rectangle.ColorKey] = pcloud;
        var Terminals = new PointsCloud();
        Terminals.PointsColor = WebGL.Vector4Colors.DarkGreen;
        Terminals.AddPoint(rectangle.ColorKey, rectangle.TopLeftPointLocation);
        Terminals.AddPoint(rectangle.ColorKey, rectangle.TopRightPointLocation);
        Terminals.AddPoint(rectangle.ColorKey, rectangle.BottomRightPointLocation);
        Terminals.AddPoint(rectangle.ColorKey, rectangle.BottomLeftPointLocation);
        this.ShapesTerminals[rectangle.ColorKey] = Terminals;
    };
    DrawingLayer.prototype.ProcessShape = function (shape, existing) {
        if (shape instanceof Shapes.R2Line) {
            this.FindLineIntersectionsWithCoordinateLines(shape);
            this.DetermineLineIntersectionsAndTerminals(shape);
        }
        else if (shape instanceof Shapes.R2Arc) {
            this.FindArcIntersectionsWithCoordinateLines(shape);
            this.DetermineArcIntersectionsAndTerminals(shape);
        }
        else if (shape instanceof Shapes.R2Circle) {
            this.FindCircleIntersectionsWithCoordinateLines(shape);
            this.DetermineCircleIntersectionsAndTerminals(shape);
        }
        else if (shape instanceof Shapes.R2Rectangle) {
            this.FindRectangleIntersectionsWithCoordinateLines(shape);
            this.DetermineRectangleIntersectionsAndTerminals(shape);
        }
        else {
            return;
        }
        this.DetermineOtherShapesProjectionsOnMe(shape);
        this.ProjectMyTerminalsOnOtherShapes(shape);
        if (!existing) {
            this.ProcessedShapes.push(shape);
        }
    };
    DrawingLayer.prototype.ModifyProcessedShape = function (shape) {
        this.RemoveShapeInvolvedPointsInTheOtherShapes(shape);
        this.ProcessShape(shape, true);
    };
    DrawingLayer.prototype.GetShapeCoordinatesIntersectionsPointsCloud = function (shape) {
        if (this.ShapesCoordinatesIntersections.hasOwnProperty(shape.ColorKey)) {
            var psc = this.ShapesCoordinatesIntersections[shape.ColorKey];
            return psc;
        }
        return null;
    };
    DrawingLayer.prototype.GetShapeIntersectionsPointsCloud = function (shape) {
        if (this.ShapesIntersections.hasOwnProperty(shape.ColorKey)) {
            var psc = this.ShapesIntersections[shape.ColorKey];
            return psc;
        }
        return null;
    };
    DrawingLayer.prototype.GetShapeTerminalsPointsCloud = function (shape) {
        if (this.ShapesTerminals.hasOwnProperty(shape.ColorKey)) {
            var psc = this.ShapesTerminals[shape.ColorKey];
            return psc;
        }
        return null;
    };
    DrawingLayer.prototype.GetShapeProjectionsPointsCloud = function (shape) {
        if (this.ShapesProjections.hasOwnProperty(shape.ColorKey)) {
            var psc = this.ShapesProjections[shape.ColorKey];
            return psc;
        }
        return null;
    };
    DrawingLayer.prototype.RemoveShapeInvolvedPointsInTheOtherShapes = function (sourceShape) {
        var ix = 0;
        for (ix = 0; ix < this.ProcessedShapes.length; ix++) {
            var shape = this.ProcessedShapes[ix];
            var pcloud = this.ShapesIntersections[shape.ColorKey];
            if (pcloud != null)
                pcloud.RemovePointsByKey(sourceShape.ColorKey);
            var rcloud = this.ShapesProjections[shape.ColorKey];
            if (rcloud != null)
                rcloud.RemovePointsByKey(sourceShape.ColorKey);
        }
    };
    DrawingLayer.prototype.RemoveProcessedShape = function (shape) {
        delete this.ShapesCoordinatesIntersections[shape.ColorKey];
        delete this.ShapesIntersections[shape.ColorKey];
        delete this.ShapesTerminals[shape.ColorKey];
        delete this.ShapesProjections[shape.ColorKey];
        this.RemoveShapeInvolvedPointsInTheOtherShapes(shape);
        var ix = 0;
        for (ix = this.ProcessedShapes.length - 1; ix >= 0; ix--) {
            if (this.ProcessedShapes[ix].ColorKey == shape.ColorKey) {
                this.ProcessedShapes.splice(ix, 1);
                break;
            }
        }
    };
    DrawingLayer.prototype.GetAllIntersectionsAndTerminals = function () {
        var allIntersectionsAndTerminals = new Array();
        var ix;
        for (ix = 0; ix < this.ProcessedShapes.length; ix++) {
            var shape = this.ProcessedShapes[ix];
            var isc = this.GetShapeIntersectionsPointsCloud(shape);
            isc.PointsLocations.forEach(function (p) { return allIntersectionsAndTerminals.push(p.Xy); });
            var its = this.GetShapeTerminalsPointsCloud(shape);
            its.PointsLocations.forEach(function (p) { return allIntersectionsAndTerminals.push(p.Xy); });
        }
        return allIntersectionsAndTerminals;
    };
    DrawingLayer.prototype.GetIntersectionsSegmentsLayer = function () {
        var slayer = new DrawingLayer();
        this.DetermineShapesIntersections();
        this.DrawingShapes.forEach(function (shape) {
            shape.ApplySegmentation();
            shape.InnerSegments.forEach(function (seg) { return slayer.AddShape(seg); });
        });
        return slayer;
    };
    DrawingLayer.prototype.GetIntersectionsSegmentsLayerExcludingSpecialShapes = function () {
        var slayer = new DrawingLayer();
        var specialLines = new Array();
        var lls = this.AllLines;
        lls.forEach(function (x) {
            if (x.BuildingType == Shapes.BuildingType.Door
                || x.BuildingType == Shapes.BuildingType.Window
                || x.BuildingType == Shapes.BuildingType.Opening
                || x.IsDimension == true) {
            }
            else {
                slayer.AddShape(x);
            }
        });
        return slayer.GetIntersectionsSegmentsLayer();
    };
    DrawingLayer.SpecifyDuplicateLines = function (lines) {
        var RemovalLines = new Array();
        for (var ix = 0; ix < lines.length; ix++) {
            var sourceLine = lines[ix];
            for (var iy = ix; iy < lines.length; iy++) {
                if (ix != iy) {
                    var targetLine = lines[iy];
                    if (sourceLine.FirstPointLocation.NearlyEqual(targetLine.FirstPointLocation)
                        && sourceLine.LastPointLocation.NearlyEqual(targetLine.LastPointLocation)) {
                        RemovalLines.push(targetLine);
                    }
                    if (sourceLine.FirstPointLocation.NearlyEqual(targetLine.LastPointLocation)
                        && sourceLine.LastPointLocation.NearlyEqual(targetLine.FirstPointLocation)) {
                        RemovalLines.push(targetLine);
                    }
                }
            }
        }
        return RemovalLines;
    };
    Object.defineProperty(DrawingLayer.prototype, "AllLines", {
        get: function () {
            var lines = new Array();
            this._DrawingShapes.forEach(function (x) {
                if (x instanceof Shapes.R2Line) {
                    var line = x;
                    lines.push(line);
                }
            });
            return lines;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "AllWorkingLines", {
        get: function () {
            var lines = new Array();
            var alll = this.AllLines;
            alll.forEach(function (l) {
                if (l.IsDimension == false && (l.BuildingType == Shapes.BuildingType.Wall || l.BuildingType == Shapes.BuildingType.Undefined))
                    lines.push(l);
            });
            return lines;
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.prototype.RemoveDuplicateLines = function () {
        var _this = this;
        var lines = this.AllLines;
        var RemovalLines = DrawingLayer.SpecifyDuplicateLines(lines);
        RemovalLines.forEach(function (x) { return _this.RemoveShape(_this.GetShapeIndex(x)); });
        return RemovalLines.length;
    };
    DrawingLayer.prototype.RemoveZeroLengthLines = function () {
        var _this = this;
        var lines = this.AllLines;
        var RemovalLines = new Array();
        for (var ix = 0; ix < lines.length; ix++) {
            var sourceLine = lines[ix];
            if (FloatingMath.NearlyEqual(sourceLine.Length, 0)) {
                RemovalLines.push(sourceLine);
            }
        }
        RemovalLines.forEach(function (x) {
            x.FirstPoint.RemoveOwner(x);
            x.LastPoint.RemoveOwner(x);
        });
        RemovalLines.forEach(function (x) { return _this.RemoveShape(_this.GetShapeIndex(x)); });
        return RemovalLines.length;
    };
    Object.defineProperty(DrawingLayer.prototype, "Name", {
        get: function () {
            return this._Name;
        },
        set: function (value) {
            this._Name = value;
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.prototype.ToCdtXElement = function () {
        var layer = "  <Layer>";
        layer = layer.concat("\n", "<Name>" + this.Name + "</Name>");
        var ix;
        for (ix = 0; ix < this.ShapesCount; ix++) {
            layer = layer.concat("\n");
            layer = layer.concat(this._DrawingShapes[ix].ToCdtXElement());
        }
        layer = layer.concat("\n  </Layer>");
        return layer;
    };
    DrawingLayer.prototype.ToSliceArrayXElement = function () {
        var layer = "<?xml version=\"1.0\"?>";
        layer = layer.concat("\n", "<ArrayOfSlice xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
        var ix;
        for (ix = 0; ix < this.ShapesCount; ix++) {
            layer = layer.concat("\n");
            layer = layer.concat(this._DrawingShapes[ix].ToSliceXElement());
        }
        layer = layer.concat("\n  </ArrayOfSlice>");
        return layer;
    };
    DrawingLayer.prototype.ClearCache = function () {
        this.BoundaryLines = null;
        this._MaximumX = null;
        this._MinimumX = null;
        this._MaximumY = null;
        this._MinimumY = null;
        this._WholePointsLocations = null;
        this.ShouldRefreshLayers = true;
        this._AveragePoint = null;
    };
    Object.defineProperty(DrawingLayer.prototype, "MaximumX", {
        get: function () {
            if (this.ShapesCount == 0)
                return 0;
            if (this._MaximumX == null) {
                var arr = new Array();
                this._DrawingShapes.forEach(function (x) { return arr.push(x.MaximumX); });
                this._MaximumX = Math.max.apply(null, arr);
            }
            return this._MaximumX;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "MinimumX", {
        get: function () {
            if (this.ShapesCount == 0)
                return 0;
            if (this._MinimumX == null) {
                var arr = new Array();
                this._DrawingShapes.forEach(function (x) { return arr.push(x.MinimumX); });
                this._MinimumX = Math.min.apply(null, arr);
            }
            return this._MinimumX;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "MaximumY", {
        get: function () {
            if (this.ShapesCount == 0)
                return 0;
            if (this._MaximumY == null) {
                var arr = new Array();
                this._DrawingShapes.forEach(function (x) { return arr.push(x.MaximumY); });
                this._MaximumY = Math.max.apply(null, arr);
            }
            return this._MaximumY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "MinimumY", {
        get: function () {
            if (this.ShapesCount == 0)
                return 0;
            if (this._MinimumY == null) {
                var arr = new Array();
                this._DrawingShapes.forEach(function (x) { return arr.push(x.MinimumY); });
                this._MinimumY = Math.min.apply(null, arr);
            }
            return this._MinimumY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "Width", {
        get: function () {
            if (this.MaximumX < 0 && this.MinimumX < 0) {
                return Math.abs(this.MinimumX) - Math.abs(this.MaximumX);
            }
            else if (this.MaximumX > 0 && this.MinimumX < 0) {
                return this.MaximumX - this.MinimumX;
            }
            else {
                return this.MaximumX - this.MinimumX;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "Height", {
        get: function () {
            if (this.MaximumY < 0 && this.MinimumY < 0) {
                return Math.abs(this.MinimumY) - Math.abs(this.MaximumY);
            }
            else if (this.MaximumY > 0 && this.MinimumY < 0) {
                return this.MaximumY - this.MinimumY;
            }
            else {
                return this.MaximumY - this.MinimumY;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "Center", {
        get: function () {
            var x = this.MinimumX + this.Width / 2;
            var y = this.MinimumY + this.Height / 2;
            return new WebGL.Vector2(x, y);
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.prototype.IsPointInsideBoundary = function (point) {
        var xm = point.X;
        var ym = point.Y;
        if ((xm > this.Left && xm < this.Right) && (ym > this.Bottom && ym < this.Top))
            return true;
        else
            return false;
    };
    DrawingLayer.prototype.LinkConnectedLines = function () {
        var lines = this.AllWorkingLines;
        var counter = 0;
        for (var ix = 0; ix < lines.length; ix++) {
            var sourceLine = lines[ix];
            for (var iy = 0; iy < lines.length; iy++) {
                var targetLine = lines[iy];
                if (ix != iy) {
                    if (!sourceLine.LastPoint.Equals(targetLine.FirstPoint)) {
                        if (sourceLine.LastPointLocation.NearlyEqual(targetLine.FirstPointLocation)) {
                            targetLine.FirstPoint = sourceLine.LastPoint;
                            counter++;
                        }
                    }
                    if (!sourceLine.FirstPoint.Equals(targetLine.FirstPoint)) {
                        if (sourceLine.FirstPointLocation.NearlyEqual(targetLine.FirstPointLocation)) {
                            targetLine.FirstPoint = sourceLine.FirstPoint;
                            counter++;
                        }
                    }
                    if (!sourceLine.LastPoint.Equals(targetLine.LastPoint)) {
                        if (sourceLine.LastPointLocation.NearlyEqual(targetLine.LastPointLocation)) {
                            targetLine.LastPoint = sourceLine.LastPoint;
                            counter++;
                        }
                    }
                }
            }
        }
        return counter;
    };
    DrawingLayer.prototype.GetAllShapesPoints = function () {
        var points = new Array();
        var allw = this.DrawingShapes;
        var PointExists = function (point) {
            for (var ix = 0; ix < points.length; ix++) {
                var p = points[ix];
                if (point.Equals(p)) {
                    return true;
                }
            }
            return false;
        };
        allw.forEach(function (sh) {
            if (sh instanceof Shapes.R2Line) {
                var line = sh;
                if (PointExists(line.FirstPoint) == false) {
                    points.push(line.FirstPoint);
                }
                if (PointExists(line.LastPoint) == false) {
                    points.push(line.LastPoint);
                }
            }
            if (sh instanceof Shapes.R2Arc) {
                var arc = sh;
                if (PointExists(arc.FirstPoint) == false) {
                    points.push(arc.FirstPoint);
                }
                if (PointExists(arc.MiddlePoint) == false) {
                    points.push(arc.MiddlePoint);
                }
                if (PointExists(arc.LastPoint) == false) {
                    points.push(arc.LastPoint);
                }
            }
        });
        return points;
    };
    DrawingLayer.prototype.GetAllLinesPoints = function () {
        var points = new Array();
        var allw = this.AllWorkingLines;
        allw.forEach(function (line) {
            var fp = false;
            var lp = false;
            for (var ix = 0; ix < points.length; ix++) {
                var p = points[ix];
                if (line.FirstPoint.Equals(p)) {
                    fp = true;
                    break;
                }
            }
            if (fp == false)
                points.push(line.FirstPoint);
            for (var ix = 0; ix < points.length; ix++) {
                var p = points[ix];
                if (line.LastPoint.Equals(p)) {
                    lp = true;
                    break;
                }
            }
            if (lp == false)
                points.push(line.LastPoint);
        });
        return points;
    };
    Object.defineProperty(DrawingLayer.prototype, "AveragePoint", {
        get: function () {
            if (this._AveragePoint == null) {
                var points = this.GetAllLinesPoints();
                var pc = new PointsCloud();
                points.forEach(function (p) { return pc.AddPoint("", p.ShapeCenter); });
                var avgp = pc.AveragePoint;
                this._AveragePoint = avgp.Xy;
            }
            return this._AveragePoint;
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.prototype.GetFarthestTopRightPoint = function () {
        var points = this.GetAllLinesPoints();
        var pc = new PointsCloud();
        points.forEach(function (p) { return pc.AddPoint("", p.ShapeCenter); });
        var avgp = pc.AveragePoint;
        var rightPoints = new Array();
        points.forEach(function (p) {
            if (p.ShapeCenter.X > avgp.X)
                rightPoints.push(p);
        });
        var rightTopPoints = new Array();
        rightPoints.forEach(function (p) {
            if (p.ShapeCenter.Y > avgp.Y) {
                var vp = WebGL.Vector3.Subtract(p.ShapeCenter, avgp);
                rightTopPoints.push([p, vp.Length]);
            }
        });
        rightTopPoints.sort(function (a, b) { return b[1] - a[1]; });
        return rightTopPoints[0][0];
    };
    DrawingLayer.prototype.GetLinesOutFromPoint = function (point) {
        var ll = new Array();
        var allw = this.AllWorkingLines;
        allw.forEach(function (l) {
            if (l.FirstPoint.Equals(point))
                ll.push(l);
            if (l.LastPoint.Equals(point))
                ll.push(l);
        });
        return ll;
    };
    DrawingLayer.prototype.DiscoverExteriorPolygonFrom = function (point) {
        var maxLinesLimit = this.AllWorkingLines.length;
        var lines = this.GetLinesOutFromPoint(point);
        if (lines.length == 0)
            return null;
        lines.sort(function (a, b) { return a.GetAngleWithReferencePoint(point).Radians - b.GetAngleWithReferencePoint(point).Radians; });
        var iLine = lines[0];
        var ls = new Shapes.R2LineStrips(point.ShapeCenter.Xy);
        var otherPoint = iLine.GetTheOtherPoint(point);
        var DiscoveredLines = new Array();
        DiscoveredLines.push(iLine);
        ls.AddPoint(otherPoint.ShapeCenter.Xy);
        while (true == true && DiscoveredLines.length <= maxLinesLimit) {
            lines = this.GetLinesOutFromPoint(otherPoint);
            var toDeleteIndex = -1;
            for (var ix = 0; ix < lines.length; ix++) {
                var l = lines[ix];
                for (var iy = 0; iy < DiscoveredLines.length; iy++) {
                    var dl = DiscoveredLines[iy];
                    if (l.Equals(dl)) {
                        toDeleteIndex = ix;
                        break;
                    }
                }
                if (toDeleteIndex >= 0)
                    break;
            }
            if (toDeleteIndex >= 0)
                lines.splice(toDeleteIndex, 1);
            if (lines.length == 0)
                return null;
            if (lines.length > 1) {
                lines.sort(function (a, b) { return iLine.GetRelativeAngleWithReference(b, otherPoint).Radians - iLine.GetRelativeAngleWithReference(a, otherPoint).Radians; });
            }
            iLine = lines[0];
            DiscoveredLines.push(iLine);
            otherPoint = iLine.GetTheOtherPoint(otherPoint);
            ls.AddPoint(otherPoint.ShapeCenter.Xy);
            if (otherPoint.Equals(point)) {
                ls.LastLine.LastPoint = ls.FirstPoint;
                break;
            }
            if (ls._Lines.length > maxLinesLimit) {
                throw ("We entered an infinite loop");
            }
        }
        return ls;
    };
    DrawingLayer.prototype.DiscoverExteriorBoundary = function () {
        if (this.AllWorkingLines.length < 3)
            return null;
        var dl = this.GetIntersectionsSegmentsLayerExcludingSpecialShapes();
        dl.RemoveDuplicateLines();
        dl.RemoveZeroLengthLines();
        var ii = dl.LinkConnectedLines();
        var fp = dl.GetFarthestTopRightPoint();
        var ls = dl.DiscoverExteriorPolygonFrom(fp);
        return ls;
    };
    DrawingLayer.prototype.DiscoverInteriorPolygonFrom = function (point, branch) {
        var maxLinesLimit = this.AllWorkingLines.length;
        var iLine = branch;
        var ls = new Shapes.R2LineStrips(point.ShapeCenter.Xy);
        var otherPoint = iLine.GetTheOtherPoint(point);
        var DiscoveredLines = new Array();
        DiscoveredLines.push(iLine);
        ls.AddPoint(otherPoint.ShapeCenter.Xy);
        var lines = null;
        while (true == true && DiscoveredLines.length <= maxLinesLimit) {
            lines = this.GetLinesOutFromPoint(otherPoint);
            var toDeleteIndex = -1;
            for (var ix = 0; ix < lines.length; ix++) {
                var l = lines[ix];
                for (var iy = 0; iy < DiscoveredLines.length; iy++) {
                    var dl = DiscoveredLines[iy];
                    if (l.Equals(dl)) {
                        toDeleteIndex = ix;
                        break;
                    }
                }
                if (toDeleteIndex >= 0)
                    break;
            }
            if (toDeleteIndex >= 0)
                lines.splice(toDeleteIndex, 1);
            if (lines.length == 0)
                return null;
            if (lines.length > 1) {
                lines.sort(function (a, b) { return iLine.GetRelativeAngleWithReference(b, otherPoint).Radians - iLine.GetRelativeAngleWithReference(a, otherPoint).Radians; });
            }
            iLine = lines[lines.length - 1];
            DiscoveredLines.push(iLine);
            otherPoint = iLine.GetTheOtherPoint(otherPoint);
            ls.AddPoint(otherPoint.ShapeCenter.Xy);
            if (otherPoint.Equals(point)) {
                ls.LastLine.LastPoint = ls.FirstPoint;
                break;
            }
            if (ls._Lines.length > maxLinesLimit) {
                throw ("We entered an infinite loop");
            }
        }
        return ls;
    };
    DrawingLayer.prototype.DiscoverPolygonsOfLinkedLines = function () {
        var points = this.GetAllLinesPoints();
        var DiscoveredPolygons = new Array();
        while (points.length > 0) {
            var point = points.pop();
            var lines = this.GetLinesOutFromPoint(point);
            for (var ix = 0; ix < lines.length; ix++) {
                var branch = lines[ix];
                var ls = this.DiscoverInteriorPolygonFrom(point, branch);
                if (ls != null) {
                    var exists = false;
                    for (var ii = 0; ii < DiscoveredPolygons.length; ii++) {
                        if (DiscoveredPolygons[ii].ArePointsEqualsTo(ls)) {
                            exists = true;
                            break;
                        }
                    }
                    if (exists == false)
                        DiscoveredPolygons.push(ls);
                }
            }
        }
        return DiscoveredPolygons;
    };
    DrawingLayer.prototype.DiscoverAllPolygons = function () {
        if (this.AllWorkingLines.length < 3)
            return null;
        var dl = this.GetIntersectionsSegmentsLayerExcludingSpecialShapes();
        dl.RemoveDuplicateLines();
        dl.RemoveZeroLengthLines();
        var ii = dl.LinkConnectedLines();
        var lss = dl.DiscoverPolygonsOfLinkedLines();
        console.log(lss.length + " Total Polygons");
        return lss;
    };
    DrawingLayer.prototype.DiscoverDiscreetPolygons = function () {
        var allPolygons = this.DiscoverAllPolygons();
        allPolygons.sort(function (a, b) { return b.LinesCount - a.LinesCount; });
        var toberemoved = new Array();
        for (var ix = 0; ix < allPolygons.length; ix++) {
            var big = allPolygons[ix];
            for (var iy = 0; iy < allPolygons.length; iy++) {
                if (ix == iy)
                    continue;
                var smaller = allPolygons[iy];
                var existsinremoved = false;
                for (var ii; ii < toberemoved.length; ii++) {
                    if (toberemoved[ii].Equals(smaller)) {
                        existsinremoved = true;
                        break;
                    }
                }
                if (existsinremoved)
                    continue;
                if (big.ContainsPolygon(smaller)) {
                    toberemoved.push(big);
                }
            }
        }
        for (var iT = 0; iT < toberemoved.length; iT++) {
            var rem = toberemoved[iT];
            for (var il = allPolygons.length - 1; il >= 0; il--) {
                if (allPolygons[il].Equals(rem)) {
                    allPolygons.splice(il, 1);
                    break;
                }
            }
        }
        console.log(allPolygons.length + " Discreet Polygons");
        return allPolygons;
    };
    Object.defineProperty(DrawingLayer.prototype, "JsonObject", {
        get: function () {
            var dlshapes = new Array();
            this.DrawingShapes.forEach(function (sh) {
                dlshapes.push(sh.JsonObject);
            });
            var dl = {
                Name: this.Name,
                Shapes: dlshapes
            };
            return dl;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "Json", {
        get: function () {
            return JSON.stringify(this.JsonObject);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DrawingLayer.prototype, "WholePointsLocations", {
        get: function () {
            if (this._WholePointsLocations == null) {
                this._WholePointsLocations = new Array();
                var visishapes = this.DrawingShapes;
                var ix = 0;
                for (ix = 0; ix < visishapes.length; ix++) {
                    var sh = visishapes[ix];
                    if (sh instanceof Shapes.R2Line || sh instanceof Shapes.R2Arc) {
                        this._WholePointsLocations.push(sh.FirstPointLocation);
                        this._WholePointsLocations.push(sh.LastPointLocation);
                    }
                }
            }
            return this._WholePointsLocations;
        },
        enumerable: true,
        configurable: true
    });
    DrawingLayer.LastId = 0;
    return DrawingLayer;
}());
var Point2Descriptor = (function () {
    function Point2Descriptor() {
    }
    Object.defineProperty(Point2Descriptor.prototype, "Vector2", {
        get: function () {
            return new WebGL.Vector2(this.X, this.Y);
        },
        enumerable: true,
        configurable: true
    });
    return Point2Descriptor;
}());
var CloudPoint = (function () {
    function CloudPoint() {
    }
    return CloudPoint;
}());
var PointsCloud = (function () {
    function PointsCloud() {
        this.PointSize = 6;
        this._PointsLocations = new Array();
        this._Keys = new Array();
        this.PointsColor = new WebGL.Vector4(0.5, 0.1, 1, 1);
    }
    Object.defineProperty(PointsCloud.prototype, "PointsCount", {
        get: function () {
            return this._Keys.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PointsCloud.prototype, "PointsLocations", {
        get: function () {
            return this._PointsLocations;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PointsCloud.prototype, "CloudPoints", {
        get: function () {
            var ix;
            var cps = new Array();
            for (ix = 0; ix < this._Keys.length; ix++) {
                var cp = new CloudPoint();
                cp.Key = this._Keys[ix];
                cp.Location = this._PointsLocations[ix];
                cps.push(cp);
            }
            return cps;
        },
        enumerable: true,
        configurable: true
    });
    PointsCloud.prototype.RemovePointsByKey = function (key) {
        var ix;
        for (ix = this._Keys.length - 1; ix >= 0; ix--) {
            if (this._Keys[ix] == key) {
                this._PointsLocations.splice(ix, 1);
                this._Keys.splice(ix, 1);
            }
        }
    };
    PointsCloud.prototype.AddPoints = function (sourceKey, points) {
        var _this = this;
        points.forEach(function (p) { return _this.AddPoint(sourceKey, p); });
    };
    PointsCloud.prototype.AddPoint = function (sourceKey, point) {
        this._PointsLocations.push(point);
        this._Keys.push(sourceKey);
    };
    PointsCloud.prototype.Render = function (gl) {
        if (!PointsCloud.Program.IsLinked(gl))
            PointsCloud.Program.Link(gl);
        PointsCloud.Program.Uniforms.PointSize = this.PointSize;
        PointsCloud.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
        PointsCloud.Program.Uniforms.PointsColor = this.PointsColor;
        PointsCloud.Program.ArrayVertexSize = 3;
        PointsCloud.Program.ArrayVertexAttributes.Locations = new Float32Array(WebGL.Vectors3ToArray(this._PointsLocations));
        PointsCloud.Program.BeginUse(gl);
        gl.drawArrays(WebGLRenderingContext.POINTS, 0, this._PointsLocations.length);
        PointsCloud.Program.EndUse(gl);
    };
    PointsCloud.CompareLengths = function (a, b) {
        if (a.Length < b.Length) {
            return -1;
        }
        if (a.Length > b.Length) {
            return 1;
        }
        return 0;
    };
    PointsCloud.prototype.GetClosestPointTo = function (centerLocation) {
        var additionalPoints = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additionalPoints[_i - 1] = arguments[_i];
        }
        var Points = new Array();
        this.PointsLocations.forEach(function (x) {
            var pd = new Point2Descriptor();
            pd.X = x.X;
            pd.Y = x.Y;
            pd.Length = WebGL.Vector3.Subtract(x, centerLocation.ToVector3()).Length;
            Points.push(pd);
        });
        additionalPoints.forEach(function (x) {
            var pd = new Point2Descriptor();
            pd.X = x.X;
            pd.Y = x.Y;
            pd.Length = WebGL.Vector3.Subtract(x.ToVector3(), centerLocation.ToVector3()).Length;
            Points.push(pd);
        });
        Points.sort(PointsCloud.CompareLengths);
        return Points[0].Vector2.ToVector3();
    };
    PointsCloud.prototype.OrderByLength = function (centerLocation) {
        var additionalPoints = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additionalPoints[_i - 1] = arguments[_i];
        }
        var sortfunction = function (a, b) {
            var aPolar = WebGL.Vector2.Subtract(a, centerLocation).ToPolarVector();
            var bPolar = WebGL.Vector2.Subtract(b, centerLocation).ToPolarVector();
            if (aPolar.Radius > bPolar.Radius)
                return 1;
            else if (aPolar.Radius < bPolar.Radius)
                return -1;
            else {
                return 0;
            }
        };
        var Points = new Array();
        this.PointsLocations.forEach(function (x) {
            Points.push(x.Xy);
        });
        additionalPoints.forEach(function (x) {
            Points.push(x);
        });
        Points.sort(sortfunction);
        return Points;
    };
    PointsCloud.prototype.OrderCounterClockWise = function (centerLocation) {
        var additionalPoints = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            additionalPoints[_i - 1] = arguments[_i];
        }
        var sortfunction = function (a, b) {
            var aPolar = WebGL.Vector2.Subtract(a, centerLocation).ToPolarVector();
            var bPolar = WebGL.Vector2.Subtract(b, centerLocation).ToPolarVector();
            if (aPolar.Theta.AbsoluteAngle.Radians > bPolar.Theta.AbsoluteAngle.Radians)
                return 1;
            else if (aPolar.Theta.AbsoluteAngle.Radians < bPolar.Theta.AbsoluteAngle.Radians)
                return -1;
            else {
                if (aPolar.Radius > bPolar.Radius)
                    return 1;
                else if (aPolar.Radius < bPolar.Radius)
                    return -1;
                else {
                    return 0;
                }
            }
        };
        var Points = new Array();
        this.PointsLocations.forEach(function (x) {
            Points.push(x.Xy);
        });
        additionalPoints.forEach(function (x) {
            Points.push(x);
        });
        Points.sort(sortfunction);
        return Points;
    };
    Object.defineProperty(PointsCloud.prototype, "AveragePoint", {
        get: function () {
            var total = WebGL.Vector3.Zero;
            for (var ix = 0; ix < this._PointsLocations.length; ix++) {
                total = WebGL.Vector3.Add(total, this._PointsLocations[ix]);
            }
            return WebGL.Vector3.Divide(total, this._PointsLocations.length);
        },
        enumerable: true,
        configurable: true
    });
    PointsCloud.ProjectPointOn = function (point, lineFirstPoint, lineLastPoint) {
        var a = WebGL.Vector3.Subtract(point, lineFirstPoint);
        var b = WebGL.Vector3.Subtract(lineLastPoint, lineFirstPoint);
        var ab = WebGL.Vector3.Dot(a, b);
        var bb = WebGL.Vector3.Dot(b, b);
        var result = WebGL.Vector3.Add(lineFirstPoint, WebGL.Vector3.Multiply(b, (ab / bb)));
        return result;
    };
    PointsCloud.RotatePoints = function (points, rotationQuaternion) {
        var rotatedPoints = new Array();
        for (var ix = 0; ix < points.length; ix++) {
            var point = points[ix];
            var rotPoint = WebGL.Vector3.TransformByQuaternion(point, rotationQuaternion);
            rotatedPoints.push(rotPoint);
        }
        return rotatedPoints;
    };
    PointsCloud.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 Locations;\
                uniform float PointSize;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * Locations;\
                        gl_PointSize = PointSize;\
                    }\
                } ");
    PointsCloud.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 PointsColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = PointsColor;\
                    }\
                } \
            ");
    PointsCloud.Program = new WebGL.Shaders.PipelineProgram("PointsCloudProgram", PointsCloud.VertexShader, PointsCloud.FragmentShader);
    return PointsCloud;
}());
var DrawingTools;
(function (DrawingTools) {
    var BaseGrid = (function () {
        function BaseGrid() {
            this.MajorPointSize = 4;
            this.MinorPointSize = 1;
            this.MajorUnits = 4;
        }
        Object.defineProperty(BaseGrid.prototype, "HalfWidth", {
            get: function () {
                return this.Width / 2.0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseGrid.prototype, "HalfHeight", {
            get: function () {
                return this.Height / 2.0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseGrid.prototype, "NegativeWidth", {
            get: function () {
                return this.HalfWidth + this.DeltaWidth;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseGrid.prototype, "PositiveWidth", {
            get: function () {
                return this.Width - this.NegativeWidth;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseGrid.prototype, "NegativeHeight", {
            get: function () {
                return this.HalfHeight + this.DeltaHeight;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseGrid.prototype, "PositiveHeight", {
            get: function () {
                return this.Height - this.NegativeHeight;
            },
            enumerable: true,
            configurable: true
        });
        BaseGrid.prototype.VerticallLines = function () {
            var lines = new Array();
            var VW = 0;
            while (VW < this.NegativeWidth) {
                VW = VW + 1;
                var P1 = new WebGL.Vector2(-VW, -this.NegativeHeight);
                var P2 = new WebGL.Vector2(-VW, this.PositiveHeight);
                lines.push(P1);
                lines.push(P2);
            }
            VW = 0;
            while (VW < this.PositiveWidth) {
                VW = VW + 1;
                var P1 = new WebGL.Vector2(VW, -this.NegativeHeight);
                var P2 = new WebGL.Vector2(VW, this.PositiveHeight);
                lines.push(P1);
                lines.push(P2);
            }
            return lines;
        };
        BaseGrid.prototype.HorizontalLines = function () {
            var lines = new Array();
            var VH = 0;
            while (VH < this.NegativeHeight) {
                VH = VH + 1;
                var P1 = new WebGL.Vector2(-this.NegativeWidth, -VH);
                var P2 = new WebGL.Vector2(this.PositiveWidth, -VH);
                lines.push(P1);
                lines.push(P2);
            }
            VH = 0;
            while (VH < this.PositiveHeight) {
                VH = VH + 1;
                var P1 = new WebGL.Vector2(-this.NegativeWidth, VH);
                var P2 = new WebGL.Vector2(this.PositiveWidth, VH);
                lines.push(P1);
                lines.push(P2);
            }
            return lines;
        };
        BaseGrid.prototype.CenterLines = function () {
            var XAxisLineP0 = new WebGL.Vector2(-this.NegativeWidth, 0);
            var XAxisLinePF = new WebGL.Vector2(this.PositiveWidth, 0);
            var YAxisLineP0 = new WebGL.Vector2(0, -this.NegativeHeight);
            var YAxisLinePF = new WebGL.Vector2(0, this.PositiveHeight);
            var lines = new Array();
            lines.push(XAxisLineP0);
            lines.push(XAxisLinePF);
            lines.push(YAxisLineP0);
            lines.push(YAxisLinePF);
            return lines;
        };
        BaseGrid.prototype.MajorVerticallLines = function () {
            var lines = new Array();
            var VW = 0;
            while (VW < this.NegativeWidth) {
                VW = VW + this.MajorUnits;
                var P1 = new WebGL.Vector2(-VW, -this.NegativeHeight);
                var P2 = new WebGL.Vector2(-VW, this.PositiveHeight);
                lines.push(P1);
                lines.push(P2);
            }
            VW = 0;
            while (VW < this.PositiveWidth) {
                VW = VW + this.MajorUnits;
                var P1 = new WebGL.Vector2(VW, -this.NegativeHeight);
                var P2 = new WebGL.Vector2(VW, this.PositiveHeight);
                lines.push(P1);
                lines.push(P2);
            }
            return lines;
        };
        BaseGrid.prototype.MajorHorizontalLines = function () {
            var lines = new Array();
            var VH = 0;
            while (VH < this.NegativeHeight) {
                VH = VH + this.MajorUnits;
                var P1 = new WebGL.Vector2(-this.NegativeWidth, -VH);
                var P2 = new WebGL.Vector2(this.PositiveWidth, -VH);
                lines.push(P1);
                lines.push(P2);
            }
            VH = 0;
            while (VH < this.PositiveHeight) {
                VH = VH + this.MajorUnits;
                var P1 = new WebGL.Vector2(-this.NegativeWidth, VH);
                var P2 = new WebGL.Vector2(this.PositiveWidth, VH);
                lines.push(P1);
                lines.push(P2);
            }
            return lines;
        };
        BaseGrid.prototype.MajorLines = function () {
            var v = this.MajorVerticallLines();
            var h = this.MajorHorizontalLines();
            var lines = new Array();
            v.forEach(function (x) {
                lines.push(x);
            });
            h.forEach(function (x) {
                lines.push(x);
            });
            return lines;
        };
        BaseGrid.prototype.Render = function (gl) {
            if (!BaseGrid.RenderingProgram.IsLinked(gl))
                BaseGrid.RenderingProgram.Link(gl);
            BaseGrid.RenderingProgram.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            BaseGrid.RenderingProgram.ArrayVertexSize = 2;
            BaseGrid.RenderingProgram.Uniforms.DrawingColor = new WebGL.Vector4(211 / 255.0, 211 / 255.0, 211 / 255.0, 1);
            var lines = this.VerticallLines();
            BaseGrid.RenderingProgram.ArrayVertexAttributes.PointPosition = new Float32Array(WebGL.Vectors2ToArray(lines));
            BaseGrid.RenderingProgram.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            BaseGrid.RenderingProgram.EndUse(gl);
            var lines = this.HorizontalLines();
            BaseGrid.RenderingProgram.ArrayVertexAttributes.PointPosition = new Float32Array(WebGL.Vectors2ToArray(lines));
            BaseGrid.RenderingProgram.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            BaseGrid.RenderingProgram.EndUse(gl);
            BaseGrid.RenderingProgram.Uniforms.DrawingColor = new WebGL.Vector4(200 / 255.0, 0 / 255.0, 1, 1);
            var lines = this.MajorLines();
            BaseGrid.RenderingProgram.ArrayVertexAttributes.PointPosition = new Float32Array(WebGL.Vectors2ToArray(lines));
            BaseGrid.RenderingProgram.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            BaseGrid.RenderingProgram.EndUse(gl);
        };
        BaseGrid.prototype.RenderCenterLines = function (gl) {
            BaseGrid.RenderingProgram.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            BaseGrid.RenderingProgram.ArrayVertexSize = 2;
            BaseGrid.RenderingProgram.Uniforms.DrawingColor = new WebGL.Vector4(165 / 255, 42 / 255, 42 / 255, 1);
            var lines = this.CenterLines();
            BaseGrid.RenderingProgram.ArrayVertexAttributes.PointPosition = new Float32Array(WebGL.Vectors2ToArray(lines));
            BaseGrid.RenderingProgram.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            BaseGrid.RenderingProgram.EndUse(gl);
        };
        BaseGrid.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec2 PointPosition;\
                uniform mat4 ProjectionMatrix;\
			    void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * vec4(PointPosition.x, PointPosition.y, 0, 1);\
                    }\
                }\
                ");
        BaseGrid.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 DrawingColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = DrawingColor;\
                    }\
                } \
            ");
        BaseGrid.RenderingProgram = new WebGL.Shaders.PipelineProgram("BaseGridRenderingProgram", BaseGrid.VertexShader, BaseGrid.FragmentShader);
        return BaseGrid;
    }());
    DrawingTools.BaseGrid = BaseGrid;
})(DrawingTools || (DrawingTools = {}));
var EditorCommands;
(function (EditorCommands) {
    EditorCommands[EditorCommands["None"] = 0] = "None";
    EditorCommands[EditorCommands["Drawing"] = 1] = "Drawing";
    EditorCommands[EditorCommands["RegionSelection"] = 2] = "RegionSelection";
    EditorCommands[EditorCommands["RegionDelete"] = 3] = "RegionDelete";
    EditorCommands[EditorCommands["RegionCrop"] = 4] = "RegionCrop";
    EditorCommands[EditorCommands["TotalTranslation"] = 5] = "TotalTranslation";
    EditorCommands[EditorCommands["TotalRotation"] = 6] = "TotalRotation";
})(EditorCommands || (EditorCommands = {}));
var InvariantShapes;
(function (InvariantShapes) {
    var CursorHair = (function () {
        function CursorHair() {
            var _this = this;
            this.ShapeColor = new WebGL.Vector4(1, 0, 0, 1);
            this.WindowPercentage = 2 / 100;
            this.HairCenter = new WebGL.Vector3(0, 0, 0);
            this.CurrentMouseDrawingShape = MouseDrawingShape.Selection;
            this.CurrentSnappingOption = SnappingOption.None;
            this.ProjectionAngle = Angle.Zero;
            this.IsOutBoundary = false;
            this.SelectionHairPoints = new Array();
            this.SelectionHairPoints.push(new WebGL.Vector3(-1, 0, 0));
            this.SelectionHairPoints.push(new WebGL.Vector3(+1, 0, 0));
            this.SelectionHairPoints.push(new WebGL.Vector3(0, -1, 0));
            this.SelectionHairPoints.push(new WebGL.Vector3(0, +1, 0));
            this.LineModeHairPoints = new Array();
            this.SelectionHairPoints.forEach(function (x) { return _this.LineModeHairPoints.push(x); });
            this.LineModeHairPoints.push(new WebGL.Vector3(0.25, 0.25, 0));
            this.LineModeHairPoints.push(new WebGL.Vector3(0.75, 0.75, 0));
            this.RectangleModeHairPoints = new Array();
            this.SelectionHairPoints.forEach(function (x) { return _this.RectangleModeHairPoints.push(x); });
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.25, 0.25, 0));
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.25, 0.75, 0));
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.25, 0.75, 0));
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.75, 0.75, 0));
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.75, 0.75, 0));
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.75, 0.25, 0));
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.75, 0.25, 0));
            this.RectangleModeHairPoints.push(new WebGL.Vector3(0.25, 0.25, 0));
            this.DimensionModeHairPoints = new Array();
            this.SelectionHairPoints.forEach(function (x) { return _this.DimensionModeHairPoints.push(x); });
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.375, 0.5, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.25, 0.375, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.25, 0.375, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.375, 0.5 - 0.25, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.75 - 0.125, 0.25, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.75, 0.375, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.75, 0.375, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.75 - 0.125, 0.5, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.25, 0.375, 0));
            this.DimensionModeHairPoints.push(new WebGL.Vector3(0.75, 0.375, 0));
            this.OutBoundaryPoints = new Array();
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.5, -0.3, 0));
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.5, -0.5, 0));
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.5, -0.5, 0));
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.3, -0.5, 0));
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.5 - 0.1, -0.3 - 0.1, 0));
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.5 - 0.1, -0.5 - 0.1, 0));
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.5 - 0.1, -0.5 - 0.1, 0));
            this.OutBoundaryPoints.push(new WebGL.Vector3(-0.3 - 0.1, -0.5 - 0.1, 0));
            this.PerpendicularSnapPoints = new Array();
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.6, 0.6).ToVector3());
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.6, 0.2).ToVector3());
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.6, 0.2).ToVector3());
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.2, 0.2).ToVector3());
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.6, 0.4).ToVector3());
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.4, 0.4).ToVector3());
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.4, 0.4).ToVector3());
            this.PerpendicularSnapPoints.push(new WebGL.Vector2(-0.4, 0.2).ToVector3());
            this.NearestSnapPoints = new Array();
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.6, 0.6).ToVector3());
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.2, 0.6).ToVector3());
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.2, 0.6).ToVector3());
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.6, 0.2).ToVector3());
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.6, 0.2).ToVector3());
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.2, 0.2).ToVector3());
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.2, 0.2).ToVector3());
            this.NearestSnapPoints.push(new WebGL.Vector2(-0.6, 0.6).ToVector3());
            this.TerminalSnapPoints = new Array();
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.6, 0.6).ToVector3());
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.2, 0.6).ToVector3());
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.2, 0.6).ToVector3());
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.2, 0.2).ToVector3());
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.2, 0.2).ToVector3());
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.6, 0.2).ToVector3());
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.6, 0.2).ToVector3());
            this.TerminalSnapPoints.push(new WebGL.Vector2(-0.6, 0.6).ToVector3());
        }
        Object.defineProperty(CursorHair.prototype, "ProjectionRotationAngleDegrees", {
            get: function () {
                return this.ProjectionAngle.Degrees;
            },
            set: function (value) {
                this.ProjectionAngle = Angle.FromDegrees(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CursorHair.prototype, "RotationQuaternion", {
            get: function () {
                var q = WebGL.Quaternion.FromAxisAngle(WebGL.Vector3.UnitZ, -this.ProjectionAngle.Radians);
                return q;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CursorHair.prototype, "HairPoints", {
            get: function () {
                if (this.CurrentMouseDrawingShape == MouseDrawingShape.Lines || this.CurrentMouseDrawingShape == MouseDrawingShape.PolyLine)
                    return this.LineModeHairPoints;
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.Rectangles || this.CurrentMouseDrawingShape == MouseDrawingShape.RectanglesWithLines)
                    return this.RectangleModeHairPoints;
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.DimensionLines)
                    return this.DimensionModeHairPoints;
                else
                    return this.SelectionHairPoints;
            },
            enumerable: true,
            configurable: true
        });
        CursorHair.prototype.Render = function (gl) {
            if (!CursorHair.Program.IsLinked(gl))
                CursorHair.Program.Link(gl);
            CursorHair.Program.Uniforms.ShapeColor = this.ShapeColor;
            var scaledMatrix = WebGL.Matrix4.CreateScale(this.WindowPercentage * this.ProjectionWidth, this.WindowPercentage * this.ProjectionWidth, 1);
            CursorHair.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            CursorHair.Program.Uniforms.ScaledMatrix = scaledMatrix;
            CursorHair.Program.Uniforms.HairCenter = this.HairCenter;
            CursorHair.Program.ArrayVertexSize = 3;
            CursorHair.Program.ArrayVertexAttributes.position = new Float32Array(WebGL.Vectors3ToArray(PointsCloud.RotatePoints(this.HairPoints, this.RotationQuaternion)));
            CursorHair.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, this.HairPoints.length);
            CursorHair.Program.EndUse(gl);
            if (this.IsOutBoundary) {
                CursorHair.Program.ArrayVertexAttributes.position = new Float32Array(WebGL.Vectors3ToArray(PointsCloud.RotatePoints(this.OutBoundaryPoints, this.RotationQuaternion)));
                CursorHair.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, this.OutBoundaryPoints.length);
                CursorHair.Program.EndUse(gl);
            }
            if (this.CurrentSnappingOption == SnappingOption.Perpendicular) {
                CursorHair.Program.ArrayVertexAttributes.position = new Float32Array(WebGL.Vectors3ToArray(PointsCloud.RotatePoints(this.PerpendicularSnapPoints, this.RotationQuaternion)));
                CursorHair.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, this.PerpendicularSnapPoints.length);
                CursorHair.Program.EndUse(gl);
            }
            if (this.CurrentSnappingOption == SnappingOption.Nearest) {
                CursorHair.Program.ArrayVertexAttributes.position = new Float32Array(WebGL.Vectors3ToArray(PointsCloud.RotatePoints(this.NearestSnapPoints, this.RotationQuaternion)));
                CursorHair.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, this.NearestSnapPoints.length);
                CursorHair.Program.EndUse(gl);
            }
            if (this.CurrentSnappingOption == SnappingOption.Terminals) {
                CursorHair.Program.ArrayVertexAttributes.position = new Float32Array(WebGL.Vectors3ToArray(PointsCloud.RotatePoints(this.TerminalSnapPoints, this.RotationQuaternion)));
                CursorHair.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, this.TerminalSnapPoints.length);
                CursorHair.Program.EndUse(gl);
            }
        };
        CursorHair.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 position;\
                uniform mat4 ProjectionMatrix;\
                uniform mat4 ScaledMatrix;\
                uniform vec3 HairCenter;\
                void main()\
                {\
                    {\
                        vec4 finalPosition = vec4(HairCenter, 0) + (ScaledMatrix * position);\
                        gl_Position = ProjectionMatrix * finalPosition;\
                    }\
                } ");
        CursorHair.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        CursorHair.Program = new WebGL.Shaders.PipelineProgram("CursorHairProgram", CursorHair.VertexShader, CursorHair.FragmentShader);
        return CursorHair;
    }());
    InvariantShapes.CursorHair = CursorHair;
})(InvariantShapes || (InvariantShapes = {}));
var QuadraticRegressionSolver = (function () {
    function QuadraticRegressionSolver(points) {
        this.pointArray = points;
    }
    QuadraticRegressionSolver.prototype.aTerm = function () {
        if (this.pointArray.length < 3)
            throw "Insufficient pairs of co-ordinates";
        var s40 = this.getSx4();
        var s30 = this.getSx3();
        var s20 = this.getSx2();
        var s10 = this.getSx();
        var s00 = this.pointArray.length;
        var s21 = this.getSx2y();
        var s11 = this.getSxy();
        var s01 = this.getSy();
        return (s21 * (s20 * s00 - s10 * s10) -
            s11 * (s30 * s00 - s10 * s20) +
            s01 * (s30 * s10 - s20 * s20))
            /
                (s40 * (s20 * s00 - s10 * s10) -
                    s30 * (s30 * s00 - s10 * s20) +
                    s20 * (s30 * s10 - s20 * s20));
    };
    QuadraticRegressionSolver.prototype.bTerm = function () {
        if (this.pointArray.length < 3)
            "Insufficient pairs of co-ordinates";
        var s40 = this.getSx4();
        var s30 = this.getSx3();
        var s20 = this.getSx2();
        var s10 = this.getSx();
        var s00 = this.pointArray.length;
        var s21 = this.getSx2y();
        var s11 = this.getSxy();
        var s01 = this.getSy();
        return (s40 * (s11 * s00 - s01 * s10) -
            s30 * (s21 * s00 - s01 * s20) +
            s20 * (s21 * s10 - s11 * s20))
            /
                (s40 * (s20 * s00 - s10 * s10) -
                    s30 * (s30 * s00 - s10 * s20) +
                    s20 * (s30 * s10 - s20 * s20));
    };
    QuadraticRegressionSolver.prototype.cTerm = function () {
        if (this.pointArray.length < 3)
            throw "Insufficient pairs of co-ordinates";
        var s40 = this.getSx4();
        var s30 = this.getSx3();
        var s20 = this.getSx2();
        var s10 = this.getSx();
        var s00 = this.pointArray.length;
        var s21 = this.getSx2y();
        var s11 = this.getSxy();
        var s01 = this.getSy();
        return (s40 * (s20 * s01 - s10 * s11) -
            s30 * (s30 * s01 - s10 * s21) +
            s20 * (s30 * s11 - s20 * s21))
            /
                (s40 * (s20 * s00 - s10 * s10) -
                    s30 * (s30 * s00 - s10 * s20) +
                    s20 * (s30 * s10 - s20 * s20));
    };
    QuadraticRegressionSolver.prototype.rSquare = function () {
        if (this.pointArray.length < 3)
            throw "Insufficient pairs of co-ordinates";
        return 1 - this.getSSerr() / this.getSStot();
    };
    QuadraticRegressionSolver.prototype.getSx = function () {
        var Sx = 0;
        this.pointArray.forEach(function (ppair) {
            Sx += ppair.X;
        });
        return Sx;
    };
    QuadraticRegressionSolver.prototype.getSy = function () {
        var Sy = 0;
        this.pointArray.forEach(function (ppair) {
            Sy += ppair.Y;
        });
        return Sy;
    };
    QuadraticRegressionSolver.prototype.getSx2 = function () {
        var Sx2 = 0;
        this.pointArray.forEach(function (ppair) {
            Sx2 += Math.pow(ppair.X, 2);
        });
        return Sx2;
    };
    QuadraticRegressionSolver.prototype.getSx3 = function () {
        var Sx3 = 0;
        this.pointArray.forEach(function (ppair) {
            Sx3 += Math.pow(ppair.X, 3);
        });
        return Sx3;
    };
    QuadraticRegressionSolver.prototype.getSx4 = function () {
        var Sx4 = 0;
        this.pointArray.forEach(function (ppair) {
            Sx4 += Math.pow(ppair.X, 4);
        });
        return Sx4;
    };
    QuadraticRegressionSolver.prototype.getSxy = function () {
        var Sxy = 0;
        this.pointArray.forEach(function (ppair) {
            Sxy += ppair.X * ppair.Y;
        });
        return Sxy;
    };
    QuadraticRegressionSolver.prototype.getSx2y = function () {
        var Sx2y = 0;
        this.pointArray.forEach(function (ppair) {
            Sx2y += Math.pow(ppair.X, 2) * ppair.Y;
        });
        return Sx2y;
    };
    QuadraticRegressionSolver.prototype.getYMean = function () {
        var y_tot = 0;
        this.pointArray.forEach(function (ppair) {
            y_tot += ppair.X;
        });
        return y_tot / this.pointArray.length;
    };
    QuadraticRegressionSolver.prototype.getSStot = function () {
        var _this = this;
        var ss_tot = 0;
        this.pointArray.forEach(function (ppair) {
            ss_tot += Math.pow(ppair.Y - _this.getYMean(), 2);
        });
        return ss_tot;
    };
    QuadraticRegressionSolver.prototype.getSSerr = function () {
        var _this = this;
        var ss_err = 0;
        this.pointArray.forEach(function (ppair) {
            ss_err += Math.pow(ppair.Y - _this.getPredictedY(ppair.X), 2);
        });
        return ss_err;
    };
    QuadraticRegressionSolver.prototype.getPredictedY = function (x) {
        return this.aTerm() * Math.pow(x, 2) + this.bTerm() * x + this.cTerm();
    };
    Object.defineProperty(QuadraticRegressionSolver.prototype, "EquationText", {
        get: function () {
            return this.aTerm() + "*x^2+" + this.bTerm() + "*x+" + this.cTerm();
        },
        enumerable: true,
        configurable: true
    });
    return QuadraticRegressionSolver;
}());
var WebGL;
(function (WebGL) {
    var FrameBufferImage = (function () {
        function FrameBufferImage(id, width, height) {
            this._ImageBufferId = id;
            this._Width = width;
            this._Height = height;
        }
        Object.defineProperty(FrameBufferImage.prototype, "ImageBufferId", {
            get: function () {
                return this._ImageBufferId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FrameBufferImage.prototype, "Width", {
            get: function () {
                return this._Width;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FrameBufferImage.prototype, "Height", {
            get: function () {
                return this._Height;
            },
            enumerable: true,
            configurable: true
        });
        FrameBufferImage.prototype.Dispose = function (gl) {
        };
        return FrameBufferImage;
    }());
    WebGL.FrameBufferImage = FrameBufferImage;
    (function (RenderingFramBufferImageType) {
        RenderingFramBufferImageType[RenderingFramBufferImageType["ColorBuffer"] = 0] = "ColorBuffer";
        RenderingFramBufferImageType[RenderingFramBufferImageType["DepthBuffer"] = 1] = "DepthBuffer";
        RenderingFramBufferImageType[RenderingFramBufferImageType["StencilBuffer"] = 2] = "StencilBuffer";
    })(WebGL.RenderingFramBufferImageType || (WebGL.RenderingFramBufferImageType = {}));
    var RenderingFramBufferImageType = WebGL.RenderingFramBufferImageType;
    var RenderingFrameBufferImage = (function (_super) {
        __extends(RenderingFrameBufferImage, _super);
        function RenderingFrameBufferImage(id, type, width, height) {
            _super.call(this, id, width, height);
            this.RenderingImageType = type;
        }
        RenderingFrameBufferImage.prototype.Dispose = function (gl) {
            gl.deleteRenderbuffer(this.ImageBufferId);
        };
        return RenderingFrameBufferImage;
    }(FrameBufferImage));
    WebGL.RenderingFrameBufferImage = RenderingFrameBufferImage;
    var TextureFrameBufferImage = (function (_super) {
        __extends(TextureFrameBufferImage, _super);
        function TextureFrameBufferImage(id, width, height) {
            _super.call(this, id, width, height);
        }
        TextureFrameBufferImage.prototype.Dispose = function (gl) {
            gl.deleteTexture(this.ImageBufferId);
        };
        return TextureFrameBufferImage;
    }(FrameBufferImage));
    WebGL.TextureFrameBufferImage = TextureFrameBufferImage;
    var FrameBuffer = (function () {
        function FrameBuffer(bufferId) {
            this._ColorBufferImage = null;
            this._BufferId = bufferId;
        }
        Object.defineProperty(FrameBuffer.prototype, "BufferId", {
            get: function () {
                return this._BufferId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FrameBuffer.prototype, "ColorBufferImage", {
            get: function () {
                return this._ColorBufferImage;
            },
            set: function (fbi) {
                this._ColorBufferImage = fbi;
            },
            enumerable: true,
            configurable: true
        });
        FrameBuffer.prototype.Use = function (gl) {
            gl.bindFramebuffer(WebGLRenderingContext.FRAMEBUFFER, this._BufferId);
            if (this._ColorBufferImage != null) {
                gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, this._ColorBufferImage.ImageBufferId, 0);
                gl.bindTexture(gl.TEXTURE_2D, this._ColorBufferImage.ImageBufferId);
            }
        };
        FrameBuffer.prototype.UnUse = function (gl) {
            gl.bindFramebuffer(WebGLRenderingContext.FRAMEBUFFER, null);
        };
        FrameBuffer.prototype.ReadPixelColor = function (gl, x, y) {
            var colorPicked = new Uint8Array(4);
            gl.readPixels(x, y, 1, 1, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, colorPicked);
            var Pixel = new ColorByte4(colorPicked[0], colorPicked[1], colorPicked[2], colorPicked[3]);
            return Pixel;
        };
        FrameBuffer.prototype.ReadPixelsColors = function (gl, x, y, windowSize) {
            var colorPicked = new Uint8Array(windowSize * windowSize * 4);
            gl.readPixels(x - windowSize / 2, y - windowSize / 2, windowSize, windowSize, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, colorPicked);
            var Pixels = new Array(windowSize * windowSize);
            var ix = 0;
            for (ix = 0; ix < windowSize * windowSize; ix++) {
                var times = ix * 4;
                var Pixel = new ColorByte4(colorPicked[times + 0], colorPicked[times + 1], colorPicked[times + 2], colorPicked[times + 3]);
                Pixels[ix] = Pixel;
            }
            return Pixels;
        };
        FrameBuffer.GetPrimaryFrameBuffer = function (gl) {
            return new FrameBuffer(null);
        };
        FrameBuffer.CreateFrameBuffer = function (gl) {
            var id = gl.createFramebuffer();
            return new FrameBuffer(id);
        };
        FrameBuffer.CreateColorRenderBufferImage = function (gl, width, height) {
            var id = gl.createRenderbuffer();
            gl.bindRenderbuffer(WebGLRenderingContext.RENDERBUFFER, id);
            gl.renderbufferStorage(WebGLRenderingContext.RENDERBUFFER, WebGLRenderingContext.RGBA4, width, height);
            return new RenderingFrameBufferImage(id, RenderingFramBufferImageType.ColorBuffer, width, height);
        };
        FrameBuffer.CreateColorTextureBufferImage = function (gl, width, height) {
            var colorTexture = gl.createTexture();
            gl.bindTexture(gl.TEXTURE_2D, colorTexture);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
            return new TextureFrameBufferImage(colorTexture, width, height);
        };
        return FrameBuffer;
    }());
    WebGL.FrameBuffer = FrameBuffer;
})(WebGL || (WebGL = {}));
var Shapes;
(function (Shapes) {
    var MyPoint = (function () {
        function MyPoint() {
            this.ShapeCenter = new WebGL.Vector3(10, 0, 0);
            this.PointSize = 4;
            this.ShapeColor = new WebGL.Vector4(1, 0, 0, 1);
        }
        MyPoint.prototype.Render = function (gl) {
            if (!MyPoint.Program.IsLinked(gl))
                MyPoint.Program.Link(gl);
            MyPoint.Program.Uniforms.PointSize = this.PointSize;
            MyPoint.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            MyPoint.Program.Uniforms.ShapeColor = this.ShapeColor;
            MyPoint.Program.ArrayVertexSize = 3;
            MyPoint.Program.ArrayVertexAttributes.ShapeCenter = this.ShapeCenter.ToFloatArray();
            MyPoint.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.POINTS, 0, 1);
            MyPoint.Program.EndUse(gl);
        };
        MyPoint.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform float PointSize;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                        gl_PointSize = PointSize;\
                    }\
                } ");
        MyPoint.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        MyPoint.Program = new WebGL.Shaders.PipelineProgram("MyPointProgram", MyPoint.VertexShader, MyPoint.FragmentShader);
        return MyPoint;
    }());
    Shapes.MyPoint = MyPoint;
})(Shapes || (Shapes = {}));
var Shapes;
(function (Shapes) {
    var R2Point = (function (_super) {
        __extends(R2Point, _super);
        function R2Point() {
            _super.call(this);
            this.PointSize = 6;
            this.ShapeColor = WebGL.Vector4Colors.Grey;
        }
        Object.defineProperty(R2Point.prototype, "ShapeName", {
            get: function () {
                return "R2Point";
            },
            enumerable: true,
            configurable: true
        });
        R2Point.prototype.OnRender = function (gl) {
            if (!R2Point.Program.IsLinked(gl))
                R2Point.Program.Link(gl);
            R2Point.Program.Uniforms.PointSize = this.PointSize;
            R2Point.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Point.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2Point.Program.ArrayVertexSize = 3;
            R2Point.Program.ArrayVertexAttributes.ShapeCenter = this.ShapeCenter.ToFloatArray();
            R2Point.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.POINTS, 0, 1);
            R2Point.Program.EndUse(gl);
        };
        R2Point.prototype.OnRenderForPicking = function (gl) {
            if (!R2Point.Program.IsLinked(gl))
                R2Point.Program.Link(gl);
            R2Point.Program.Uniforms.PointSize = this.PointSize;
            R2Point.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Point.Program.Uniforms.ShapeColor = this.SelectionColorVector;
            R2Point.Program.ArrayVertexSize = 3;
            R2Point.Program.ArrayVertexAttributes.ShapeCenter = this.ShapeCenter.ToFloatArray();
            R2Point.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.POINTS, 0, 1);
            R2Point.Program.EndUse(gl);
        };
        R2Point.prototype.OnMouseMove = function (args) {
            this.ShapeCenter = args.ProjectedLocation;
        };
        R2Point.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform float PointSize;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                        gl_PointSize = PointSize;\
                    }\
                } ");
        R2Point.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        R2Point.Program = new WebGL.Shaders.PipelineProgram("R2PointProgram", R2Point.VertexShader, R2Point.FragmentShader);
        return R2Point;
    }(Shapes.R2Shape));
    Shapes.R2Point = R2Point;
})(Shapes || (Shapes = {}));
var WebGL;
(function (WebGL) {
    var Quaternion = (function () {
        function Quaternion(x, y, z, w) {
            this.xyz = new WebGL.Vector3(x, y, z);
            this.w = w;
        }
        Object.defineProperty(Quaternion.prototype, "Xyz", {
            get: function () {
                return this.xyz;
            },
            set: function (v) {
                this.xyz = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Quaternion.prototype, "W", {
            get: function () {
                return this.w;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Quaternion.prototype, "Length", {
            get: function () {
                return Math.sqrt(this.w * this.w + this.xyz.LengthSquared);
            },
            enumerable: true,
            configurable: true
        });
        Quaternion.Normalize = function (input) {
            var output = Quaternion.Identity;
            var scale = 1.0 / input.Length;
            output.Xyz = WebGL.Vector3.Multiply(input.Xyz, scale);
            output.w = input.w * scale;
            return output;
        };
        Object.defineProperty(Quaternion, "Identity", {
            get: function () {
                return new Quaternion(0, 0, 0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Quaternion.FromAxisAngle = function (axis, angle) {
            if (axis.LengthSquared == 0.0)
                return Quaternion.Identity;
            var result = Quaternion.Identity;
            angle *= 0.5;
            axis.Normalize();
            result.xyz = WebGL.Vector3.Multiply(axis, Math.sin(angle));
            result.w = Math.cos(angle);
            return Quaternion.Normalize(result);
        };
        Object.defineProperty(Quaternion.prototype, "AxisAngle", {
            get: function () {
                var q = new Quaternion(this.xyz.X, this.xyz.Y, this.xyz.Z, this.w);
                if (Math.abs(q.W) > 1.0)
                    q = Quaternion.Normalize(q);
                var result = new WebGL.Vector4(0, 0, 0, 0);
                var W = 2.0 * Math.acos(q.W);
                var axis;
                var den = Math.sqrt(1.0 - q.W * q.W);
                if (den > 0.0001) {
                    axis = WebGL.Vector3.Divide(q.Xyz, den);
                }
                else {
                    axis = new WebGL.Vector3(1, 0, 0);
                }
                return {
                    Axis: axis, Angle: W
                };
            },
            enumerable: true,
            configurable: true
        });
        Quaternion.Multiply = function (left, right) {
            var vec = WebGL.Vector3.Add(WebGL.Vector3.Add(WebGL.Vector3.Multiply(left.Xyz, right.W), WebGL.Vector3.Multiply(right.Xyz, left.W)), WebGL.Vector3.Cross(left.Xyz, right.Xyz));
            return new Quaternion(vec.X, vec.Y, vec.Z, left.W * right.W - WebGL.Vector3.Dot(left.Xyz, right.Xyz));
        };
        Quaternion.prototype.Invert = function () {
            this.w = -this.w;
        };
        Quaternion.prototype.Inverted = function () {
            var q = new Quaternion(this.xyz.X, this.xyz.Y, this.xyz.Z, this.w);
            q.Invert();
            return q;
        };
        return Quaternion;
    }());
    WebGL.Quaternion = Quaternion;
})(WebGL || (WebGL = {}));
var Shapes;
(function (Shapes) {
    (function (CircleDrawingMode) {
        CircleDrawingMode[CircleDrawingMode["TwoPoints"] = 0] = "TwoPoints";
        CircleDrawingMode[CircleDrawingMode["ThreePoints"] = 1] = "ThreePoints";
    })(Shapes.CircleDrawingMode || (Shapes.CircleDrawingMode = {}));
    var CircleDrawingMode = Shapes.CircleDrawingMode;
    var R2Circle = (function (_super) {
        __extends(R2Circle, _super);
        function R2Circle() {
            _super.call(this);
            this.CenterPoint = new Shapes.R2Point();
            this.RadiusPoint = new Shapes.R2Point();
            this.FirstPoint = new Shapes.R2Point();
            this.MiddlePoint = new Shapes.R2Point();
            this.LastPoint = new Shapes.R2Point();
            this.ArcLine = new Shapes.R2Line();
            this.DrawingMode = CircleDrawingMode.TwoPoints;
            this.DrawingStep = 1;
            this.RadialSegments = 40;
            this.EditingControls[this.CenterPoint.ColorKey] = this.CenterPoint;
            this.EditingControls[this.RadiusPoint.ColorKey] = this.RadiusPoint;
            this.AddChild(this.CenterPoint);
            this.AddChild(this.RadiusPoint);
            this.EditingControls[this.FirstPoint.ColorKey] = this.FirstPoint;
            this.EditingControls[this.MiddlePoint.ColorKey] = this.MiddlePoint;
            this.EditingControls[this.LastPoint.ColorKey] = this.LastPoint;
            this.AddChild(this.FirstPoint);
            this.AddChild(this.MiddlePoint);
            this.AddChild(this.LastPoint);
        }
        Object.defineProperty(R2Circle.prototype, "ShapeName", {
            get: function () {
                return "R2Circle";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "CenterPointLocation", {
            get: function () {
                return this.CenterPoint.ShapeCenter;
            },
            set: function (value) {
                this.CenterPoint.ShapeCenter = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "RadiusPointLocation", {
            get: function () {
                return this.RadiusPoint.ShapeCenter;
            },
            set: function (value) {
                this.RadiusPoint.ShapeCenter = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "Radius", {
            get: function () {
                return WebGL.Vector3.Subtract(this.RadiusPointLocation, this.CenterPointLocation);
            },
            enumerable: true,
            configurable: true
        });
        R2Circle.prototype.CalcCenter = function () {
            var a = this.FirstPointLocation.Xy;
            var b = this.MiddlePointLocation.Xy;
            var c = this.LastPointLocation.Xy;
            var d = 2 * (a.X - c.X) * (c.Y - b.Y) + 2 * (b.X - c.X) * (a.Y - c.Y);
            var m1 = (Math.pow(a.X, 2) - Math.pow(c.X, 2) + Math.pow(a.Y, 2) - Math.pow(c.Y, 2));
            var m2 = (Math.pow(c.X, 2) - Math.pow(b.X, 2) + Math.pow(c.Y, 2) - Math.pow(b.Y, 2));
            var nx = m1 * (c.Y - b.Y) + m2 * (c.Y - a.Y);
            var ny = m1 * (b.X - c.X) + m2 * (a.X - c.X);
            var cx = nx / d;
            var cy = ny / d;
            var dx = cx - a.X;
            var dy = cy - a.Y;
            var distance = Math.sqrt(dx * dx + dy * dy);
            var c_center = new WebGL.Vector2(cx, cy);
            if (distance > 13e6)
                distance = Number.POSITIVE_INFINITY;
            var t = {
                Radius: distance, Center: c_center
            };
            return t;
        };
        R2Circle.prototype.CalculateCenterAndRadius = function () {
            this.ArcLine.FirstPointLocation = this.FirstPointLocation;
            this.ArcLine.LastPointLocation = this.LastPointLocation;
            if (this.DrawingStep == 2) {
                this.CenterPointLocation = this.ArcLine.MiddlePointLocation;
                this.RadiusPointLocation = this.FirstPointLocation;
            }
            else {
                var cc = this.CalcCenter();
                this.CenterPointLocation = cc.Center.ToVector3();
                this.RadiusPointLocation = this.MiddlePointLocation;
            }
        };
        Object.defineProperty(R2Circle.prototype, "FirstPointLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter;
            },
            set: function (value) {
                this.FirstPoint.ShapeCenter = value;
                this.CalculateCenterAndRadius();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MiddlePointLocation", {
            get: function () {
                return this.MiddlePoint.ShapeCenter;
            },
            set: function (value) {
                this.MiddlePoint.ShapeCenter = value;
                this.CalculateCenterAndRadius();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "LastPointLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter;
            },
            set: function (value) {
                this.LastPoint.ShapeCenter = value;
                this.CalculateCenterAndRadius();
            },
            enumerable: true,
            configurable: true
        });
        R2Circle.prototype.CalculateCirlcePoints = function () {
            var step = (Angle.Two_Pi.Radians / this.RadialSegments);
            var Points = new Array(this.RadialSegments + 1);
            var initial = WebGL.Quaternion.FromAxisAngle(new WebGL.Vector3(0, 0, -1), 0);
            var rstep = WebGL.Quaternion.FromAxisAngle(new WebGL.Vector3(0, 0, -1), step);
            var total = initial;
            var vrvector = new WebGL.Vector2(this.Radius.Length, 0);
            var i;
            for (i = 0; i <= this.RadialSegments; i++) {
                var covector = vrvector.ToVector3().ToCoVector();
                var npm = WebGL.Matrix3.Multiply(WebGL.Matrix3.CreateFromQuaternion(total), covector);
                var tnpm = npm.Transpose();
                var vec = new WebGL.Vector2(tnpm.Row0.X, tnpm.Row0.Y);
                Points[i] = WebGL.Vector3.Add(vec.ToVector3(), this.CenterPointLocation);
                total = WebGL.Quaternion.Multiply(total, rstep);
            }
            return Points;
        };
        R2Circle.prototype.CalculateUnitCirlcePoints = function () {
            var step = (Angle.Two_Pi.Radians / this.RadialSegments);
            var Points = new Array(this.RadialSegments + 1);
            var initial = WebGL.Quaternion.FromAxisAngle(new WebGL.Vector3(0, 0, -1), 0);
            var rstep = WebGL.Quaternion.FromAxisAngle(new WebGL.Vector3(0, 0, -1), step);
            var total = initial;
            var vrvector = new WebGL.Vector2(0.5, 0);
            var i;
            for (i = 0; i <= this.RadialSegments; i++) {
                var covector = vrvector.ToVector3().ToCoVector();
                var npm = WebGL.Matrix3.Multiply(WebGL.Matrix3.CreateFromQuaternion(total), covector);
                var tnpm = npm.Transpose();
                var vec = new WebGL.Vector2(tnpm.Row0.X, tnpm.Row0.Y);
                Points[i] = WebGL.Vector3.Add(vec.ToVector3(), this.CenterPointLocation);
                total = WebGL.Quaternion.Multiply(total, rstep);
            }
            return Points;
        };
        R2Circle.prototype.OnEditingControlChange = function (childShape) {
            if (childShape.ColorKey == this.CenterPoint.ColorKey) {
                var delta = WebGL.Vector3.Subtract(this.CenterPoint.ShapeCenter, this.CenterPoint.PreviousShapeCenter);
                this.RadiusPointLocation = WebGL.Vector3.Add(this.RadiusPointLocation, delta);
            }
            if (childShape.ColorKey == this.FirstPoint.ColorKey || childShape.ColorKey == this.MiddlePoint.ColorKey || childShape.ColorKey == this.LastPoint.ColorKey) {
                this.CalculateCenterAndRadius();
            }
        };
        R2Circle.prototype.OnRender = function (gl) {
            if (!R2Circle.Program.IsLinked(gl))
                R2Circle.Program.Link(gl);
            R2Circle.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Circle.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2Circle.Program.ArrayVertexSize = 3;
            var points = this.CalculateCirlcePoints();
            R2Circle.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
            R2Circle.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINE_STRIP, 0, points.length);
            R2Circle.Program.EndUse(gl);
            if (this.IsDrawing || this.IsSelected) {
                if (this.DrawingMode == CircleDrawingMode.TwoPoints) {
                    this.CenterPoint.ProjectionMatrix = this.ProjectionMatrix;
                    this.CenterPoint.Render(gl);
                    this.RadiusPoint.ProjectionMatrix = this.ProjectionMatrix;
                    this.RadiusPoint.Render(gl);
                }
                else {
                    this.FirstPoint.ProjectionMatrix = this.ProjectionMatrix;
                    this.FirstPoint.Render(gl);
                    this.MiddlePoint.ProjectionMatrix = this.ProjectionMatrix;
                    this.MiddlePoint.Render(gl);
                    this.LastPoint.ProjectionMatrix = this.ProjectionMatrix;
                    this.LastPoint.Render(gl);
                }
            }
            var rpl = new WebGL.Vector2(this.Radius.Length, 0).ToPolarVector();
            var topleft = rpl.AddAngle(Angle.FromDegrees(90 + 45)).ToCartesian();
            var topright = rpl.AddAngle(Angle.FromDegrees(45)).ToCartesian();
            var bottomright = rpl.AddAngle(Angle.FromDegrees(-45)).ToCartesian();
            var tWidth = WebGL.Vector2.Subtract(topright, topleft).X;
            var tHeight = WebGL.Vector2.Subtract(topright, bottomright).Y;
            var translated_topleft = WebGL.Vector2.Add(this.CenterPointLocation.Xy, topleft);
            if (this.InnerTextSprite != null) {
                this.InnerTextSprite.Left = translated_topleft.X;
                this.InnerTextSprite.Top = translated_topleft.Y;
                this.InnerTextSprite.Width = tWidth;
                this.InnerTextSprite.Height = tHeight;
                this.InnerTextSprite.ProjectionMatrix = this.ProjectionMatrix;
                this.InnerTextSprite.UpdateTexture = true;
                this.InnerTextSprite.Render(gl);
            }
        };
        R2Circle.prototype.OnRenderForPicking = function (gl) {
            if (!R2Circle.Program.IsLinked(gl))
                R2Circle.Program.Link(gl);
            R2Circle.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Circle.Program.Uniforms.ShapeColor = this.SelectionColorVector;
            R2Circle.Program.ArrayVertexSize = 3;
            if (this.IsSelected == false) {
                var points = this.CalculateCirlcePoints();
                R2Circle.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
                R2Circle.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINE_STRIP, 0, points.length);
                R2Circle.Program.EndUse(gl);
                var points = this.CalculateUnitCirlcePoints();
                points.splice(0, 0, this.CenterPointLocation);
                R2Circle.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
                R2Circle.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.TRIANGLE_FAN, 0, points.length);
                R2Circle.Program.EndUse(gl);
            }
        };
        R2Circle.prototype.IntersectionWithRay = function (firstPointLocation, lastPointLocation) {
            var cx = this.CenterPoint.ShapeCenter.X;
            var cy = this.CenterPoint.ShapeCenter.Y;
            var x1 = firstPointLocation.X;
            var x2 = lastPointLocation.X;
            var y1 = firstPointLocation.Y;
            var y2 = lastPointLocation.Y;
            var dx = x2 - x1;
            var dy = y2 - y1;
            var A = dx * dx + dy * dy;
            var B = 2 * (dx * (x1 - cx) + dy * (y1 - cy));
            var C = (x1 - cx) * (x1 - cx) + (y1 - cy) * (y1 - cy) - this.Radius.Length * this.Radius.Length;
            var det = B * B - 4 * A * C;
            var firstPoint = null;
            var secondPoint = null;
            if ((A <= 0.0000001) || (det < 0)) {
                firstPoint = null;
                secondPoint = null;
            }
            else if (det == 0) {
                var t = -B / (2 * A);
                var ix1 = x1 + t * dx;
                var iy1 = y1 + t * dy;
                firstPoint = new WebGL.Vector2(FloatingMath.FixedNumber(ix1), FloatingMath.FixedNumber(iy1));
                secondPoint = null;
            }
            else {
                var t = (-B + Math.sqrt(det)) / (2 * A);
                var ix1 = x1 + t * dx;
                var iy1 = y1 + t * dy;
                t = (-B - Math.sqrt(det)) / (2 * A);
                var ix2 = x1 + t * dx;
                var iy2 = y1 + t * dy;
                firstPoint = new WebGL.Vector2(FloatingMath.FixedNumber(ix1), FloatingMath.FixedNumber(iy1));
                secondPoint = new WebGL.Vector2(FloatingMath.FixedNumber(ix2), FloatingMath.FixedNumber(iy2));
            }
            return { FirstPoint: firstPoint, SecondPoint: secondPoint };
        };
        Object.defineProperty(R2Circle.prototype, "MinimumCircleX", {
            get: function () {
                return this.CenterPoint.ShapeCenter.X - this.Radius.Length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MaximumCircleX", {
            get: function () {
                return this.CenterPoint.ShapeCenter.X + this.Radius.Length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MinimumCircleY", {
            get: function () {
                return this.CenterPoint.ShapeCenter.Y - this.Radius.Length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MaximumCircleY", {
            get: function () {
                return this.CenterPoint.ShapeCenter.Y + this.Radius.Length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MinimumX", {
            get: function () {
                return this.MinimumCircleX;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MaximumX", {
            get: function () {
                return this.MaximumCircleX;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MinimumY", {
            get: function () {
                return this.MinimumCircleY;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "MaximumY", {
            get: function () {
                return this.MaximumCircleY;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "TopPoint", {
            get: function () {
                return new WebGL.Vector2(this.CenterPoint.ShapeCenter.X, this.MaximumY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "BottomPoint", {
            get: function () {
                return new WebGL.Vector2(this.CenterPoint.ShapeCenter.X, this.MinimumY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "LeftPoint", {
            get: function () {
                return new WebGL.Vector2(this.MinimumX, this.CenterPoint.ShapeCenter.Y);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Circle.prototype, "RightPoint", {
            get: function () {
                return new WebGL.Vector2(this.MaximumX, this.CenterPoint.ShapeCenter.Y);
            },
            enumerable: true,
            configurable: true
        });
        R2Circle.prototype.IntersectionWithArc = function (arc) {
            var d = WebGL.Vector2.Subtract(this.CenterPoint.ShapeCenter.Xy, arc.ArcCenter.ShapeCenter.Xy).Length;
            var firstPoint;
            var secondPoint;
            if (d > this.Radius.Length + arc.Radius) {
                firstPoint = null;
                secondPoint = null;
            }
            else if (FloatingMath.NearlyEqual(d, 0) && this.Radius.Length == arc.Radius) {
                firstPoint = arc.FirstPointLocation.Xy;
                secondPoint = arc.LastPointLocation.Xy;
            }
            else if (d + Math.min(this.Radius.Length, arc.Radius) < Math.max(this.Radius.Length, arc.Radius)) {
                firstPoint = null;
                secondPoint = null;
            }
            else {
                var a = (this.Radius.Length * this.Radius.Length - arc.Radius * arc.Radius + d * d) / (2.0 * d);
                var h = Math.sqrt(this.Radius.Length * this.Radius.Length - a * a);
                var cx = this.CenterPoint.ShapeCenter.X;
                var cy = this.CenterPoint.ShapeCenter.Y;
                var p2 = new WebGL.Vector2(cx + (a * (arc.ArcCenter.ShapeCenter.X - cx)) / d, cy + (a * (arc.ArcCenter.ShapeCenter.Y - cy)) / d);
                var i1 = new WebGL.Vector2(p2.X + (h * (arc.ArcCenter.ShapeCenter.Y - cy) / d), p2.Y - (h * (arc.ArcCenter.ShapeCenter.X - cx) / d));
                var i2 = new WebGL.Vector2(p2.X - (h * (arc.ArcCenter.ShapeCenter.Y - cy) / d), p2.Y + (h * (arc.ArcCenter.ShapeCenter.X - cx) / d));
                i1.X = FloatingMath.FixedNumber(i1.X);
                i1.Y = FloatingMath.FixedNumber(i1.Y);
                i2.X = FloatingMath.FixedNumber(i2.X);
                i2.Y = FloatingMath.FixedNumber(i2.Y);
                firstPoint = i1;
                secondPoint = i2;
            }
            return {
                FirstPoint: firstPoint, SecondPoint: secondPoint
            };
        };
        R2Circle.prototype.IntersectionWithCircle = function (circle) {
            var d = WebGL.Vector2.Subtract(this.CenterPoint.ShapeCenter.Xy, circle.CenterPoint.ShapeCenter.Xy).Length;
            var firstPoint;
            var secondPoint;
            if (d > this.Radius.Length + circle.Radius.Length) {
                firstPoint = null;
                secondPoint = null;
            }
            else if (FloatingMath.NearlyEqual(d, 0) && this.Radius == circle.Radius) {
                firstPoint = null;
                secondPoint = null;
            }
            else if (d + Math.min(this.Radius.Length, circle.Radius.Length) < Math.max(this.Radius.Length, circle.Radius.Length)) {
                firstPoint = null;
                secondPoint = null;
            }
            else {
                var a = (this.Radius.Length * this.Radius.Length - circle.Radius.Length * circle.Radius.Length + d * d) / (2.0 * d);
                var h = Math.sqrt(this.Radius.Length * this.Radius.Length - a * a);
                var cx = this.CenterPoint.ShapeCenter.X;
                var cy = this.CenterPoint.ShapeCenter.Y;
                var p2 = new WebGL.Vector2(cx + (a * (circle.CenterPoint.ShapeCenter.X - cx)) / d, cy + (a * (circle.CenterPoint.ShapeCenter.Y - cy)) / d);
                var i1 = new WebGL.Vector2(p2.X + (h * (circle.CenterPoint.ShapeCenter.Y - cy) / d), p2.Y - (h * (circle.CenterPoint.ShapeCenter.X - cx) / d));
                var i2 = new WebGL.Vector2(p2.X - (h * (circle.CenterPoint.ShapeCenter.Y - cy) / d), p2.Y + (h * (circle.CenterPoint.ShapeCenter.X - cx) / d));
                i1.X = FloatingMath.FixedNumber(i1.X);
                i1.Y = FloatingMath.FixedNumber(i1.Y);
                i2.X = FloatingMath.FixedNumber(i2.X);
                i2.Y = FloatingMath.FixedNumber(i2.Y);
                firstPoint = i1;
                secondPoint = i2;
            }
            return {
                FirstPoint: firstPoint, SecondPoint: secondPoint
            };
        };
        R2Circle.prototype.GetIntersectionsWith = function (shape) {
            var intersections = new Array();
            if (shape instanceof Shapes.R2Line) {
                var targetLine = shape;
                var iL = this.IntersectionWithRay(targetLine.FirstPointLocation.Xy, targetLine.LastPointLocation.Xy);
                if (iL.FirstPoint != null && targetLine.IsPointOnLineSegment(iL.FirstPoint)) {
                    intersections.push(iL.FirstPoint);
                }
                if (iL.SecondPoint != null && targetLine.IsPointOnLineSegment(iL.SecondPoint)) {
                    intersections.push(iL.SecondPoint);
                }
            }
            if (shape instanceof Shapes.R2Arc) {
                var targetArc = shape;
                var iA = this.IntersectionWithArc(targetArc);
                if (iA.FirstPoint != null && targetArc.IsPointOnArc(iA.FirstPoint) != null) {
                    intersections.push(iA.FirstPoint);
                }
                if (iA.SecondPoint != null && targetArc.IsPointOnArc(iA.SecondPoint) != null) {
                    intersections.push(iA.SecondPoint);
                }
            }
            if (shape instanceof R2Circle) {
                var targetCircle = shape;
                var iA = targetCircle.IntersectionWithCircle(this);
                if (iA.FirstPoint != null) {
                    intersections.push(iA.FirstPoint);
                }
                if (iA.SecondPoint != null) {
                    intersections.push(iA.SecondPoint);
                }
            }
            if (shape instanceof Shapes.R2Rectangle) {
                var targetRectangle = shape;
                var iis = this.GetIntersectionsWith(targetRectangle.TopLine);
                iis.forEach(function (x) { return intersections.push(x); });
                iis = this.GetIntersectionsWith(targetRectangle.RightLine);
                iis.forEach(function (x) { return intersections.push(x); });
                iis = this.GetIntersectionsWith(targetRectangle.BottomLine);
                iis.forEach(function (x) { return intersections.push(x); });
                iis = this.GetIntersectionsWith(targetRectangle.LeftLine);
                iis.forEach(function (x) { return intersections.push(x); });
            }
            return intersections;
        };
        Object.defineProperty(R2Circle.prototype, "TerminalPoints", {
            get: function () {
                var vv = new Array();
                vv.push(this.TopPoint);
                vv.push(this.RightPoint);
                vv.push(this.BottomPoint);
                vv.push(this.LeftPoint);
                return vv;
            },
            enumerable: true,
            configurable: true
        });
        R2Circle.prototype.ApplySegmentation = function () {
            var ipoints = this.IntersectedPoints;
            var pc = new PointsCloud();
            for (var key in this.IntersectedShapes) {
                var ShapeIntersectedPoints = this.IntersectedShapes[key];
                ShapeIntersectedPoints.forEach(function (x) { return pc.AddPoint(key, x.ToVector3()); });
            }
            var orderd_points = pc.OrderCounterClockWise(this.CenterPoint.ShapeCenter.Xy);
            this._InnerSegments = new Array();
            var io;
            for (io = 0; io < orderd_points.length - 1; io++) {
                var arc = Shapes.R2Arc.MakeCcwArc(this.CenterPoint.ShapeCenter.Xy, orderd_points[io], orderd_points[io + 1]);
                this._InnerSegments.push(arc);
            }
            var lastArc = Shapes.R2Arc.MakeCcwArc(this.CenterPoint.ShapeCenter.Xy, orderd_points[orderd_points.length - 1], orderd_points[0]);
            this._InnerSegments.push(lastArc);
        };
        R2Circle.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                    }\
                } ");
        R2Circle.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        R2Circle.Program = new WebGL.Shaders.PipelineProgram("R2CircleProgram", R2Circle.VertexShader, R2Circle.FragmentShader);
        return R2Circle;
    }(Shapes.R2Shape));
    Shapes.R2Circle = R2Circle;
})(Shapes || (Shapes = {}));
var Shapes;
(function (Shapes) {
    var R2Line = (function (_super) {
        __extends(R2Line, _super);
        function R2Line() {
            _super.call(this);
            this._FirstPoint = new Shapes.R2Point();
            this._LastPoint = new Shapes.R2Point();
            this.IsDimension = false;
            this.ArrowPercentageLength = 0.08;
            this.RightArrow = false;
            this.LeftArrow = false;
            this.DashWidth = 0.5;
            this.LineDots = null;
            this.IdentificationCircle = null;
            this.IsDoorNormalNegative = false;
            this._DoorArrow = null;
            this._DoorOrientation = null;
            this.ArrowOffset = 1.5;
            this.FirstBottomHeight = 0;
            this.LastBottomHeight = 0;
            this.FirstTopHeight = 10;
            this.LastTopHeight = 10;
            this.EditingControls[this.FirstPoint.ColorKey] = this.FirstPoint;
            this.EditingControls[this.LastPoint.ColorKey] = this.LastPoint;
            this.AddChild(this.FirstPoint);
            this.AddChild(this.LastPoint);
        }
        Object.defineProperty(R2Line.prototype, "FirstPoint", {
            get: function () {
                return this._FirstPoint;
            },
            set: function (value) {
                delete this.EditingControls[this._FirstPoint.ColorKey];
                this._FirstPoint.RemoveOwner(this);
                this._FirstPoint = value;
                this.EditingControls[this._FirstPoint.ColorKey] = this.FirstPoint;
                this._FirstPoint.AddOwner(this);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "LastPoint", {
            get: function () {
                return this._LastPoint;
            },
            set: function (value) {
                delete this.EditingControls[this._LastPoint.ColorKey];
                this._LastPoint.RemoveOwner(this);
                this._LastPoint = value;
                this.EditingControls[this._LastPoint.ColorKey] = this.LastPoint;
                this._LastPoint.AddOwner(this);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "ShapeName", {
            get: function () {
                return "R2Line";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "FirstPointLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter;
            },
            set: function (value) {
                this.FirstPoint.ShapeCenter = value;
                this.LineDots = null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "LastPointLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter;
            },
            set: function (value) {
                this.LastPoint.ShapeCenter = value;
                this.LineDots = null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "MinimumX", {
            get: function () {
                if (this.LastPointLocation.X < this.FirstPointLocation.X)
                    return this.LastPointLocation.X;
                else
                    return this.FirstPointLocation.X;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "MaximumX", {
            get: function () {
                if (this.LastPointLocation.X > this.FirstPointLocation.X)
                    return this.LastPointLocation.X;
                else
                    return this.FirstPointLocation.X;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "MinimumY", {
            get: function () {
                if (this.LastPointLocation.Y < this.FirstPointLocation.Y)
                    return this.LastPointLocation.Y;
                else
                    return this.FirstPointLocation.Y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "MaximumY", {
            get: function () {
                if (this.LastPointLocation.Y > this.FirstPointLocation.Y)
                    return this.LastPointLocation.Y;
                else
                    return this.FirstPointLocation.Y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "Vector", {
            get: function () {
                var fp = this.FirstPointLocation;
                var sp = this.LastPointLocation;
                var dp = WebGL.Vector2.Subtract(sp.Xy, fp.Xy);
                return dp;
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.DashedLinePoints = function () {
            var dl = WebGL.Vector3.Subtract(this.LastPointLocation, this.FirstPointLocation);
            var dashes = Math.floor(Math.sqrt(dl.X * dl.X + dl.Y * dl.Y) / this.DashWidth);
            var dashX = dl.X / dashes;
            var dashY = dl.Y / dashes;
            var q = 0;
            var x = this.FirstPointLocation.X;
            var y = this.FirstPointLocation.Y;
            var vs = new Array();
            while (q < dashes) {
                var v1 = new WebGL.Vector2(x, y).ToVector3();
                x += dashX;
                y += dashY;
                var v2 = new WebGL.Vector2(x, y).ToVector3();
                if (q % 2 == 0) {
                    vs.push(v1);
                    vs.push(v2);
                }
                q++;
            }
            return vs;
        };
        R2Line.prototype.DottedLinePoints = function () {
            if (this.LineDots != null)
                return this.LineDots;
            var dl = WebGL.Vector3.Subtract(this.LastPointLocation, this.FirstPointLocation);
            var lineLength = Math.floor(Math.sqrt(dl.X * dl.X + dl.Y * dl.Y));
            var divX = dl.X / lineLength;
            var divY = dl.Y / lineLength;
            var q = 0;
            var x = this.FirstPointLocation.X;
            var y = this.FirstPointLocation.Y;
            this.LineDots = new Array();
            while (q < lineLength) {
                var v = new WebGL.Vector2(x, y).ToVector3();
                x += divX * 0.1;
                y += divY * 0.1;
                this.LineDots.push(v);
                q += 0.1;
            }
            return this.LineDots;
        };
        Object.defineProperty(R2Line.prototype, "Normal", {
            get: function () {
                var fp = this.FirstPointLocation;
                var sp = this.LastPointLocation;
                var dp = WebGL.Vector2.Subtract(sp.Xy, fp.Xy);
                dp.Normalize();
                var _NormalXY = new WebGL.Vector2(-dp.Y, dp.X);
                _NormalXY.Normalize();
                return _NormalXY;
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.MoveLineAlongNormal = function (length) {
            var dVector = this.Normal.Extend(length);
            this.FirstPointLocation = WebGL.Vector3.Add(this.FirstPointLocation, dVector.ToVector3());
            this.LastPointLocation = WebGL.Vector3.Add(this.LastPointLocation, dVector.ToVector3());
        };
        R2Line.prototype.RenderOpenning = function (gl) {
            var bo3d = 0.5;
            var dVector = this.Normal.Extend(bo3d);
            var pfl = WebGL.Vector3.Add(this.FirstPointLocation, dVector.ToVector3());
            var pll = WebGL.Vector3.Add(this.LastPointLocation, dVector.ToVector3());
            var nfl = WebGL.Vector3.Subtract(this.FirstPointLocation, dVector.ToVector3());
            var nll = WebGL.Vector3.Subtract(this.LastPointLocation, dVector.ToVector3());
            var lines = new Array();
            lines.push(pfl);
            lines.push(nfl);
            lines.push(pll);
            lines.push(nll);
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.RenderPlainLine = function (gl) {
            var lines = new Array();
            lines.push(this.FirstPointLocation);
            lines.push(this.LastPointLocation);
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.RenderWindow = function (gl) {
            var bo3d = 0.5;
            var dVector = this.Normal.Extend(bo3d);
            var pfl = WebGL.Vector3.Add(this.FirstPointLocation, dVector.ToVector3());
            var pll = WebGL.Vector3.Add(this.LastPointLocation, dVector.ToVector3());
            var nfl = WebGL.Vector3.Subtract(this.FirstPointLocation, dVector.ToVector3());
            var nll = WebGL.Vector3.Subtract(this.LastPointLocation, dVector.ToVector3());
            var lines = new Array();
            lines.push(pfl);
            lines.push(pll);
            lines.push(nfl);
            lines.push(nll);
            lines.push(pfl);
            lines.push(nfl);
            lines.push(pll);
            lines.push(nll);
            var tvector = this.Normal.Extend(bo3d / 3);
            var ofl = WebGL.Vector3.Add(this.FirstPointLocation, tvector.ToVector3());
            var oll = WebGL.Vector3.Add(this.LastPointLocation, tvector.ToVector3());
            lines.push(ofl);
            lines.push(oll);
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.ChangeDoorArrow = function (shape) {
            var dv = this.Normal.Extend(this.Vector.Length * 2 / 10);
            if (this.IsDoorNormalNegative) {
                this.IsDoorNormalNegative = false;
            }
            else {
                this.IsDoorNormalNegative = true;
            }
            if (this.IsDoorNormalNegative)
                dv = dv.Inverted();
            var np = WebGL.Vector3.Add(this._DoorArrow.FirstPointLocation, dv.ToVector3());
            this._DoorArrow.LastPointLocation = np;
        };
        R2Line.prototype.ChangeDoorOrientation = function (shape) {
            this.Reverse();
            var vloc = this.ExecuteEquation(0.9);
            var dv = this.Vector.Normalized.Inverted().Extend(this.Vector.Length * 2 / 10);
            var np = WebGL.Vector3.Add(vloc, dv.ToVector3());
            this._DoorOrientation.FirstPointLocation = vloc;
            this._DoorOrientation.LastPointLocation = np;
        };
        Object.defineProperty(R2Line.prototype, "DoorArrow", {
            get: function () {
                var t = 1 - (0.5 / this.Length);
                var vloc = this.ExecuteEquation(t);
                if (this._DoorArrow == null) {
                    this._DoorArrow = new R2Line();
                    this._DoorArrow.RightArrow = true;
                    this._DoorArrow.FirstPointLocation = vloc;
                    var dv = this.Normal.Extend(this.Vector.Length * 2 / 10);
                    if (this.IsDoorNormalNegative)
                        dv = dv.Inverted();
                    var np = WebGL.Vector3.Add(vloc, dv.ToVector3());
                    this._DoorArrow.LastPointLocation = np;
                    this.EditingControls[this._DoorArrow.ColorKey] = this._DoorArrow;
                    this._DoorArrow.MouseUp = this.ChangeDoorArrow.bind(this);
                }
                return this._DoorArrow;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "DoorOrientation", {
            get: function () {
                var vloc = this.ExecuteEquation(0.9);
                if (this._DoorOrientation == null) {
                    this._DoorOrientation = new R2Line();
                    this._DoorOrientation.RightArrow = true;
                    this._DoorOrientation.FirstPointLocation = vloc;
                    var dv = this.Vector.Normalized.Inverted().Extend(this.Vector.Length * 2 / 10);
                    var np = WebGL.Vector3.Add(vloc, dv.ToVector3());
                    this._DoorOrientation.LastPointLocation = np;
                    this.EditingControls[this._DoorOrientation.ColorKey] = this._DoorOrientation;
                    this._DoorOrientation.MouseUp = this.ChangeDoorOrientation.bind(this);
                }
                return this._DoorOrientation;
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.Reverse = function () {
            var loc = this.FirstPointLocation;
            this.FirstPointLocation = this.LastPointLocation;
            this.LastPointLocation = loc;
        };
        R2Line.prototype.RenderDoorControllingShapes = function (gl) {
            this.DoorArrow.ProjectionMatrix = this.ProjectionMatrix;
            this.DoorArrow.Render(gl);
        };
        R2Line.prototype.RenderDoor = function (gl) {
            var fp = this.FirstPointLocation;
            var lp = this.LastPointLocation;
            var dVector = this.Normal.Extend(this.Vector.Length);
            if (this.IsDoorNormalNegative)
                dVector = dVector.Inverted();
            var np = WebGL.Vector3.Add(this.LastPointLocation, dVector.ToVector3());
            var lines = new Array();
            lines.push(lp);
            lines.push(np);
            lines.push(np);
            lines.push(fp);
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, lines.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.RenderIdentificationCircle = function (gl) {
            if (this.IdentificationCircle == null) {
                this.IdentificationCircle = new Shapes.R2Circle();
                this.IdentificationCircle.InnerTextSprite = new Sprites.TextSprite(0, 0, 100, 100);
                this.IdentificationCircle.InnerTextSprite.Text = "";
            }
            if (this.BuildingType == Shapes.BuildingType.Window) {
                this.IdentificationCircle.InnerTextSprite.Text = "W";
            }
            else if (this.BuildingType == Shapes.BuildingType.Door) {
                this.IdentificationCircle.InnerTextSprite.Text = "D";
            }
            else {
                this.IdentificationCircle.InnerTextSprite.Text = "";
            }
            this.Normal.Extend(5);
            if (this.BuildingType == Shapes.BuildingType.Window) {
                this.IdentificationCircle.CenterPointLocation = WebGL.Vector3.Add(this.MiddlePointLocation, this.Normal.Extend(3).ToVector3());
            }
            else {
                this.IdentificationCircle.CenterPointLocation = this.MiddlePointLocation;
            }
            this.IdentificationCircle.RadiusPointLocation = WebGL.Vector3.Add(this.IdentificationCircle.CenterPointLocation, new WebGL.Vector3(2, 0, 0));
            this.IdentificationCircle.ProjectionMatrix = this.ProjectionMatrix;
            this.IdentificationCircle.ShapeColor = WebGL.Vector4Colors.Green;
            this.IdentificationCircle.Render(gl);
        };
        R2Line.prototype.RenderRightArrow = function (gl) {
            var ArrowHeight = 0.5;
            var points = new Array();
            var percentage = this.ArrowOffset / this.Length;
            var endLoc = this.ExecuteEquation(1 - percentage);
            var uL = WebGL.Vector2.Add(endLoc.Xy, this.Normal.Extend(ArrowHeight));
            var dL = WebGL.Vector2.Add(endLoc.Xy, this.Normal.Extend(-ArrowHeight));
            points.push(this.LastPointLocation);
            points.push(uL.ToVector3());
            points.push(this.LastPointLocation);
            points.push(dL.ToVector3());
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, points.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.RenderLeftArrow = function (gl) {
            var ArrowHeight = 0.5;
            var points = new Array();
            var percentage = this.ArrowOffset / this.Length;
            var beginLoc = this.ExecuteEquation(percentage);
            var uL = WebGL.Vector2.Add(beginLoc.Xy, this.Normal.Extend(ArrowHeight));
            var dL = WebGL.Vector2.Add(beginLoc.Xy, this.Normal.Extend(-ArrowHeight));
            points.push(this.FirstPointLocation);
            points.push(uL.ToVector3());
            points.push(this.FirstPointLocation);
            points.push(dL.ToVector3());
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, points.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.GetDimensionRectangle = function () {
            var segment = 1 / 6;
            var leftPercentage = 0.5 - segment;
            var rightPercentage = 0.5 + segment;
            var left = this.ExecuteEquation(leftPercentage).Xy;
            var right = this.ExecuteEquation(rightPercentage).Xy;
            var width = WebGL.Vector2.Subtract(right, left);
            var height = new WebGL.Vector2(0, 4);
            var top = WebGL.Vector2.Add(left, height);
            var cr = { left: left.X, top: left.Y, right: right.X, bottom: right.Y, width: width.X, height: height.Y };
            return cr;
        };
        R2Line.prototype.RenderDimension = function (gl) {
            var middle = this.ExecuteEquation(0.5);
            var dimension = new Sprites.TextSprite(middle.Y + 2, middle.X - 5, 10, 4);
            dimension.Text = this.Length.toFixed(1).toString() + "";
            dimension.ProjectionMatrix = this.ProjectionMatrix;
            dimension.Render(gl);
        };
        R2Line.prototype.GetLinePoints = function () {
            if (this.BuildingShapes.length == 0) {
                var twop = new Array(this.FirstPointLocation, this.LastPointLocation);
                return twop;
            }
            var pc = new PointsCloud();
            var ix;
            for (ix = 0; ix < this.BuildingShapes.length; ix++) {
                var bshape = this.BuildingShapes[ix];
                if (bshape instanceof R2Line) {
                    var bLine = bshape;
                    pc.AddPoint(bLine.ColorKey, bLine.FirstPointLocation);
                    pc.AddPoint(bLine.ColorKey, bLine.LastPointLocation);
                }
            }
            pc.AddPoint(this.ColorKey, this.FirstPointLocation);
            pc.AddPoint(this.ColorKey, this.LastPointLocation);
            var orderedPoints = pc.OrderByLength(this.FirstPointLocation.Xy);
            var points = new Array();
            for (ix = 0; ix < orderedPoints.length - 1; ix += 2) {
                points.push(orderedPoints[ix].ToVector3());
                points.push(orderedPoints[ix + 1].ToVector3());
            }
            return points;
        };
        R2Line.prototype.RenderDotted = function (gl) {
            if (!R2Line.Program.IsLinked(gl))
                R2Line.Program.Link(gl);
            R2Line.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Line.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2Line.Program.ArrayVertexSize = 3;
            this.LineDots = null;
            var points = this.DottedLinePoints();
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, points.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.OnRender = function (gl) {
            if (!R2Line.Program.IsLinked(gl))
                R2Line.Program.Link(gl);
            R2Line.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Line.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2Line.Program.ArrayVertexSize = 3;
            if (this.BuildingType == Shapes.BuildingType.Window) {
                this.RenderWindow(gl);
                return;
            }
            if (this.BuildingType == Shapes.BuildingType.Door) {
                this.RenderDoor(gl);
                return;
            }
            if (this.BuildingType == Shapes.BuildingType.Opening) {
                this.RenderOpenning(gl);
                return;
            }
            var points = this.DottedLinePoints();
            if (this.LineType == Shapes.ShapeLineType.Dotted && points.length > 2) {
                R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
                R2Line.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, points.length);
                R2Line.Program.EndUse(gl);
            }
            else {
                if (this.RightArrow == true || this.IsDimension == true) {
                    this.RenderRightArrow(gl);
                }
                if (this.LeftArrow || this.IsDimension == true) {
                    this.RenderLeftArrow(gl);
                }
                if (this.IsDimension) {
                    this.RenderDimension(gl);
                    var pps = new Array();
                    var cr = this.GetDimensionRectangle();
                    var topleft = new WebGL.Vector3(cr.left, cr.top, 0);
                    var bottomright = new WebGL.Vector3(cr.right, cr.bottom, 0);
                    pps.push(this.FirstPointLocation);
                    pps.push(topleft);
                    pps.push(bottomright);
                    pps.push(this.LastPointLocation);
                    R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(pps));
                    R2Line.Program.BeginUse(gl);
                    gl.drawArrays(WebGLRenderingContext.LINES, 0, pps.length);
                    R2Line.Program.EndUse(gl);
                }
                else {
                    var points = this.GetLinePoints();
                    R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
                    R2Line.Program.BeginUse(gl);
                    gl.drawArrays(WebGLRenderingContext.LINES, 0, points.length);
                    R2Line.Program.EndUse(gl);
                }
            }
            if (this.IsDrawing || this.IsSelected) {
                this.FirstPoint.ProjectionMatrix = this.ProjectionMatrix;
                this.FirstPoint.Render(gl);
                this.LastPoint.ProjectionMatrix = this.ProjectionMatrix;
                this.LastPoint.Render(gl);
            }
        };
        R2Line.prototype.OnRenderForPicking = function (gl) {
            if (!R2Line.Program.IsLinked(gl))
                R2Line.Program.Link(gl);
            R2Line.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Line.Program.Uniforms.ShapeColor = this.SelectionColorVector;
            if (this.BuildingType == Shapes.BuildingType.Window) {
                this.RenderWindow(gl);
                this.RenderPlainLine(gl);
                return;
            }
            if (this.BuildingType == Shapes.BuildingType.Door) {
                this.RenderDoor(gl);
                this.DoorArrow.ProjectionMatrix = this.ProjectionMatrix;
                this.DoorArrow.OnRenderForPicking(gl);
            }
            if (this.BuildingType == Shapes.BuildingType.Opening) {
                this.RenderOpenning(gl);
                this.RenderPlainLine(gl);
                return;
            }
            var points = new Array();
            points.push(this.FirstPointLocation);
            points.push(this.LastPointLocation);
            R2Line.Program.ArrayVertexSize = 3;
            R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
            R2Line.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, points.length);
            R2Line.Program.EndUse(gl);
        };
        R2Line.prototype.RenderStrongly = function (gl) {
            var invertedColor = new WebGL.Vector4(0, 1 - this.ShapeColor.Y, 0, this.ShapeColor.W);
            R2Line.Program.Uniforms.ShapeColor = invertedColor;
            this.RenderPlainLine(gl);
        };
        R2Line.prototype.RenderRectangleLine = function (gl) {
            if (!R2Line.ThicknessProgram.IsLinked(gl))
                R2Line.ThicknessProgram.Link(gl);
            R2Line.ThicknessProgram.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Line.ThicknessProgram.Uniforms.ShapeColor = this.ShapeColor;
            R2Line.ThicknessProgram.Uniforms.thickness = 0.3;
            var p1 = this.FirstPointLocation.Xy;
            var p2 = this.LastPointLocation.Xy;
            var points = new Array();
            points.push(p1);
            points.push(p2);
            points.push(p2);
            points.push(p2);
            points.push(p1);
            points.push(p1);
            R2Line.ThicknessProgram.ArrayVertexSize = 2;
            R2Line.ThicknessProgram.ArrayVertexAttributes.position = new Float32Array(WebGL.Vectors2ToArray(points));
            var nr = this.Normal;
            var inr = this.Normal.Inverted();
            var norm = new Array();
            norm.push(nr);
            norm.push(nr);
            norm.push(inr);
            norm.push(inr);
            norm.push(inr);
            norm.push(nr);
            R2Line.ThicknessProgram.ArrayVertexAttributes.normal = new Float32Array(WebGL.Vectors2ToArray(norm));
            var start = new Date().getTime();
            R2Line.ThicknessProgram.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, points.length);
            R2Line.ThicknessProgram.EndUse(gl);
            var end = new Date().getTime();
            var time = end - start;
        };
        Object.defineProperty(R2Line.prototype, "WallFirstPointTopLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter.Xy.ToVector3(this.FirstTopHeight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "WallLastPointTopLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter.Xy.ToVector3(this.LastTopHeight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "WallFirstPointBottomLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter.Xy.ToVector3(this.FirstBottomHeight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "WallLastPointBottomLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter.Xy.ToVector3(this.LastBottomHeight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "DoorFirstPointTopLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter.Xy.ToVector3(2 * this.FirstTopHeight / 3);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "DoorLastPointTopLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter.Xy.ToVector3(2 * this.LastTopHeight / 3);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "DoorFirstPointBottomLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter.Xy.ToVector3(this.FirstBottomHeight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "DoorLastPointBottomLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter.Xy.ToVector3(this.LastBottomHeight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "WindowFirstPointTopLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter.Xy.ToVector3(2 * this.FirstTopHeight / 3);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "WindowLastPointTopLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter.Xy.ToVector3(2 * this.LastTopHeight / 3);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "WindowFirstPointBottomLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter.Xy.ToVector3(this.FirstBottomHeight + this.FirstTopHeight / 3);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "WindowLastPointBottomLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter.Xy.ToVector3(this.LastBottomHeight + this.LastTopHeight / 3);
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.RenderWall = function (gl, translation) {
            if (!R2Line.WallProgram.IsLinked(gl))
                R2Line.WallProgram.Link(gl);
            R2Line.WallProgram.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Line.WallProgram.Uniforms.ShapeColor = WebGL.Vector4Colors.White;
            R2Line.WallProgram.Uniforms.Translation = translation.ToVector3().ToVector4();
            var points = new Array();
            points.push(this.WallFirstPointBottomLocation);
            points.push(this.WallFirstPointTopLocation);
            points.push(this.WallLastPointBottomLocation);
            points.push(this.WallFirstPointTopLocation);
            points.push(this.WallLastPointTopLocation);
            points.push(this.WallLastPointBottomLocation);
            gl.enable(gl.POLYGON_OFFSET_FILL);
            gl.polygonOffset(2, 6);
            R2Line.WallProgram.ArrayVertexSize = 3;
            R2Line.WallProgram.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
            R2Line.WallProgram.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, points.length);
            R2Line.WallProgram.EndUse(gl);
            gl.disable(gl.POLYGON_OFFSET_FILL);
            var outlinePoints = new Array();
            outlinePoints.push(this.WallFirstPointBottomLocation);
            outlinePoints.push(this.WallFirstPointTopLocation);
            outlinePoints.push(this.WallLastPointTopLocation);
            outlinePoints.push(this.WallLastPointBottomLocation);
            R2Line.WallProgram.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(outlinePoints));
            R2Line.WallProgram.Uniforms.ShapeColor = WebGL.Vector4Colors.Blue;
            R2Line.WallProgram.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINE_LOOP, 0, outlinePoints.length);
            R2Line.WallProgram.EndUse(gl);
            this.BuildingShapes.forEach(function (bs) {
                if (bs instanceof Shapes.R2Line) {
                    var bls = bs;
                    if (bls.BuildingType == Shapes.BuildingType.Door) {
                        var doorPoints = new Array();
                        doorPoints.push(bls.DoorFirstPointBottomLocation);
                        doorPoints.push(bls.DoorFirstPointTopLocation);
                        doorPoints.push(bls.DoorLastPointBottomLocation);
                        doorPoints.push(bls.DoorFirstPointTopLocation);
                        doorPoints.push(bls.DoorLastPointTopLocation);
                        doorPoints.push(bls.DoorLastPointBottomLocation);
                        R2Line.WallProgram.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(doorPoints));
                        gl.enable(gl.POLYGON_OFFSET_FILL);
                        gl.polygonOffset(2, 3);
                        R2Line.WallProgram.Uniforms.ShapeColor = WebGL.Vector4Colors.Brown;
                        R2Line.WallProgram.BeginUse(gl);
                        gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, doorPoints.length);
                        R2Line.WallProgram.EndUse(gl);
                        gl.disable(gl.POLYGON_OFFSET_FILL);
                    }
                    if (bls.BuildingType == Shapes.BuildingType.Window) {
                        var winPoints = new Array();
                        winPoints.push(bls.WindowFirstPointBottomLocation);
                        winPoints.push(bls.WindowFirstPointTopLocation);
                        winPoints.push(bls.WindowLastPointBottomLocation);
                        winPoints.push(bls.WindowFirstPointTopLocation);
                        winPoints.push(bls.WindowLastPointTopLocation);
                        winPoints.push(bls.WindowLastPointBottomLocation);
                        R2Line.WallProgram.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(winPoints));
                        gl.enable(gl.POLYGON_OFFSET_FILL);
                        gl.polygonOffset(2, 3);
                        R2Line.WallProgram.Uniforms.ShapeColor = WebGL.Vector4Colors.Yellow;
                        R2Line.WallProgram.BeginUse(gl);
                        gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, winPoints.length);
                        R2Line.WallProgram.EndUse(gl);
                        gl.disable(gl.POLYGON_OFFSET_FILL);
                    }
                }
            });
        };
        R2Line.prototype.ExecuteEquation = function (t) {
            var xt = this.LastPointLocation.X - this.FirstPointLocation.X;
            var yt = this.LastPointLocation.Y - this.FirstPointLocation.Y;
            var zt = this.LastPointLocation.Z - this.FirstPointLocation.Z;
            var point = new WebGL.Vector3(t * xt + this.FirstPointLocation.X, t * yt + this.FirstPointLocation.Y, t * zt + this.FirstPointLocation.Z);
            return point;
        };
        Object.defineProperty(R2Line.prototype, "MiddlePointLocation", {
            get: function () {
                return this.ExecuteEquation(0.5);
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.IntersectionWithRay = function (firstPointLocation, lastPointLocation) {
            var Ax = this.FirstPointLocation.X;
            var Bx = this.LastPointLocation.X;
            var Ay = this.FirstPointLocation.Y;
            var By = this.LastPointLocation.Y;
            var Cx = firstPointLocation.X;
            var Dx = lastPointLocation.X;
            var Cy = firstPointLocation.Y;
            var Dy = lastPointLocation.Y;
            var distAB, theCos, theSin, newX, ABpos;
            var intersection = new WebGL.Vector2(0, 0);
            if (Ax == Bx && Ay == By || Cx == Dx && Cy == Dy) {
                return null;
            }
            Bx -= Ax;
            By -= Ay;
            Cx -= Ax;
            Cy -= Ay;
            Dx -= Ax;
            Dy -= Ay;
            distAB = Math.sqrt(Bx * Bx + By * By);
            theCos = Bx / distAB;
            theSin = By / distAB;
            newX = Cx * theCos + Cy * theSin;
            Cy = Cy * theCos - Cx * theSin;
            Cx = newX;
            newX = Dx * theCos + Dy * theSin;
            Dy = Dy * theCos - Dx * theSin;
            Dx = newX;
            if (FloatingMath.NearlyEqual(Cy, Dy))
                return null;
            ABpos = Dx + (Cx - Dx) * Dy / (Dy - Cy);
            intersection.X = FloatingMath.FixedNumber(Ax + ABpos * theCos);
            intersection.Y = FloatingMath.FixedNumber(Ay + ABpos * theSin);
            return intersection;
        };
        R2Line.prototype.IsPointOnLineSegment = function (point) {
            if (this.IsPointTerminal(point.ToVector3()))
                return true;
            var l1 = WebGL.Vector2.Subtract(point, this.FirstPointLocation.Xy).Length;
            var l2 = WebGL.Vector2.Subtract(point, this.LastPointLocation.Xy).Length;
            var l = l1 + l2;
            var percentage = l / this.Vector.Length;
            if (percentage < 1 || FloatingMath.NearlyEqual(percentage, 1)) {
            }
            else
                return false;
            var dxc = point.X - this.FirstPointLocation.X;
            var dyc = point.Y - this.FirstPointLocation.Y;
            var dxl = this.LastPointLocation.X - this.FirstPointLocation.X;
            var dyl = this.LastPointLocation.Y - this.FirstPointLocation.Y;
            if (Math.abs(dxl) >= Math.abs(dyl))
                return dxl > 0 ?
                    this.FirstPointLocation.X <= point.X && point.X <= this.LastPointLocation.X :
                    this.LastPointLocation.X <= point.X && point.X <= this.FirstPointLocation.X;
            else
                return dyl > 0 ?
                    this.FirstPointLocation.Y <= point.Y && point.Y <= this.LastPointLocation.Y :
                    this.LastPointLocation.Y <= point.Y && point.Y <= this.FirstPointLocation.Y;
        };
        R2Line.IsCollinear = function (ab, cd) {
            var a1 = ab.FirstPointLocation.X;
            var a2 = ab.FirstPointLocation.Y;
            var b1 = ab.LastPointLocation.X;
            var b2 = ab.LastPointLocation.Y;
            var c1 = cd.FirstPointLocation.X;
            var c2 = cd.FirstPointLocation.Y;
            var d1 = cd.LastPointLocation.X;
            var d2 = cd.LastPointLocation.Y;
            var condition_1 = (a1 - b1) * (c2 - d2) - (c1 - d1) * (a2 - b2);
            var condition_2 = (a1 - c1) * (b2 - d2) - (b1 - d1) * (a2 - c2);
            var condition_3 = (a1 - d1) * (c2 - b2) - (c1 - b1) * (a2 - d2);
            var g = (FloatingMath.NearlyEqual(condition_1, 0) && FloatingMath.NearlyEqual(condition_2, 0) && FloatingMath.NearlyEqual(condition_3, 0));
            return g;
        };
        R2Line.prototype.GetAngleWithReferencePoint = function (point) {
            if (this.FirstPoint.Equals(point))
                return this.Angle;
            if (this.LastPoint.Equals(point)) {
                var v = WebGL.Vector3.Subtract(this.FirstPointLocation, this.LastPointLocation);
                return this.CalculateAngle(v.Xy);
            }
            throw "the point don't belong to this line";
        };
        Object.defineProperty(R2Line.prototype, "Angle", {
            get: function () {
                return this.CalculateAngle(this.Vector);
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.CalculateAngle = function (v) {
            var aa = WebGL.Vector3.CalculateAngle(v.Normalized.ToVector3(), WebGL.Vector3.UnitX);
            var cr = WebGL.Vector3.Cross(v.Normalized.ToVector3(), WebGL.Vector3.UnitX);
            if (cr.Z <= 0) {
                return new Angle(aa);
            }
            else {
                return new Angle(Angle.Pi.Radians + (Angle.Pi.Radians - aa));
            }
        };
        R2Line.prototype.GetRelativeAngle = function (line) {
            return Angle.GetDeltaAngle(line.Angle, this.Angle);
        };
        R2Line.prototype.GetRelativeAngleWithReference = function (line, referencePoint) {
            var lineAngle = line.GetAngleWithReferencePoint(referencePoint);
            var thisAngle = this.GetAngleWithReferencePoint(referencePoint);
            var deltaAngle = Angle.GetDeltaAngle(lineAngle, thisAngle);
            return deltaAngle;
        };
        R2Line.prototype.IsLyingOn = function (line) {
            var lyingPoints = -1;
            if (R2Line.IsCollinear(this, line)) {
                lyingPoints = 0;
                if (line.IsPointOnLineSegment(this.FirstPointLocation.Xy) == true ||
                    FloatingMath.Vector3NearlyEqual(line.FirstPointLocation, this.FirstPointLocation) ||
                    FloatingMath.Vector3NearlyEqual(line.LastPointLocation, this.FirstPointLocation)) {
                    lyingPoints++;
                }
                if (line.IsPointOnLineSegment(this.LastPointLocation.Xy) == true ||
                    FloatingMath.Vector3NearlyEqual(line.FirstPointLocation, this.LastPointLocation) ||
                    FloatingMath.Vector3NearlyEqual(line.LastPointLocation, this.LastPointLocation)) {
                    lyingPoints++;
                }
            }
            return lyingPoints;
        };
        R2Line.prototype.IsPerpendicularTo = function (line) {
            var vv = WebGL.Vector2.Dot(this.Vector.Normalized, line.Vector.Normalized);
            if (FloatingMath.NearlyEqual(vv, 0))
                return true;
            return false;
        };
        R2Line.prototype.IntersectionWithLineSegment = function (line) {
            if (this.FirstPointLocation.Equals(line.FirstPointLocation))
                return this.FirstPointLocation.Fixed.Xy;
            if (this.FirstPointLocation.Equals(line.LastPointLocation))
                return this.FirstPointLocation.Fixed.Xy;
            if (this.LastPointLocation.Equals(line.FirstPointLocation))
                return this.LastPointLocation.Fixed.Xy;
            if (this.LastPointLocation.Equals(line.LastPointLocation))
                return this.LastPointLocation.Fixed.Xy;
            var lli = this.IntersectionWithRay(line.FirstPointLocation.Xy, line.LastPointLocation.Xy);
            if (lli != null) {
                if (this.IsPointOnLineSegment(lli) == true && line.IsPointOnLineSegment(lli) == true) {
                    return lli;
                }
            }
            return null;
        };
        Object.defineProperty(R2Line.prototype, "TerminalPoints", {
            get: function () {
                var vv = new Array();
                vv.push(this.FirstPointLocation.Xy);
                vv.push(this.LastPointLocation.Xy);
                return vv;
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.GetIntersectionsWith = function (shape) {
            var intersections = new Array();
            if (shape instanceof R2Line) {
                var targetLine = shape;
                var iL = this.IntersectionWithLineSegment(targetLine);
                if (iL != null)
                    intersections.push(iL);
            }
            if (shape instanceof Shapes.R2Arc) {
                var targetArc = shape;
                var iA = targetArc.IntersectionWithRay(this.FirstPointLocation.Xy, this.LastPointLocation.Xy);
                if (iA.FirstPoint != null && targetArc.IsPointOnArc(iA.FirstPoint) != null && this.IsPointOnLineSegment(iA.FirstPoint)) {
                    intersections.push(iA.FirstPoint);
                }
                if (iA.SecondPoint != null && targetArc.IsPointOnArc(iA.SecondPoint) != null && this.IsPointOnLineSegment(iA.SecondPoint)) {
                    intersections.push(iA.SecondPoint);
                }
            }
            if (shape instanceof Shapes.R2Circle) {
                var targetCircle = shape;
                var iA = targetCircle.IntersectionWithRay(this.FirstPointLocation.Xy, this.LastPointLocation.Xy);
                if (iA.FirstPoint != null && this.IsPointOnLineSegment(iA.FirstPoint)) {
                    intersections.push(iA.FirstPoint);
                }
                if (iA.SecondPoint != null && this.IsPointOnLineSegment(iA.SecondPoint)) {
                    intersections.push(iA.SecondPoint);
                }
            }
            if (shape instanceof Shapes.R2Rectangle) {
                var targetRectangle = shape;
                var iR = this.IntersectionWithLineSegment(targetRectangle.TopLine);
                if (iR != null)
                    intersections.push(iR);
                iR = this.IntersectionWithLineSegment(targetRectangle.RightLine);
                if (iR != null)
                    intersections.push(iR);
                iR = this.IntersectionWithLineSegment(targetRectangle.BottomLine);
                if (iR != null)
                    intersections.push(iR);
                iR = this.IntersectionWithLineSegment(targetRectangle.LeftLine);
                if (iR != null)
                    intersections.push(iR);
            }
            return intersections;
        };
        R2Line.prototype.ApplySegmentation = function () {
            var ipoints = this.IntersectedPoints;
            var pc = new PointsCloud();
            for (var key in this.IntersectedShapes) {
                var ShapeIntersectedPoints = this.IntersectedShapes[key];
                ShapeIntersectedPoints.forEach(function (x) { return pc.AddPoint(key, x.ToVector3()); });
            }
            pc.AddPoint(this.ColorKey, this.FirstPointLocation);
            pc.AddPoint(this.ColorKey, this.LastPointLocation);
            var orderd_points = pc.OrderByLength(this.FirstPointLocation.Xy);
            this._InnerSegments = new Array();
            var io;
            for (io = 0; io < orderd_points.length - 1; io++) {
                var LineSegment = new Shapes.R2Line();
                LineSegment.FirstPointLocation = orderd_points[io].ToVector3();
                LineSegment.LastPointLocation = orderd_points[io + 1].ToVector3();
                if (FloatingMath.NearlyEqual(LineSegment.Length, 0)) {
                }
                else {
                    this._InnerSegments.push(LineSegment);
                }
            }
        };
        R2Line.prototype.GetPointProjection = function (point) {
            var pp = PointsCloud.ProjectPointOn(point, this.FirstPointLocation, this.LastPointLocation);
            return pp.Xy;
        };
        R2Line.prototype.GetNearestPoint = function (point) {
            return this.GetPointProjection(point.ToVector3());
        };
        R2Line.prototype.IsPointTerminal = function (point) {
            if (point == null)
                return false;
            if (FloatingMath.Vector3NearlyEqual(point, this.FirstPointLocation) ||
                FloatingMath.Vector3NearlyEqual(point, this.FirstPointLocation))
                return true;
            if (FloatingMath.Vector3NearlyEqual(point, this.LastPointLocation) ||
                FloatingMath.Vector3NearlyEqual(point, this.LastPointLocation))
                return true;
            return false;
        };
        R2Line.prototype.IsHavingSameTerminals = function (line) {
            var lyingPoints = 0;
            if (FloatingMath.Vector3NearlyEqual(line.FirstPointLocation, this.FirstPointLocation) ||
                FloatingMath.Vector3NearlyEqual(line.LastPointLocation, this.FirstPointLocation)) {
                lyingPoints++;
            }
            if (FloatingMath.Vector3NearlyEqual(line.FirstPointLocation, this.LastPointLocation) ||
                FloatingMath.Vector3NearlyEqual(line.LastPointLocation, this.LastPointLocation)) {
                lyingPoints++;
            }
            if (lyingPoints == 2)
                return true;
            return false;
        };
        Object.defineProperty(R2Line.prototype, "Length", {
            get: function () {
                return this.Vector.Length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "IsZero", {
            get: function () {
                return FloatingMath.NearlyEqual(this.Length, 0);
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.GetTheOtherPoint = function (source) {
            if (this.LastPoint.Equals(source))
                return this.FirstPoint;
            if (this.FirstPoint.Equals(source))
                return this.LastPoint;
            throw "Point is not related to this line.";
        };
        R2Line.prototype.MakeCDTXmlLine = function (firstPoint, lastPoint) {
            var xe = "  <Line BuildingType=\"";
            if (this.BuildingType == Shapes.BuildingType.Wall)
                xe += "Wall";
            if (this.BuildingType == Shapes.BuildingType.Window)
                xe += "Window";
            if (this.BuildingType == Shapes.BuildingType.Door)
                xe += "Door";
            if (this.BuildingType == Shapes.BuildingType.Opening)
                xe += "Opening";
            if (this.BuildingType == Shapes.BuildingType.Undefined)
                xe += "Undefined";
            xe += "\">";
            xe = xe.concat("\n");
            xe = xe.concat("\n    <Point>");
            xe = xe.concat("\n      <X>" + firstPoint.X.toFixed(FloatingMath.DecimalDigits) + "</X>");
            xe = xe.concat("\n      <Y>" + firstPoint.Y.toFixed(FloatingMath.DecimalDigits) + "</Y>");
            xe = xe.concat("\n      <Z>" + firstPoint.Z.toFixed(FloatingMath.DecimalDigits) + "</Z>");
            xe = xe.concat("\n    </Point>");
            xe = xe.concat("\n");
            xe = xe.concat("\n    <Point>");
            xe = xe.concat("\n      <X>" + lastPoint.X.toFixed(FloatingMath.DecimalDigits) + "</X>");
            xe = xe.concat("\n      <Y>" + lastPoint.Y.toFixed(FloatingMath.DecimalDigits) + "</Y>");
            xe = xe.concat("\n      <Z>" + lastPoint.Z.toFixed(FloatingMath.DecimalDigits) + "</Z>");
            xe = xe.concat("\n    </Point>");
            xe = xe.concat("\n  </Line>");
            return xe;
        };
        R2Line.prototype.ToCDTXElement = function () {
            if (this.BuildingShapes.length > 0) {
                var points = this.GetLinePoints();
                var lines = "";
                var ix = 0;
                for (ix = 0; ix < points.length - 1; ix += 2) {
                    lines = lines.concat("\n", this.MakeCDTXmlLine(points[ix], points[ix + 1]));
                }
                return lines;
            }
            else {
                return this.MakeCDTXmlLine(this.FirstPointLocation, this.LastPointLocation);
            }
        };
        Object.defineProperty(R2Line.prototype, "BuildingTypeText", {
            get: function () {
                if (this.BuildingType == Shapes.BuildingType.Wall)
                    return "Wall";
                if (this.BuildingType == Shapes.BuildingType.Window)
                    return "Window";
                if (this.BuildingType == Shapes.BuildingType.Door)
                    return "Door";
                if (this.BuildingType == Shapes.BuildingType.Opening)
                    return "Opening";
                return "Undefined";
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.ToSliceXElement = function () {
            var factor = 7.5;
            var slice = "  <Slice>";
            slice = slice.concat("\n", "    <Start>");
            slice = slice.concat("\n", "      <X>" + (this.FirstPointLocation.X * factor).toFixed(FloatingMath.DecimalDigits) + "</X>");
            slice = slice.concat("\n", "      <Y>" + (this.FirstPointLocation.Y * factor).toFixed(FloatingMath.DecimalDigits) + "</Y>");
            slice = slice.concat("\n", "    </Start>");
            slice = slice.concat("\n", "    <End>");
            slice = slice.concat("\n", "      <X>" + (this.LastPointLocation.X * factor).toFixed(FloatingMath.DecimalDigits) + "</X>");
            slice = slice.concat("\n", "      <Y>" + (this.LastPointLocation.Y * factor).toFixed(FloatingMath.DecimalDigits) + "</Y>");
            slice = slice.concat("\n", "    </End>");
            if (this.BuildingType == Shapes.BuildingType.Door) {
                slice = slice.concat("\n", "    <DoorPoint>");
                slice = slice.concat("\n", "      <X>" + (this.LastPointLocation.X * factor).toFixed(FloatingMath.DecimalDigits) + "</X>");
                slice = slice.concat("\n", "      <Y>" + (this.LastPointLocation.Y * factor).toFixed(FloatingMath.DecimalDigits) + "</Y>");
                slice = slice.concat("\n", "    </DoorPoint>");
            }
            else {
                slice = slice.concat("\n", "    <DoorPoint xsi:nil=\"true\" />");
            }
            slice = slice.concat("\n", "    <CurvePoint xsi:nil=\"true\" />");
            slice = slice.concat("\n", "    <IsCircle>false</IsCircle>");
            slice = slice.concat("\n", "    <Type>" + this.BuildingTypeText + "</Type>");
            slice = slice.concat("\n", "    <Base>0</Base>");
            slice = slice.concat("\n", "    <Height>0</Height>");
            slice = slice.concat("\n", "  </Slice>");
            return slice;
        };
        Object.defineProperty(R2Line.prototype, "JsonObject", {
            get: function () {
                var sh = {
                    ShapeName: "R2Line",
                    LayerName: this.LayerName,
                    ShapeContent: {
                        FirstPointLocation: { X: this.FirstPointLocation.X, Y: this.FirstPointLocation.Y },
                        LastPointLocation: { X: this.LastPointLocation.X, Y: this.LastPointLocation.Y }
                    }
                };
                return sh;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Line.prototype, "Json", {
            get: function () {
                return JSON.stringify(this.JsonObject);
            },
            enumerable: true,
            configurable: true
        });
        R2Line.prototype.OnTranslation = function (translation) {
            this.FirstPointLocation = WebGL.Vector3.Add(this.FirstPointLocation, translation.ToVector3());
            this.LastPointLocation = WebGL.Vector3.Add(this.LastPointLocation, translation.ToVector3());
        };
        R2Line.prototype.RevertToLastLocation = function () {
            this.FirstPoint.RevertToLastLocation();
            this.LastPoint.RevertToLastLocation();
        };
        R2Line.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                        gl_PointSize = 1.0;\
                    }\
                } ");
        R2Line.WallVertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform mat4 ProjectionMatrix;\
                uniform vec4 Translation;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * (Translation + ShapeCenter);\
                        gl_PointSize = 1.0;\
                    }\
                } ");
        R2Line.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        R2Line.Program = new WebGL.Shaders.PipelineProgram("R2LineProgram", R2Line.VertexShader, R2Line.FragmentShader);
        R2Line.WallProgram = new WebGL.Shaders.PipelineProgram("R2LineWallProgram", R2Line.WallVertexShader, R2Line.FragmentShader);
        R2Line.ThicknessVertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec2 position;\
                attribute vec2 normal;\
                uniform mat4 ProjectionMatrix;\
                uniform float thickness;\
                void main() {\
                    vec2 p = position.xy + vec2(normal * thickness / 2.0);\
                    gl_Position = ProjectionMatrix * vec4(p, 0.0, 1.0);\
                }");
        R2Line.ThicknessProgram = new WebGL.Shaders.PipelineProgram("R2LineThicknessProgram", R2Line.ThicknessVertexShader, R2Line.FragmentShader);
        return R2Line;
    }(Shapes.R2Shape));
    Shapes.R2Line = R2Line;
})(Shapes || (Shapes = {}));
var Shapes;
(function (Shapes) {
    var R2CrossSectionLine = (function (_super) {
        __extends(R2CrossSectionLine, _super);
        function R2CrossSectionLine() {
            _super.call(this);
            this.FirstPoint = new Shapes.R2Point();
            this.LastPoint = new Shapes.R2Point();
            this.OrientationLine = new Shapes.R2Line();
            this.EditingControls[this.FirstPoint.ColorKey] = this.FirstPoint;
            this.EditingControls[this.LastPoint.ColorKey] = this.LastPoint;
            this.AddChild(this.FirstPoint);
            this.AddChild(this.LastPoint);
            this.ShapeColor = new WebGL.Vector4(1, 0, 0, 1);
            this.OrientationLine.RightArrow = true;
        }
        Object.defineProperty(R2CrossSectionLine.prototype, "ShapeName", {
            get: function () {
                return "R2CrossSectionLine";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2CrossSectionLine.prototype, "FirstPointLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter;
            },
            set: function (value) {
                this.FirstPoint.ShapeCenter = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2CrossSectionLine.prototype, "LastPointLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter;
            },
            set: function (value) {
                this.LastPoint.ShapeCenter = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2CrossSectionLine.prototype, "Vector", {
            get: function () {
                var fp = this.FirstPointLocation;
                var sp = this.LastPointLocation;
                var dp = WebGL.Vector2.Subtract(sp.Xy, fp.Xy);
                return dp;
            },
            enumerable: true,
            configurable: true
        });
        R2CrossSectionLine.prototype.OnRender = function (gl) {
            if (!R2CrossSectionLine.Program.IsLinked(gl))
                R2CrossSectionLine.Program.Link(gl);
            R2CrossSectionLine.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2CrossSectionLine.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2CrossSectionLine.Program.ArrayVertexSize = 3;
            var lines = new Array();
            lines.push(this.FirstPointLocation);
            lines.push(this.LastPointLocation);
            R2CrossSectionLine.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
            R2CrossSectionLine.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, 2);
            R2CrossSectionLine.Program.EndUse(gl);
            this.OrientationLine.ProjectionMatrix = this.ProjectionMatrix;
            var vex = this.Normal.Extend(this.ArrowsLength / 2);
            this.OrientationLine.FirstPointLocation = WebGL.Vector3.Subtract(this.AboveCrossPoint.ToVector3(), vex.ToVector3());
            this.OrientationLine.LastPointLocation = WebGL.Vector3.Add(this.AboveCrossPoint.ToVector3(), vex.ToVector3());
            this.OrientationLine.OnRender(gl);
            this.OrientationLine.FirstPointLocation = WebGL.Vector3.Subtract(this.BelowCrossPoint.ToVector3(), vex.ToVector3());
            this.OrientationLine.LastPointLocation = WebGL.Vector3.Add(this.BelowCrossPoint.ToVector3(), vex.ToVector3());
            this.OrientationLine.OnRender(gl);
        };
        R2CrossSectionLine.prototype.OnRenderForPicking = function (gl) {
            var lines = new Array();
            lines.push(this.FirstPointLocation);
            lines.push(this.LastPointLocation);
            if (!R2CrossSectionLine.Program.IsLinked(gl))
                R2CrossSectionLine.Program.Link(gl);
            R2CrossSectionLine.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2CrossSectionLine.Program.Uniforms.ShapeColor = this.SelectionColorVector;
            R2CrossSectionLine.Program.ArrayVertexSize = 3;
            R2CrossSectionLine.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
            R2CrossSectionLine.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINES, 0, 2);
            R2CrossSectionLine.Program.EndUse(gl);
        };
        R2CrossSectionLine.prototype.ExecuteEquation = function (t) {
            var xt = this.LastPointLocation.X - this.FirstPointLocation.X;
            var yt = this.LastPointLocation.Y - this.FirstPointLocation.Y;
            var zt = this.LastPointLocation.Z - this.FirstPointLocation.Z;
            var point = new WebGL.Vector3(t * xt + this.FirstPointLocation.X, t * yt + this.FirstPointLocation.Y, t * zt + this.FirstPointLocation.Z);
            return point;
        };
        Object.defineProperty(R2CrossSectionLine.prototype, "MiddlePointLocation", {
            get: function () {
                return this.ExecuteEquation(0.5);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2CrossSectionLine.prototype, "AboveCrossPoint", {
            get: function () {
                return this.ExecuteEquation(1 - R2CrossSectionLine.TerminalOffset).Xy;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2CrossSectionLine.prototype, "BelowCrossPoint", {
            get: function () {
                return this.ExecuteEquation(R2CrossSectionLine.TerminalOffset).Xy;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2CrossSectionLine.prototype, "ArrowsLength", {
            get: function () {
                var v0 = this.ExecuteEquation(0);
                var v1 = this.ExecuteEquation(0.1);
                var v = WebGL.Vector2.Subtract(v1.Xy, v0.Xy);
                return v.Length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2CrossSectionLine.prototype, "Normal", {
            get: function () {
                var fp = this.FirstPointLocation;
                var sp = this.LastPointLocation;
                var dp = WebGL.Vector2.Subtract(sp.Xy, fp.Xy);
                dp.Normalize();
                var _NormalXY = new WebGL.Vector2(-dp.Y, dp.X);
                _NormalXY.Normalize();
                return _NormalXY;
            },
            enumerable: true,
            configurable: true
        });
        R2CrossSectionLine.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                        gl_PointSize = 1.0;\
                    }\
                } ");
        R2CrossSectionLine.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        R2CrossSectionLine.Program = new WebGL.Shaders.PipelineProgram("R2CrossSectionLineProgram", R2CrossSectionLine.VertexShader, R2CrossSectionLine.FragmentShader);
        R2CrossSectionLine.TerminalOffset = 0.05;
        return R2CrossSectionLine;
    }(Shapes.R2Shape));
    Shapes.R2CrossSectionLine = R2CrossSectionLine;
})(Shapes || (Shapes = {}));
var Shapes;
(function (Shapes) {
    var R2Arc = (function (_super) {
        __extends(R2Arc, _super);
        function R2Arc() {
            _super.call(this);
            this.FirstPoint = new Shapes.R2Point();
            this.MiddlePoint = new Shapes.R2Point();
            this.LastPoint = new Shapes.R2Point();
            this.ArcCenter = new Shapes.R2Point();
            this.FirstLine = new Shapes.R2Line();
            this.MiddleLine = new Shapes.R2Line();
            this.LastLine = new Shapes.R2Line();
            this.ArcLine = new Shapes.R2Line();
            this.RadialSegments = 40;
            this.EditingControls[this.FirstPoint.ColorKey] = this.FirstPoint;
            this.EditingControls[this.MiddlePoint.ColorKey] = this.MiddlePoint;
            this.EditingControls[this.LastPoint.ColorKey] = this.LastPoint;
            this.AddChild(this.FirstPoint);
            this.AddChild(this.MiddlePoint);
            this.AddChild(this.LastPoint);
            this.MiddlePoint.PointSize = 6;
        }
        Object.defineProperty(R2Arc.prototype, "ShapeName", {
            get: function () {
                return "R2Arc";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "FirstPointLocation", {
            get: function () {
                return this.FirstPoint.ShapeCenter;
            },
            set: function (value) {
                this.FirstPoint.ShapeCenter = value;
                this.CalculateCenterAndAngles();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MiddlePointLocation", {
            get: function () {
                return this.MiddlePoint.ShapeCenter;
            },
            set: function (value) {
                this.MiddlePoint.ShapeCenter = value;
                this.CalculateCenterAndAngles();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "LastPointLocation", {
            get: function () {
                return this.LastPoint.ShapeCenter;
            },
            set: function (value) {
                this.LastPoint.ShapeCenter = value;
                this.CalculateCenterAndAngles();
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.prototype.Refresh = function () {
            this.CalculateCenterAndAngles();
        };
        Object.defineProperty(R2Arc.prototype, "FirstVector", {
            get: function () {
                return this.FirstLine.Vector;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MiddleVector", {
            get: function () {
                return this.MiddleLine.Vector;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "LastVector", {
            get: function () {
                return this.LastLine.Vector;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "StepModifier", {
            get: function () {
                return this._StepModifier;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "FirstAngle", {
            get: function () {
                return this._FirstAngle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "LastAngle", {
            get: function () {
                return this._LastAngle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "SweepAngle", {
            get: function () {
                return Angle.Add(this.FirstAngle, this.LastAngle);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "StartAngle", {
            get: function () {
                return this._StartAngle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "EndAngle", {
            get: function () {
                var vv = this._StartAngle.Radians + (this.StepModifier * this.SweepAngle.Radians);
                return new Angle(vv);
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.prototype.CalcCenter = function () {
            var a = this.FirstPointLocation.Xy;
            var b = this.MiddlePointLocation.Xy;
            var c = this.LastPointLocation.Xy;
            var d = 2 * (a.X - c.X) * (c.Y - b.Y) + 2 * (b.X - c.X) * (a.Y - c.Y);
            var m1 = (Math.pow(a.X, 2) - Math.pow(c.X, 2) + Math.pow(a.Y, 2) - Math.pow(c.Y, 2));
            var m2 = (Math.pow(c.X, 2) - Math.pow(b.X, 2) + Math.pow(c.Y, 2) - Math.pow(b.Y, 2));
            var nx = m1 * (c.Y - b.Y) + m2 * (c.Y - a.Y);
            var ny = m1 * (b.X - c.X) + m2 * (a.X - c.X);
            var cx = nx / d;
            var cy = ny / d;
            var dx = cx - a.X;
            var dy = cy - a.Y;
            var distance = Math.sqrt(dx * dx + dy * dy);
            var c_center = new WebGL.Vector2(cx, cy);
            if (distance > 13e6)
                distance = Number.POSITIVE_INFINITY;
            var t = {
                Radius: distance, Center: c_center
            };
            return t;
        };
        Object.defineProperty(R2Arc.prototype, "Radius", {
            get: function () {
                return this._Radius;
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.prototype.CalculateCenterAndAngles = function () {
            var cc = this.CalcCenter();
            var cpl2 = cc.Center;
            this._Radius = cc.Radius;
            var cpl = new WebGL.Vector3(cpl2.X, cpl2.Y, 0);
            this.ArcCenter.ShapeCenter = cpl;
            var a = this.FirstPointLocation;
            var b = this.MiddlePointLocation;
            var c = this.LastPointLocation;
            this.FirstLine.FirstPointLocation = cpl;
            this.FirstLine.LastPointLocation = a;
            this.MiddleLine.FirstPointLocation = cpl;
            this.MiddleLine.LastPointLocation = b;
            this.LastLine.FirstPointLocation = cpl;
            this.LastLine.LastPointLocation = c;
            this.ArcLine.FirstPointLocation = a;
            this.ArcLine.LastPointLocation = c;
            var _FV = new WebGL.Vector2(this.FirstVector.X, this.FirstVector.Y);
            var _MV = new WebGL.Vector2(this.MiddleVector.X, this.MiddleVector.Y);
            var _LV = new WebGL.Vector2(this.LastVector.X, this.LastVector.Y);
            var xaxis = new WebGL.Vector2(1, 0);
            this._StartAngle = new Angle(WebGL.Vector3.CalculateAngle(xaxis.ToVector3(), _FV.ToVector3()));
            var gsa = WebGL.Vector3.Cross(xaxis.ToVector3(), _FV.ToVector3());
            if (gsa.Z < 0) {
                this._StartAngle = this._StartAngle.Negate;
            }
            this._FirstAngle = new Angle(WebGL.Vector3.CalculateAngle(_FV.ToVector3(), _MV.ToVector3()));
            this._LastAngle = new Angle(WebGL.Vector3.CalculateAngle(_MV.ToVector3(), _LV.ToVector3()));
            var vac = new Angle(WebGL.Vector3.CalculateAngle(_FV.ToVector3(), _LV.ToVector3()));
            var gvbc = WebGL.Vector3.Cross(_MV.ToVector3(), _LV.ToVector3());
            var gvab = WebGL.Vector3.Cross(_FV.ToVector3(), _MV.ToVector3());
            var vacrossvc = WebGL.Vector3.Cross(_FV.ToVector3(), _LV.ToVector3());
            this._StepModifier = 1;
            if (gvbc.Z > 0 && gvab.Z > 0) {
            }
            else if (gvbc.Z < 0 && gvab.Z < 0) {
                this._StepModifier = -1;
            }
            else if (gvbc.Z < 0 && gvab.Z > 0) {
                if (vacrossvc.Z < 0) {
                    this._LastAngle = Angle.Subtract(Angle.Two_Pi, this._LastAngle);
                }
                else {
                    this._FirstAngle = Angle.Subtract(Angle.Two_Pi, this._FirstAngle);
                    this._StepModifier = -1;
                }
            }
            else if (gvbc.Z > 0 && gvab.Z < 0) {
                if (vacrossvc.Z < 0) {
                    this._FirstAngle = Angle.Subtract(Angle.Two_Pi, this._FirstAngle);
                }
                else {
                    this._LastAngle = Angle.Subtract(Angle.Two_Pi, this._LastAngle);
                    this._StepModifier = -1;
                }
            }
        };
        Object.defineProperty(R2Arc.prototype, "IsLine", {
            get: function () {
                return this._Radius == Number.POSITIVE_INFINITY || isNaN(this._Radius);
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.prototype.CalculateArcPoints = function () {
            var step = (this.SweepAngle.Radians / this.RadialSegments) * this._StepModifier;
            var Points = new Array(this.RadialSegments + 1);
            var initial = WebGL.Quaternion.FromAxisAngle(new WebGL.Vector3(0, 0, -1), this._StartAngle.Radians);
            var rstep = WebGL.Quaternion.FromAxisAngle(new WebGL.Vector3(0, 0, -1), step);
            var total = initial;
            var vrvector = new WebGL.Vector2(this.FirstVector.Length, 0);
            var i;
            for (i = 0; i <= this.RadialSegments; i++) {
                var covector = vrvector.ToVector3().ToCoVector();
                var npm = WebGL.Matrix3.Multiply(WebGL.Matrix3.CreateFromQuaternion(total), covector);
                var tnpm = npm.Transpose();
                var vec = new WebGL.Vector2(tnpm.Row0.X, tnpm.Row0.Y);
                Points[i] = WebGL.Vector3.Add(vec.ToVector3(), this.ArcCenter.ShapeCenter);
                total = WebGL.Quaternion.Multiply(total, rstep);
            }
            return Points;
        };
        R2Arc.prototype.OnEditingControlChange = function (childShape) {
            this.CalculateCenterAndAngles();
        };
        R2Arc.prototype.OnRender = function (gl) {
            if (!R2Arc.Program.IsLinked(gl))
                R2Arc.Program.Link(gl);
            R2Arc.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Arc.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2Arc.Program.ArrayVertexSize = 3;
            if (this.IsLine) {
                var lines = new Array();
                lines.push(this.FirstPointLocation);
                lines.push(this.LastPointLocation);
                R2Arc.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
                R2Arc.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, 2);
                R2Arc.Program.EndUse(gl);
            }
            else {
                var points = this.CalculateArcPoints();
                R2Arc.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
                R2Arc.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINE_STRIP, 0, points.length);
                R2Arc.Program.EndUse(gl);
            }
            if (this.IsDrawing || this.IsSelected) {
                this.FirstPoint.ProjectionMatrix = this.ProjectionMatrix;
                this.FirstPoint.Render(gl);
                this.MiddlePoint.ProjectionMatrix = this.ProjectionMatrix;
                this.MiddlePoint.Render(gl);
                this.LastPoint.ProjectionMatrix = this.ProjectionMatrix;
                this.LastPoint.Render(gl);
            }
        };
        R2Arc.prototype.OnRenderForPicking = function (gl) {
            if (this.IsSelected)
                return;
            if (!R2Arc.Program.IsLinked(gl))
                R2Arc.Program.Link(gl);
            R2Arc.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2Arc.Program.Uniforms.ShapeColor = this.SelectionColorVector;
            R2Arc.Program.ArrayVertexSize = 3;
            if (this.IsLine) {
                var lines = new Array();
                lines.push(this.FirstPointLocation);
                lines.push(this.LastPointLocation);
                R2Arc.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
                R2Arc.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, 2);
                R2Arc.Program.EndUse(gl);
            }
            else {
                var points = this.CalculateArcPoints();
                R2Arc.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(points));
                R2Arc.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINE_STRIP, 0, points.length);
                R2Arc.Program.EndUse(gl);
            }
        };
        R2Arc.prototype.IntersectionWithRay = function (firstPointLocation, lastPointLocation) {
            var cx = this.ArcCenter.ShapeCenter.X;
            var cy = this.ArcCenter.ShapeCenter.Y;
            var x1 = firstPointLocation.X;
            var x2 = lastPointLocation.X;
            var y1 = firstPointLocation.Y;
            var y2 = lastPointLocation.Y;
            var dx = x2 - x1;
            var dy = y2 - y1;
            var A = dx * dx + dy * dy;
            var B = 2 * (dx * (x1 - cx) + dy * (y1 - cy));
            var C = (x1 - cx) * (x1 - cx) + (y1 - cy) * (y1 - cy) - this._Radius * this._Radius;
            var det = B * B - 4 * A * C;
            var firstPoint = null;
            var secondPoint = null;
            if ((A <= 0.0000001) || (det < 0)) {
                firstPoint = null;
                secondPoint = null;
            }
            else if (det == 0) {
                var t = -B / (2 * A);
                var ix1 = x1 + t * dx;
                var iy1 = y1 + t * dy;
                firstPoint = new WebGL.Vector2(FloatingMath.FixedNumber(ix1), FloatingMath.FixedNumber(iy1));
                secondPoint = null;
            }
            else {
                var t = (-B + Math.sqrt(det)) / (2 * A);
                var ix1 = x1 + t * dx;
                var iy1 = y1 + t * dy;
                t = (-B - Math.sqrt(det)) / (2 * A);
                var ix2 = x1 + t * dx;
                var iy2 = y1 + t * dy;
                firstPoint = new WebGL.Vector2(FloatingMath.FixedNumber(ix1), FloatingMath.FixedNumber(iy1));
                secondPoint = new WebGL.Vector2(FloatingMath.FixedNumber(ix2), FloatingMath.FixedNumber(iy2));
            }
            return { FirstPoint: firstPoint, SecondPoint: secondPoint };
        };
        R2Arc.prototype.IsPointOnArc = function (point) {
            if (this.FirstPointLocation.Xy.FixedEquals(point)) {
                return Angle.Zero;
            }
            if (this.LastPointLocation.Xy.FixedEquals(point)) {
                return this.SweepAngle;
            }
            var cx = this.ArcCenter.ShapeCenter.X;
            var cy = this.ArcCenter.ShapeCenter.Y;
            var point_v = new WebGL.Vector2(point.X - cx, point.Y - cy);
            var point_angle_a = new Angle(WebGL.Vector3.CalculateAngle(this.FirstVector.ToVector3(), point_v.ToVector3()));
            var r_a = WebGL.Vector3.Cross(this.FirstVector.ToVector3(), point_v.ToVector3());
            var r_c = WebGL.Vector3.Cross(this.FirstVector.ToVector3(), point_v.ToVector3());
            if (r_a.Z > 0 && r_c.Z > 0) {
                if (this.StepModifier < 0)
                    point_angle_a = Angle.Subtract(Angle.Two_Pi, point_angle_a);
            }
            else if (r_a.Z < 0 && r_c.Z > 0) {
                if (this.StepModifier > 0)
                    point_angle_a = Angle.Subtract(Angle.Two_Pi, point_angle_a);
            }
            else if (r_a.Z < 0 && r_c.Z < 0) {
                if (this.StepModifier > 0)
                    point_angle_a = Angle.Subtract(Angle.Two_Pi, point_angle_a);
            }
            else if (r_a.Z > 0 && r_c.Z < 0) {
                if (this.StepModifier < 0)
                    point_angle_a = Angle.Subtract(Angle.Two_Pi, point_angle_a);
            }
            var percentage = point_angle_a.Radians / this.SweepAngle.Radians;
            var truePointAngle = point_angle_a;
            if (percentage < 1 || FloatingMath.NearlyEqual(percentage, 1)) {
                return truePointAngle;
            }
            else {
                return null;
            }
        };
        R2Arc.prototype.GetPointAtAngle = function (desiredAngle) {
            var pointAngle = this._StartAngle.Radians + desiredAngle * this._StepModifier;
            var rotation = WebGL.Quaternion.FromAxisAngle(new WebGL.Vector3(0, 0, -1), pointAngle);
            var vrvector = new WebGL.Vector2(this.FirstVector.Length, 0);
            var covector = vrvector.ToVector3().ToCoVector();
            var npm = WebGL.Matrix3.Multiply(WebGL.Matrix3.CreateFromQuaternion(rotation), covector);
            npm.Transpose();
            var vec = new WebGL.Vector2(npm.Row0.X, npm.Row0.Y);
            return WebGL.Vector2.Add(vec, this.ArcCenter.ShapeCenter.Xy);
        };
        Object.defineProperty(R2Arc.prototype, "HalfSweepAngle", {
            get: function () {
                return new Angle(this.SweepAngle.Radians / 2.0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "ArcHalfPoint", {
            get: function () {
                return this.ExecuteEquation(1 / 2);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "ArcQuarterPoint", {
            get: function () {
                return this.ExecuteEquation(1 / 4);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "ArcThirdQuarterPoint", {
            get: function () {
                return this.ExecuteEquation(3 / 4);
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.prototype.ExecuteEquation = function (t) {
            var desiredAngle = this.SweepAngle.Radians * t;
            return this.GetPointAtAngle(desiredAngle);
        };
        R2Arc.prototype.IntersectionWithArc = function (arc) {
            var d = WebGL.Vector2.Subtract(this.ArcCenter.ShapeCenter.Xy, arc.ArcCenter.ShapeCenter.Xy).Length;
            var firstPoint;
            var secondPoint;
            if (d > this.Radius + arc.Radius) {
                firstPoint = null;
                secondPoint = null;
            }
            else if (FloatingMath.NearlyEqual(d, 0) && this.Radius == arc.Radius) {
                if ((this.FirstPointLocation.NearlyEqual(arc.FirstPointLocation) || this.FirstPointLocation.NearlyEqual(arc.LastPointLocation))
                    &&
                        (this.LastPointLocation.NearlyEqual(arc.FirstPointLocation) || this.LastPointLocation.NearlyEqual(arc.LastPointLocation))) {
                    firstPoint = this.FirstPointLocation.Xy;
                    secondPoint = this.LastPointLocation.Xy;
                }
                else {
                    firstPoint = null;
                    secondPoint = null;
                }
            }
            else if (d + Math.min(this.Radius, arc.Radius) < Math.max(this.Radius, arc.Radius)) {
                firstPoint = null;
                secondPoint = null;
            }
            else {
                var a = (this.Radius * this.Radius - arc.Radius * arc.Radius + d * d) / (2.0 * d);
                var h = Math.sqrt(this.Radius * this.Radius - a * a);
                var cx = this.ArcCenter.ShapeCenter.X;
                var cy = this.ArcCenter.ShapeCenter.Y;
                var p2 = new WebGL.Vector2(cx + (a * (arc.ArcCenter.ShapeCenter.X - cx)) / d, cy + (a * (arc.ArcCenter.ShapeCenter.Y - cy)) / d);
                var i1 = new WebGL.Vector2(p2.X + (h * (arc.ArcCenter.ShapeCenter.Y - cy) / d), p2.Y - (h * (arc.ArcCenter.ShapeCenter.X - cx) / d));
                var i2 = new WebGL.Vector2(p2.X - (h * (arc.ArcCenter.ShapeCenter.Y - cy) / d), p2.Y + (h * (arc.ArcCenter.ShapeCenter.X - cx) / d));
                i1.X = FloatingMath.FixedNumber(i1.X);
                i1.Y = FloatingMath.FixedNumber(i1.Y);
                i2.X = FloatingMath.FixedNumber(i2.X);
                i2.Y = FloatingMath.FixedNumber(i2.Y);
                firstPoint = i1;
                secondPoint = i2;
            }
            return {
                FirstPoint: firstPoint, SecondPoint: secondPoint
            };
        };
        Object.defineProperty(R2Arc.prototype, "MinimumCircleX", {
            get: function () {
                return this.ArcCenter.ShapeCenter.X - this._Radius;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MaximumCircleX", {
            get: function () {
                return this.ArcCenter.ShapeCenter.X + this._Radius;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MinimumCircleY", {
            get: function () {
                return this.ArcCenter.ShapeCenter.Y - this._Radius;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MaximumCircleY", {
            get: function () {
                return this.ArcCenter.ShapeCenter.Y + this._Radius;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MinimumX", {
            get: function () {
                if (this.IsPointOnArc(new WebGL.Vector2(this.MinimumCircleX, this.ArcCenter.ShapeCenter.Y)))
                    return this.MinimumCircleX;
                else {
                    var nns = [this.FirstPointLocation.X, this.MiddlePointLocation.X, this.LastPointLocation.X,
                        this.ArcQuarterPoint.X, this.ArcHalfPoint.X, this.ArcThirdQuarterPoint.X];
                    nns.sort(function (a, b) { return a - b; });
                    return nns[0];
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MaximumX", {
            get: function () {
                if (this.IsPointOnArc(new WebGL.Vector2(this.MaximumCircleX, this.ArcCenter.ShapeCenter.Y)))
                    return this.MaximumCircleX;
                else {
                    var nns = [this.FirstPointLocation.X, this.MiddlePointLocation.X, this.LastPointLocation.X,
                        this.ArcQuarterPoint.X, this.ArcHalfPoint.X, this.ArcThirdQuarterPoint.X];
                    nns.sort(function (a, b) { return b - a; });
                    return nns[0];
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MinimumY", {
            get: function () {
                if (this.IsPointOnArc(new WebGL.Vector2(this.ArcCenter.ShapeCenter.X, this.MinimumCircleY)))
                    return this.MinimumCircleY;
                else {
                    var nns = [this.FirstPointLocation.Y, this.MiddlePointLocation.Y, this.LastPointLocation.Y,
                        this.ArcQuarterPoint.Y, this.ArcHalfPoint.Y, this.ArcThirdQuarterPoint.Y];
                    nns.sort(function (a, b) { return a - b; });
                    return nns[0];
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "MaximumY", {
            get: function () {
                if (this.IsPointOnArc(new WebGL.Vector2(this.ArcCenter.ShapeCenter.X, this.MaximumCircleY)))
                    return this.MaximumCircleY;
                else {
                    var nns = [this.FirstPointLocation.Y, this.MiddlePointLocation.Y, this.LastPointLocation.Y,
                        this.ArcQuarterPoint.Y, this.ArcHalfPoint.Y, this.ArcThirdQuarterPoint.Y];
                    nns.sort(function (a, b) { return b - a; });
                    return nns[0];
                }
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.prototype.GetIntersectionsWith = function (shape) {
            var intersections = new Array();
            if (shape instanceof Shapes.R2Line) {
                var targetLine = shape;
                var iL = this.IntersectionWithRay(targetLine.FirstPointLocation.Xy, targetLine.LastPointLocation.Xy);
                if (iL.FirstPoint != null && this.IsPointOnArc(iL.FirstPoint) != null && targetLine.IsPointOnLineSegment(iL.FirstPoint)) {
                    intersections.push(iL.FirstPoint);
                }
                if (iL.SecondPoint != null && this.IsPointOnArc(iL.SecondPoint) != null && targetLine.IsPointOnLineSegment(iL.SecondPoint)) {
                    intersections.push(iL.SecondPoint);
                }
            }
            if (shape instanceof R2Arc) {
                var targetArc = shape;
                var iA = this.IntersectionWithArc(targetArc);
                if (iA.FirstPoint != null && targetArc.IsPointOnArc(iA.FirstPoint) != null && this.IsPointOnArc(iA.FirstPoint)) {
                    intersections.push(iA.FirstPoint);
                }
                if (iA.SecondPoint != null && targetArc.IsPointOnArc(iA.SecondPoint) != null && this.IsPointOnArc(iA.SecondPoint)) {
                    intersections.push(iA.SecondPoint);
                }
            }
            if (shape instanceof Shapes.R2Circle) {
                var targetCircle = shape;
                var iA = targetCircle.IntersectionWithArc(this);
                if (iA.FirstPoint != null && this.IsPointOnArc(iA.FirstPoint)) {
                    intersections.push(iA.FirstPoint);
                }
                if (iA.SecondPoint != null && this.IsPointOnArc(iA.SecondPoint)) {
                    intersections.push(iA.SecondPoint);
                }
            }
            if (shape instanceof Shapes.R2Rectangle) {
                var targetRectangle = shape;
                var iis = this.GetIntersectionsWith(targetRectangle.TopLine);
                iis.forEach(function (x) { return intersections.push(x); });
                iis = this.GetIntersectionsWith(targetRectangle.RightLine);
                iis.forEach(function (x) { return intersections.push(x); });
                iis = this.GetIntersectionsWith(targetRectangle.BottomLine);
                iis.forEach(function (x) { return intersections.push(x); });
                iis = this.GetIntersectionsWith(targetRectangle.LeftLine);
                iis.forEach(function (x) { return intersections.push(x); });
            }
            return intersections;
        };
        Object.defineProperty(R2Arc.prototype, "TerminalPoints", {
            get: function () {
                var vv = new Array();
                vv.push(this.FirstPointLocation.Xy);
                vv.push(this.LastPointLocation.Xy);
                return vv;
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.prototype.ApplySegmentation = function () {
            var ipoints = this.IntersectedPoints;
            var pc = new PointsCloud();
            pc.AddPoint(this.ColorKey, this.FirstPointLocation);
            for (var key in this.IntersectedShapes) {
                var ShapeIntersectedPoints = this.IntersectedShapes[key];
                ShapeIntersectedPoints.forEach(function (x) { return pc.AddPoint(key, x.ToVector3()); });
            }
            pc.AddPoint(this.ColorKey, this.LastPointLocation);
            var orderd_points = pc.OrderCounterClockWise(this.ArcCenter.ShapeCenter.Xy);
            if (this.StepModifier < 1) {
                while (orderd_points[0].Equals(this.LastPointLocation.Xy) == false) {
                    var ss = orderd_points.shift();
                    orderd_points.push(ss);
                }
            }
            else {
                while (orderd_points[0].Equals(this.FirstPointLocation.Xy) == false) {
                    var sg = orderd_points.shift();
                    orderd_points.push(sg);
                }
            }
            this._InnerSegments = new Array();
            var io;
            for (io = 0; io < orderd_points.length - 1; io++) {
                var arc = R2Arc.MakeCcwArc(this.ArcCenter.ShapeCenter.Xy, orderd_points[io], orderd_points[io + 1]);
                this._InnerSegments.push(arc);
            }
        };
        R2Arc.MakeCcwArc = function (center, firstPoint, lastPoint) {
            var FTP = WebGL.Vector2.Subtract(firstPoint, center);
            var LTP = WebGL.Vector2.Subtract(lastPoint, center);
            var PF = FTP.ToPolarVector();
            var PL = LTP.ToPolarVector();
            var DeltaAngle;
            if (PF.Theta.Quadrant == AngleQuadrant.First) {
                if (PL.Theta.Quadrant == AngleQuadrant.First) {
                    DeltaAngle = Angle.Subtract(PL.Theta, PF.Theta);
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Second) {
                    DeltaAngle = Angle.Subtract(PL.Theta, PF.Theta);
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Third) {
                    DeltaAngle = Angle.Subtract(PL.Theta.AbsoluteAngle, PF.Theta);
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Fourth) {
                    DeltaAngle = Angle.Subtract(PL.Theta.AbsoluteAngle, PF.Theta);
                }
            }
            if (PF.Theta.Quadrant == AngleQuadrant.Second) {
                if (PL.Theta.Quadrant == AngleQuadrant.First) {
                    DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.Theta, PL.Theta));
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Second) {
                    DeltaAngle = Angle.Subtract(PL.Theta, PF.Theta);
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Third) {
                    DeltaAngle = Angle.Subtract(PL.Theta.AbsoluteAngle, PF.Theta);
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Fourth) {
                    DeltaAngle = Angle.Subtract(PL.Theta.AbsoluteAngle, PF.Theta);
                }
            }
            if (PF.Theta.Quadrant == AngleQuadrant.Third) {
                if (PL.Theta.Quadrant == AngleQuadrant.First) {
                    DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.Theta.AbsoluteAngle, PL.Theta));
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Second) {
                    DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.Theta.AbsoluteAngle, PL.Theta));
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Third) {
                    DeltaAngle = Angle.Subtract(PL.Theta.AbsoluteAngle, PF.Theta.AbsoluteAngle);
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Fourth) {
                    DeltaAngle = Angle.Subtract(PL.Theta.AbsoluteAngle, PF.Theta.AbsoluteAngle);
                }
            }
            if (PF.Theta.Quadrant == AngleQuadrant.Fourth) {
                if (PL.Theta.Quadrant == AngleQuadrant.First) {
                    DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.Theta.AbsoluteAngle, PL.Theta));
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Second) {
                    DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.Theta.AbsoluteAngle, PL.Theta));
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Third) {
                    DeltaAngle = Angle.Subtract(Angle.Two_Pi, Angle.Subtract(PF.Theta.AbsoluteAngle, PL.Theta.AbsoluteAngle));
                }
                if (PL.Theta.Quadrant == AngleQuadrant.Fourth) {
                    DeltaAngle = Angle.Subtract(PL.Theta.AbsoluteAngle, PF.Theta.AbsoluteAngle);
                }
            }
            if (PF.Theta.Quadrant == PL.Theta.Quadrant) {
                if (DeltaAngle.Radians < 0) {
                    DeltaAngle = new Angle(Angle.Two_Pi.Radians - Math.abs(DeltaAngle.Radians));
                }
            }
            var HalfDeltaAngle = new Angle(DeltaAngle.Radians / 2);
            var MiddleAngle = Angle.Add(PF.Theta.AbsoluteAngle, HalfDeltaAngle);
            var PM = new WebGL.PolarVector(PF.Radius, MiddleAngle.RelativeAngle);
            var MiddlePoint = WebGL.Vector2.Add(center, PM.ToCartesian());
            var arc = new Shapes.R2Arc();
            arc.FirstPointLocation = firstPoint.ToVector3();
            arc.MiddlePointLocation = MiddlePoint.ToVector3();
            arc.LastPointLocation = lastPoint.ToVector3();
            return arc;
        };
        Object.defineProperty(R2Arc.prototype, "JsonObject", {
            get: function () {
                var sh = {
                    ShapeName: "R2Arc",
                    LayerName: this.LayerName,
                    ShapeContent: {
                        FirstPointLocation: { X: this.FirstPointLocation.X, Y: this.FirstPointLocation.Y },
                        MiddlePointLocation: { X: this.MiddlePointLocation.X, Y: this.MiddlePointLocation.Y },
                        LastPointLocation: { X: this.LastPointLocation.X, Y: this.LastPointLocation.Y }
                    }
                };
                return sh;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Arc.prototype, "Json", {
            get: function () {
                return JSON.stringify(this.JsonObject);
            },
            enumerable: true,
            configurable: true
        });
        R2Arc.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                    }\
                } ");
        R2Arc.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        R2Arc.Program = new WebGL.Shaders.PipelineProgram("R2ArcProgram", R2Arc.VertexShader, R2Arc.FragmentShader);
        return R2Arc;
    }(Shapes.R2Shape));
    Shapes.R2Arc = R2Arc;
})(Shapes || (Shapes = {}));
var Meshing;
(function (Meshing) {
    var MeshingException = (function () {
        function MeshingException() {
        }
        return MeshingException;
    }());
    Meshing.MeshingException = MeshingException;
    (function (MeshSpaceType) {
        MeshSpaceType[MeshSpaceType["Open"] = 0] = "Open";
        MeshSpaceType[MeshSpaceType["Closed"] = 1] = "Closed";
    })(Meshing.MeshSpaceType || (Meshing.MeshSpaceType = {}));
    var MeshSpaceType = Meshing.MeshSpaceType;
    var MeshSpace = (function () {
        function MeshSpace(spaceNumber) {
            this.SpaceType = null;
            this.SpaceNumber = 0;
            this.InnerNodes = null;
            this.EdgeNodes = null;
            this._FilteredEdgeNodes = null;
            this._CCWLoopEdgeNodes = null;
            this._InnerSpaces = new Array();
            this._NeighbourSpaces = new Array();
            this.SpaceNumber = spaceNumber;
        }
        MeshSpace.GetNewSpaceNumber = function () {
            this.LastSpaceNumber = this.LastSpaceNumber + 1;
            return this.LastSpaceNumber;
        };
        MeshSpace.ResetNumbering = function () {
            MeshSpace.LastSpaceNumber = 0;
        };
        Object.defineProperty(MeshSpace.prototype, "FilteredEdgeNodes", {
            get: function () {
                if (this._FilteredEdgeNodes == null) {
                    this._FilteredEdgeNodes = new Array();
                    var inode;
                    for (inode = 0; inode < this.EdgeNodes.length; inode++) {
                        var enode = this.EdgeNodes[inode];
                        var AroundSpacesCount = 0;
                        var TotalAroundNodes = 0;
                        if (enode.TopNode != null) {
                            TotalAroundNodes++;
                            if (enode.TopNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.TopRightNode != null) {
                            TotalAroundNodes++;
                            if (enode.TopRightNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.RightNode != null) {
                            TotalAroundNodes++;
                            if (enode.RightNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.BottomRightNode != null) {
                            TotalAroundNodes++;
                            if (enode.BottomRightNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.BottomNode != null) {
                            TotalAroundNodes++;
                            if (enode.BottomNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.BottomLeftNode != null) {
                            TotalAroundNodes++;
                            if (enode.BottomLeftNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.LeftNode != null) {
                            TotalAroundNodes++;
                            if (enode.LeftNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.TopLeftNode != null) {
                            TotalAroundNodes++;
                            if (enode.TopLeftNode.IsInSpace(this.SpaceNumber))
                                AroundSpacesCount++;
                        }
                        if (enode.IsCorner) {
                            this._FilteredEdgeNodes.push(enode);
                        }
                        else {
                            if (AroundSpacesCount == TotalAroundNodes) {
                            }
                            else {
                                this._FilteredEdgeNodes.push(enode);
                            }
                        }
                    }
                }
                return this._FilteredEdgeNodes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshSpace.prototype, "CornerNodes", {
            get: function () {
                var gg = new Array();
                this.EdgeNodes.forEach(function (x) {
                    if (x.IsCorner)
                        gg.push(x);
                });
                return gg;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshSpace.prototype, "AverageCenterPoint", {
            get: function () {
                if (this._AverageCenterPoint == null) {
                    var TotalVectorsSum = new WebGL.Vector2(0, 0);
                    this.FilteredEdgeNodes.forEach(function (x) {
                        TotalVectorsSum = WebGL.Vector2.Add(TotalVectorsSum, x.Location);
                    });
                    this._AverageCenterPoint = new WebGL.Vector2(TotalVectorsSum.X / this.FilteredEdgeNodes.length, TotalVectorsSum.Y / this.FilteredEdgeNodes.length);
                }
                return this._AverageCenterPoint;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshSpace.prototype, "CCWLoopEdgeNodes", {
            get: function () {
                var _this = this;
                if (this._CCWLoopEdgeNodes == null) {
                    this._CCWLoopEdgeNodes = this.FilteredEdgeNodes;
                    this._CCWLoopEdgeNodes.sort(function (ia, ib) {
                        var aPolar = WebGL.Vector2.Subtract(ia.Location, _this.AverageCenterPoint).ToPolarVector();
                        var bPolar = WebGL.Vector2.Subtract(ib.Location, _this.AverageCenterPoint).ToPolarVector();
                        if (aPolar.Theta.AbsoluteAngle > bPolar.Theta.AbsoluteAngle)
                            return -1;
                        else if (aPolar.Theta.AbsoluteAngle < bPolar.Theta.AbsoluteAngle)
                            return 1;
                        else {
                            if (aPolar.Radius > bPolar.Radius)
                                return 1;
                            else if (aPolar.Radius < bPolar.Radius)
                                return -1;
                            else {
                                return 0;
                            }
                        }
                    });
                }
                return this._CCWLoopEdgeNodes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshSpace.prototype, "InnerSpaces", {
            get: function () {
                return this._InnerSpaces;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshSpace.prototype, "NeighbourSpaces", {
            get: function () {
                return this._NeighbourSpaces;
            },
            enumerable: true,
            configurable: true
        });
        MeshSpace.prototype.DiscoverRelatedSpaces = function (spaces) {
            var ispace;
            for (ispace = 0; ispace < spaces.length; ispace++) {
                var space = spaces[ispace];
                if (space.SpaceNumber == this.SpaceNumber)
                    continue;
                if (space.SpaceType == MeshSpaceType.Open) {
                    this._NeighbourSpaces.push(space);
                    continue;
                }
                var inode;
                var spacenodesCounter = 0;
                for (inode = 0; inode < this.EdgeNodes.length; inode++) {
                    var enode = this.EdgeNodes[inode];
                    if (enode.IsInSpace(space.SpaceNumber))
                        spacenodesCounter++;
                }
                if (space.EdgeNodes.length == spacenodesCounter) {
                    this._InnerSpaces.push(space);
                }
                else if (spacenodesCounter == 0) {
                }
                else {
                    this._NeighbourSpaces.push(space);
                }
            }
        };
        MeshSpace.LastSpaceNumber = 0;
        return MeshSpace;
    }());
    Meshing.MeshSpace = MeshSpace;
    (function (CoordinateLineGenerationType) {
        CoordinateLineGenerationType[CoordinateLineGenerationType["Natural"] = 0] = "Natural";
        CoordinateLineGenerationType[CoordinateLineGenerationType["Deduced"] = 1] = "Deduced";
    })(Meshing.CoordinateLineGenerationType || (Meshing.CoordinateLineGenerationType = {}));
    var CoordinateLineGenerationType = Meshing.CoordinateLineGenerationType;
    var CoordinateLine = (function () {
        function CoordinateLine() {
            this.Origin = new WebGL.Vector2(0, 0);
            this.Through = new WebGL.Vector2(0, 0);
            this.Nodes = new Object();
            this.NodesArray = new Array();
            this.NodesLengthes = new Array();
        }
        CoordinateLine.prototype.AddNode = function (node) {
            this.Nodes[node.Key] = node;
            var colength = 0;
            if (this.IsHorizontal)
                colength = node.X;
            else if (this.IsVertical)
                colength = node.Y;
            else {
                throw "non perpendicular lines are not supported";
            }
            if (this.NodesArray.length > 0) {
                var ia;
                var inserted = false;
                for (ia = 0; ia < this.NodesArray.length; ia++) {
                    var coordlength = this.NodesLengthes[ia];
                    if (coordlength > colength) {
                        this.NodesLengthes.splice(ia, 0, colength);
                        this.NodesArray.splice(ia, 0, node);
                        inserted = true;
                        break;
                    }
                }
                if (!inserted) {
                    this.NodesArray.push(node);
                    this.NodesLengthes.push(colength);
                }
            }
            else {
                this.NodesArray.push(node);
                this.NodesLengthes.push(colength);
            }
        };
        Object.defineProperty(CoordinateLine.prototype, "NormalizedVector", {
            get: function () {
                var v = WebGL.Vector2.Subtract(this.Through, this.Origin);
                v.Normalize();
                return v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoordinateLine.prototype, "IsHorizontal", {
            get: function () {
                if (this.Origin.Y == this.Through.Y)
                    return true;
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoordinateLine.prototype, "IsVertical", {
            get: function () {
                if (this.Origin.X == this.Through.X)
                    return true;
                return false;
            },
            enumerable: true,
            configurable: true
        });
        CoordinateLine.prototype.RetrieveNodeAtPoint = function (point) {
            var node = null;
            if (this.Nodes.hasOwnProperty(point.Key)) {
                node = this.Nodes[point.Key];
            }
            return node;
        };
        CoordinateLine.prototype.IntersectWithLine = function (line) {
            var point = line.IntersectionWithRay(this.Origin, this.Through);
            return point;
        };
        CoordinateLine.prototype.IntersectWithArc = function (arc) {
            var ci = arc.IntersectionWithRay(this.Origin, this.Through);
            return ci;
        };
        CoordinateLine.prototype.SearchForNodeForward = function (refPoint) {
            var ii;
            for (ii = 0; ii < this.NodesLengthes.length; ii++) {
                if (this.IsHorizontal)
                    if (this.NodesLengthes[ii] > refPoint.X)
                        return this.NodesArray[ii];
                if (this.IsVertical)
                    if (this.NodesLengthes[ii] > refPoint.Y)
                        return this.NodesArray[ii];
            }
            return null;
        };
        CoordinateLine.prototype.SearchForNodeTypeForward = function (refPoint, type) {
            var n = this.SearchForNodeForward(refPoint);
            while (n != null) {
                if (n.Type == type)
                    return n;
                n = this.SearchForNodeForward(n.Location);
            }
            return n;
        };
        CoordinateLine.prototype.SearchForNodeBackward = function (refPoint) {
            var ii;
            for (ii = this.NodesLengthes.length - 1; ii >= 0; ii--) {
                if (this.IsHorizontal)
                    if (this.NodesLengthes[ii] < refPoint.X)
                        return this.NodesArray[ii];
                if (this.IsVertical)
                    if (this.NodesLengthes[ii] < refPoint.Y)
                        return this.NodesArray[ii];
            }
            return null;
        };
        CoordinateLine.prototype.SearchForNodeTypeBackward = function (refPoint, type) {
            var n = this.SearchForNodeBackward(refPoint);
            while (n != null) {
                if (n.Type == type)
                    return n;
                n = this.SearchForNodeBackward(n.Location);
            }
            return n;
        };
        Object.defineProperty(CoordinateLine.prototype, "EdgeNodes", {
            get: function () {
                var edgenodes = new Array();
                for (var key in this.Nodes) {
                    var node = this.Nodes[key];
                    if (node.Type == NodeType.Edge)
                        edgenodes.push(node);
                }
                return edgenodes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CoordinateLine.prototype, "SpaceNodes", {
            get: function () {
                var spacenodes = new Array();
                for (var key in this.Nodes) {
                    var node = this.Nodes[key];
                    if (node.Type == NodeType.Space)
                        spacenodes.push(node);
                }
                return spacenodes;
            },
            enumerable: true,
            configurable: true
        });
        return CoordinateLine;
    }());
    Meshing.CoordinateLine = CoordinateLine;
    (function (NodeType) {
        NodeType[NodeType["Edge"] = 0] = "Edge";
        NodeType[NodeType["Space"] = 1] = "Space";
    })(Meshing.NodeType || (Meshing.NodeType = {}));
    var NodeType = Meshing.NodeType;
    var Node = (function () {
        function Node() {
            this.Type = null;
            this.IsCorner = false;
            this.HorizontalLine = null;
            this.VerticalLine = null;
            this._SpacesNumbers = new Array();
            this._LeftNode = null;
            this._RightNode = null;
            this._TopNode = null;
            this._BottomNode = null;
            this._TopLeftNode = null;
            this._BottomRightNode = null;
            this._TopRightNode = null;
            this._BottomLeftNode = null;
        }
        Object.defineProperty(Node.prototype, "X", {
            get: function () {
                return FloatingMath.FixedNumber(this._X);
            },
            set: function (value) {
                this._X = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "Y", {
            get: function () {
                return FloatingMath.FixedNumber(this._Y);
            },
            set: function (value) {
                this._Y = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "Location", {
            get: function () {
                return new WebGL.Vector2(this.X, this.Y);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "Key", {
            get: function () {
                return "X" + this.X.toFixed(FloatingMath.DecimalDigits) + "Y" + this.Y.toFixed(FloatingMath.DecimalDigits);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "CoordinateLinesCount", {
            get: function () {
                var cc = 0;
                if (this.HorizontalLine != null)
                    cc++;
                if (this.VerticalLine != null)
                    cc++;
                return cc;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "SpaceNumber", {
            get: function () {
                return this._SpacesNumbers[0];
            },
            set: function (value) {
                var ix;
                var existsBefore = false;
                for (ix = 0; ix < this._SpacesNumbers.length; ix++) {
                    if (this._SpacesNumbers[ix] == value) {
                        existsBefore = true;
                    }
                }
                if (!existsBefore)
                    this._SpacesNumbers.push(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "SpacesCount", {
            get: function () {
                return this._SpacesNumbers.length;
            },
            enumerable: true,
            configurable: true
        });
        Node.prototype.IsInSpace = function (spaceNumber) {
            var ix;
            for (ix = 0; ix < this._SpacesNumbers.length; ix++) {
                if (this._SpacesNumbers[ix] == spaceNumber)
                    return true;
            }
            return false;
        };
        Object.defineProperty(Node.prototype, "LeftNode", {
            get: function () {
                return this._LeftNode;
            },
            set: function (value) {
                this._LeftNode = value;
                if (value != null)
                    value._RightNode = this;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "RightNode", {
            get: function () {
                return this._RightNode;
            },
            set: function (value) {
                this._RightNode = value;
                if (value != null)
                    value._LeftNode = this;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "TopNode", {
            get: function () {
                return this._TopNode;
            },
            set: function (value) {
                this._TopNode = value;
                if (value != null)
                    value._BottomNode = this;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "BottomNode", {
            get: function () {
                return this._BottomNode;
            },
            set: function (value) {
                this._BottomNode = value;
                if (value != null)
                    value._TopNode = this;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "TopLeftNode", {
            get: function () {
                return this._TopLeftNode;
            },
            set: function (value) {
                this._TopLeftNode = value;
                if (value != null)
                    value._BottomRightNode = this;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "BottomRightNode", {
            get: function () {
                return this._BottomRightNode;
            },
            set: function (value) {
                this._BottomRightNode = value;
                if (value != null)
                    value._TopLeftNode = this;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "TopRightNode", {
            get: function () {
                return this._TopRightNode;
            },
            set: function (value) {
                this._TopRightNode = value;
                if (value != null)
                    value._BottomLeftNode = this;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "BottomLeftNode", {
            get: function () {
                return this._BottomLeftNode;
            },
            set: function (value) {
                this._BottomLeftNode = value;
                if (value != null)
                    value._TopRightNode = this;
            },
            enumerable: true,
            configurable: true
        });
        return Node;
    }());
    Meshing.Node = Node;
    var MeshGrid = (function () {
        function MeshGrid() {
            this.HorizontalLines = new Array();
            this.VerticalLines = new Array();
            this.HorizontalFrom = -5;
            this.HorizontalTo = 5;
            this.VerticalFrom = -5;
            this.VerticalTo = 5;
            this.HorizontalSpacesCount = 4;
            this.VerticalSpacesCount = 4;
            this.Shapes = new Array();
            this._MeshSpaces = new Array();
            this._EdgeNodes = null;
            this._EdgeNodesLocations = null;
            this._CurrentSpaceNodes = null;
            this._CurrentSpaceNodesLocations = null;
            this.BoundaryMargin = 2;
            this._NaturalCoordinateLines = null;
            this._DeducedCoordinateLines = null;
            this.NaturalGridLinesColor = new WebGL.Vector4(0, 0, 0.5, 1);
            this.DeducedGridLinesColor = new WebGL.Vector4(0, 0.5, 0.5, 1);
            this.WholeGridLinesColor = new WebGL.Vector4(0, 1, 0.5, 1);
            this.ShowNaturalGridLines = true;
            this.ShowDeducedGridLines = true;
            this.ShowEdgeNodes = true;
            this.CurrentShowSpaceNodes = true;
        }
        Object.defineProperty(MeshGrid.prototype, "HorizontalStep", {
            get: function () {
                var length = this.HorizontalTo - this.HorizontalFrom;
                return length / this.HorizontalSpacesCount;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshGrid.prototype, "VerticalStep", {
            get: function () {
                var length = this.VerticalTo - this.VerticalFrom;
                return length / this.VerticalSpacesCount;
            },
            enumerable: true,
            configurable: true
        });
        MeshGrid.prototype.GenerateHorizontalCoordinateLines = function () {
            var iy;
            for (iy = this.VerticalFrom; iy <= this.VerticalTo; iy += this.VerticalStep) {
                var ho = new CoordinateLine();
                ho.Origin.X = FloatingMath.FixedNumber(this.HorizontalFrom);
                ho.Origin.Y = FloatingMath.FixedNumber(iy);
                ho.Through.X = FloatingMath.FixedNumber(this.HorizontalTo);
                ho.Through.Y = FloatingMath.FixedNumber(iy);
                ho.GenerationType = CoordinateLineGenerationType.Natural;
                this.HorizontalLines.push(ho);
            }
        };
        MeshGrid.prototype.GenerateVerticalCoordinateLines = function () {
            var ix;
            for (ix = this.HorizontalFrom; ix <= this.HorizontalTo; ix += this.HorizontalStep) {
                var vi = new CoordinateLine();
                vi.Origin.X = FloatingMath.FixedNumber(ix);
                vi.Origin.Y = FloatingMath.FixedNumber(this.VerticalFrom);
                vi.Through.X = FloatingMath.FixedNumber(ix);
                vi.Through.Y = FloatingMath.FixedNumber(this.VerticalTo);
                vi.GenerationType = CoordinateLineGenerationType.Natural;
                this.VerticalLines.push(vi);
            }
        };
        MeshGrid.prototype.HorizontalCoordinateExistsFor = function (point) {
            var iy;
            for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                var horizontal_coordinate = this.HorizontalLines[iy];
                if (point.Y == horizontal_coordinate.Origin.Y)
                    return horizontal_coordinate;
            }
            return null;
        };
        MeshGrid.prototype.VerticalCoordinateExistsFor = function (point) {
            var ix;
            for (ix = 0; ix < this.VerticalLines.length; ix++) {
                var vertical_coordinate = this.VerticalLines[ix];
                if (point.X == vertical_coordinate.Origin.X)
                    return vertical_coordinate;
            }
            return null;
        };
        MeshGrid.prototype.DeduceHorizontalCoordinateLineFor = function (point) {
            var ho = new CoordinateLine();
            ho.Origin.X = FloatingMath.FixedNumber(this.HorizontalFrom);
            ho.Origin.Y = FloatingMath.FixedNumber(point.Y);
            ho.Through.X = FloatingMath.FixedNumber(this.HorizontalTo);
            ho.Through.Y = FloatingMath.FixedNumber(point.Y);
            ho.GenerationType = CoordinateLineGenerationType.Deduced;
            this.HorizontalLines.push(ho);
            return ho;
        };
        MeshGrid.prototype.DeduceVerticalCoordinateLineFor = function (point) {
            var vi = new CoordinateLine();
            vi.Origin.X = FloatingMath.FixedNumber(point.X);
            vi.Origin.Y = FloatingMath.FixedNumber(this.VerticalFrom);
            vi.Through.X = FloatingMath.FixedNumber(point.X);
            vi.Through.Y = FloatingMath.FixedNumber(this.VerticalTo);
            vi.GenerationType = CoordinateLineGenerationType.Deduced;
            this.VerticalLines.push(vi);
            return vi;
        };
        MeshGrid.prototype.DeduceCoordinateLinesForTerminalPointsAndMakeIntersectionsAsCornerNodes = function () {
            var _this = this;
            var ish;
            for (ish = 0; ish < this.Shapes.length; ish++) {
                var shape = this.Shapes[ish];
                var firstlocation;
                var lastlocation;
                if (shape instanceof Shapes.R2Line) {
                    firstlocation = shape.FirstPointLocation.Xy.Fixed;
                    lastlocation = shape.LastPointLocation.Xy.Fixed;
                }
                if (shape instanceof Shapes.R2Arc) {
                    firstlocation = shape.FirstPointLocation.Xy.Fixed;
                    lastlocation = shape.LastPointLocation.Xy.Fixed;
                }
                if (this.HorizontalCoordinateExistsFor(firstlocation) == null) {
                    this.DeduceHorizontalCoordinateLineFor(firstlocation);
                }
                if (this.VerticalCoordinateExistsFor(firstlocation) == null) {
                    this.DeduceVerticalCoordinateLineFor(firstlocation);
                }
                if (this.HorizontalCoordinateExistsFor(lastlocation) == null) {
                    this.DeduceHorizontalCoordinateLineFor(lastlocation);
                }
                if (this.VerticalCoordinateExistsFor(lastlocation) == null) {
                    this.DeduceVerticalCoordinateLineFor(lastlocation);
                }
            }
            for (ish = 0; ish < this.Shapes.length; ish++) {
                var shape = this.Shapes[ish];
                var intersected_points = shape.IntersectedPoints;
                intersected_points.forEach(function (point) {
                    var pfixed = point.Fixed;
                    var HL = _this.HorizontalCoordinateExistsFor(pfixed);
                    if (HL == null) {
                        HL = _this.DeduceHorizontalCoordinateLineFor(pfixed);
                    }
                    var nh = _this.AddNodeForPoint(HL, pfixed, NodeType.Edge);
                    nh.IsCorner = true;
                    var VL = _this.VerticalCoordinateExistsFor(pfixed);
                    if (VL == null) {
                        VL = _this.DeduceVerticalCoordinateLineFor(pfixed);
                    }
                    var nv = _this.AddNodeForPoint(VL, pfixed, NodeType.Edge);
                    nv.IsCorner = true;
                });
            }
        };
        MeshGrid.prototype.AddNodeForPoint = function (coordinateLine, point, nodeType) {
            var PerpendicularLine = null;
            if (coordinateLine.IsHorizontal) {
                PerpendicularLine = this.DiscoverExactVerticalLine(point);
            }
            if (coordinateLine.IsVertical) {
                PerpendicularLine = this.DiscoverExactHorizontalLine(point);
            }
            if (PerpendicularLine != null) {
                var node = PerpendicularLine.RetrieveNodeAtPoint(point);
                if (node != null) {
                    if (coordinateLine.IsHorizontal)
                        node.HorizontalLine = coordinateLine;
                    if (coordinateLine.IsVertical)
                        node.VerticalLine = coordinateLine;
                    coordinateLine.AddNode(node);
                }
                else {
                    node = new Node();
                    node.X = point.X;
                    node.Y = point.Y;
                    node.Type = nodeType;
                    if (coordinateLine.IsHorizontal) {
                        node.HorizontalLine = coordinateLine;
                        node.VerticalLine = PerpendicularLine;
                    }
                    if (coordinateLine.IsVertical) {
                        node.VerticalLine = coordinateLine;
                        node.HorizontalLine = PerpendicularLine;
                    }
                    coordinateLine.AddNode(node);
                    PerpendicularLine.AddNode(node);
                }
                return node;
            }
            else {
                var node = coordinateLine.RetrieveNodeAtPoint(point);
                if (node == null) {
                    node = new Node();
                    node.X = point.X;
                    node.Y = point.Y;
                    node.Type = nodeType;
                    if (coordinateLine.IsHorizontal)
                        node.HorizontalLine = coordinateLine;
                    if (coordinateLine.IsVertical)
                        node.VerticalLine = coordinateLine;
                    coordinateLine.AddNode(node);
                }
                else {
                }
                return node;
            }
        };
        MeshGrid.prototype.DiscoverEdgeNodes = function () {
            var ix;
            var iy;
            var ish;
            for (ix = 0; ix < this.VerticalLines.length; ix++) {
                var vertical_coordinate = this.VerticalLines[ix];
                for (ish = 0; ish < this.Shapes.length; ish++) {
                    var shape = this.Shapes[ish];
                    if (shape instanceof Shapes.R2Line) {
                        var line = shape;
                        var point = vertical_coordinate.IntersectWithLine(line);
                        if (point != null && line.IsPointOnLineSegment(point)) {
                            this.AddNodeForPoint(vertical_coordinate, point, NodeType.Edge);
                        }
                    }
                    if (shape instanceof Shapes.R2Arc) {
                        var arc = shape;
                        var ci = vertical_coordinate.IntersectWithArc(arc);
                        if (ci.FirstPoint != null && arc.IsPointOnArc(ci.FirstPoint) != null) {
                            this.AddNodeForPoint(vertical_coordinate, ci.FirstPoint, NodeType.Edge);
                        }
                        if (ci.SecondPoint != null && arc.IsPointOnArc(ci.SecondPoint) != null) {
                            this.AddNodeForPoint(vertical_coordinate, ci.SecondPoint, NodeType.Edge);
                        }
                    }
                }
            }
            for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                var horizontal_coordinate = this.HorizontalLines[iy];
                for (ish = 0; ish < this.Shapes.length; ish++) {
                    var shape = this.Shapes[ish];
                    if (shape instanceof Shapes.R2Line) {
                        var line = shape;
                        var point = horizontal_coordinate.IntersectWithLine(line);
                        if (point != null && line.IsPointOnLineSegment(point)) {
                            this.AddNodeForPoint(horizontal_coordinate, point, NodeType.Edge);
                        }
                    }
                    if (shape instanceof Shapes.R2Arc) {
                        var arc = shape;
                        var ci = horizontal_coordinate.IntersectWithArc(shape);
                        if (ci.FirstPoint != null && arc.IsPointOnArc(ci.FirstPoint) != null) {
                            this.AddNodeForPoint(horizontal_coordinate, ci.FirstPoint, NodeType.Edge);
                        }
                        if (ci.SecondPoint != null && arc.IsPointOnArc(ci.SecondPoint) != null) {
                            this.AddNodeForPoint(horizontal_coordinate, ci.SecondPoint, NodeType.Edge);
                        }
                    }
                }
            }
        };
        MeshGrid.prototype.GetPosition = function (ix, iy) {
            if (ix < 0 || iy < 0)
                return null;
            if (ix > this.VerticalLines.length - 1 || iy > this.HorizontalLines.length - 1)
                return null;
            var vline = this.VerticalLines[ix];
            var hline = this.HorizontalLines[iy];
            return new WebGL.Vector2(vline.Origin.X, hline.Origin.Y);
        };
        MeshGrid.prototype.GetNodeAt = function (ix, iy) {
            if (ix < 0 || iy < 0)
                return null;
            if (ix > this.VerticalLines.length - 1 || iy > this.HorizontalLines.length - 1)
                return null;
            var vline = this.VerticalLines[ix];
            var hline = this.HorizontalLines[iy];
            return hline.RetrieveNodeAtPoint(new WebGL.Vector2(vline.Origin.X, hline.Origin.Y));
        };
        MeshGrid.prototype.DiscoverRightCoordinateLineIndex = function (location) {
            var ii;
            for (ii = this.VerticalLines.length - 1; ii >= 0; ii--) {
                if (this.VerticalLines[ii].Origin.X < location.X) {
                    break;
                }
            }
            if ((ii + 1) == this.VerticalLines.length)
                return -1;
            return ii + 1;
        };
        MeshGrid.prototype.DiscoverLeftCoordinateLineIndex = function (location) {
            var ii;
            for (ii = 0; ii < this.VerticalLines.length; ii++) {
                if (this.VerticalLines[ii].Origin.X > location.X) {
                    return ii - 1;
                }
            }
        };
        MeshGrid.prototype.DiscoverTopCoordinateLineIndex = function (location) {
            var ii;
            for (ii = this.HorizontalLines.length - 1; ii >= 0; ii--) {
                if (this.HorizontalLines[ii].Origin.Y < location.Y) {
                    break;
                }
            }
            if ((ii + 1) == this.HorizontalLines.length)
                return -1;
            return ii + 1;
        };
        MeshGrid.prototype.DiscoverBottomCoordinateLineIndex = function (location) {
            var ii;
            for (ii = 0; ii < this.HorizontalLines.length; ii++) {
                if (this.HorizontalLines[ii].Origin.Y > location.Y) {
                    return ii - 1;
                }
            }
        };
        MeshGrid.prototype.DiscoverNearestCoordinateLines = function (location) {
            var left = this.DiscoverLeftCoordinateLineIndex(location);
            var right = this.DiscoverRightCoordinateLineIndex(location);
            var top = this.DiscoverTopCoordinateLineIndex(location);
            var bottom = this.DiscoverBottomCoordinateLineIndex(location);
            var TL = this.GetPosition(left, top);
            var TR = this.GetPosition(right, top);
            var BL = this.GetPosition(left, bottom);
            var BR = this.GetPosition(right, bottom);
            var lengthes = new Object();
            if (TL != null)
                lengthes.TL = WebGL.Vector2.Subtract(TL, location).Length;
            else
                lengthes.TL = Number.POSITIVE_INFINITY;
            if (TR != null)
                lengthes.TR = WebGL.Vector2.Subtract(TR, location).Length;
            else
                lengthes.TR = Number.POSITIVE_INFINITY;
            if (BL != null)
                lengthes.BL = WebGL.Vector2.Subtract(BL, location).Length;
            else
                lengthes.BL = Number.POSITIVE_INFINITY;
            if (BR != null)
                lengthes.BR = WebGL.Vector2.Subtract(BR, location).Length;
            else
                lengthes.BR = Number.POSITIVE_INFINITY;
            if (lengthes.TL < lengthes.TR) {
                if (lengthes.TL < lengthes.BL) {
                    if (lengthes.TL < lengthes.BR) {
                        return { VerticalLine: this.VerticalLines[left], HorizontalLine: this.HorizontalLines[top], TruePosition: TL };
                    }
                    else {
                        return { VerticalLine: this.VerticalLines[right], HorizontalLine: this.HorizontalLines[bottom], TruePosition: BR };
                    }
                }
                else {
                    if (lengthes.BL < lengthes.BR) {
                        return { VerticalLine: this.VerticalLines[left], HorizontalLine: this.HorizontalLines[bottom], TruePosition: BL };
                    }
                    else {
                        return { VerticalLine: this.VerticalLines[right], HorizontalLine: this.HorizontalLines[bottom], TruePosition: BR };
                    }
                }
            }
            else {
                if (lengthes.TR < lengthes.BL) {
                    if (lengthes.TR < lengthes.BR) {
                        return { VerticalLine: this.VerticalLines[right], HorizontalLine: this.HorizontalLines[top], TruePosition: TR };
                    }
                    else {
                        return { VerticalLine: this.VerticalLines[right], HorizontalLine: this.HorizontalLines[bottom], TruePosition: BR };
                    }
                }
                else {
                    if (lengthes.BL < lengthes.BR) {
                        return { VerticalLine: this.VerticalLines[left], HorizontalLine: this.HorizontalLines[bottom], TruePosition: BL };
                    }
                    else {
                        return { VerticalLine: this.VerticalLines[right], HorizontalLine: this.HorizontalLines[bottom], TruePosition: BR };
                    }
                }
            }
        };
        MeshGrid.prototype.DiscoverExactHorizontalLine = function (location) {
            var iy;
            var HorizontalLine = null;
            for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                if (location.Y == this.HorizontalLines[iy].Origin.Y) {
                    HorizontalLine = this.HorizontalLines[iy];
                    break;
                }
            }
            return HorizontalLine;
        };
        MeshGrid.prototype.DiscoverExactVerticalLine = function (location) {
            var ix;
            var VerticalLine = null;
            for (ix = 0; ix < this.VerticalLines.length; ix++) {
                if (location.X == this.VerticalLines[ix].Origin.X) {
                    VerticalLine = this.VerticalLines[ix];
                    break;
                }
            }
            return VerticalLine;
        };
        MeshGrid.prototype.DiscoverTopNodeFrom = function (node) {
            var HorizontalLineIndex;
            var UpHorizontalLineIndex;
            var UpHorizontalLine;
            if (node.HorizontalLine != null) {
                HorizontalLineIndex = node.HorizontalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = this.HorizontalLines.length - 1; ii >= 0; ii--) {
                    if (this.HorizontalLines[ii].Origin.Y < node.Y) {
                        HorizontalLineIndex = ii;
                        break;
                    }
                }
            }
            UpHorizontalLineIndex = HorizontalLineIndex + 1;
            if (UpHorizontalLineIndex >= this.HorizontalLines.length)
                return null;
            UpHorizontalLine = this.HorizontalLines[UpHorizontalLineIndex];
            var loc = new WebGL.Vector2(node.X, UpHorizontalLine.Origin.Y);
            var DiscoveredNode = null;
            if (node.VerticalLine != null) {
                DiscoveredNode = node.VerticalLine.SearchForNodeForward(node.Location);
                if (DiscoveredNode != null) {
                    if (loc.Y >= DiscoveredNode.Y) {
                    }
                    else {
                        DiscoveredNode = null;
                    }
                }
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = node.X;
                    DiscoveredNode.Y = UpHorizontalLine.Origin.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = UpHorizontalLine;
                    DiscoveredNode.VerticalLine = node.VerticalLine;
                    UpHorizontalLine.AddNode(DiscoveredNode);
                    node.VerticalLine.AddNode(DiscoveredNode);
                }
                node.TopNode = DiscoveredNode;
                return DiscoveredNode;
            }
            node.TopNode = null;
            return null;
        };
        MeshGrid.prototype.DiscoverBottomNodeFrom = function (node) {
            var HorizontalLineIndex;
            var DownHorizontalLineIndex;
            var DownHorizontalLine;
            if (node.HorizontalLine != null) {
                HorizontalLineIndex = node.HorizontalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = 0; ii < this.HorizontalLines.length; ii++) {
                    if (this.HorizontalLines[ii].Origin.Y > node.Y) {
                        HorizontalLineIndex = ii;
                        break;
                    }
                }
            }
            DownHorizontalLineIndex = HorizontalLineIndex - 1;
            if (DownHorizontalLineIndex < 0)
                return null;
            DownHorizontalLine = this.HorizontalLines[DownHorizontalLineIndex];
            var loc = new WebGL.Vector2(node.X, DownHorizontalLine.Origin.Y);
            var DiscoveredNode = null;
            if (node.VerticalLine != null) {
                DiscoveredNode = node.VerticalLine.SearchForNodeBackward(node.Location);
                if (DiscoveredNode != null) {
                    if (loc.Y <= DiscoveredNode.Y) {
                    }
                    else {
                        DiscoveredNode = null;
                    }
                }
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = node.X;
                    DiscoveredNode.Y = DownHorizontalLine.Origin.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = DownHorizontalLine;
                    DiscoveredNode.VerticalLine = node.VerticalLine;
                    DownHorizontalLine.AddNode(DiscoveredNode);
                    node.VerticalLine.AddNode(DiscoveredNode);
                }
                node.BottomNode = DiscoveredNode;
                return DiscoveredNode;
            }
            node.BottomNode = null;
            return null;
        };
        MeshGrid.prototype.DiscoverLeftNodeFrom = function (node) {
            var VerticalLineIndex;
            var LeftVerticalLineIndex;
            var LeftVerticalLine;
            if (node.VerticalLine != null) {
                VerticalLineIndex = node.VerticalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = 0; ii < this.VerticalLines.length; ii++) {
                    if (this.VerticalLines[ii].Origin.X > node.X) {
                        VerticalLineIndex = ii;
                        break;
                    }
                }
            }
            LeftVerticalLineIndex = VerticalLineIndex - 1;
            if (LeftVerticalLineIndex < 0)
                return null;
            LeftVerticalLine = this.VerticalLines[LeftVerticalLineIndex];
            var loc = new WebGL.Vector2(LeftVerticalLine.Origin.X, node.Y);
            var DiscoveredNode = null;
            if (node.HorizontalLine != null) {
                DiscoveredNode = node.HorizontalLine.SearchForNodeBackward(node.Location);
                if (DiscoveredNode != null) {
                    if (loc.X <= DiscoveredNode.X) {
                    }
                    else {
                        DiscoveredNode = null;
                    }
                }
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = LeftVerticalLine.Origin.X;
                    DiscoveredNode.Y = node.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = node.HorizontalLine;
                    DiscoveredNode.VerticalLine = LeftVerticalLine;
                    node.HorizontalLine.AddNode(DiscoveredNode);
                    LeftVerticalLine.AddNode(DiscoveredNode);
                }
                node.LeftNode = DiscoveredNode;
                return DiscoveredNode;
            }
            node.LeftNode = null;
            return null;
        };
        MeshGrid.prototype.DiscoverRightNodeFrom = function (node) {
            var VerticalLineIndex;
            var RightVerticalLineIndex;
            var RightVerticalLine;
            if (node.VerticalLine != null) {
                VerticalLineIndex = node.VerticalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = this.VerticalLines.length - 1; ii >= 0; ii--) {
                    if (this.VerticalLines[ii].Origin.X < node.X) {
                        VerticalLineIndex = ii;
                        break;
                    }
                }
            }
            RightVerticalLineIndex = VerticalLineIndex + 1;
            if (RightVerticalLineIndex >= this.VerticalLines.length)
                return null;
            RightVerticalLine = this.VerticalLines[RightVerticalLineIndex];
            var loc = new WebGL.Vector2(RightVerticalLine.Origin.X, node.Y);
            var DiscoveredNode = null;
            if (node.HorizontalLine != null) {
                DiscoveredNode = node.HorizontalLine.SearchForNodeForward(node.Location);
                if (DiscoveredNode != null) {
                    if (loc.X >= DiscoveredNode.X) {
                    }
                    else {
                        DiscoveredNode = null;
                    }
                }
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = RightVerticalLine.Origin.X;
                    DiscoveredNode.Y = node.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = node.HorizontalLine;
                    DiscoveredNode.VerticalLine = RightVerticalLine;
                    node.HorizontalLine.AddNode(DiscoveredNode);
                    RightVerticalLine.AddNode(DiscoveredNode);
                }
                node.RightNode = DiscoveredNode;
                return DiscoveredNode;
            }
            node.RightNode = null;
            return null;
        };
        MeshGrid.prototype.NearestLocation = function (referenceLocation, firstLocation, secondLocation, thirdLocation) {
            if (referenceLocation == null)
                throw "No reference location has been given to the function";
            var firstLength = firstLocation == null ? null : WebGL.Vector2.Subtract(firstLocation, referenceLocation).Length;
            var secondLength = secondLocation == null ? null : WebGL.Vector2.Subtract(secondLocation, referenceLocation).Length;
            var thirdLength = thirdLocation == null ? null : WebGL.Vector2.Subtract(thirdLocation, referenceLocation).Length;
            var array = new Array();
            if (firstLength != null)
                array.push({ length: firstLength, location: firstLocation });
            if (secondLength != null)
                array.push({ length: secondLength, location: secondLocation });
            if (thirdLength != null)
                array.push({ length: thirdLength, location: thirdLocation });
            if (array.length == 0)
                throw "no locations have been feeded to the function";
            array.sort(function (a, b) { return a.length - b.length; });
            return array[0].location;
        };
        MeshGrid.prototype.DiscoverTopLeftNodeFrom = function (node) {
            var HorizontalLineIndex;
            var UpHorizontalLineIndex;
            var UpHorizontalLine;
            if (node.HorizontalLine != null) {
                HorizontalLineIndex = node.HorizontalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = this.HorizontalLines.length - 1; ii >= 0; ii--) {
                    if (this.HorizontalLines[ii].Origin.Y < node.Y) {
                        HorizontalLineIndex = ii;
                        break;
                    }
                }
            }
            UpHorizontalLineIndex = HorizontalLineIndex + 1;
            if (UpHorizontalLineIndex >= this.HorizontalLines.length)
                UpHorizontalLine = null;
            else
                UpHorizontalLine = this.HorizontalLines[UpHorizontalLineIndex];
            var VerticalLineIndex;
            var LeftVerticalLineIndex;
            var LeftVerticalLine;
            if (node.VerticalLine != null) {
                VerticalLineIndex = node.VerticalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = 0; ii < this.VerticalLines.length; ii++) {
                    if (this.VerticalLines[ii].Origin.X > node.X) {
                        VerticalLineIndex = ii;
                        break;
                    }
                }
            }
            LeftVerticalLineIndex = VerticalLineIndex - 1;
            if (LeftVerticalLineIndex < 0)
                LeftVerticalLine = null;
            else
                LeftVerticalLine = this.VerticalLines[LeftVerticalLineIndex];
            if (UpHorizontalLine == null && LeftVerticalLine == null) {
                return {
                    DiscoveredNode: null, FirstBlockingEdge: null, SecondBlockingEdge: null
                };
            }
            var TheoriticalLocation = null;
            if (LeftVerticalLine != null && UpHorizontalLine != null)
                TheoriticalLocation = new WebGL.Vector2(LeftVerticalLine.Origin.X, UpHorizontalLine.Origin.Y);
            var DiscoveredTopLeftNode = UpHorizontalLine == null ? null : UpHorizontalLine.SearchForNodeBackward(new WebGL.Vector2(node.X, UpHorizontalLine.Origin.Y));
            var DiscoveredLeftTopNode = LeftVerticalLine == null ? null : LeftVerticalLine.SearchForNodeForward(new WebGL.Vector2(LeftVerticalLine.Origin.X, node.Y));
            var TopBranchBlocked = false;
            var LeftBranchBlocked = false;
            var TopBranchBlockingEdge = null;
            var LeftBranchBlockingEdge = null;
            if (node.VerticalLine != null) {
                TopBranchBlockingEdge = node.VerticalLine.SearchForNodeTypeForward(node.Location, NodeType.Edge);
                if (TopBranchBlockingEdge != null) {
                    if (TopBranchBlockingEdge.Y <= UpHorizontalLine.Origin.Y) {
                        DiscoveredTopLeftNode = null;
                        TopBranchBlocked = true;
                    }
                    else {
                        TopBranchBlockingEdge = null;
                    }
                }
            }
            if (node.HorizontalLine != null) {
                LeftBranchBlockingEdge = node.HorizontalLine.SearchForNodeTypeBackward(node.Location, NodeType.Edge);
                if (LeftBranchBlockingEdge != null) {
                    if (LeftBranchBlockingEdge.X >= LeftVerticalLine.Origin.X) {
                        DiscoveredLeftTopNode = null;
                        LeftBranchBlocked = true;
                    }
                    else {
                        LeftBranchBlockingEdge = null;
                    }
                }
            }
            if (DiscoveredTopLeftNode == null && DiscoveredLeftTopNode == null) {
                var TheoriticalNode = null;
                if (TheoriticalLocation != null) {
                    TheoriticalNode = this.GetNodeAt(LeftVerticalLineIndex, UpHorizontalLineIndex);
                    if (TheoriticalNode != null) {
                        if (TheoriticalNode.Type != NodeType.Edge)
                            TheoriticalNode = null;
                    }
                }
                return {
                    DiscoveredNode: TheoriticalNode,
                    FirstBlockingEdge: TopBranchBlocked == true ? TopBranchBlockingEdge : null,
                    SecondBlockingEdge: LeftBranchBlocked == true ? LeftBranchBlockingEdge : null
                };
            }
            if (TopBranchBlockingEdge == null) {
                if (DiscoveredTopLeftNode != null) {
                    if (DiscoveredTopLeftNode.Type == NodeType.Edge && DiscoveredTopLeftNode.X >= LeftVerticalLine.Origin.Y) {
                        TopBranchBlockingEdge = DiscoveredTopLeftNode;
                    }
                }
            }
            if (LeftBranchBlockingEdge == null) {
                if (DiscoveredLeftTopNode != null) {
                    if (DiscoveredLeftTopNode.Type == NodeType.Edge && DiscoveredLeftTopNode.Y <= UpHorizontalLine.Origin.Y) {
                        LeftBranchBlockingEdge = DiscoveredLeftTopNode;
                    }
                }
            }
            var nearest = this.NearestLocation(node.Location, TheoriticalLocation, DiscoveredLeftTopNode == null ? null : DiscoveredLeftTopNode.Location, DiscoveredTopLeftNode == null ? null : DiscoveredTopLeftNode.Location);
            var DiscoveredNode = null;
            if (nearest.Equals(TheoriticalLocation)) {
                DiscoveredNode = this.GetNodeAt(LeftVerticalLineIndex, UpHorizontalLineIndex);
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = TheoriticalLocation.X;
                    DiscoveredNode.Y = TheoriticalLocation.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = UpHorizontalLine;
                    DiscoveredNode.VerticalLine = LeftVerticalLine;
                    UpHorizontalLine.AddNode(DiscoveredNode);
                    LeftVerticalLine.AddNode(DiscoveredNode);
                }
            }
            else {
                if (DiscoveredTopLeftNode != null) {
                    if (nearest.Equals(DiscoveredTopLeftNode.Location))
                        DiscoveredNode = DiscoveredTopLeftNode;
                }
                if (DiscoveredLeftTopNode != null) {
                    if (nearest.Equals(DiscoveredLeftTopNode.Location))
                        DiscoveredNode = DiscoveredLeftTopNode;
                }
            }
            node.TopLeftNode = DiscoveredNode;
            return {
                DiscoveredNode: DiscoveredNode,
                FirstBlockingEdge: TopBranchBlockingEdge,
                SecondBlockingEdge: LeftBranchBlockingEdge
            };
        };
        MeshGrid.prototype.DiscoverTopRightNodeFrom = function (node) {
            var HorizontalLineIndex;
            var UpHorizontalLineIndex;
            var UpHorizontalLine;
            if (node.HorizontalLine != null) {
                HorizontalLineIndex = node.HorizontalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = this.HorizontalLines.length - 1; ii >= 0; ii--) {
                    if (this.HorizontalLines[ii].Origin.Y < node.Y) {
                        HorizontalLineIndex = ii;
                        break;
                    }
                }
            }
            UpHorizontalLineIndex = HorizontalLineIndex + 1;
            if (UpHorizontalLineIndex >= this.HorizontalLines.length)
                UpHorizontalLine = null;
            else
                UpHorizontalLine = this.HorizontalLines[UpHorizontalLineIndex];
            var VerticalLineIndex;
            var RightVerticalLineIndex;
            var RightVerticalLine;
            if (node.VerticalLine != null) {
                VerticalLineIndex = node.VerticalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = this.VerticalLines.length - 1; ii >= 0; ii--) {
                    if (this.VerticalLines[ii].Origin.X < node.X) {
                        VerticalLineIndex = ii;
                        break;
                    }
                }
            }
            RightVerticalLineIndex = VerticalLineIndex + 1;
            if (RightVerticalLineIndex >= this.VerticalLines.length)
                RightVerticalLine = null;
            else
                RightVerticalLine = this.VerticalLines[RightVerticalLineIndex];
            if (UpHorizontalLine == null && RightVerticalLine == null) {
                return {
                    DiscoveredNode: null, FirstBlockingEdge: null, SecondBlockingEdge: null
                };
            }
            var TheoriticalLocation = null;
            if (RightVerticalLine != null && UpHorizontalLine != null)
                TheoriticalLocation = new WebGL.Vector2(RightVerticalLine.Origin.X, UpHorizontalLine.Origin.Y);
            var DiscoveredTopRightNode = UpHorizontalLine == null ? null : UpHorizontalLine.SearchForNodeForward(new WebGL.Vector2(node.X, UpHorizontalLine.Origin.Y));
            var DiscoveredRightTopNode = RightVerticalLine == null ? null : RightVerticalLine.SearchForNodeForward(new WebGL.Vector2(RightVerticalLine.Origin.X, node.Y));
            var TopBranchBlocked = false;
            var RightBranchBlocked = false;
            var TopBranchBlockingEdge = null;
            var RightBranchBlockingEdge = null;
            if (node.VerticalLine != null) {
                TopBranchBlockingEdge = node.VerticalLine.SearchForNodeTypeForward(node.Location, NodeType.Edge);
                if (TopBranchBlockingEdge != null) {
                    if (TopBranchBlockingEdge.Y <= UpHorizontalLine.Origin.Y) {
                        DiscoveredTopRightNode = null;
                        TopBranchBlocked = true;
                    }
                    else {
                        TopBranchBlockingEdge = null;
                    }
                }
            }
            if (node.HorizontalLine != null) {
                RightBranchBlockingEdge = node.HorizontalLine.SearchForNodeTypeForward(node.Location, NodeType.Edge);
                if (RightBranchBlockingEdge != null) {
                    if (RightBranchBlockingEdge.X <= RightVerticalLine.Origin.X) {
                        DiscoveredRightTopNode = null;
                        RightBranchBlocked = true;
                    }
                    else {
                        RightBranchBlockingEdge = null;
                    }
                }
            }
            if (DiscoveredTopRightNode == null && DiscoveredRightTopNode == null) {
                var TheoriticalNode = null;
                if (TheoriticalLocation != null) {
                    TheoriticalNode = this.GetNodeAt(RightVerticalLineIndex, UpHorizontalLineIndex);
                    if (TheoriticalNode != null) {
                        if (TheoriticalNode.Type != NodeType.Edge)
                            TheoriticalNode = null;
                    }
                }
                return {
                    DiscoveredNode: TheoriticalNode,
                    FirstBlockingEdge: TopBranchBlocked == true ? TopBranchBlockingEdge : null,
                    SecondBlockingEdge: RightBranchBlocked == true ? RightBranchBlockingEdge : null
                };
            }
            if (TopBranchBlockingEdge == null) {
                if (DiscoveredTopRightNode != null) {
                    if (DiscoveredTopRightNode.Type == NodeType.Edge && DiscoveredTopRightNode.X <= RightVerticalLine.Origin.Y) {
                        TopBranchBlockingEdge = DiscoveredTopRightNode;
                    }
                }
            }
            if (RightBranchBlockingEdge == null) {
                if (DiscoveredRightTopNode != null) {
                    if (DiscoveredRightTopNode.Type == NodeType.Edge && DiscoveredRightTopNode.Y <= UpHorizontalLine.Origin.Y) {
                        RightBranchBlockingEdge = DiscoveredRightTopNode;
                    }
                }
            }
            var nearest = this.NearestLocation(node.Location, TheoriticalLocation, DiscoveredTopRightNode == null ? null : DiscoveredTopRightNode.Location, DiscoveredRightTopNode == null ? null : DiscoveredRightTopNode.Location);
            var DiscoveredNode = null;
            if (nearest.Equals(TheoriticalLocation)) {
                DiscoveredNode = this.GetNodeAt(RightVerticalLineIndex, UpHorizontalLineIndex);
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = TheoriticalLocation.X;
                    DiscoveredNode.Y = TheoriticalLocation.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = UpHorizontalLine;
                    DiscoveredNode.VerticalLine = RightVerticalLine;
                    UpHorizontalLine.AddNode(DiscoveredNode);
                    RightVerticalLine.AddNode(DiscoveredNode);
                }
            }
            else {
                if (DiscoveredTopRightNode != null) {
                    if (nearest.Equals(DiscoveredTopRightNode.Location))
                        DiscoveredNode = DiscoveredTopRightNode;
                }
                if (DiscoveredRightTopNode != null) {
                    if (nearest.Equals(DiscoveredRightTopNode.Location))
                        DiscoveredNode = DiscoveredRightTopNode;
                }
            }
            node.TopRightNode = DiscoveredNode;
            return {
                DiscoveredNode: DiscoveredNode,
                FirstBlockingEdge: TopBranchBlockingEdge,
                SecondBlockingEdge: RightBranchBlockingEdge
            };
        };
        MeshGrid.prototype.DiscoverBottomRightNodeFrom = function (node) {
            var HorizontalLineIndex;
            var DownHorizontalLineIndex;
            var DownHorizontalLine;
            if (node.HorizontalLine != null) {
                HorizontalLineIndex = node.HorizontalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = 0; ii < this.HorizontalLines.length; ii++) {
                    if (this.HorizontalLines[ii].Origin.Y > node.Y) {
                        HorizontalLineIndex = ii;
                        break;
                    }
                }
            }
            DownHorizontalLineIndex = HorizontalLineIndex - 1;
            if (DownHorizontalLineIndex < 0)
                DownHorizontalLine = null;
            else
                DownHorizontalLine = this.HorizontalLines[DownHorizontalLineIndex];
            var VerticalLineIndex;
            var RightVerticalLineIndex;
            var RightVerticalLine;
            if (node.VerticalLine != null) {
                VerticalLineIndex = node.VerticalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = this.VerticalLines.length - 1; ii >= 0; ii--) {
                    if (this.VerticalLines[ii].Origin.X < node.X) {
                        VerticalLineIndex = ii;
                        break;
                    }
                }
            }
            RightVerticalLineIndex = VerticalLineIndex + 1;
            if (RightVerticalLineIndex >= this.VerticalLines.length)
                RightVerticalLine = null;
            else
                RightVerticalLine = this.VerticalLines[RightVerticalLineIndex];
            if (DownHorizontalLine == null && RightVerticalLine == null) {
                return {
                    DiscoveredNode: null, FirstBlockingEdge: null, SecondBlockingEdge: null
                };
            }
            var TheoriticalLocation = null;
            if (RightVerticalLine != null && DownHorizontalLine != null)
                TheoriticalLocation = new WebGL.Vector2(RightVerticalLine.Origin.X, DownHorizontalLine.Origin.Y);
            var DiscoveredBottomRightNode = DownHorizontalLine == null ? null : DownHorizontalLine.SearchForNodeForward(new WebGL.Vector2(node.X, DownHorizontalLine.Origin.Y));
            var DiscoveredRightBottomNode = RightVerticalLine == null ? null : RightVerticalLine.SearchForNodeBackward(new WebGL.Vector2(RightVerticalLine.Origin.X, node.Y));
            var BottomBranchBlocked = false;
            var RightBranchBlocked = false;
            var BottomBranchBlockingEdge = null;
            var RightBranchBlockingEdge = null;
            if (node.VerticalLine != null) {
                BottomBranchBlockingEdge = node.VerticalLine.SearchForNodeTypeBackward(node.Location, NodeType.Edge);
                if (BottomBranchBlockingEdge != null) {
                    if (BottomBranchBlockingEdge.Y >= DownHorizontalLine.Origin.Y) {
                        DiscoveredBottomRightNode = null;
                        BottomBranchBlocked = true;
                    }
                    else {
                        BottomBranchBlockingEdge = null;
                    }
                }
            }
            if (node.HorizontalLine != null) {
                RightBranchBlockingEdge = node.HorizontalLine.SearchForNodeTypeForward(node.Location, NodeType.Edge);
                if (RightBranchBlockingEdge != null) {
                    if (RightBranchBlockingEdge.X <= RightVerticalLine.Origin.X) {
                        DiscoveredRightBottomNode = null;
                        RightBranchBlocked = true;
                    }
                    else {
                        RightBranchBlockingEdge = null;
                    }
                }
            }
            if (DiscoveredBottomRightNode == null && DiscoveredRightBottomNode == null) {
                var TheoriticalNode = null;
                if (TheoriticalLocation != null) {
                    TheoriticalNode = this.GetNodeAt(RightVerticalLineIndex, DownHorizontalLineIndex);
                    if (TheoriticalNode != null) {
                        if (TheoriticalNode.Type != NodeType.Edge)
                            TheoriticalNode = null;
                    }
                }
                return {
                    DiscoveredNode: TheoriticalNode,
                    FirstBlockingEdge: BottomBranchBlocked == true ? BottomBranchBlockingEdge : null,
                    SecondBlockingEdge: RightBranchBlocked == true ? RightBranchBlockingEdge : null
                };
            }
            if (BottomBranchBlockingEdge == null) {
                if (DiscoveredBottomRightNode != null) {
                    if (DiscoveredBottomRightNode.Type == NodeType.Edge && DiscoveredBottomRightNode.X <= RightVerticalLine.Origin.Y) {
                        BottomBranchBlockingEdge = DiscoveredBottomRightNode;
                    }
                }
            }
            if (RightBranchBlockingEdge == null) {
                if (DiscoveredRightBottomNode != null) {
                    if (DiscoveredRightBottomNode.Type == NodeType.Edge && DiscoveredRightBottomNode.Y >= DownHorizontalLine.Origin.Y) {
                        RightBranchBlockingEdge = DiscoveredRightBottomNode;
                    }
                }
            }
            var nearest = this.NearestLocation(node.Location, TheoriticalLocation, DiscoveredBottomRightNode == null ? null : DiscoveredBottomRightNode.Location, DiscoveredRightBottomNode == null ? null : DiscoveredRightBottomNode.Location);
            var DiscoveredNode = null;
            if (nearest.Equals(TheoriticalLocation)) {
                DiscoveredNode = this.GetNodeAt(RightVerticalLineIndex, DownHorizontalLineIndex);
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = TheoriticalLocation.X;
                    DiscoveredNode.Y = TheoriticalLocation.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = DownHorizontalLine;
                    DiscoveredNode.VerticalLine = RightVerticalLine;
                    DownHorizontalLine.AddNode(DiscoveredNode);
                    RightVerticalLine.AddNode(DiscoveredNode);
                }
            }
            else {
                if (DiscoveredBottomRightNode != null) {
                    if (nearest.Equals(DiscoveredBottomRightNode.Location))
                        DiscoveredNode = DiscoveredBottomRightNode;
                }
                if (DiscoveredRightBottomNode != null) {
                    if (nearest.Equals(DiscoveredRightBottomNode.Location))
                        DiscoveredNode = DiscoveredRightBottomNode;
                }
            }
            node.BottomRightNode = DiscoveredNode;
            return {
                DiscoveredNode: DiscoveredNode,
                FirstBlockingEdge: BottomBranchBlockingEdge,
                SecondBlockingEdge: RightBranchBlockingEdge
            };
        };
        MeshGrid.prototype.DiscoverBottomLeftNodeFrom = function (node) {
            var HorizontalLineIndex;
            var DownHorizontalLineIndex;
            var DownHorizontalLine;
            if (node.HorizontalLine != null) {
                HorizontalLineIndex = node.HorizontalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = 0; ii < this.HorizontalLines.length; ii++) {
                    if (this.HorizontalLines[ii].Origin.Y > node.Y) {
                        HorizontalLineIndex = ii;
                        break;
                    }
                }
            }
            DownHorizontalLineIndex = HorizontalLineIndex - 1;
            if (DownHorizontalLineIndex < 0)
                DownHorizontalLine = null;
            else
                DownHorizontalLine = this.HorizontalLines[DownHorizontalLineIndex];
            var VerticalLineIndex;
            var LeftVerticalLineIndex;
            var LeftVerticalLine;
            if (node.VerticalLine != null) {
                VerticalLineIndex = node.VerticalLine.GridIndex;
            }
            else {
                var ii;
                for (ii = 0; ii < this.VerticalLines.length; ii++) {
                    if (this.VerticalLines[ii].Origin.X > node.X) {
                        VerticalLineIndex = ii;
                        break;
                    }
                }
            }
            LeftVerticalLineIndex = VerticalLineIndex - 1;
            if (LeftVerticalLineIndex < 0)
                LeftVerticalLine = null;
            else
                LeftVerticalLine = this.VerticalLines[LeftVerticalLineIndex];
            if (DownHorizontalLine == null && LeftVerticalLine == null) {
                return {
                    DiscoveredNode: null, FirstBlockingEdge: null, SecondBlockingEdge: null
                };
            }
            var TheoriticalLocation = null;
            if (LeftVerticalLine != null && DownHorizontalLine != null)
                TheoriticalLocation = new WebGL.Vector2(LeftVerticalLine.Origin.X, DownHorizontalLine.Origin.Y);
            var DiscoveredBottomLeftNode = DownHorizontalLine == null ? null : DownHorizontalLine.SearchForNodeBackward(new WebGL.Vector2(node.X, DownHorizontalLine.Origin.Y));
            var DiscoveredLeftBottomNode = LeftVerticalLine == null ? null : LeftVerticalLine.SearchForNodeBackward(new WebGL.Vector2(LeftVerticalLine.Origin.X, node.Y));
            var BottomBranchBlocked = false;
            var LeftBranchBlocked = false;
            var BottomBranchBlockingEdge = null;
            var LeftBranchBlockingEdge = null;
            if (node.VerticalLine != null) {
                BottomBranchBlockingEdge = node.VerticalLine.SearchForNodeTypeBackward(node.Location, NodeType.Edge);
                if (BottomBranchBlockingEdge != null) {
                    if (BottomBranchBlockingEdge.Y >= DownHorizontalLine.Origin.Y) {
                        DiscoveredBottomLeftNode = null;
                        BottomBranchBlocked = true;
                    }
                    else {
                        BottomBranchBlockingEdge = null;
                    }
                }
            }
            if (node.HorizontalLine != null) {
                LeftBranchBlockingEdge = node.HorizontalLine.SearchForNodeTypeBackward(node.Location, NodeType.Edge);
                if (LeftBranchBlockingEdge != null) {
                    if (LeftBranchBlockingEdge.X >= LeftVerticalLine.Origin.X) {
                        DiscoveredLeftBottomNode = null;
                        LeftBranchBlocked = true;
                    }
                    else {
                        LeftBranchBlockingEdge = null;
                    }
                }
            }
            if (DiscoveredBottomLeftNode == null && DiscoveredLeftBottomNode == null) {
                var TheoriticalNode = null;
                if (TheoriticalLocation != null) {
                    TheoriticalNode = this.GetNodeAt(LeftVerticalLineIndex, DownHorizontalLineIndex);
                    if (TheoriticalNode != null) {
                        if (TheoriticalNode.Type != NodeType.Edge)
                            TheoriticalNode = null;
                    }
                }
                return {
                    DiscoveredNode: TheoriticalNode,
                    FirstBlockingEdge: BottomBranchBlocked == true ? BottomBranchBlockingEdge : null,
                    SecondBlockingEdge: LeftBranchBlocked == true ? LeftBranchBlockingEdge : null
                };
            }
            if (BottomBranchBlockingEdge == null) {
                if (DiscoveredBottomLeftNode != null) {
                    if (DiscoveredBottomLeftNode.Type == NodeType.Edge && DiscoveredBottomLeftNode.X >= LeftVerticalLine.Origin.Y) {
                        BottomBranchBlockingEdge = DiscoveredBottomLeftNode;
                    }
                }
            }
            if (LeftBranchBlockingEdge == null) {
                if (DiscoveredLeftBottomNode != null) {
                    if (DiscoveredLeftBottomNode.Type == NodeType.Edge && DiscoveredLeftBottomNode.Y >= DownHorizontalLine.Origin.Y) {
                        LeftBranchBlockingEdge = DiscoveredLeftBottomNode;
                    }
                }
            }
            var nearest = this.NearestLocation(node.Location, TheoriticalLocation, DiscoveredBottomLeftNode == null ? null : DiscoveredBottomLeftNode.Location, DiscoveredLeftBottomNode == null ? null : DiscoveredLeftBottomNode.Location);
            var DiscoveredNode = null;
            if (nearest.Equals(TheoriticalLocation)) {
                DiscoveredNode = this.GetNodeAt(LeftVerticalLineIndex, DownHorizontalLineIndex);
                if (DiscoveredNode == null) {
                    DiscoveredNode = new Node();
                    DiscoveredNode.X = TheoriticalLocation.X;
                    DiscoveredNode.Y = TheoriticalLocation.Y;
                    DiscoveredNode.Type = NodeType.Space;
                    DiscoveredNode.HorizontalLine = DownHorizontalLine;
                    DiscoveredNode.VerticalLine = LeftVerticalLine;
                    DownHorizontalLine.AddNode(DiscoveredNode);
                    LeftVerticalLine.AddNode(DiscoveredNode);
                }
            }
            else {
                if (DiscoveredLeftBottomNode != null) {
                    if (nearest.Equals(DiscoveredLeftBottomNode.Location)) {
                        DiscoveredNode = DiscoveredLeftBottomNode;
                    }
                }
                if (DiscoveredBottomLeftNode != null) {
                    if (nearest.Equals(DiscoveredBottomLeftNode.Location)) {
                        DiscoveredNode = DiscoveredBottomLeftNode;
                    }
                }
            }
            node.BottomLeftNode = DiscoveredNode;
            return {
                DiscoveredNode: DiscoveredNode,
                FirstBlockingEdge: BottomBranchBlockingEdge,
                SecondBlockingEdge: LeftBranchBlockingEdge
            };
        };
        MeshGrid.prototype.GetDiscoveredSpace = function (spacenumber) {
            var ix;
            for (ix = 0; ix < this._MeshSpaces.length; ix++) {
                if (this._MeshSpaces[ix].SpaceNumber == spacenumber) {
                    return this._MeshSpaces[ix];
                }
            }
            return null;
        };
        MeshGrid.prototype.DiscoverMeshSpace = function (location) {
            var closestLocation = this.DiscoverNearestCoordinateLines(location);
            var node = closestLocation.HorizontalLine.RetrieveNodeAtPoint(closestLocation.TruePosition);
            if (node == null) {
                node = this.AddNodeForPoint(closestLocation.HorizontalLine, closestLocation.TruePosition, NodeType.Space);
                this.AddNodeForPoint(closestLocation.VerticalLine, closestLocation.TruePosition, NodeType.Space);
            }
            if (node.Type == NodeType.Space) {
                if (node.SpacesCount > 0) {
                    return this.GetDiscoveredSpace(node.SpaceNumber);
                }
            }
            else {
                var msg = "Edge point cannot be used as a basis of search for space points";
                alert(msg);
                throw msg;
            }
            var RestNodes = new Array();
            RestNodes.push(node);
            var iNodes;
            var ProcessedNodes = new Object();
            var DiscoveredEdges = new Object();
            var IsOpenedSpace = false;
            for (iNodes = 0; iNodes < RestNodes.length; iNodes++) {
                var current_node = RestNodes[iNodes];
                if (current_node.Type != NodeType.Edge) {
                    if (!ProcessedNodes.hasOwnProperty(current_node.Key)) {
                        ProcessedNodes[current_node.Key] = current_node;
                    }
                }
                else {
                    if (!DiscoveredEdges.hasOwnProperty(current_node.Key)) {
                        DiscoveredEdges[current_node.Key] = current_node;
                    }
                }
                var topNode = this.DiscoverTopNodeFrom(current_node);
                if (topNode != null) {
                    if (topNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(topNode.Key)) {
                            RestNodes.push(topNode);
                            ProcessedNodes[topNode.Key] = topNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(topNode.Key)) {
                            DiscoveredEdges[topNode.Key] = topNode;
                        }
                    }
                }
                else {
                    IsOpenedSpace = true;
                }
                var topRightNode = this.DiscoverTopRightNodeFrom(current_node);
                if (topRightNode.DiscoveredNode != null) {
                    if (topRightNode.DiscoveredNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(topRightNode.DiscoveredNode.Key)) {
                            RestNodes.push(topRightNode.DiscoveredNode);
                            ProcessedNodes[topRightNode.DiscoveredNode.Key] = topRightNode.DiscoveredNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(topRightNode.DiscoveredNode.Key)) {
                            DiscoveredEdges[topRightNode.DiscoveredNode.Key] = topRightNode.DiscoveredNode;
                        }
                    }
                }
                else {
                    if (topRightNode.FirstBlockingEdge == null && topRightNode.SecondBlockingEdge == null) {
                        IsOpenedSpace = true;
                    }
                }
                if (topRightNode.FirstBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(topRightNode.FirstBlockingEdge.Key)) {
                        DiscoveredEdges[topRightNode.FirstBlockingEdge.Key] = topRightNode.FirstBlockingEdge;
                    }
                }
                if (topRightNode.SecondBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(topRightNode.SecondBlockingEdge.Key)) {
                        DiscoveredEdges[topRightNode.SecondBlockingEdge.Key] = topRightNode.SecondBlockingEdge;
                    }
                }
                var rightNode = this.DiscoverRightNodeFrom(current_node);
                if (rightNode != null) {
                    if (rightNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(rightNode.Key)) {
                            RestNodes.push(rightNode);
                            ProcessedNodes[rightNode.Key] = rightNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(rightNode.Key)) {
                            DiscoveredEdges[rightNode.Key] = rightNode;
                        }
                    }
                }
                else {
                    IsOpenedSpace = true;
                }
                var bottomRightNode = this.DiscoverBottomRightNodeFrom(current_node);
                if (bottomRightNode.DiscoveredNode != null) {
                    if (bottomRightNode.DiscoveredNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(bottomRightNode.DiscoveredNode.Key)) {
                            RestNodes.push(bottomRightNode.DiscoveredNode);
                            ProcessedNodes[bottomRightNode.DiscoveredNode.Key] = bottomRightNode.DiscoveredNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(bottomRightNode.DiscoveredNode.Key)) {
                            DiscoveredEdges[bottomRightNode.DiscoveredNode.Key] = bottomRightNode.DiscoveredNode;
                        }
                    }
                }
                else {
                    if (bottomRightNode.FirstBlockingEdge == null && bottomRightNode.SecondBlockingEdge == null) {
                        IsOpenedSpace = true;
                    }
                }
                if (bottomRightNode.FirstBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(bottomRightNode.FirstBlockingEdge.Key)) {
                        DiscoveredEdges[bottomRightNode.FirstBlockingEdge.Key] = bottomRightNode.FirstBlockingEdge;
                    }
                }
                if (bottomRightNode.SecondBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(bottomRightNode.SecondBlockingEdge.Key)) {
                        DiscoveredEdges[bottomRightNode.SecondBlockingEdge.Key] = bottomRightNode.SecondBlockingEdge;
                    }
                }
                var bottomNode = this.DiscoverBottomNodeFrom(current_node);
                if (bottomNode != null) {
                    if (bottomNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(bottomNode.Key)) {
                            RestNodes.push(bottomNode);
                            ProcessedNodes[bottomNode.Key] = bottomNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(bottomNode.Key)) {
                            DiscoveredEdges[bottomNode.Key] = bottomNode;
                        }
                    }
                }
                else {
                    IsOpenedSpace = true;
                }
                var bottomLeftNode = this.DiscoverBottomLeftNodeFrom(current_node);
                if (bottomLeftNode.DiscoveredNode != null) {
                    if (bottomLeftNode.DiscoveredNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(bottomLeftNode.DiscoveredNode.Key)) {
                            RestNodes.push(bottomLeftNode.DiscoveredNode);
                            ProcessedNodes[bottomLeftNode.DiscoveredNode.Key] = bottomLeftNode.DiscoveredNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(bottomLeftNode.DiscoveredNode.Key)) {
                            DiscoveredEdges[bottomLeftNode.DiscoveredNode.Key] = bottomLeftNode.DiscoveredNode;
                        }
                    }
                }
                else {
                    if (bottomLeftNode.FirstBlockingEdge == null && bottomLeftNode.SecondBlockingEdge == null) {
                        IsOpenedSpace = true;
                    }
                }
                if (bottomLeftNode.FirstBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(bottomLeftNode.FirstBlockingEdge.Key)) {
                        DiscoveredEdges[bottomLeftNode.FirstBlockingEdge.Key] = bottomLeftNode.FirstBlockingEdge;
                    }
                }
                if (bottomLeftNode.SecondBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(bottomLeftNode.SecondBlockingEdge.Key)) {
                        DiscoveredEdges[bottomLeftNode.SecondBlockingEdge.Key] = bottomLeftNode.SecondBlockingEdge;
                    }
                }
                var leftNode = this.DiscoverLeftNodeFrom(current_node);
                if (leftNode != null) {
                    if (leftNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(leftNode.Key)) {
                            RestNodes.push(leftNode);
                            ProcessedNodes[leftNode.Key] = leftNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(leftNode.Key)) {
                            DiscoveredEdges[leftNode.Key] = leftNode;
                        }
                    }
                }
                else {
                    IsOpenedSpace = true;
                }
                var topLeftNode = this.DiscoverTopLeftNodeFrom(current_node);
                if (topLeftNode.DiscoveredNode != null) {
                    if (topLeftNode.DiscoveredNode.Type != NodeType.Edge) {
                        if (!ProcessedNodes.hasOwnProperty(topLeftNode.DiscoveredNode.Key)) {
                            RestNodes.push(topLeftNode.DiscoveredNode);
                            ProcessedNodes[topLeftNode.DiscoveredNode.Key] = topLeftNode.DiscoveredNode;
                        }
                    }
                    else {
                        if (!DiscoveredEdges.hasOwnProperty(topLeftNode.DiscoveredNode.Key)) {
                            DiscoveredEdges[topLeftNode.DiscoveredNode.Key] = topLeftNode.DiscoveredNode;
                        }
                    }
                }
                else {
                    if (topLeftNode.FirstBlockingEdge == null && topLeftNode.SecondBlockingEdge == null) {
                        IsOpenedSpace = true;
                    }
                }
                if (topLeftNode.FirstBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(topLeftNode.FirstBlockingEdge.Key)) {
                        DiscoveredEdges[topLeftNode.FirstBlockingEdge.Key] = topLeftNode.FirstBlockingEdge;
                    }
                }
                if (topLeftNode.SecondBlockingEdge != null) {
                    if (!DiscoveredEdges.hasOwnProperty(topLeftNode.SecondBlockingEdge.Key)) {
                        DiscoveredEdges[topLeftNode.SecondBlockingEdge.Key] = topLeftNode.SecondBlockingEdge;
                    }
                }
            }
            var space_nodes = new Array();
            for (var key in ProcessedNodes) {
                space_nodes.push(ProcessedNodes[key]);
            }
            var edge_nodes = new Array();
            for (var key in DiscoveredEdges) {
                edge_nodes.push(DiscoveredEdges[key]);
            }
            if (space_nodes[0].SpacesCount == 0) {
                var DiscoveredSpaceNumber = MeshSpace.GetNewSpaceNumber();
                space_nodes.forEach(function (x) { return x.SpaceNumber = DiscoveredSpaceNumber; });
                edge_nodes.forEach(function (x) { return x.SpaceNumber = DiscoveredSpaceNumber; });
                var space = new MeshSpace(DiscoveredSpaceNumber);
                space.InnerNodes = space_nodes;
                space.EdgeNodes = edge_nodes;
                if (IsOpenedSpace)
                    space.SpaceType = MeshSpaceType.Open;
                else
                    space.SpaceType = MeshSpaceType.Closed;
                this._MeshSpaces.push(space);
                return space;
            }
            else {
                var space = this.GetDiscoveredSpace(space_nodes[0].SpaceNumber);
                space.InnerNodes = space_nodes;
                space.EdgeNodes = edge_nodes;
                return space;
            }
        };
        MeshGrid.prototype.DiscoverSpaceInnerNodes = function (location) {
            var space = this.DiscoverMeshSpace(location);
            this._CurrentSpaceNodes = space.InnerNodes;
            this._CurrentSpaceNodesLocations = null;
        };
        MeshGrid.prototype.DiscoverSpaceEdgeNodes = function (location) {
            var space = this.DiscoverMeshSpace(location);
            this._CurrentSpaceNodes = space.FilteredEdgeNodes;
            this._CurrentSpaceNodesLocations = null;
        };
        Object.defineProperty(MeshGrid.prototype, "EdgeNodes", {
            get: function () {
                if (this._EdgeNodes == null) {
                    var ix;
                    var iy;
                    var UniqueNodes = new Object();
                    for (ix = 0; ix < this.VerticalLines.length; ix++) {
                        var vertical_coordinate = this.VerticalLines[ix];
                        vertical_coordinate.EdgeNodes.forEach(function (x) {
                            if (!UniqueNodes.hasOwnProperty(x.Key))
                                UniqueNodes[x.Key] = x;
                        });
                    }
                    for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                        var horizontal_coordinate = this.HorizontalLines[iy];
                        horizontal_coordinate.EdgeNodes.forEach(function (x) {
                            if (!UniqueNodes.hasOwnProperty(x.Key))
                                UniqueNodes[x.Key] = x;
                        });
                    }
                    this._EdgeNodes = new Array();
                    for (var key in UniqueNodes) {
                        this._EdgeNodes.push(UniqueNodes[key]);
                    }
                }
                return this._EdgeNodes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshGrid.prototype, "EdgeNodesLocations", {
            get: function () {
                var _this = this;
                if (this._EdgeNodesLocations == null) {
                    this._EdgeNodesLocations = new Array();
                    this.EdgeNodes.forEach(function (x) { return _this._EdgeNodesLocations.push(new WebGL.Vector3(x.X, x.Y, 0)); });
                }
                return this._EdgeNodesLocations;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshGrid.prototype, "CurrentSpaceNodes", {
            get: function () {
                var _this = this;
                if (this._CurrentSpaceNodes == null) {
                    this._CurrentSpaceNodes = new Array();
                    var ix;
                    var iy;
                    for (ix = 0; ix < this.VerticalLines.length; ix++) {
                        var vertical_coordinate = this.VerticalLines[ix];
                        vertical_coordinate.SpaceNodes.forEach(function (x) { return _this._CurrentSpaceNodes.push(x); });
                    }
                    for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                        var horizontal_coordinate = this.HorizontalLines[iy];
                        horizontal_coordinate.SpaceNodes.forEach(function (x) { return _this._CurrentSpaceNodes.push(x); });
                    }
                }
                return this._CurrentSpaceNodes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshGrid.prototype, "CurrentSpaceNodesLocations", {
            get: function () {
                var _this = this;
                if (this._CurrentSpaceNodesLocations == null) {
                    this._CurrentSpaceNodesLocations = new Array();
                    this.CurrentSpaceNodes.forEach(function (x) { return _this._CurrentSpaceNodesLocations.push(new WebGL.Vector3(x.X, x.Y, 0)); });
                }
                return this._CurrentSpaceNodesLocations;
            },
            enumerable: true,
            configurable: true
        });
        MeshGrid.prototype.SortAndCalculateMinimumAndMaximumCoordinateLines = function () {
            this.HorizontalLines.sort(function (a, b) {
                if (a.Origin.Y < b.Origin.Y)
                    return -1;
                if (a.Origin.Y > b.Origin.Y)
                    return 1;
                return 0;
            });
            this.VerticalLines.sort(function (a, b) {
                if (a.Origin.X < b.Origin.X)
                    return -1;
                if (a.Origin.X > b.Origin.X)
                    return 1;
                return 0;
            });
            this._MinimumDeducedVerticalFrom = this.VerticalLines[0].Origin.X;
            this._MaximumDeducedVerticalTo = this.VerticalLines[this.VerticalLines.length - 1].Origin.X;
            this._MinimumDeducedHorizontalFrom = this.HorizontalLines[0].Origin.Y;
            this._MaximumDeducedHorizontalTo = this.HorizontalLines[this.HorizontalLines.length - 1].Origin.Y;
            var idx;
            for (idx = 0; idx < this.VerticalLines.length; idx++)
                this.VerticalLines[idx].GridIndex = idx;
            for (idx = 0; idx < this.HorizontalLines.length; idx++)
                this.HorizontalLines[idx].GridIndex = idx;
        };
        MeshGrid.prototype.ClearCache = function () {
            this._EdgeNodesLocations = null;
            this._EdgeNodes = null;
            this._DeducedCoordinateLines = null;
            this._NaturalCoordinateLines = null;
        };
        MeshGrid.prototype.PrepareConstantIntensityGridFromShapes = function () {
            var first = this.Shapes[0];
            this.HorizontalFrom = FloatingMath.MinimumInteger(first.MinimumX);
            this.HorizontalTo = FloatingMath.MaximumInteger(first.MaximumX);
            this.VerticalFrom = FloatingMath.MinimumInteger(first.MinimumY);
            this.VerticalTo = FloatingMath.MaximumInteger(first.MaximumY);
            var ix;
            for (ix = 1; ix < this.Shapes.length; ix++) {
                var shape = this.Shapes[ix];
                if (shape.MinimumX < this.HorizontalFrom)
                    this.HorizontalFrom = FloatingMath.MinimumInteger(shape.MinimumX);
                if (shape.MaximumX > this.HorizontalTo)
                    this.HorizontalTo = FloatingMath.MaximumInteger(shape.MaximumX);
                if (shape.MinimumY < this.VerticalFrom)
                    this.VerticalFrom = FloatingMath.MinimumInteger(shape.MinimumY);
                if (shape.MaximumY > this.VerticalTo)
                    this.VerticalTo = FloatingMath.MaximumInteger(shape.MaximumY);
            }
            this.HorizontalFrom -= this.BoundaryMargin;
            this.HorizontalTo += this.BoundaryMargin;
            this.VerticalFrom -= this.BoundaryMargin;
            this.VerticalTo += this.BoundaryMargin;
            this.HorizontalSpacesCount = this.HorizontalTo - this.HorizontalFrom;
            this.VerticalSpacesCount = this.VerticalTo - this.VerticalFrom;
        };
        MeshGrid.prototype.InjectMoreCoordinateLinesForIncompleteEdgeNodes = function () {
            var _this = this;
            var count = 0;
            var dodes = new Array();
            this.EdgeNodes.forEach(function (x) {
                if (x.CoordinateLinesCount < 2)
                    dodes.push(x);
            });
            while (dodes.length > 0) {
                dodes.forEach(function (x) {
                    var pfixed = x.Location.Fixed;
                    if (!_this.HorizontalCoordinateExistsFor(pfixed)) {
                        _this.DeduceHorizontalCoordinateLineFor(pfixed);
                    }
                    if (!_this.VerticalCoordinateExistsFor(pfixed)) {
                        _this.DeduceVerticalCoordinateLineFor(pfixed);
                    }
                });
                this.ClearCache();
                this.DiscoverEdgeNodes();
                dodes = new Array();
                this.EdgeNodes.forEach(function (x) {
                    if (x.CoordinateLinesCount < 2)
                        dodes.push(x);
                });
            }
        };
        MeshGrid.prototype.DiscoverSpacesByScanLines = function () {
            var ix;
            var iy;
            for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                for (ix = 0; ix < this.VerticalLines.length; ix++) {
                    var position = this.GetPosition(ix, iy);
                    var hl = this.HorizontalLines[iy];
                    var node = hl.RetrieveNodeAtPoint(position);
                    if (node == null) {
                        this.DiscoverMeshSpace(position);
                    }
                    else {
                    }
                }
            }
        };
        MeshGrid.prototype.SpecifySpacesRelations = function () {
            var _this = this;
            this._MeshSpaces.forEach(function (x) { return x.DiscoverRelatedSpaces(_this._MeshSpaces); });
        };
        MeshGrid.prototype.DiscoverNodes = function () {
            this.ClearCache();
            MeshSpace.ResetNumbering();
            this.GenerateHorizontalCoordinateLines();
            this.GenerateVerticalCoordinateLines();
            this.DeduceCoordinateLinesForTerminalPointsAndMakeIntersectionsAsCornerNodes();
            this.DiscoverEdgeNodes();
            this.SortAndCalculateMinimumAndMaximumCoordinateLines();
            this.DiscoverSpacesByScanLines();
            this.SpecifySpacesRelations();
        };
        Object.defineProperty(MeshGrid.prototype, "NaturalCoordinateLines", {
            get: function () {
                if (this._NaturalCoordinateLines == null) {
                    this._NaturalCoordinateLines = new Array();
                    var ix;
                    var iy;
                    for (ix = 0; ix < this.VerticalLines.length; ix++) {
                        var vertical_coordinate = this.VerticalLines[ix];
                        if (vertical_coordinate.GenerationType == CoordinateLineGenerationType.Natural)
                            this._NaturalCoordinateLines.push(vertical_coordinate);
                    }
                    for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                        var horizontal_coordinate = this.HorizontalLines[iy];
                        if (horizontal_coordinate.GenerationType == CoordinateLineGenerationType.Natural)
                            this._NaturalCoordinateLines.push(horizontal_coordinate);
                    }
                }
                return this._NaturalCoordinateLines;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MeshGrid.prototype, "DeducedCoordinateLines", {
            get: function () {
                if (this._DeducedCoordinateLines == null) {
                    this._DeducedCoordinateLines = new Array();
                    var ix;
                    var iy;
                    for (ix = 0; ix < this.VerticalLines.length; ix++) {
                        var vertical_coordinate = this.VerticalLines[ix];
                        if (vertical_coordinate.GenerationType == CoordinateLineGenerationType.Deduced)
                            this._DeducedCoordinateLines.push(vertical_coordinate);
                    }
                    for (iy = 0; iy < this.HorizontalLines.length; iy++) {
                        var horizontal_coordinate = this.HorizontalLines[iy];
                        if (horizontal_coordinate.GenerationType == CoordinateLineGenerationType.Deduced)
                            this._DeducedCoordinateLines.push(horizontal_coordinate);
                    }
                }
                return this._DeducedCoordinateLines;
            },
            enumerable: true,
            configurable: true
        });
        MeshGrid.prototype.PointsFromCoordinateLines = function (lines) {
            var points = new Array();
            lines.forEach(function (x) {
                points.push(x.Origin.ToVector3());
                points.push(x.Through.ToVector3());
            });
            return points;
        };
        MeshGrid.prototype.Render = function (gl) {
            if (!Shapes.R2Line.Program.IsLinked(gl))
                Shapes.R2Line.Program.Link(gl);
            if (!Shapes.R2Point.Program.IsLinked(gl))
                Shapes.R2Point.Program.Link(gl);
            Shapes.R2Line.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            Shapes.R2Line.Program.ArrayVertexSize = 3;
            Shapes.R2Point.Program.Uniforms.PointSize = 6;
            Shapes.R2Point.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            Shapes.R2Point.Program.ArrayVertexSize = 3;
            if (this.ShowNaturalGridLines) {
                var NaturalLinesPoints = this.PointsFromCoordinateLines(this.NaturalCoordinateLines);
                Shapes.R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(NaturalLinesPoints));
                Shapes.R2Line.Program.Uniforms.ShapeColor = this.NaturalGridLinesColor;
                Shapes.R2Line.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, NaturalLinesPoints.length);
                Shapes.R2Line.Program.EndUse(gl);
            }
            if (this.ShowDeducedGridLines) {
                var DeducedLinesPoints = this.PointsFromCoordinateLines(this.DeducedCoordinateLines);
                Shapes.R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(DeducedLinesPoints));
                Shapes.R2Line.Program.Uniforms.ShapeColor = this.DeducedGridLinesColor;
                Shapes.R2Line.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINES, 0, DeducedLinesPoints.length);
                Shapes.R2Line.Program.EndUse(gl);
            }
            if (this.ShowEdgeNodes) {
                Shapes.R2Point.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(this.EdgeNodesLocations));
                Shapes.R2Point.Program.Uniforms.ShapeColor = new WebGL.Vector4(1, 0, 1, 1);
                Shapes.R2Point.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.POINTS, 0, this.EdgeNodesLocations.length);
                Shapes.R2Point.Program.EndUse(gl);
            }
            if (this.CurrentShowSpaceNodes) {
                Shapes.R2Point.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(this.CurrentSpaceNodesLocations));
                Shapes.R2Point.Program.Uniforms.ShapeColor = new WebGL.Vector4(1, 0.1, 0.1, 1);
                Shapes.R2Point.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.POINTS, 0, this.CurrentSpaceNodesLocations.length);
                Shapes.R2Point.Program.EndUse(gl);
            }
        };
        Object.defineProperty(MeshGrid.prototype, "CurrentSpaceNumber", {
            get: function () {
                return this._CurrentSpaceNumber;
            },
            set: function (value) {
                var space = this.GetDiscoveredSpace(value);
                this._CurrentSpaceNodes = space.InnerNodes;
                this._CurrentSpaceNodesLocations = null;
                this._CurrentSpaceNumber = value;
            },
            enumerable: true,
            configurable: true
        });
        MeshGrid.prototype.GetSpacesTree = function () {
            var tree = [];
            var ix;
            for (ix = 0; ix < this._MeshSpaces.length; ix++) {
                var space = this._MeshSpaces[ix];
                var SpaceEdges = [];
                space.FilteredEdgeNodes.forEach(function (x) {
                    SpaceEdges.push({
                        text: x.Location.ToTupleString()
                    });
                });
                var SpaceCorners = [];
                space.CornerNodes.forEach(function (x) {
                    SpaceCorners.push({
                        text: x.Location.ToTupleString()
                    });
                });
                var InnerSpaces = [];
                space.InnerSpaces.forEach(function (x) {
                    InnerSpaces.push({
                        text: "Space[" + x.SpaceNumber + "]"
                    });
                });
                var NeighbourSpaces = [];
                space.NeighbourSpaces.forEach(function (x) {
                    NeighbourSpaces.push({
                        text: "Space[" + x.SpaceNumber + "]"
                    });
                });
                var SpaceNodes = [];
                space.InnerNodes.forEach(function (x) {
                    SpaceNodes.push({
                        text: x.Location.ToTupleString()
                    });
                });
                var spacename = space.SpaceType == MeshSpaceType.Open ? "External Space " + space.SpaceNumber.toString() + "" : "[ Space " + space.SpaceNumber.toString() + "]";
                tree.push({
                    text: spacename,
                    Space: space,
                    nodes: [
                        {
                            text: "Inner Spaces",
                            nodes: InnerSpaces
                        },
                        {
                            text: "Corners",
                            nodes: SpaceCorners
                        },
                        {
                            text: "Edge Nodes",
                            nodes: SpaceEdges
                        },
                        {
                            text: "Space Nodes",
                            nodes: SpaceEdges
                        },
                        { text: "Edges" }
                    ]
                });
            }
            return tree;
        };
        return MeshGrid;
    }());
    Meshing.MeshGrid = MeshGrid;
})(Meshing || (Meshing = {}));
var WebGL;
(function (WebGL) {
    var Geometry;
    (function (Geometry) {
        var Line = (function () {
            function Line() {
            }
            Object.defineProperty(Line.prototype, "Slope", {
                get: function () {
                    var dy = this.LastPoint.Y - this.FirstPoint.Y;
                    var dx = this.LastPoint.X - this.FirstPoint.X;
                    return dy / dx;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Line.prototype, "Vector", {
                get: function () {
                    return WebGL.Vector2.Subtract(this.LastPoint, this.FirstPoint);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Line.prototype, "NormalizedVector", {
                get: function () {
                    var t = this.Vector;
                    t.Normalize();
                    return t;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Line.prototype, "Angle", {
                get: function () {
                    var aa = WebGL.Vector3.CalculateAngle(this.NormalizedVector.ToVector3(), new WebGL.Vector3(1, 0, 0));
                    var cr = WebGL.Vector3.Cross(this.NormalizedVector.ToVector3(), new WebGL.Vector3(1, 0, 0));
                    if (cr.Z < 0) {
                        return new Angle(aa);
                    }
                    else {
                        return new Angle(Angle.Pi.Radians + (Angle.Pi.Radians - aa));
                    }
                },
                enumerable: true,
                configurable: true
            });
            Line.prototype.GetRelativeAngle = function (line) {
                return new Angle(this.Angle.Radians - line.Angle.Radians);
            };
            Object.defineProperty(Line.prototype, "Length", {
                get: function () {
                    return WebGL.Vector2.Subtract(this.LastPoint, this.FirstPoint).Length;
                },
                enumerable: true,
                configurable: true
            });
            Line.prototype.Execute = function (t) {
                var xt = this.LastPoint.X - this.FirstPoint.X;
                var yt = this.LastPoint.Y - this.FirstPoint.Y;
                var point = new WebGL.Vector2(t * xt + this.FirstPoint.X, t * yt + this.FirstPoint.Y);
                return point;
            };
            Object.defineProperty(Line.prototype, "MiddlePoint", {
                get: function () {
                    return this.Execute(0.5);
                },
                enumerable: true,
                configurable: true
            });
            Line.prototype.IntersectionWithRay = function (firstPointLocation, lastPointLocation) {
                var Ax = this.FirstPoint.X;
                var Bx = this.LastPoint.X;
                var Ay = this.FirstPoint.Y;
                var By = this.LastPoint.Y;
                var Cx = firstPointLocation.X;
                var Dx = lastPointLocation.X;
                var Cy = firstPointLocation.Y;
                var Dy = lastPointLocation.Y;
                var distAB, theCos, theSin, newX, ABpos;
                var intersection = new WebGL.Vector2(0, 0);
                if (Ax == Bx && Ay == By || Cx == Dx && Cy == Dy) {
                    return null;
                }
                Bx -= Ax;
                By -= Ay;
                Cx -= Ax;
                Cy -= Ay;
                Dx -= Ax;
                Dy -= Ay;
                distAB = Math.sqrt(Bx * Bx + By * By);
                theCos = Bx / distAB;
                theSin = By / distAB;
                newX = Cx * theCos + Cy * theSin;
                Cy = Cy * theCos - Cx * theSin;
                Cx = newX;
                newX = Dx * theCos + Dy * theSin;
                Dy = Dy * theCos - Dx * theSin;
                Dx = newX;
                if (FloatingMath.NearlyEqual(Cy, Dy))
                    return null;
                ABpos = Dx + (Cx - Dx) * Dy / (Dy - Cy);
                intersection.X = FloatingMath.FixedNumber(Ax + ABpos * theCos);
                intersection.Y = FloatingMath.FixedNumber(Ay + ABpos * theSin);
                return intersection;
            };
            Line.prototype.IsPointOnLineSegment = function (point) {
                var l1 = WebGL.Vector2.Subtract(point, this.FirstPoint).Length;
                var l2 = WebGL.Vector2.Subtract(point, this.LastPoint).Length;
                var l = l1 + l2;
                var percentage = l / this.Vector.Length;
                if (percentage < 1 || FloatingMath.NearlyEqual(percentage, 1)) {
                }
                else
                    return false;
                var dxc = point.X - this.FirstPoint.X;
                var dyc = point.Y - this.FirstPoint.Y;
                var dxl = this.LastPoint.X - this.FirstPoint.X;
                var dyl = this.LastPoint.Y - this.FirstPoint.Y;
                if (Math.abs(dxl) >= Math.abs(dyl))
                    return dxl > 0 ?
                        this.FirstPoint.X <= point.X && point.X <= this.LastPoint.X :
                        this.LastPoint.X <= point.X && point.X <= this.FirstPoint.X;
                else
                    return dyl > 0 ?
                        this.FirstPoint.Y <= point.Y && point.Y <= this.LastPoint.Y :
                        this.LastPoint.Y <= point.Y && point.Y <= this.FirstPoint.Y;
            };
            Line.prototype.IntersectionWithLineSegment = function (firstPointLocation, lastPointLocation) {
                var line = new Line();
                line.FirstPoint = firstPointLocation;
                line.LastPoint = lastPointLocation;
                if (this.FirstPoint.Equals(firstPointLocation))
                    return this.FirstPoint;
                if (this.FirstPoint.Equals(lastPointLocation))
                    return this.FirstPoint;
                if (this.LastPoint.Equals(firstPointLocation))
                    return this.LastPoint;
                if (this.LastPoint.Equals(lastPointLocation))
                    return this.LastPoint;
                var lli = this.IntersectionWithRay(firstPointLocation, lastPointLocation);
                if (lli != null) {
                    if (this.IsPointOnLineSegment(lli) == true && line.IsPointOnLineSegment(lli) == true) {
                        return lli;
                    }
                }
                return null;
            };
            return Line;
        }());
        Geometry.Line = Line;
    })(Geometry = WebGL.Geometry || (WebGL.Geometry = {}));
})(WebGL || (WebGL = {}));
var Shapes;
(function (Shapes) {
    var R2FreeHand = (function (_super) {
        __extends(R2FreeHand, _super);
        function R2FreeHand() {
            _super.call(this);
            this.Points = new Array();
            this._Lines = null;
            this._DeltaAngles = null;
        }
        Object.defineProperty(R2FreeHand.prototype, "ShapeName", {
            get: function () {
                return "R2FreeHand";
            },
            enumerable: true,
            configurable: true
        });
        R2FreeHand.prototype.AddPoint = function (point) {
            this.Points.push(point);
            if (this.Lines.length >= 2) {
                var da = this.DeltaAngles[this.DeltaAngles.length - 1];
            }
        };
        R2FreeHand.prototype.OnRender = function (gl) {
            if (!R2FreeHand.Program.IsLinked(gl))
                R2FreeHand.Program.Link(gl);
            R2FreeHand.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2FreeHand.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2FreeHand.Program.ArrayVertexSize = 3;
            R2FreeHand.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(WebGL.Vectors2ToVectors3(this.Points)));
            R2FreeHand.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINE_STRIP, 0, this.Points.length);
            R2FreeHand.Program.EndUse(gl);
        };
        R2FreeHand.prototype.OnRenderForPicking = function (gl) {
            if (!R2FreeHand.Program.IsLinked(gl))
                R2FreeHand.Program.Link(gl);
            R2FreeHand.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2FreeHand.Program.Uniforms.ShapeColor = this.SelectionColorVector;
            R2FreeHand.Program.ArrayVertexSize = 3;
            R2FreeHand.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(WebGL.Vectors2ToVectors3(this.Points)));
            R2FreeHand.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINE_STRIP, 0, this.Points.length);
            R2FreeHand.Program.EndUse(gl);
        };
        Object.defineProperty(R2FreeHand.prototype, "Lines", {
            get: function () {
                if (this._Lines == null || (this._Lines.length < this.Points.length - 2)) {
                    this._Lines = new Array();
                    var ix;
                    for (ix = 1; ix < this.Points.length; ix++) {
                        var line = new WebGL.Geometry.Line();
                        line.FirstPoint = this.Points[ix - 1];
                        line.LastPoint = this.Points[ix];
                        this._Lines.push(line);
                    }
                }
                return this._Lines;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "LinesCount", {
            get: function () {
                return this.Lines.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "DeltaAngles", {
            get: function () {
                if (this._DeltaAngles == null || (this._DeltaAngles.length != this.Lines.length)) {
                    this._DeltaAngles = new Array();
                    this._DeltaAngles.push(this.Lines[0].Angle);
                    var ix;
                    for (ix = 1; ix < this.Lines.length; ix++) {
                        this._DeltaAngles.push(this.Lines[ix].GetRelativeAngle(this.Lines[ix - 1]));
                    }
                }
                return this._DeltaAngles;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "SecondDeltaAngles", {
            get: function () {
                if (this._SecondDeltaAngles == null) {
                    this._SecondDeltaAngles = new Array();
                    this._SecondDeltaAngles.push(this.DeltaAngles[0]);
                    var ix;
                    for (ix = 1; ix < this.DeltaAngles.length; ix++) {
                        this._SecondDeltaAngles.push(new Angle(this.DeltaAngles[ix].Radians - this.DeltaAngles[ix - 1].Radians));
                    }
                }
                return this._SecondDeltaAngles;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "Lengths", {
            get: function () {
                var bg = new Array();
                this.Lines.forEach(function (x) {
                    bg.push(x.Length);
                });
                return bg;
            },
            enumerable: true,
            configurable: true
        });
        R2FreeHand.prototype.GetHardTurnsIndices = function () {
            return R2FreeHand.GetHardTurnsIndices(this.Lines);
        };
        Object.defineProperty(R2FreeHand.prototype, "TotalLength", {
            get: function () {
                if (this.Points.length < 2)
                    return 0;
                return this.Lengths.reduce(function (a, b) { return a + b; });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "MiddleLengthPoint", {
            get: function () {
                var halfLength = this.TotalLength / 2;
                var ls = this.Lengths;
                var ix;
                var totallength = 0;
                for (ix = 0; ls.length; ix++) {
                    totallength = totallength + ls[ix];
                    if (totallength >= halfLength)
                        return this.Lines[ix].FirstPoint;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "FirstPointLocation", {
            get: function () {
                return this.Points[0];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "LastPointLocation", {
            get: function () {
                return this.Points[this.Points.length - 1];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2FreeHand.prototype, "TerminalsLine", {
            get: function () {
                var ll = new WebGL.Geometry.Line();
                ll.FirstPoint = this.FirstPointLocation;
                ll.LastPoint = this.LastPointLocation;
                return ll;
            },
            enumerable: true,
            configurable: true
        });
        R2FreeHand.prototype.RoundFirstAndLastPoints = function () {
            this.Points[0].X = Math.round(this.Points[0].X);
            this.Points[0].Y = Math.round(this.Points[0].Y);
            this.Points[this.Points.length - 1].X = Math.round(this.Points[this.Points.length - 1].X);
            this.Points[this.Points.length - 1].Y = Math.round(this.Points[this.Points.length - 1].Y);
        };
        R2FreeHand.GetDeltaAngles = function (lines) {
            var _DeltaAngles = new Array();
            _DeltaAngles.push(lines[0].Angle);
            var ix;
            for (ix = 1; ix < lines.length; ix++) {
                _DeltaAngles.push(lines[ix].GetRelativeAngle(lines[ix - 1]));
            }
            return _DeltaAngles;
        };
        R2FreeHand.GetHardTurnsIndices = function (lines) {
            var deltaAngles = R2FreeHand.GetDeltaAngles(lines);
            var HardTurnsIndices = new Array();
            HardTurnsIndices.push({
                Index: 0, DeltaAngleDegrees: deltaAngles[0].Degrees
            });
            var ix;
            for (ix = 1; ix < lines.length; ix++) {
                {
                    if (Math.abs(deltaAngles[ix].Degrees) >= R2FreeHand.HardTurnsMinimumDeltaAngle)
                        HardTurnsIndices.push({ Index: ix, DeltaAngleDegrees: deltaAngles[ix].Degrees });
                }
            }
            HardTurnsIndices.push({
                Index: lines.length - 1, DeltaAngleDegrees: deltaAngles[lines.length - 1].Degrees
            });
            return HardTurnsIndices;
        };
        R2FreeHand.ReduceLines = function (lines) {
            var hardturns = R2FreeHand.GetHardTurnsIndices(lines);
            var outLines = new Array();
            var advancingpoint = lines[hardturns[0].Index].FirstPoint;
            for (var ix = 0; ix < hardturns.length; ix++) {
                var secondPoint = lines[hardturns[ix].Index].LastPoint;
                var line = new WebGL.Geometry.Line();
                line.FirstPoint = advancingpoint;
                line.LastPoint = secondPoint;
                advancingpoint = secondPoint;
                outLines.push(line);
            }
            return outLines;
        };
        R2FreeHand.ReduceLinesByLengths = function (lines) {
            var total_length = 0;
            lines.forEach(function (x) { return total_length += x.Length; });
            var outLines = new Array();
            outLines.push(lines[0]);
            var ix;
            for (ix = 1; ix < lines.length; ix++) {
                var line = lines[ix];
                var lpercentage = line.Length / total_length;
                if (lpercentage <= R2FreeHand.RemovalMinimumLengthPercentage
                    || (line.Length <= R2FreeHand.RemovalMinimumLineLength)) {
                    var previousLine = outLines[outLines.length - 1];
                    previousLine.LastPoint = line.LastPoint;
                }
                else {
                    outLines.push(line);
                }
            }
            return outLines;
        };
        R2FreeHand.MergeLinesWithSlightAngleDifference = function (lines) {
            var outLines = new Array();
            outLines.push(lines[0]);
            var ix;
            for (ix = 1; ix < lines.length; ix++) {
                var line = lines[ix];
                var previousLine = outLines[outLines.length - 1];
                var delta = Math.abs(line.Angle.Degrees - previousLine.Angle.Degrees);
                if (delta <= R2FreeHand.LinesMergeMaximumDifference) {
                    previousLine.LastPoint = line.LastPoint;
                }
                else {
                    outLines.push(line);
                }
            }
            return outLines;
        };
        R2FreeHand.RoundLinesTerminalPoints = function (lines) {
            lines.forEach(function (l) {
                l.FirstPoint.Round();
                l.LastPoint.Round();
            });
        };
        R2FreeHand.prototype.GetSelfIntersectionIndex = function () {
            var iforward;
            var ibackward;
            var lines = this.Lines;
            for (iforward = 0; iforward < lines.length; iforward++) {
                var srcLine = lines[iforward];
                for (ibackward = lines.length - 1; ibackward > (iforward + 1); ibackward--) {
                    var dstLine = lines[ibackward];
                    var intersection = srcLine.IntersectionWithRay(dstLine.FirstPoint, dstLine.LastPoint);
                    if (intersection != null) {
                        if (srcLine.IsPointOnLineSegment(intersection) && dstLine.IsPointOnLineSegment(intersection)) {
                            return iforward;
                        }
                    }
                }
            }
            return -1;
        };
        R2FreeHand.prototype.GuessLines = function () {
            var FirstPassLines = R2FreeHand.ReduceLines(this.Lines);
            var LastPassLines = R2FreeHand.MergeLinesWithSlightAngleDifference(FirstPassLines);
            LastPassLines = R2FreeHand.ReduceLinesByLengths(LastPassLines);
            return LastPassLines;
        };
        R2FreeHand.ConvertLinesToShapeLines = function (lines) {
            var shapes = new Array();
            for (var ix = 0; ix < lines.length; ix++) {
                var line = new Shapes.R2Line();
                line.FirstPointLocation = lines[ix].FirstPoint.ToVector3();
                line.LastPointLocation = lines[ix].LastPoint.ToVector3();
                shapes.push(line);
            }
            return shapes;
        };
        R2FreeHand.prototype.CheckCurvature = function () {
            var d2Angles = this.DeltaAngles;
            var degs = new Array();
            d2Angles.forEach(function (x) { return degs.push(x.Degrees); });
            var totalAngles = degs.reduce(function (a, b) { return a + b; });
            var mean = totalAngles / degs.length;
            var max = Math.max.apply(Math, degs);
            var min = Math.min.apply(Math, degs);
            var sumofsmeansquares = 0;
            d2Angles.forEach(function (x) {
                sumofsmeansquares += Math.pow(x.Degrees - mean, 2);
            });
            var Variance = sumofsmeansquares / degs.length;
            var Std = Math.sqrt(Variance);
            console.debug("max:" + max.toString() + " min:" + min.toString() + " mean:" + mean.toString() + " Std:" + Std.toString());
            return { Std: Math.abs(Std), Mean: Math.abs(mean), Max: Math.abs(max), Min: Math.abs(min) };
        };
        R2FreeHand.prototype.GuessLinesShapes = function () {
            var statistics = this.CheckCurvature();
            var guessedLines = this.GuessLines();
            var shapes = new Array();
            var minimum = statistics.Min;
            if (minimum > 180)
                minimum = 360 - minimum;
            if (minimum < 70 && statistics.Mean < 3 && statistics.Std < 100 && guessedLines.length < 4) {
                shapes.push(this.PredictArc());
            }
            else {
                R2FreeHand.RoundLinesTerminalPoints(guessedLines);
                var nonzerolines = new Array();
                guessedLines.forEach(function (x) {
                    if (x.Length != 0)
                        nonzerolines.push(x);
                });
                shapes = R2FreeHand.ConvertLinesToShapeLines(nonzerolines);
            }
            return shapes;
        };
        R2FreeHand.prototype.PredictArc = function () {
            var arc = new Shapes.R2Arc();
            arc.FirstPointLocation = this.FirstPointLocation.ToVector3().Rounded;
            arc.LastPointLocation = this.LastPointLocation.ToVector3().Rounded;
            arc.MiddlePointLocation = this.MiddleLengthPoint.ToVector3().Rounded;
            if (arc.IsLine) {
                this.RoundFirstAndLastPoints();
                var line = new Shapes.R2Line();
                line.FirstPointLocation = this.Points[0].ToVector3();
                line.LastPointLocation = this.Points[this.Points.length - 1].ToVector3();
                return line;
            }
            else {
                return arc;
            }
        };
        Object.defineProperty(R2FreeHand.prototype, "IsIntersected", {
            get: function () {
                var ii = this.GetSelfIntersectionIndex();
                return ii > -1;
            },
            enumerable: true,
            configurable: true
        });
        R2FreeHand.prototype.GuessOneShape = function () {
            var shapes = new Array();
            var l1 = this.TotalLength;
            var l2 = this.TerminalsLine.Length;
            var percentage = l1 / l2;
            if (percentage > 1.04) {
                var length = WebGL.Vector2.Subtract(this.LastPointLocation, this.FirstPointLocation).Length;
                if (length <= 1 || this.IsIntersected) {
                    var l = new WebGL.Geometry.Line();
                    l.FirstPoint = this.FirstPointLocation;
                    l.LastPoint = this.MiddleLengthPoint;
                    var circle = new Shapes.R2Circle();
                    circle.CenterPointLocation = l.MiddlePoint.ToVector3();
                    circle.RadiusPointLocation = this.LastPointLocation.ToVector3();
                    shapes.push(circle);
                }
                else {
                    var arc = new Shapes.R2Arc();
                    arc.FirstPointLocation = this.FirstPointLocation.ToVector3().Rounded;
                    arc.LastPointLocation = this.LastPointLocation.ToVector3().Rounded;
                    arc.MiddlePointLocation = this.MiddleLengthPoint.ToVector3().Rounded;
                    if (arc.IsLine) {
                        this.RoundFirstAndLastPoints();
                        var line = new Shapes.R2Line();
                        line.FirstPointLocation = this.Points[0].ToVector3();
                        line.LastPointLocation = this.Points[this.Points.length - 1].ToVector3();
                        shapes.push(line);
                    }
                    else {
                        shapes.push(arc);
                    }
                }
            }
            else {
                this.RoundFirstAndLastPoints();
                var line = new Shapes.R2Line();
                line.FirstPointLocation = this.Points[0].ToVector3();
                line.LastPointLocation = this.Points[this.Points.length - 1].ToVector3();
                shapes.push(line);
            }
            return shapes;
        };
        R2FreeHand.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                    }\
                } ");
        R2FreeHand.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        R2FreeHand.Program = new WebGL.Shaders.PipelineProgram("R2FreeHandProgram", Shapes.R2Line.VertexShader, Shapes.R2Line.FragmentShader);
        R2FreeHand.HardTurnsMinimumDeltaAngle = 45;
        R2FreeHand.RemovalMinimumLengthPercentage = 0.04;
        R2FreeHand.RemovalMinimumLineLength = 0.05;
        R2FreeHand.LinesMergeMaximumDifference = 20;
        return R2FreeHand;
    }(Shapes.R2Shape));
    Shapes.R2FreeHand = R2FreeHand;
})(Shapes || (Shapes = {}));
var DeviceInputMode;
(function (DeviceInputMode) {
    DeviceInputMode[DeviceInputMode["Mouse"] = 0] = "Mouse";
    DeviceInputMode[DeviceInputMode["Tablet"] = 1] = "Tablet";
})(DeviceInputMode || (DeviceInputMode = {}));
var MouseDrawingShape;
(function (MouseDrawingShape) {
    MouseDrawingShape[MouseDrawingShape["Selection"] = 0] = "Selection";
    MouseDrawingShape[MouseDrawingShape["Lines"] = 1] = "Lines";
    MouseDrawingShape[MouseDrawingShape["DimensionLines"] = 2] = "DimensionLines";
    MouseDrawingShape[MouseDrawingShape["PolyLine"] = 3] = "PolyLine";
    MouseDrawingShape[MouseDrawingShape["RectanglesWithLines"] = 4] = "RectanglesWithLines";
    MouseDrawingShape[MouseDrawingShape["Rectangles"] = 5] = "Rectangles";
    MouseDrawingShape[MouseDrawingShape["Arcs"] = 6] = "Arcs";
    MouseDrawingShape[MouseDrawingShape["Circles"] = 7] = "Circles";
    MouseDrawingShape[MouseDrawingShape["Circles3P"] = 8] = "Circles3P";
})(MouseDrawingShape || (MouseDrawingShape = {}));
var SnappingOption;
(function (SnappingOption) {
    SnappingOption[SnappingOption["None"] = 0] = "None";
    SnappingOption[SnappingOption["Nearest"] = 1] = "Nearest";
    SnappingOption[SnappingOption["Terminals"] = 2] = "Terminals";
    SnappingOption[SnappingOption["Perpendicular"] = 3] = "Perpendicular";
})(SnappingOption || (SnappingOption = {}));
var TabletDrawingStyle;
(function (TabletDrawingStyle) {
    TabletDrawingStyle[TabletDrawingStyle["Stylus"] = 0] = "Stylus";
    TabletDrawingStyle[TabletDrawingStyle["FreeHand"] = 1] = "FreeHand";
    TabletDrawingStyle[TabletDrawingStyle["FreeHandWithRecognition"] = 2] = "FreeHandWithRecognition";
})(TabletDrawingStyle || (TabletDrawingStyle = {}));
var UserInput;
(function (UserInput) {
    UserInput[UserInput["MouseLeftButtonDown"] = 0] = "MouseLeftButtonDown";
    UserInput[UserInput["MouseLeftButtonUp"] = 1] = "MouseLeftButtonUp";
    UserInput[UserInput["MouseRightButtonDown"] = 2] = "MouseRightButtonDown";
    UserInput[UserInput["MouseRightButtonUp"] = 3] = "MouseRightButtonUp";
    UserInput[UserInput["MouseMiddleButtonDown"] = 4] = "MouseMiddleButtonDown";
    UserInput[UserInput["MouseMiddleButtonUp"] = 5] = "MouseMiddleButtonUp";
    UserInput[UserInput["MouseLeftClick"] = 6] = "MouseLeftClick";
    UserInput[UserInput["MouseRightClick"] = 7] = "MouseRightClick";
    UserInput[UserInput["MouseMiddleClick"] = 8] = "MouseMiddleClick";
    UserInput[UserInput["MouseLeftClickOnShape"] = 9] = "MouseLeftClickOnShape";
    UserInput[UserInput["MouseRightClickOnShape"] = 10] = "MouseRightClickOnShape";
    UserInput[UserInput["MouseMiddleClickOnShape"] = 11] = "MouseMiddleClickOnShape";
    UserInput[UserInput["MouseLeftDoubleClick"] = 12] = "MouseLeftDoubleClick";
    UserInput[UserInput["MouseRightDoubleClick"] = 13] = "MouseRightDoubleClick";
    UserInput[UserInput["MouseMiddleDoubleClick"] = 14] = "MouseMiddleDoubleClick";
    UserInput[UserInput["MouseLeftDoubleClickOnShape"] = 15] = "MouseLeftDoubleClickOnShape";
    UserInput[UserInput["MouseRightDoubleClickOnShape"] = 16] = "MouseRightDoubleClickOnShape";
    UserInput[UserInput["MouseMiddleDoubleClickOnShape"] = 17] = "MouseMiddleDoubleClickOnShape";
    UserInput[UserInput["MouseLeftDrag"] = 18] = "MouseLeftDrag";
    UserInput[UserInput["MouseRightDrag"] = 19] = "MouseRightDrag";
    UserInput[UserInput["MouseMiddleDrag"] = 20] = "MouseMiddleDrag";
    UserInput[UserInput["MousePositiveWheel"] = 21] = "MousePositiveWheel";
    UserInput[UserInput["MouseNegativeWheel"] = 22] = "MouseNegativeWheel";
    UserInput[UserInput["OneFingerTouchDrag"] = 23] = "OneFingerTouchDrag";
    UserInput[UserInput["TwoFingersTouchDrag"] = 24] = "TwoFingersTouchDrag";
    UserInput[UserInput["ThreeFingersTouchDrag"] = 25] = "ThreeFingersTouchDrag";
})(UserInput || (UserInput = {}));
var UserCommand;
(function (UserCommand) {
    UserCommand[UserCommand["Selection"] = 0] = "Selection";
    UserCommand[UserCommand["BeginDrawing"] = 1] = "BeginDrawing";
    UserCommand[UserCommand["EndDrawing"] = 2] = "EndDrawing";
    UserCommand[UserCommand["Erase"] = 3] = "Erase";
    UserCommand[UserCommand["DrawingModeChange"] = 4] = "DrawingModeChange";
    UserCommand[UserCommand["ZoomIn"] = 5] = "ZoomIn";
    UserCommand[UserCommand["ZoomOut"] = 6] = "ZoomOut";
})(UserCommand || (UserCommand = {}));
var UserInputAction = (function () {
    function UserInputAction(input, command) {
        this.SubActions = new Array();
        this.Input = input;
        this.Command = command;
    }
    UserInputAction.prototype.Add = function (input, command) {
        var ui = new UserInputAction(input, command);
        this.SubActions.push(ui);
        return ui;
    };
    return UserInputAction;
}());
var UxProfile = (function () {
    function UxProfile() {
        this.CurrentDrawingShape = MouseDrawingShape.Lines;
        this.InputActions = new Array();
    }
    UxProfile.prototype.Add = function (input, command) {
        var ui = new UserInputAction(input, command);
        this.InputActions.push(ui);
        return ui;
    };
    Object.defineProperty(UxProfile.prototype, "LastCommand", {
        get: function () {
            return this.LastInputAction.Command;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxProfile.prototype, "LastInput", {
        get: function () {
            return this.LastInputAction.Input;
        },
        enumerable: true,
        configurable: true
    });
    UxProfile.prototype.SendUserInput = function (userInput) {
        var ix;
        for (ix = 0; ix < this.InputActions.length; ix++) {
            var action = this.InputActions[ix];
            if (action.Input == userInput) {
                this.LastInputAction = action;
                break;
            }
        }
    };
    UxProfile.prototype.ChangeDrawingMode = function (mode) {
        this.CurrentDrawingShape = mode;
    };
    UxProfile.HafezProfile = function () {
        var ux = new UxProfile();
        ux.CurrentDrawingShape = MouseDrawingShape.PolyLine;
        ux.Add(UserInput.MouseLeftClick, UserCommand.BeginDrawing).Add(UserInput.MouseRightClick, UserCommand.EndDrawing);
        ux.Add(UserInput.MouseLeftDoubleClick, UserCommand.DrawingModeChange);
        ux.Add(UserInput.MouseLeftDoubleClickOnShape, UserCommand.Selection);
        ux.Add(UserInput.MouseRightDoubleClick, UserCommand.DrawingModeChange);
        ux.Add(UserInput.MouseRightDrag, UserCommand.Erase);
        ux.Add(UserInput.MousePositiveWheel, UserCommand.ZoomIn);
        ux.Add(UserInput.MouseNegativeWheel, UserCommand.ZoomOut);
        ux.DrawingModeChanging = function (self) {
            if (self.CurrentDrawingShape == MouseDrawingShape.PolyLine)
                self.ChangeDrawingMode(MouseDrawingShape.RectanglesWithLines);
            else if (self.CurrentDrawingShape == MouseDrawingShape.RectanglesWithLines)
                self.ChangeDrawingMode(MouseDrawingShape.PolyLine);
        };
        return ux;
    };
    UxProfile.MouseProfile = function () {
        var ux = new UxProfile();
        ux.Add(UserInput.MouseLeftClick, UserCommand.BeginDrawing).Add(UserInput.MouseLeftClick, UserCommand.EndDrawing);
        ux.Add(UserInput.MouseRightDrag, UserCommand.Erase);
        ux.Add(UserInput.MousePositiveWheel, UserCommand.ZoomIn);
        ux.Add(UserInput.MouseNegativeWheel, UserCommand.ZoomOut);
        return ux;
    };
    UxProfile.TabletProfile = function () {
        return new UxProfile();
    };
    return UxProfile;
}());
var R2Editor = (function () {
    function R2Editor(glcanvas) {
        var _this = this;
        this._CurrentCommand = EditorCommands.Drawing;
        this._LastCommand = EditorCommands.Drawing;
        this.CommandClicksStack = new Array();
        this.CurrentUxProfile = UxProfile.MouseProfile();
        this._ZoomFactor = 0.1;
        this.CurrentTabletDrawingStyle = TabletDrawingStyle.Stylus;
        this._CurrentMouseDrawingShape = MouseDrawingShape.Selection;
        this._CurrentMouseLocation = new WebGL.Vector2(0, 0);
        this.GridSnapping = true;
        this.ShapePointsSnapping = false;
        this.ShapeTerminalsAndIntersectionsSnapping = false;
        this.ShapeEdgesProjectionsSnapping = false;
        this.AlignmentSnapping = true;
        this.SnappingRadius = 1;
        this.PickingAreaChanged = true;
        this.CurrentDrawingShape = null;
        this.MouseTouchMoving = null;
        this.MouseDoubleClicked = null;
        this.MouseClicked = null;
        this.EngineNotification = null;
        this.EngineStateChanged = null;
        this.RegionSelected = null;
        this.RegionSelectionCancelled = null;
        this.PointerCursor = new InvariantShapes.CursorHair();
        this.TangentSnapPoint = new Shapes.R2Point();
        this.ClearColor = new WebGL.Vector4(1, 1, 1, 1);
        this.FarClippingPlane = 1000;
        this.PanTemporaryTranslationX = 0;
        this.PanTemporaryTranslationY = 0;
        this.PanTemporaryTranslationZ = 0;
        this.PanFinalTranslationX = 0;
        this.PanFinalTranslationY = 0;
        this.PanFinalTranslationZ = 0;
        this.ProjectionAngle = Angle.Zero;
        this.DrawingBaseGrid = new DrawingTools.BaseGrid();
        this.CurrentDrawingLayer = new DrawingLayer();
        this._PlanLayer = new DrawingLayer();
        this._RoofLayer = new DrawingLayer();
        this._FrontElevationLayer = new DrawingLayer();
        this._BackElevationLayer = new DrawingLayer();
        this._LeftElevationLayer = new DrawingLayer();
        this._RightElevationLayer = new DrawingLayer();
        this.AdditionalLayers = new Array();
        this._Sprites = new Array();
        this.AlignmentLine = new Shapes.R2Line();
        this._AdditionalPointsCloud = null;
        this._SelectedShape = null;
        this.HideDrawingGrid = false;
        this.HideCenterLines = false;
        this.HidePointerCursor = false;
        this.DisplayPickingBuffer = false;
        this._DisplayMeshingGrid = false;
        this._MeshingGrid = new Meshing.MeshGrid();
        this.HorizontalAlignmentLines = new Array();
        this.VerticalAlignmentLines = new Array();
        this.EnableAlignmentLines = false;
        this.CurrentDeviceInputMode = DeviceInputMode.Tablet;
        this.ShapeWhenPointerDown = null;
        this.IsLeftMouseButtonDown = false;
        this.IsRightMouseButtonDown = false;
        this.LongPress = null;
        this.ShapeWhenPointerUp = null;
        this.CancelMouseClick = false;
        this.ShapeWhenPointerMove = null;
        this.PanButton = 4;
        this.HafezDrawingMode = true;
        this.IsThere_A_MovingControlPoint = false;
        this.AcceptHandDrawing = false;
        this.FixedZoomingDeltaWidth = 80;
        this.CurrentDrawingShapeMouseClicks = 0;
        if (glcanvas == undefined) {
            alert("No valid canvas has been given");
            throw "No valid canvas";
        }
        this.Canvas = glcanvas;
        this.gl = null;
        try {
            this.gl = (this.Canvas.getContext("webgl") || this.Canvas.getContext("experimental-webgl"));
        }
        catch (e) { }
        if (!this.gl) {
            alert("Unable to initialize WebGL. Your browser may not support it.");
            this.gl = null;
        }
        this.Canvas.width = this.Canvas.parentElement.clientWidth;
        this.Canvas.height = this.Canvas.parentElement.clientHeight;
        this.PickingBuffer = WebGL.FrameBuffer.CreateFrameBuffer(this.gl);
        var self = this;
        var this_Canvas = this.Canvas;
        window.addEventListener("resize", function (e) { return _this.CanvasResize(_this, e); }, false);
        this_Canvas.addEventListener("mousedown", function (e) { return _this.MouseDown(_this, e); }, false);
        this_Canvas.addEventListener("touchstart", function (e) { return _this.TouchStart(_this, e); }, false);
        this_Canvas.addEventListener("mouseup", function (e) { return _this.MouseUp(_this, e); }, false);
        this_Canvas.addEventListener("touchend", function (e) { return _this.TouchEnd(_this, e); }, false);
        this_Canvas.addEventListener("mousemove", function (e) { return _this.MouseMove(_this, e); }, false);
        this_Canvas.addEventListener("touchmove", function (e) { return _this.TouchMove(_this, e); }, false);
        this_Canvas.addEventListener("contextmenu", function (e) { return e.preventDefault(); }, false);
        this_Canvas.addEventListener("click", function (e) { return _this.MouseClick(_this, e); }, false);
        this_Canvas.addEventListener("dblclick", function (e) { return _this.MouseDoubleClick(_this, e); }, false);
        this_Canvas.addEventListener("DOMMouseScroll", function (e) { return _this.MouseScroll(_this, e); }, false);
        this_Canvas.addEventListener("mousewheel", function (e) { return _this.MouseScroll(_this, e); }, false);
        window.addEventListener("keydown", function (e) { return _this.KeyDown(_this, e); }, false);
        this.InvalidateSize();
        this.PointerCursor.ShapeColor = WebGL.Vector4Colors.Black;
        this.TangentSnapPoint.ShapeColor = WebGL.Vector4Colors.GoldenRod;
        this._PlanLayer.Name = "Plan";
        this._RoofLayer.Name = "Roof";
        this._BackElevationLayer.Name = "Back Elevation";
        this._FrontElevationLayer.Name = "Front Elevation";
        this._LeftElevationLayer.Name = "Left Elevation";
        this._RightElevationLayer.Name = "Right Elevation";
        this._RoofLayer.DefaultDrawingColor = WebGL.Vector4Colors.Red;
        this._RoofLayer.ShouldCalculateOffsetBoundary = false;
        this._BackElevationLayer.ShouldCalculateOffsetBoundary = false;
        this._FrontElevationLayer.ShouldCalculateOffsetBoundary = false;
        this._LeftElevationLayer.ShouldCalculateOffsetBoundary = false;
        this._RightElevationLayer.ShouldCalculateOffsetBoundary = false;
    }
    Object.defineProperty(R2Editor.prototype, "CurrentCommand", {
        get: function () {
            return this._CurrentCommand;
        },
        set: function (value) {
            this._LastCommand = this._CurrentCommand;
            this._CurrentCommand = value;
            if (this._CurrentCommand == EditorCommands.Drawing) {
                this.PointerCursor.CurrentMouseDrawingShape = this.CurrentMouseDrawingShape;
            }
            else {
                this.PointerCursor.CurrentMouseDrawingShape = MouseDrawingShape.Selection;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "CommandClicks", {
        get: function () {
            return this.CommandClicksStack.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "ZoomFactor", {
        get: function () {
            return this._ZoomFactor;
        },
        set: function (value) {
            this._ZoomFactor = value;
            if (this._ZoomFactor < 0.001)
                this._ZoomFactor = 0.001;
            if (this._ZoomFactor > 3)
                this._ZoomFactor = 3;
            this.Paint();
            this.PickingAreaChanged = true;
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.findPos = function (obj) {
        var curleft = 0, curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return { x: curleft, y: curtop };
        }
        return undefined;
    };
    Object.defineProperty(R2Editor.prototype, "CurrentMouseDrawingShape", {
        get: function () {
            return this._CurrentMouseDrawingShape;
        },
        set: function (value) {
            this._CurrentMouseDrawingShape = value;
            this.PointerCursor.CurrentMouseDrawingShape = value;
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.MousePointToScaledViewportPoint = function (p) {
        var x = ((2.0 * p.X) / this.Canvas.width) - 1.0;
        var y = 1.0 - ((2.0 * p.Y) / this.Canvas.height);
        var pf = new WebGL.Vector2(x, y);
        return pf;
    };
    R2Editor.prototype.MousePointToViewportPoint = function (p) {
        var x = p.X;
        var y = this.Canvas.height - p.Y;
        return new WebGL.Vector2(x, y);
    };
    R2Editor.prototype.UnProject2D = function (scaledPoint) {
        var finalMatrix;
        var _in = new WebGL.Vector4(0, 0, 0, 0);
        var _out = new WebGL.Vector4(0, 0, 0, 0);
        finalMatrix = this.OrthogonalMatrix;
        finalMatrix = WebGL.Matrix4.Invert(finalMatrix);
        _in.X = scaledPoint.X;
        _in.Y = scaledPoint.Y;
        _in.Z = 0.0;
        _in.W = 1.0;
        _out = WebGL.Vector4.Transform(_in, finalMatrix);
        if (_out.W == 0.0)
            return (_out.Xyz);
        _out.X /= _out.W;
        _out.Y /= _out.W;
        _out.Z /= _out.W;
        return (_out.Xyz);
    };
    Object.defineProperty(R2Editor.prototype, "CurrentMouseLocation", {
        get: function () {
            return this._CurrentMouseLocation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "CurrentScaledMouseLocation", {
        get: function () {
            return this.MousePointToScaledViewportPoint(this._CurrentMouseLocation);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "CurrentProjectedMouseLocation", {
        get: function () {
            return this.UnProject2D(this.CurrentScaledMouseLocation);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "AlignedPoint", {
        get: function () {
            var ii = 0;
            var xarr = new Array();
            var yarr = new Array();
            for (ii = 0; ii < this.HorizontalAlignmentLines.length; ii++) {
                var hline = this.HorizontalAlignmentLines[ii];
                var dy = hline.LastPoint.Y - this.CurrentProjectedMouseLocation.Y;
                var item = [dy, Math.abs(dy)];
                yarr.push(item);
            }
            for (ii = 0; ii < this.VerticalAlignmentLines.length; ii++) {
                var vline = this.VerticalAlignmentLines[ii];
                var dx = vline.LastPoint.X - this.CurrentProjectedMouseLocation.X;
                var item = [dx, Math.abs(dx)];
                xarr.push(item);
            }
            if (xarr.length > 0 || yarr.length > 0) {
                xarr.sort(function (a, b) { return a[1] - b[1]; });
                yarr.sort(function (a, b) { return a[1] - b[1]; });
            }
            if (xarr.length > 0 && yarr.length > 0)
                return new WebGL.Vector2(this.CurrentProjectedMouseLocation.X + xarr[0][0], this.CurrentProjectedMouseLocation.Y + yarr[0][0]);
            else if (xarr.length > 0)
                return new WebGL.Vector2(this.CurrentProjectedMouseLocation.X + xarr[0][0], this.CurrentProjectedMouseLocation.Y);
            else if (yarr.length > 0)
                return new WebGL.Vector2(this.CurrentProjectedMouseLocation.X, this.CurrentProjectedMouseLocation.Y + yarr[0][0]);
            else
                return null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "CurrentProjectedIntegerMouseLocation", {
        get: function () {
            var xi = Math.round(this.CurrentProjectedMouseLocation.X);
            var yi = Math.round(this.CurrentProjectedMouseLocation.Y);
            var zi = Math.round(this.CurrentProjectedMouseLocation.Z);
            return new WebGL.Vector3(xi, yi, zi);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "CurrentSnappedMouseLocation", {
        get: function () {
            var CoordinatesPoints = null;
            var Intersections = null;
            var Terminals = null;
            var Projections = null;
            var TotalPoints = new PointsCloud();
            if (this.ShapeWhenPointerMove != null) {
                if (this.ShapePointsSnapping) {
                    CoordinatesPoints = this.CurrentDrawingLayer.GetShapeCoordinatesIntersectionsPointsCloud(this.ShapeWhenPointerMove);
                }
                if (this.ShapeTerminalsAndIntersectionsSnapping) {
                    Intersections = this.CurrentDrawingLayer.GetShapeIntersectionsPointsCloud(this.ShapeWhenPointerMove);
                    Terminals = this.CurrentDrawingLayer.GetShapeTerminalsPointsCloud(this.ShapeWhenPointerMove);
                }
                if (this.ShapeEdgesProjectionsSnapping) {
                    Projections = this.CurrentDrawingLayer.GetShapeProjectionsPointsCloud(this.ShapeWhenPointerMove);
                }
            }
            if (CoordinatesPoints != null)
                CoordinatesPoints.CloudPoints.forEach(function (p) { return TotalPoints.AddPoint(p.Key, p.Location); });
            if (Intersections != null)
                Intersections.CloudPoints.forEach(function (p) { return TotalPoints.AddPoint(p.Key, p.Location); });
            if (Terminals != null)
                Terminals.CloudPoints.forEach(function (p) { return TotalPoints.AddPoint(p.Key, p.Location); });
            if (Projections != null)
                Projections.CloudPoints.forEach(function (p) { return TotalPoints.AddPoint(p.Key, p.Location); });
            this.PointerCursor.CurrentSnappingOption = SnappingOption.None;
            if (CoordinatesPoints == null && Intersections == null && Terminals == null && Projections == null) {
                if (this.GridSnapping) {
                    TotalPoints.AddPoint("Mouse", this.CurrentProjectedIntegerMouseLocation);
                    if (this.AlignmentSnapping && this.AlignedPoint != null) {
                        TotalPoints.AddPoint("Aligned", this.AlignedPoint.ToVector3());
                    }
                }
                else {
                    if (this.AlignmentSnapping && this.AlignedPoint != null) {
                        TotalPoints.AddPoint("Aligned", this.AlignedPoint.ToVector3());
                    }
                    else {
                        TotalPoints.AddPoint("Mouse", this.CurrentProjectedMouseLocation);
                    }
                }
            }
            if (TotalPoints.PointsCount > 0) {
                var vv = TotalPoints.GetClosestPointTo(this.CurrentProjectedMouseLocation.Xy);
                var tolerance = Math.abs(WebGL.Vector3.Subtract(vv, this.CurrentProjectedMouseLocation).Length);
                if (tolerance <= this.SnappingRadius || this.ShapeWhenPointerMove == null) {
                    if (TotalPoints.PointsCount > 1)
                        this.PointerCursor.CurrentSnappingOption = SnappingOption.Terminals;
                    return vv;
                }
                else {
                    var nearest = this.ShapeWhenPointerMove.GetNearestPoint(this.CurrentProjectedMouseLocation.Xy);
                    if (nearest != null) {
                        if (this.ShapeWhenPointerMove instanceof Shapes.R2Line && this.CurrentDrawingShape instanceof Shapes.R2Line) {
                            var swpm = this.ShapeWhenPointerMove;
                            var cds = this.CurrentDrawingShape;
                            var projection = swpm.GetPointProjection(cds.FirstPointLocation);
                            var tolerance = Math.abs(WebGL.Vector3.Subtract(projection.ToVector3(), this.CurrentProjectedMouseLocation).Length);
                            if (tolerance <= this.SnappingRadius || this.ShapeWhenPointerMove == null) {
                                this.PointerCursor.CurrentSnappingOption = SnappingOption.Perpendicular;
                                return projection.ToVector3();
                            }
                            else {
                                this.PointerCursor.CurrentSnappingOption = SnappingOption.Nearest;
                                return nearest.ToVector3();
                            }
                        }
                        else {
                            this.PointerCursor.CurrentSnappingOption = SnappingOption.Nearest;
                            return nearest.ToVector3();
                        }
                    }
                    else
                        return this.CurrentProjectedMouseLocation;
                }
            }
            else {
                if (this.GridSnapping) {
                    TotalPoints.AddPoint("Mouse", this.CurrentProjectedIntegerMouseLocation);
                }
                else {
                    TotalPoints.AddPoint("Mouse", this.CurrentProjectedMouseLocation);
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.Notify = function (notification) {
        if (this.EngineNotification != null) {
            this.EngineNotification(this, notification);
        }
    };
    Object.defineProperty(R2Editor.prototype, "ProjectionWidth", {
        get: function () {
            return this._ZoomFactor * this.Canvas.width;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "ProjectionHeight", {
        get: function () {
            return this._ZoomFactor * this.Canvas.height;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "MaximumX", {
        get: function () {
            return this.CurrentDrawingLayer.MaximumX;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "MinimumX", {
        get: function () {
            return this.CurrentDrawingLayer.MinimumX;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "MaximumY", {
        get: function () {
            return this.CurrentDrawingLayer.MaximumY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "MinimumY", {
        get: function () {
            return this.CurrentDrawingLayer.MinimumY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "ViewportWidth", {
        get: function () {
            if (this.MaximumX < 0 && this.MinimumX < 0) {
                return Math.abs(this.MinimumX) - Math.abs(this.MaximumX);
            }
            else if (this.MaximumX > 0 && this.MinimumX < 0) {
                return this.MaximumX - this.MinimumX;
            }
            else {
                return this.MaximumX - this.MinimumX;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "ViewportHeight", {
        get: function () {
            if (this.MaximumY < 0 && this.MinimumY < 0) {
                return Math.abs(this.MinimumY) - Math.abs(this.MaximumY);
            }
            else if (this.MaximumY > 0 && this.MinimumY < 0) {
                return this.MaximumY - this.MinimumY;
            }
            else {
                return this.MaximumY - this.MinimumY;
            }
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.ZoomExtent = function (percentage) {
        if (percentage === void 0) { percentage = 100; }
        if (this.CurrentDrawingLayer.ShapesCount != 0) {
            var max_x = this.MaximumX;
            var min_x = this.MinimumX;
            var max_y = this.MaximumY;
            var min_y = this.MinimumY;
            var deltaWidth = max_x - min_x;
            var deltaHeight = max_y - min_y;
            this.PanFinalTranslationX = -(min_x + deltaWidth / 2);
            this.PanFinalTranslationY = -(min_y + deltaHeight / 2);
            var maxwidth = this.ViewportWidth > this.ViewportHeight ? this.ViewportWidth : this.ViewportHeight;
            var controlWidth = this.ViewportWidth > this.ViewportHeight ? this.Canvas.width : this.Canvas.height;
            this._ZoomFactor = maxwidth / controlWidth;
            var zw = (this.ViewportWidth / this.Canvas.width);
            var zh = (this.ViewportHeight / this.Canvas.height);
            if (zw > zh)
                this._ZoomFactor = zw;
            else
                this._ZoomFactor = zh;
            this._ZoomFactor = this._ZoomFactor / (percentage / 100);
        }
        this.Paint();
        this.PickingAreaChanged = true;
    };
    Object.defineProperty(R2Editor.prototype, "NearClippingPlane", {
        get: function () {
            return -1000;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "ProjectionRotationAngleDegrees", {
        get: function () {
            return this.ProjectionAngle.AbsoluteAngle.Degrees;
        },
        set: function (value) {
            this.ProjectionAngle = Angle.FromDegrees(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "RotationMatrix", {
        get: function () {
            var q = WebGL.Quaternion.FromAxisAngle(WebGL.Vector3.UnitZ, this.ProjectionAngle.Radians);
            return WebGL.Matrix4.CreateFromQuaternion(q);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "OrthogonalMatrixWithoutRotation", {
        get: function () {
            var ortho = WebGL.Matrix4.CreateOrthographic(this.ProjectionWidth, this.ProjectionHeight, this.NearClippingPlane, this.FarClippingPlane);
            var PanTranslation = WebGL.Matrix4.CreateTranslation(this.PanFinalTranslationX + this.PanTemporaryTranslationX, this.PanFinalTranslationY + this.PanTemporaryTranslationY, 0);
            var t1 = WebGL.Matrix4.Multiply(PanTranslation, ortho);
            return t1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "OrthogonalMatrix", {
        get: function () {
            var ortho = WebGL.Matrix4.CreateOrthographic(this.ProjectionWidth, this.ProjectionHeight, this.NearClippingPlane, this.FarClippingPlane);
            var PanTranslation = WebGL.Matrix4.CreateTranslation(this.PanFinalTranslationX + this.PanTemporaryTranslationX, this.PanFinalTranslationY + this.PanTemporaryTranslationY, 0);
            var t1 = WebGL.Matrix4.Multiply(this.RotationMatrix, ortho);
            var t2 = WebGL.Matrix4.Multiply(PanTranslation, t1);
            return t2;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "PlanLayer", {
        get: function () {
            return this._PlanLayer;
        },
        set: function (value) {
            this._PlanLayer = value;
            this._PlanLayer.Name = "Plan";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "RoofLayer", {
        get: function () {
            return this._RoofLayer;
        },
        set: function (value) {
            this._RoofLayer = value;
            this._RoofLayer.Name = "Roof";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "FrontElevationLayer", {
        get: function () {
            return this._FrontElevationLayer;
        },
        set: function (value) {
            this._FrontElevationLayer = value;
            this._FrontElevationLayer.Name = "Front Elevation";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "BackElevationLayer", {
        get: function () {
            return this._BackElevationLayer;
        },
        set: function (value) {
            this._BackElevationLayer = value;
            this._BackElevationLayer.Name = "Back Elevation";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "LeftElevationLayer", {
        get: function () {
            return this._LeftElevationLayer;
        },
        set: function (value) {
            this._LeftElevationLayer = value;
            this._LeftElevationLayer.Name = "Left Elevation";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "RightElevationLayer", {
        get: function () {
            return this._RightElevationLayer;
        },
        set: function (value) {
            this._RightElevationLayer = value;
            this._RightElevationLayer.Name = "Right Elevation";
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.IsTargetLayerExists = function (layer) {
        for (var ix = 0; ix < this.AdditionalLayers.length; ix++) {
            if (this.AdditionalLayers[ix].Id === layer.Id)
                return true;
        }
        return false;
    };
    R2Editor.prototype.AddLineOverSingleLine = function (srcLine, underLine) {
        var innerCount = srcLine.IsLyingOn(underLine);
        if (innerCount == 2) {
            this.RemoveShape(underLine);
            if (underLine.IsHavingSameTerminals(srcLine)) {
                this.AddShape(srcLine);
            }
            else {
                var ips = new Array();
                if (srcLine.FirstPointLocation.NearlyEqual(underLine.FirstPointLocation)
                    || srcLine.FirstPointLocation.NearlyEqual(underLine.LastPointLocation)) {
                    ips.push(srcLine.LastPointLocation.Xy);
                    underLine.AddIntersectedShape(srcLine, ips);
                    underLine.ApplySegmentation();
                    var l1 = underLine.InnerSegments[0];
                    var l2 = underLine.InnerSegments[1];
                    if (FloatingMath.NearlyEqual(l1.Vector.Length, 0) == false)
                        this.AddShape(l1);
                    if (FloatingMath.NearlyEqual(l2.Vector.Length, 0) == false)
                        this.AddShape(l2);
                }
                else if (srcLine.LastPointLocation.NearlyEqual(underLine.FirstPointLocation)
                    || srcLine.LastPointLocation.NearlyEqual(underLine.LastPointLocation)) {
                    ips.push(srcLine.FirstPointLocation.Xy);
                    underLine.AddIntersectedShape(srcLine, ips);
                    underLine.ApplySegmentation();
                    var l1 = underLine.InnerSegments[0];
                    var l2 = underLine.InnerSegments[1];
                    if (FloatingMath.NearlyEqual(l1.Vector.Length, 0) == false)
                        this.AddShape(l1);
                    if (FloatingMath.NearlyEqual(l2.Vector.Length, 0) == false)
                        this.AddShape(l2);
                }
                else {
                    ips.push(srcLine.FirstPointLocation.Xy);
                    ips.push(srcLine.LastPointLocation.Xy);
                    underLine.AddIntersectedShape(srcLine, ips);
                    underLine.ApplySegmentation();
                    var l1 = underLine.InnerSegments[0];
                    var l2 = srcLine;
                    var l3 = underLine.InnerSegments[2];
                    if (FloatingMath.NearlyEqual(l1.Vector.Length, 0) == false)
                        this.AddShape(l1);
                    if (FloatingMath.NearlyEqual(l3.Vector.Length, 0) == false)
                        this.AddShape(l3);
                    this.AddShape(l2);
                }
            }
        }
        else if (innerCount == 1) {
            if (underLine.IsLyingOn(srcLine) > 1) {
                this.AddLineOverSingleLine(underLine, srcLine);
            }
            else {
                this.RemoveShape(underLine);
                var vv1;
                var freeEnd;
                if (underLine.IsPointOnLineSegment(srcLine.FirstPointLocation.Xy)) {
                    vv1 = srcLine.FirstPointLocation.Xy;
                    freeEnd = srcLine.LastPointLocation.Xy;
                }
                if (underLine.IsPointOnLineSegment(srcLine.LastPointLocation.Xy)) {
                    vv1 = srcLine.LastPointLocation.Xy;
                    freeEnd = srcLine.FirstPointLocation.Xy;
                }
                var l1 = new Shapes.R2Line();
                l1.FirstPointLocation = underLine.FirstPointLocation;
                l1.LastPointLocation = vv1.ToVector3();
                var l2 = new Shapes.R2Line();
                l2.FirstPointLocation = vv1.ToVector3();
                l2.LastPointLocation = underLine.LastPointLocation;
                var vv2;
                if (srcLine.IsPointOnLineSegment(underLine.FirstPointLocation.Xy))
                    vv2 = underLine.FirstPointLocation.Xy;
                if (srcLine.IsPointOnLineSegment(underLine.LastPointLocation.Xy))
                    vv2 = underLine.LastPointLocation.Xy;
                var l3 = new Shapes.R2Line();
                l3.FirstPointLocation = freeEnd.ToVector3();
                l3.LastPointLocation = vv2.ToVector3();
                if (!FloatingMath.NearlyEqual(l1.Vector.Length, 0))
                    this.AddShape(l1);
                if (!FloatingMath.NearlyEqual(l2.Vector.Length, 0))
                    this.AddShape(l2);
                if (!FloatingMath.NearlyEqual(l3.Vector.Length, 0))
                    this.AddShape(l3);
            }
        }
        else if (innerCount == 0) {
            if (underLine.IsLyingOn(srcLine) > 0)
                this.AddLineOverSingleLine(underLine, srcLine);
        }
    };
    R2Editor.prototype.AddLineAndBlend = function (line) {
        var all_lines = this.CurrentDrawingLayer.AllLines;
        if (all_lines.length == 0) {
            this.AddShape(line);
            return;
        }
        var excluding_shapes = new Array();
        for (var ix = 0; ix < all_lines.length; ix++) {
            var al = all_lines[ix];
            if (al.BuildingShapes.length > 0) {
                al.BuildingShapes.forEach(function (x) { return excluding_shapes.push(x); });
                excluding_shapes.push(al);
            }
        }
        var currentLines = new Array();
        for (var ix = 0; ix < all_lines.length; ix++) {
            var al = all_lines[ix];
            var exclude = false;
            for (var iy = 0; iy < excluding_shapes.length; iy++) {
                var tl = excluding_shapes[iy];
                if (al.ColorKey == tl.ColorKey) {
                    exclude = true;
                    break;
                }
            }
            if (exclude == false) {
                currentLines.push(al);
            }
        }
        var srcLine = line;
        var underLines = new Array();
        var aboveLines = new Array();
        for (var ix = 0; ix < currentLines.length; ix++) {
            var underLine = currentLines[ix];
            var innerCount = srcLine.IsLyingOn(underLine);
            if (innerCount > 0) {
                underLines.push(underLine);
            }
            else if (innerCount == 0) {
                if (underLine.IsLyingOn(srcLine) > 0)
                    aboveLines.push(underLine);
            }
        }
        if (underLines.length > 2) {
            console.log("line over more than two lines is not yet implemented");
        }
        else if (underLines.length == 2) {
            var ul1 = underLines[0];
            var ul2 = underLines[1];
            var ul1_p = null;
            if (ul1.IsPointOnLineSegment(srcLine.FirstPointLocation.Xy)) {
                ul1_p = srcLine.FirstPointLocation.Xy;
            }
            if (ul1.IsPointOnLineSegment(srcLine.LastPointLocation.Xy)) {
                ul1_p = srcLine.LastPointLocation.Xy;
            }
            var ul1_a = new Shapes.R2Line();
            var ul1_b = new Shapes.R2Line();
            var ul2_p = null;
            if (ul2.IsPointOnLineSegment(srcLine.FirstPointLocation.Xy)) {
                ul2_p = srcLine.FirstPointLocation.Xy;
            }
            if (ul2.IsPointOnLineSegment(srcLine.LastPointLocation.Xy)) {
                ul2_p = srcLine.LastPointLocation.Xy;
            }
            if (ul1_p != null && ul2_p != null && FloatingMath.Vector2NearlyEqual(ul1_p, ul2_p) == false) {
                this.RemoveShape(ul1);
                ul1_a.FirstPointLocation = ul1.FirstPointLocation;
                ul1_a.LastPointLocation = ul1_p.ToVector3();
                ul1_b.FirstPointLocation = ul1_p.ToVector3();
                ul1_b.LastPointLocation = ul1.LastPointLocation;
                this.AddShape(ul1_a);
                this.AddShape(ul1_b);
                this.RemoveShape(ul2);
                var ul2_a = new Shapes.R2Line();
                var ul2_b = new Shapes.R2Line();
                ul2_a.FirstPointLocation = ul2.FirstPointLocation;
                ul2_a.LastPointLocation = ul2_p.ToVector3();
                ul2_b.FirstPointLocation = ul2_p.ToVector3();
                ul2_b.LastPointLocation = ul2.LastPointLocation;
                this.AddShape(ul2_a);
                this.AddShape(ul2_b);
                if (ul1.FirstPointLocation.NearlyEqual(ul2.FirstPointLocation) || ul1.FirstPointLocation.NearlyEqual(ul2.LastPointLocation)) {
                }
                else {
                    var t1;
                    var t2;
                    if (srcLine.IsPointOnLineSegment(ul1.LastPointLocation.Xy))
                        t1 = ul1.LastPointLocation;
                    else
                        t1 = ul1.FirstPointLocation;
                    if (srcLine.IsPointOnLineSegment(ul2.LastPointLocation.Xy))
                        t2 = ul2.LastPointLocation;
                    else
                        t2 = ul2.FirstPointLocation;
                    var restLine = new Shapes.R2Line();
                    restLine.FirstPointLocation = t1;
                    restLine.LastPointLocation = t2;
                    this.AddShape(restLine);
                }
            }
            else {
                if (srcLine.IsLyingOn(ul1) == 2) {
                    this.AddLineOverSingleLine(srcLine, ul1);
                }
                else if (ul1.IsLyingOn(srcLine) == 2) {
                    this.AddLineOverSingleLine(ul1, srcLine);
                }
                if (srcLine.IsLyingOn(ul2) == 2) {
                    this.AddLineOverSingleLine(srcLine, ul2);
                }
                else if (ul2.IsLyingOn(srcLine) == 2) {
                    this.AddLineOverSingleLine(ul2, srcLine);
                }
            }
        }
        else if (underLines.length == 1) {
            var underLine = underLines[0];
            var success = true;
            if (srcLine.IsLyingOn(underLine) == 1) {
                if (srcLine.IsPointTerminal(underLine.FirstPointLocation)) {
                    if (!srcLine.IsPointOnLineSegment(underLine.LastPointLocation.Xy)) {
                        success = false;
                    }
                }
                else if (srcLine.IsPointTerminal(underLine.LastPointLocation)) {
                    if (!srcLine.IsPointOnLineSegment(underLine.FirstPointLocation.Xy)) {
                        success = false;
                    }
                }
            }
            if (success) {
                this.AddLineOverSingleLine(srcLine, underLine);
            }
            else {
                this.AddShape(srcLine);
            }
        }
        else {
            if (aboveLines.length == 1) {
                this.AddLineOverSingleLine(aboveLines[0], srcLine);
            }
            else {
                this.AddShape(srcLine);
            }
        }
    };
    R2Editor.prototype.StateChanged = function () {
        var _this = this;
        this.PickingAreaChanged = true;
        if (this.CurrentDrawingLayer.ShouldCalculateOffsetBoundary == true && this.IsRightMouseButtonDown == false) {
            this.CurrentDrawingLayer.CalculateOffsetBoundary();
            this.RoofLayer.ClearShapes();
            if (this.CurrentDrawingLayer.BoundaryLines != null) {
                this.CurrentDrawingLayer.BoundaryLines.forEach(function (l) {
                    _this.RoofLayer.AddShape(l);
                });
            }
        }
        if (this.EngineStateChanged != null)
            this.EngineStateChanged(this);
    };
    R2Editor.prototype.AddShape = function (shape) {
        this.CurrentDrawingLayer.AddShape(shape);
        console.log(shape.ShapeName + " Added.");
        this._LastAddedShape = shape;
        this.StateChanged();
    };
    R2Editor.prototype.ReplaceShape = function (shapeIndex, newShape) {
        this.CurrentDrawingLayer.ReplaceShape(shapeIndex, newShape);
        this.StateChanged();
    };
    R2Editor.prototype.RemoveShape = function (shape) {
        if (shape.OwnerBuildingShape != null) {
            shape.OwnerBuildingShape.RemoveBuildingShape(shape);
            shape.OwnerBuildingShape = null;
        }
        if (shape.BuildingShapes.length == 0) {
            var idx = this.CurrentDrawingLayer.GetShapeIndex(shape);
            this.RemoveShapeByIndex(idx);
        }
        this.StateChanged();
    };
    R2Editor.prototype.RemoveLastShape = function () {
        this.CurrentDrawingLayer.RemoveShape(this.CurrentDrawingLayer.ShapesCount - 1);
        this.StateChanged();
    };
    R2Editor.prototype.RemoveShapeByIndex = function (shapeIndex) {
        this.CurrentDrawingLayer.RemoveShape(shapeIndex);
    };
    R2Editor.prototype.AddSprite = function (sprite) {
        this._Sprites.push(sprite);
    };
    R2Editor.prototype.ClearSprites = function () {
        this._Sprites = new Array();
        this.Paint();
    };
    R2Editor.prototype.ClearShapes = function () {
        this.CurrentDrawingLayer.ClearShapes();
        this.Paint();
        this.PickingAreaChanged = true;
    };
    Object.defineProperty(R2Editor.prototype, "AdditionalPointsCloud", {
        get: function () {
            if (this._AdditionalPointsCloud != null)
                return this._AdditionalPointsCloud.PointsLocations;
            else
                return null;
        },
        set: function (value) {
            if (value == null) {
                this._AdditionalPointsCloud = null;
            }
            else {
                this._AdditionalPointsCloud = new PointsCloud();
                this._AdditionalPointsCloud.AddPoints("AdditionalPoints", value);
            }
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.RenderBackgroundLayer = function (layer) {
        if (this.CurrentDrawingLayer.Id === layer.Id)
            return;
        layer.ProjectionMatrix = this.OrthogonalMatrix;
        layer.ProjectionWidth = this.ProjectionWidth;
        layer.ProjectionHeight = this.ProjectionHeight;
        layer.BlendingRender(this.gl, 0.4);
    };
    R2Editor.prototype.RenderEverything = function () {
        var _this = this;
        this.AdditionalLayers.forEach(function (layer) {
            _this.RenderBackgroundLayer(layer);
        });
        if (this.PlanLayer.IsBackGroundVisible)
            this.RenderBackgroundLayer(this.PlanLayer);
        if (this.RoofLayer.IsBackGroundVisible)
            this.RenderBackgroundLayer(this.RoofLayer);
        if (this.FrontElevationLayer.IsBackGroundVisible)
            this.RenderBackgroundLayer(this.FrontElevationLayer);
        if (this.BackElevationLayer.IsBackGroundVisible)
            this.RenderBackgroundLayer(this.BackElevationLayer);
        if (this.LeftElevationLayer.IsBackGroundVisible)
            this.RenderBackgroundLayer(this.LeftElevationLayer);
        if (this.RightElevationLayer.IsBackGroundVisible)
            this.RenderBackgroundLayer(this.RightElevationLayer);
        this.CurrentDrawingLayer.ProjectionMatrix = this.OrthogonalMatrix;
        this.CurrentDrawingLayer.ProjectionWidth = this.ProjectionWidth;
        this.CurrentDrawingLayer.ProjectionHeight = this.ProjectionHeight;
        this.CurrentDrawingLayer.Render(this.gl);
        if (this.CurrentDrawingShape != null) {
            this.CurrentDrawingShape.ProjectionMatrix = this.OrthogonalMatrix;
            this.CurrentDrawingShape.Render(this.gl);
        }
    };
    Object.defineProperty(R2Editor.prototype, "SelectedShape", {
        get: function () {
            return this._SelectedShape;
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.RenderEverythingForPicking = function () {
        this.gl.clearColor(1, 1, 1, 1);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
        this.gl.viewport(0, 0, this.Canvas.width, this.Canvas.height);
        var visibleshapes = this.CurrentDrawingLayer.VisibleShapes;
        var ix;
        for (ix = 0; ix < visibleshapes.length; ix++) {
            var sh = visibleshapes[ix];
            sh.ProjectionMatrix = this.OrthogonalMatrix;
            sh.RenderForPicking(this.gl);
        }
        for (ix = 0; ix < visibleshapes.length; ix++) {
            var sh = visibleshapes[ix];
            if (sh.IsSelected) {
                sh.RenderEditingControlsForPicking(this.gl);
            }
        }
    };
    Object.defineProperty(R2Editor.prototype, "DisplayMeshingGrid", {
        get: function () {
            return this._DisplayMeshingGrid;
        },
        set: function (value) {
            if (value == false) {
                this._MeshingGrid = new Meshing.MeshGrid();
            }
            this._DisplayMeshingGrid = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R2Editor.prototype, "MeshingGrid", {
        get: function () {
            return this._MeshingGrid;
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.CalculateAlignmentLines = function () {
        this.HorizontalAlignmentLines = new Array();
        this.VerticalAlignmentLines = new Array();
        var points = this.CurrentDrawingLayer.WholePointsLocations;
        if (this.CurrentDrawingShape instanceof Shapes.R2Line) {
            var line = this.CurrentDrawingShape;
            points.push(line.FirstPointLocation);
        }
        if (this.ShapeEdgesProjectionsSnapping) {
            var intersections = this.CurrentDrawingLayer.GetAllIntersectionsAndTerminals();
            intersections.forEach(function (p) { return points.push(p.ToVector3()); });
        }
        var ix;
        for (ix = 0; ix < points.length; ix++) {
            var point = points[ix];
            if (Math.abs(this.CurrentProjectedMouseLocation.X - point.X) <= 0.8) {
                var hline = new Geometry.Line(point, new WebGL.Vector3(point.X, this.CurrentProjectedMouseLocation.Y, 0));
                this.VerticalAlignmentLines.push(hline);
            }
            if (Math.abs(this.CurrentProjectedMouseLocation.Y - point.Y) <= 0.8) {
                var vline = new Geometry.Line(point, new WebGL.Vector3(this.CurrentProjectedMouseLocation.X, point.Y, 0));
                this.HorizontalAlignmentLines.push(vline);
            }
        }
    };
    R2Editor.prototype.RenderHelperDrawingLines = function () {
        this.AlignmentLine.ProjectionMatrix = this.OrthogonalMatrix;
        this.AlignmentLine.LineType = Shapes.ShapeLineType.Dotted;
        this.AlignmentLine.ShapeColor = new WebGL.Vector4(0.5, 0.5, 0.5, 1);
        var ix;
        var iy;
        for (ix = 0; ix < this.HorizontalAlignmentLines.length; ix++) {
            this.AlignmentLine.FirstPointLocation = this.HorizontalAlignmentLines[ix].FirstPoint;
            this.AlignmentLine.LastPointLocation = this.HorizontalAlignmentLines[ix].LastPoint;
            this.AlignmentLine.Render(this.gl);
        }
        for (iy = 0; iy < this.VerticalAlignmentLines.length; iy++) {
            this.AlignmentLine.FirstPointLocation = this.VerticalAlignmentLines[iy].FirstPoint;
            this.AlignmentLine.LastPointLocation = this.VerticalAlignmentLines[iy].LastPoint;
            this.AlignmentLine.Render(this.gl);
        }
    };
    R2Editor.prototype.Paint = function () {
        if (this.gl) {
            if (this.DisplayPickingBuffer) {
                this.RenderEverythingForPicking();
                return;
            }
            this.gl.clearColor(this.ClearColor.X, this.ClearColor.Y, this.ClearColor.Z, this.ClearColor.W);
            this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT | this.gl.STENCIL_BUFFER_BIT);
            this.gl.viewport(0, 0, this.Canvas.width, this.Canvas.height);
            this.DrawingBaseGrid.Height = this.ProjectionHeight;
            this.DrawingBaseGrid.Width = this.ProjectionWidth;
            this.DrawingBaseGrid.ProjectionMatrix = this.OrthogonalMatrix;
            this.DrawingBaseGrid.DeltaWidth = this.PanFinalTranslationX + this.PanTemporaryTranslationX;
            this.DrawingBaseGrid.DeltaHeight = this.PanFinalTranslationY + this.PanTemporaryTranslationY;
            if (!this.HideDrawingGrid) {
                this.DrawingBaseGrid.Render(this.gl);
            }
            if (!this.HideCenterLines) {
                this.DrawingBaseGrid.RenderCenterLines(this.gl);
            }
            this.RenderEverything();
            if (this.IsRightMouseButtonDown == false && this.EnableAlignmentLines == true) {
                this.CalculateAlignmentLines();
                this.RenderHelperDrawingLines();
            }
            if (this.DisplayMeshingGrid) {
                this.MeshingGrid.ProjectionMatrix = this.OrthogonalMatrix;
                this.MeshingGrid.Render(this.gl);
            }
            if (this._AdditionalPointsCloud != null) {
                this._AdditionalPointsCloud.ProjectionMatrix = this.OrthogonalMatrix;
                this._AdditionalPointsCloud.Render(this.gl);
            }
            if (this.ShapeWhenPointerMove != null) {
                if (this.ShapeWhenPointerMove instanceof Shapes.R2Line) {
                    var ll = this.ShapeWhenPointerMove;
                    if (ll.BuildingType == Shapes.BuildingType.Wall && ll.IsDimension == false) {
                        ll.RenderRectangleLine(this.gl);
                    }
                }
            }
            if (this.ShapeWhenPointerMove != null
                && 1 === 0) {
                if (this.ShapePointsSnapping) {
                    var ccloud = this.CurrentDrawingLayer.GetShapeCoordinatesIntersectionsPointsCloud(this.ShapeWhenPointerMove);
                    if (ccloud != null) {
                        ccloud.ProjectionMatrix = this.OrthogonalMatrix;
                        ccloud.Render(this.gl);
                    }
                }
                if (this.ShapeTerminalsAndIntersectionsSnapping) {
                    var intersections = this.CurrentDrawingLayer.GetShapeIntersectionsPointsCloud(this.ShapeWhenPointerMove);
                    if (intersections != null) {
                        intersections.ProjectionMatrix = this.OrthogonalMatrix;
                        intersections.Render(this.gl);
                    }
                    var terminals = this.CurrentDrawingLayer.GetShapeTerminalsPointsCloud(this.ShapeWhenPointerMove);
                    if (terminals != null) {
                        terminals.ProjectionMatrix = this.OrthogonalMatrix;
                        terminals.Render(this.gl);
                    }
                }
                if (this.ShapeEdgesProjectionsSnapping) {
                    var ccloud = this.CurrentDrawingLayer.GetShapeProjectionsPointsCloud(this.ShapeWhenPointerMove);
                    if (ccloud != null) {
                        ccloud.ProjectionMatrix = this.OrthogonalMatrix;
                        ccloud.Render(this.gl);
                    }
                }
            }
            for (var sprite in this._Sprites) {
                var ix;
                for (ix = 0; ix < this._Sprites.length; ix++) {
                    var sh = this._Sprites[ix];
                    sh.ProjectionMatrix = this.OrthogonalMatrix;
                    sh.Render(this.gl);
                }
            }
            this.PointerCursor.ProjectionWidth = this.ProjectionWidth;
            this.PointerCursor.ProjectionHeight = this.ProjectionHeight;
            this.PointerCursor.ProjectionMatrix = this.OrthogonalMatrix;
            this.PointerCursor.ProjectionRotationAngleDegrees = this.ProjectionRotationAngleDegrees;
            this.PointerCursor.HairCenter = this.CurrentSnappedMouseLocation;
            this.PointerCursor.IsOutBoundary = !this.CurrentDrawingLayer.IsPointInsideBoundary(this.CurrentProjectedMouseLocation.Xy);
            if (this.CurrentDrawingLayer.ShapesCount == 0)
                this.PointerCursor.IsOutBoundary = false;
            if (!this.HidePointerCursor)
                this.PointerCursor.Render(this.gl);
        }
    };
    R2Editor.prototype.GetSelectableShapes = function () {
        var sh = new Array();
        this.CurrentDrawingLayer.VisibleShapes.forEach(function (x) {
            sh.push(x);
            var ecs = x.GetEditingControls();
            ecs.forEach(function (c) { return sh.push(c); });
        });
        return sh;
    };
    R2Editor.prototype.SelectShapeByColor = function (color) {
        var all = this.GetSelectableShapes();
        for (var ix = 0; ix < all.length; ix++) {
            if (all[ix].ColorKey === color.Key)
                return all[ix];
        }
        return null;
    };
    R2Editor.prototype.GetShapeUnderPointer = function (point, pickSquare) {
        var _this = this;
        if (pickSquare === void 0) { pickSquare = 10; }
        var windowpoint = this.MousePointToViewportPoint(point);
        if (this.PickingAreaChanged == true) {
            console.log("Rendering Color Keys");
            if (this.PickingBuffer.ColorBufferImage != null)
                this.PickingBuffer.ColorBufferImage.Dispose(this.gl);
            this.PickingBuffer.ColorBufferImage = WebGL.FrameBuffer.CreateColorTextureBufferImage(this.gl, this.Canvas.width, this.Canvas.height);
            this.PickingBuffer.Use(this.gl);
            this.RenderEverythingForPicking();
            var picked_colors = this.PickingBuffer.ReadPixelsColors(this.gl, windowpoint.X, windowpoint.Y, pickSquare);
            this.PickingBuffer.UnUse(this.gl);
            this.PickingAreaChanged = false;
        }
        else {
            this.PickingBuffer.Use(this.gl);
            var picked_colors = this.PickingBuffer.ReadPixelsColors(this.gl, windowpoint.X, windowpoint.Y, pickSquare);
            this.PickingBuffer.UnUse(this.gl);
        }
        if (picked_colors.length == 0)
            return null;
        var pickedShapes = new Array();
        picked_colors.forEach(function (pickedcolor) {
            if (pickedcolor.Equals(ColorByte4.NullColor) == false && pickedcolor.Equals(ColorByte4.VoidColor) == false) {
                var sh = _this.SelectShapeByColor(pickedcolor);
                pickedShapes.push(sh);
            }
        });
        var selectedShape = null;
        pickedShapes.forEach(function (shape) {
            if (shape instanceof Shapes.R2Point) {
                selectedShape = shape;
            }
        });
        if (selectedShape != null) {
            return selectedShape;
        }
        else {
            if (pickedShapes.length > 0) {
                return pickedShapes[0];
            }
            else {
                return null;
            }
        }
    };
    R2Editor.prototype.UnSelectViewShapes = function (but) {
        if (but === void 0) { but = null; }
        var ix = 0;
        var ExistsWithinShapes = false;
        for (ix = 0; ix < this.CurrentDrawingLayer.ShapesCount; ix++) {
            if (but != null) {
                if (but.ColorKey == this.CurrentDrawingLayer.DrawingShapes[ix].ColorKey) {
                    ExistsWithinShapes = true;
                }
                else {
                    this.CurrentDrawingLayer.DrawingShapes[ix].IsSelected = false;
                }
            }
            else {
                this.CurrentDrawingLayer.DrawingShapes[ix].IsSelected = false;
            }
        }
        return ExistsWithinShapes;
    };
    R2Editor.prototype.InvalidateSize = function () {
        this.Canvas.style.width = this.Canvas.parentElement.clientWidth + 'px';
        this.Canvas.style.height = this.Canvas.parentElement.clientHeight + 'px';
        this.Canvas.width = this.Canvas.clientWidth;
        this.Canvas.height = this.Canvas.clientHeight;
        this.Paint();
    };
    R2Editor.prototype.CanvasResize = function (self, e) {
        this.InvalidateSize();
    };
    R2Editor.prototype.WhileMouseDownTouchStart = function (self, e) {
        e.preventDefault();
        var mouseX = 0;
        var mouseY = 0;
        if (e.touches) {
            mouseX = e.touches[0].pageX - this.CanvasDomPosition.x;
            mouseY = e.touches[0].pageY - this.CanvasDomPosition.y;
            if (e.touches.length > 1) {
                var finger1_x = e.touches[1].pageX - this.CanvasDomPosition.x;
                var finger1_y = e.touches[1].pageY - this.CanvasDomPosition.y;
                var dx = finger1_x - mouseX;
                var dy = finger1_y - mouseY;
                this.PreviousTwoFingersVector = new WebGL.Vector2(dx, dy);
                this.CurrentTwoFingersVector = new WebGL.Vector2(dx, dy);
            }
        }
        else {
            mouseX = e.pageX - this.CanvasDomPosition.x;
            mouseY = e.pageY - this.CanvasDomPosition.y;
        }
        this.PointerDragStartLocation = new WebGL.Vector2(mouseX, mouseY);
        this._CurrentMouseLocation = new WebGL.Vector2(mouseX, mouseY);
        this.ShapeWhenPointerDown = this.GetShapeUnderPointer(this._CurrentMouseLocation);
        if (this.ShapeWhenPointerDown != null)
            console.log("Under Shape " + this.ShapeWhenPointerDown.ShapeName);
        if (this.ShapeWhenPointerDown != null) {
            this.ShapeWhenPointerDown.ExecuteMouseDown({
                ProjectedLocation: this.CurrentSnappedMouseLocation, OriginalMouseEvent: e
            });
        }
    };
    R2Editor.prototype.MouseDown = function (self, e) {
        if (e.button == 0) {
            this.IsLeftMouseButtonDown = true;
            if (this.HafezDrawingMode) {
            }
        }
        if (e.button == 2) {
            this.IsRightMouseButtonDown = true;
        }
        this.WhileMouseDownTouchStart(self, e);
    };
    R2Editor.prototype.BeforeLongPress = function (self, e) {
        if (self.IsLeftMouseButtonDown) {
            if (this.IsThere_A_MovingControlPoint)
                return;
            if (self.CurrentDrawingShape instanceof Shapes.R2FreeHand) {
                var freeHand = self.CurrentDrawingShape;
                if (freeHand.TotalLength > 1)
                    return;
            }
            if (self.ShapeWhenPointerMove != null)
                return;
        }
        if (self.IsLeftMouseButtonDown) {
            console.log("Long left press detected");
            self.EndDrawing(self);
            self.CancelMouseClick = true;
            self.IsLeftMouseButtonDown = false;
            if (self.LongPress != null) {
                self.LongPress(self, {
                    OriginalEvent: e,
                    MouseLocation: self.CurrentMouseLocation,
                    ProjectedMouseLocation: self.CurrentProjectedMouseLocation,
                    ProjectedSnappedMouseLocation: self.CurrentSnappedMouseLocation
                });
            }
        }
    };
    R2Editor.prototype.TouchStart = function (self, e) {
        this.WhileMouseDownTouchStart(self, e);
    };
    R2Editor.prototype.BeforeMouseUpTouchEnd = function (self, e) {
        e.preventDefault();
        if (this.PanTemporaryTranslationX != 0 || this.PanTemporaryTranslationY != 0 || this.PanTemporaryTranslationZ != 0) {
            this.PanFinalTranslationX += this.PanTemporaryTranslationX;
            this.PanFinalTranslationY += this.PanTemporaryTranslationY;
            this.PanFinalTranslationZ += this.PanTemporaryTranslationZ;
            this.PanTemporaryTranslationX = 0;
            this.PanTemporaryTranslationY = 0;
            this.PanTemporaryTranslationZ = 0;
            this.PickingAreaChanged = true;
        }
    };
    R2Editor.prototype.AfterMouseUpTouchEnd = function (self, e) {
        this.ShapeWhenPointerUp = this.GetShapeUnderPointer(this._CurrentMouseLocation);
        if (this.ShapeWhenPointerUp != null) {
            if (self.UnSelectViewShapes(this.ShapeWhenPointerUp)) {
                if (this.CurrentDeviceInputMode == DeviceInputMode.Tablet) {
                    this.ShapeWhenPointerUp.IsSelected = true;
                    self._SelectedShape = this.ShapeWhenPointerUp;
                    this.PickingAreaChanged = true;
                }
            }
            else {
                if (self._SelectedShape != null)
                    self._SelectedShape.IsSelected = true;
            }
        }
        else {
            self.UnSelectViewShapes();
            if (this.DisplayMeshingGrid == true) {
                if (e.which == 1) {
                    this.MeshingGrid.DiscoverSpaceInnerNodes(this.CurrentProjectedMouseLocation.Xy);
                }
                if (e.which == 3) {
                    this.MeshingGrid.DiscoverSpaceEdgeNodes(this.CurrentProjectedMouseLocation.Xy);
                }
            }
        }
        var ix;
        for (ix = 0; ix < this.CurrentDrawingLayer.ShapesCount; ix++) {
            this.CurrentDrawingLayer.DrawingShapes[ix].ExecuteMouseUp({ ProjectedLocation: this.CurrentProjectedMouseLocation, OriginalMouseEvent: e });
        }
        self.Paint();
    };
    R2Editor.prototype.MouseUpTouchEnd = function (self, e) {
        if (self.CurrentDrawingShape instanceof Shapes.R2Line) {
            var line = self.CurrentDrawingShape;
            if (line.Vector.Length > 0) {
                self.CurrentDrawingShape.IsDrawing = false;
                self.AddShape(self.CurrentDrawingShape);
                self.CurrentDrawingShape.Dispose();
                self.CurrentDrawingShape = null;
            }
            else {
                self.CurrentDrawingShape.Dispose();
                self.CurrentDrawingShape = null;
            }
            self.UnSelectViewShapes();
        }
        else if (self.CurrentDrawingShape instanceof Shapes.R2FreeHand) {
            var freehand = self.CurrentDrawingShape;
            self.CurrentDrawingShape.IsDrawing = false;
            self.CurrentDrawingShape.Dispose();
            self.CurrentDrawingShape = null;
            if (this.CurrentTabletDrawingStyle == TabletDrawingStyle.FreeHandWithRecognition) {
                if (freehand.Points.length > 3) {
                    var CommandProcessed = false;
                    var flines = freehand.GuessLines();
                    if (flines.length == 2) {
                        var ishape;
                        for (ishape = 0; ishape < this.CurrentDrawingLayer.ShapesCount; ishape++) {
                            var shape = this.CurrentDrawingLayer.DrawingShapes[ishape];
                            if (shape instanceof Shapes.R2Line) {
                                var line = shape;
                                if (flines[0].IntersectionWithLineSegment(line.FirstPointLocation.Xy, line.LastPointLocation.Xy) != null) {
                                    if (flines[1].IntersectionWithLineSegment(line.FirstPointLocation.Xy, line.LastPointLocation.Xy) != null) {
                                        var CommandTriangleBase = new WebGL.Geometry.Line();
                                        CommandTriangleBase.FirstPoint = flines[0].FirstPoint;
                                        CommandTriangleBase.LastPoint = flines[1].LastPoint;
                                        var CommandNormalLine = new WebGL.Geometry.Line();
                                        CommandNormalLine.FirstPoint = CommandTriangleBase.MiddlePoint;
                                        CommandNormalLine.LastPoint = flines[0].LastPoint;
                                        var angle = new Angle(WebGL.Vector3.CalculateAngle(line.Normal.ToVector3(), CommandNormalLine.NormalizedVector.ToVector3()));
                                        var cline = new Shapes.R2CrossSectionLine();
                                        if (angle.Degrees > 90) {
                                            cline.FirstPointLocation = line.LastPointLocation;
                                            cline.LastPointLocation = line.FirstPointLocation;
                                        }
                                        else {
                                            cline.FirstPointLocation = line.FirstPointLocation;
                                            cline.LastPointLocation = line.LastPointLocation;
                                        }
                                        this.ReplaceShape(ishape, cline);
                                        CommandProcessed = true;
                                        break;
                                    }
                                }
                            }
                            if (shape instanceof Shapes.R2CrossSectionLine) {
                                var rline = shape;
                                if (flines[0].IntersectionWithLineSegment(rline.FirstPointLocation.Xy, rline.LastPointLocation.Xy) != null) {
                                    if (flines[1].IntersectionWithLineSegment(rline.FirstPointLocation.Xy, rline.LastPointLocation.Xy) != null) {
                                        var CommandTriangleBase = new WebGL.Geometry.Line();
                                        CommandTriangleBase.FirstPoint = flines[0].FirstPoint;
                                        CommandTriangleBase.LastPoint = flines[1].LastPoint;
                                        var CommandNormalLine = new WebGL.Geometry.Line();
                                        CommandNormalLine.FirstPoint = CommandTriangleBase.MiddlePoint;
                                        CommandNormalLine.LastPoint = flines[0].LastPoint;
                                        var angle = new Angle(WebGL.Vector3.CalculateAngle(rline.Normal.ToVector3(), CommandNormalLine.NormalizedVector.ToVector3()));
                                        var cline = new Shapes.R2CrossSectionLine();
                                        if (angle.Degrees > 90) {
                                            cline.FirstPointLocation = rline.LastPointLocation;
                                            cline.LastPointLocation = rline.FirstPointLocation;
                                        }
                                        else {
                                            cline.FirstPointLocation = rline.FirstPointLocation;
                                            cline.LastPointLocation = rline.LastPointLocation;
                                        }
                                        this.ReplaceShape(ishape, cline);
                                        CommandProcessed = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!CommandProcessed) {
                        var shapes = freehand.GuessOneShape();
                        shapes.forEach(function (x) {
                            self.AddShape(x);
                        });
                    }
                }
            }
            else {
                self.AddShape(freehand);
            }
            self.UnSelectViewShapes();
        }
        else {
        }
    };
    R2Editor.prototype.MouseUp = function (self, e) {
        this.BeforeMouseUpTouchEnd(self, e);
        if (this.CurrentDrawingShape instanceof Shapes.R2FreeHand) {
            var freehand = this.CurrentDrawingShape;
            if (freehand.TotalLength >= 1) {
                var shapes = freehand.GuessOneShape();
                shapes.forEach(function (x) {
                    self.AddShape(x);
                });
                this.CancelMouseClick = true;
            }
            this.EndDrawing(this);
        }
        if (this.CurrentDeviceInputMode == DeviceInputMode.Mouse) {
            if (e.button == 2)
                this.MouseClick(self, e);
        }
        else {
            this.MouseUpTouchEnd(self, e);
        }
        this.AfterMouseUpTouchEnd(self, e);
        if (e.button == 0) {
            this.IsLeftMouseButtonDown = false;
            clearTimeout(this.LongPressTimer);
        }
        if (e.button == 2) {
            this.IsRightMouseButtonDown = false;
        }
    };
    R2Editor.prototype.TouchEnd = function (self, e) {
        this.BeforeMouseUpTouchEnd(self, e);
        this.MouseUpTouchEnd(self, e);
        this.AfterMouseUpTouchEnd(self, e);
    };
    Object.defineProperty(R2Editor.prototype, "CanvasDomPosition", {
        get: function () {
            var pos = R2Editor.findPos(this.Canvas);
            return pos;
        },
        enumerable: true,
        configurable: true
    });
    R2Editor.prototype.Pan = function (self) {
        var vo = self.UnProject2D(self.MousePointToScaledViewportPoint(self.PointerDragStartLocation));
        var vf = self.UnProject2D(self.MousePointToScaledViewportPoint(self._CurrentMouseLocation));
        var vd = WebGL.Vector3.Subtract(vf, vo);
        self.PanTemporaryTranslationX = vd.X;
        self.PanTemporaryTranslationY = vd.Y;
        self.PanTemporaryTranslationZ = vd.Z;
    };
    R2Editor.prototype.BeforeMouseTouchMoving = function (self, e) {
        e.preventDefault();
        var mouseX = 0;
        var mouseY = 0;
        if (e.touches) {
            mouseX = e.touches[0].pageX - this.CanvasDomPosition.x;
            mouseY = e.touches[0].pageY - this.CanvasDomPosition.y;
        }
        else {
            mouseX = e.pageX - this.CanvasDomPosition.x;
            mouseY = e.pageY - this.CanvasDomPosition.y;
        }
        self._CurrentMouseLocation = new WebGL.Vector2(mouseX, mouseY);
        if (self.IsRightMouseButtonDown != true) {
            this.IsThere_A_MovingControlPoint = false;
            var ix;
            for (ix = 0; ix < self.CurrentDrawingLayer.ShapesCount; ix++) {
                var shosho = self.CurrentDrawingLayer.DrawingShapes[ix];
                var moved = shosho.ExecuteMouseMove({ ProjectedLocation: self.CurrentSnappedMouseLocation, OriginalMouseEvent: e });
                if (moved) {
                    this.PickingAreaChanged = true;
                    this.CurrentDrawingLayer.ModifyProcessedShape(shosho);
                    this.CurrentDrawingLayer.ClearCache();
                    if (this.ShapeWhenPointerDown instanceof Shapes.R2Point) {
                        this.CancelMouseClick = true;
                    }
                    this.IsThere_A_MovingControlPoint = this.IsThere_A_MovingControlPoint || moved;
                }
            }
        }
        self.ShapeWhenPointerMove = self.GetShapeUnderPointer(this._CurrentMouseLocation);
        if (self.MouseTouchMoving != null) {
            self.MouseTouchMoving(self, {
                OriginalEvent: e,
                MouseLocation: self.CurrentMouseLocation,
                ProjectedMouseLocation: self.CurrentProjectedMouseLocation,
                ProjectedSnappedMouseLocation: self.CurrentSnappedMouseLocation
            });
        }
    };
    R2Editor.prototype.AfterMouseTouchMoving = function (self, e) {
        if (this.CurrentDeviceInputMode == DeviceInputMode.Tablet
            || this.HafezDrawingMode == true) {
            if ((e.touches != undefined && e.touches.length == 1)) {
                if (self.ShapeWhenPointerDown == null || self.ShapeWhenPointerDown instanceof Shapes.R2Line || self.ShapeWhenPointerDown instanceof Shapes.R2Arc) {
                    self.UnSelectViewShapes();
                    if (self.CurrentDrawingShape == null) {
                        self.CurrentDrawingShape = new Shapes.R2Line();
                        var line = self.CurrentDrawingShape;
                        line.FirstPointLocation = self.CurrentSnappedMouseLocation;
                        line.LastPointLocation = self.CurrentSnappedMouseLocation;
                        line.IsDrawing = true;
                    }
                    else {
                        var line = self.CurrentDrawingShape;
                        line.LastPointLocation = self.CurrentSnappedMouseLocation;
                    }
                }
            }
        }
        self.Paint();
    };
    R2Editor.prototype.MouseMoveTouchMove = function (self, e) {
        if (this.CurrentDeviceInputMode == DeviceInputMode.Tablet) {
            if (this.CurrentTabletDrawingStyle == TabletDrawingStyle.FreeHand || this.CurrentTabletDrawingStyle == TabletDrawingStyle.FreeHandWithRecognition) {
                if ((e.buttons == 1 || (e.touches != undefined && e.touches.length == 1))
                    &&
                        (self.ShapeWhenPointerDown == null
                            || self.ShapeWhenPointerDown instanceof Shapes.R2Line
                            || self.ShapeWhenPointerDown instanceof Shapes.R2Arc)) {
                    if (self.CurrentDrawingShape == null) {
                        self.CurrentDrawingShape = new Shapes.R2FreeHand();
                        var freehand = self.CurrentDrawingShape;
                        freehand.IsDrawing = true;
                        freehand.AddPoint(self.CurrentProjectedMouseLocation.Xy);
                    }
                    else {
                        var freehand = self.CurrentDrawingShape;
                        freehand.AddPoint(self.CurrentProjectedMouseLocation.Xy);
                    }
                }
            }
        }
        if (this.CurrentDeviceInputMode == DeviceInputMode.Mouse) {
            if (this.CurrentDrawingShape instanceof Shapes.R2Line) {
                var line = this.CurrentDrawingShape;
                line.LastPointLocation = this.CurrentSnappedMouseLocation;
            }
            else if (this.CurrentDrawingShape instanceof Shapes.R2Arc) {
                var arc = this.CurrentDrawingShape;
                if (this.CurrentDrawingShapeMouseClicks == 2) {
                    arc.LastPointLocation = this.CurrentSnappedMouseLocation;
                    arc.MiddlePointLocation = arc.ArcLine.MiddlePointLocation;
                }
                if (this.CurrentDrawingShapeMouseClicks == 1) {
                    arc.MiddlePointLocation = this.CurrentSnappedMouseLocation;
                }
            }
            else if (this.CurrentDrawingShape instanceof Shapes.R2Circle) {
                var circle = this.CurrentDrawingShape;
                if (circle.DrawingMode == Shapes.CircleDrawingMode.ThreePoints) {
                    if (this.CurrentDrawingShapeMouseClicks == 2) {
                        circle.LastPointLocation = this.CurrentSnappedMouseLocation;
                        circle.MiddlePointLocation = circle.ArcLine.MiddlePointLocation;
                    }
                    if (this.CurrentDrawingShapeMouseClicks == 1) {
                        circle.MiddlePointLocation = this.CurrentSnappedMouseLocation;
                    }
                }
                else {
                    circle.RadiusPointLocation = this.CurrentSnappedMouseLocation;
                }
            }
            else if (this.CurrentDrawingShape instanceof Shapes.R2Rectangle) {
                var rectangle = this.CurrentDrawingShape;
                rectangle.BottomRightPointLocation = this.CurrentSnappedMouseLocation;
                rectangle.TopRightPointLocation = new WebGL.Vector3(this.CurrentSnappedMouseLocation.X, rectangle.TopLeftPointLocation.Y, 0);
                rectangle.BottomLeftPointLocation = new WebGL.Vector3(rectangle.TopLeftPointLocation.X, this.CurrentSnappedMouseLocation.Y, 0);
            }
            else if (this.CurrentDrawingShape instanceof Shapes.R2FreeHand) {
                var freehand = self.CurrentDrawingShape;
                freehand.AddPoint(self.CurrentProjectedMouseLocation.Xy);
            }
            else {
                if (this.IsLeftMouseButtonDown && this.AcceptHandDrawing == true && this.ShapeWhenPointerMove == null && this.IsThere_A_MovingControlPoint == false) {
                    var freehand = new Shapes.R2FreeHand();
                    self.CurrentDrawingShape = freehand;
                    freehand.IsDrawing = true;
                    freehand.AddPoint(self.CurrentProjectedMouseLocation.Xy);
                }
            }
        }
    };
    R2Editor.prototype.ProcessMouseMoveForCommand = function (self, e) {
        if (this.CurrentCommand == EditorCommands.TotalTranslation && this.CommandClicks > 0) {
            var origpos = this.CommandClicksStack[this.CommandClicksStack.length - 1];
            var delta = WebGL.Vector2.Subtract(this.CurrentSnappedMouseLocation.Xy, origpos);
            var points = self.CurrentDrawingLayer.GetAllShapesPoints();
            points.forEach(function (p) {
                p.RevertToLastLocation();
                p.Translate(delta);
            });
            self.CurrentDrawingLayer.DrawingShapes.forEach(function (sh) {
                sh.Refresh();
            });
        }
        if (this.CurrentCommand == EditorCommands.TotalRotation && this.CommandClicks > 0) {
            var origpos = this.CommandClicksStack[this.CommandClicksStack.length - 1];
            var delta = WebGL.Vector2.Subtract(this.CurrentSnappedMouseLocation.Xy, origpos);
            var dpolar = delta.ToPolarVector();
            var rotAngle = dpolar.Theta;
            var points = self.CurrentDrawingLayer.GetAllShapesPoints();
            points.forEach(function (p) {
                p.RevertToLastLocation();
                p.Rotate(origpos, rotAngle);
            });
            self.CurrentDrawingLayer.DrawingShapes.forEach(function (sh) {
                sh.Refresh();
            });
        }
    };
    R2Editor.prototype.MouseMove = function (self, e) {
        this.BeforeMouseTouchMoving(self, e);
        if (e.buttons == this.PanButton) {
            this.Pan(self);
        }
        if (self.IsRightMouseButtonDown) {
            if (self.ShapeWhenPointerMove != null) {
                self.RemoveShape(self.ShapeWhenPointerMove);
            }
        }
        this.MouseMoveTouchMove(self, e);
        if (this.CurrentCommand != EditorCommands.Drawing) {
            this.ProcessMouseMoveForCommand(self, e);
        }
        this.AfterMouseTouchMoving(self, e);
    };
    R2Editor.prototype.TouchMove = function (self, e) {
        this.BeforeMouseTouchMoving(self, e);
        if (e.touches.length == 2) {
            var finger1_x = e.touches[1].pageX - this.CanvasDomPosition.x;
            var finger1_y = e.touches[1].pageY - this.CanvasDomPosition.y;
            var dx = finger1_x - this.CurrentMouseLocation.X;
            var dy = finger1_y - this.CurrentMouseLocation.Y;
            this.CurrentTwoFingersVector = new WebGL.Vector2(dx, dy);
            var zoomin = this.CurrentTwoFingersVector.Length < this.PreviousTwoFingersVector.Length;
            if (zoomin) {
                if (self.ZoomFactor > 0.06)
                    self.ZoomFactor += 0.01;
                else
                    self.ZoomFactor += 0.01;
            }
            else
                self.ZoomFactor -= 0.01;
            self.Paint();
            return;
        }
        if (e.touches.length == 3) {
            this.Pan(self);
        }
        this.MouseMoveTouchMove(self, e);
        this.AfterMouseTouchMoving(self, e);
    };
    R2Editor.prototype.MouseScroll = function (self, e) {
        var evt = window.event || e;
        var delta = evt.detail ? evt.detail * (-120) : evt.wheelDelta;
        var BeforeMouseWheel = self.CurrentProjectedMouseLocation;
        var dff = this.FixedZoomingDeltaWidth;
        if (this.ShapeWhenPointerMove != null)
            dff = dff / 10;
        if (delta < 0) {
            var newProjectionWidth = this.ProjectionWidth + dff;
            self.ZoomFactor = newProjectionWidth / this.Canvas.width;
        }
        else {
            var newProjectionWidth = this.ProjectionWidth - dff;
            self.ZoomFactor = newProjectionWidth / this.Canvas.width;
        }
        var AfterMouseWheel = self.CurrentProjectedMouseLocation;
        var pdelta = WebGL.Vector3.Subtract(AfterMouseWheel, BeforeMouseWheel);
        self.PanFinalTranslationX += pdelta.X;
        self.PanFinalTranslationY += pdelta.Y;
        self.Paint();
    };
    R2Editor.prototype.EndDrawing = function (self) {
        if (self.CurrentDrawingShape != null) {
            self.CurrentDrawingShape.IsDrawing = false;
            self.CurrentDrawingShape.Dispose();
            self.CurrentDrawingShape = null;
        }
        self.CurrentDrawingShapeMouseClicks = 0;
    };
    R2Editor.prototype.PredictDrawnShape = function (drawnLine, underLine) {
        if (underLine.BuildingType == Shapes.BuildingType.Wall) {
            drawnLine.BuildingType = Shapes.BuildingType.Window;
            drawnLine.ShapeColor = WebGL.Vector4Colors.Black;
            this.AddShape(drawnLine);
            underLine.AddBuildingShape(drawnLine);
        }
        else if (underLine.BuildingType == Shapes.BuildingType.Window) {
            var wallLine = underLine.OwnerBuildingShape;
            drawnLine.BuildingType = Shapes.BuildingType.Door;
            drawnLine.ShapeColor = WebGL.Vector4Colors.Black;
            this.AddShape(drawnLine);
            wallLine.AddBuildingShape(drawnLine);
            this.RemoveShape(underLine);
        }
        else if (underLine.BuildingType == Shapes.BuildingType.Door) {
            var wallLine = underLine.OwnerBuildingShape;
            drawnLine.BuildingType = Shapes.BuildingType.Opening;
            drawnLine.ShapeColor = WebGL.Vector4Colors.Black;
            this.AddShape(drawnLine);
            wallLine.AddBuildingShape(drawnLine);
            this.RemoveShape(underLine);
        }
    };
    R2Editor.prototype.SpecifyDrawnLine = function (drawnLine) {
        var underLines = this.CurrentDrawingLayer.AllLines;
        var underLine = null;
        for (var i = 0; i < underLines.length; i++) {
            var uL = underLines[i];
            if (uL.IsPointOnLineSegment(drawnLine.FirstPointLocation.Xy)) {
                underLine = uL;
            }
        }
        if (underLine != null) {
            if (drawnLine.IsLyingOn(underLine) == 2) {
                this.PredictDrawnShape(drawnLine, underLine);
                return true;
            }
        }
        return false;
    };
    R2Editor.prototype.ProcessMouseClickForDrawing = function (self, e) {
        if (e.button == 0) {
            if (this.CurrentDrawingShape != null) {
                if (this.CurrentDrawingShape instanceof Shapes.R2Arc) {
                    if (this.CurrentDrawingShapeMouseClicks == 2) {
                        this.CurrentDrawingShapeMouseClicks = 1;
                    }
                    else if (this.CurrentDrawingShapeMouseClicks == 1) {
                        this.AddShape(this.CurrentDrawingShape);
                        this.CurrentDrawingShape.IsDrawing = false;
                        this.CurrentDrawingShape.Dispose();
                        this.CurrentDrawingShape = null;
                        this.CurrentDrawingShapeMouseClicks = 0;
                    }
                }
                else if (this.CurrentDrawingShape instanceof Shapes.R2Circle) {
                    var circle = this.CurrentDrawingShape;
                    if (circle.DrawingMode == Shapes.CircleDrawingMode.ThreePoints) {
                        if (this.CurrentDrawingShapeMouseClicks == 2) {
                            this.CurrentDrawingShapeMouseClicks = 1;
                            circle.DrawingStep = 1;
                        }
                        else if (this.CurrentDrawingShapeMouseClicks == 1) {
                            this.AddShape(this.CurrentDrawingShape);
                            this.CurrentDrawingShape.IsDrawing = false;
                            this.CurrentDrawingShape.Dispose();
                            this.CurrentDrawingShape = null;
                            this.CurrentDrawingShapeMouseClicks = 0;
                            circle.DrawingStep = 0;
                        }
                    }
                    else {
                        if (isNaN(circle.CenterPoint.ShapeCenter.X) || isNaN(circle.CenterPoint.ShapeCenter.Y)) {
                            console.debug("Circle couldn't be added because its center is NaN");
                        }
                        else {
                            this.AddShape(this.CurrentDrawingShape);
                        }
                        this.CurrentDrawingShape.IsDrawing = false;
                        this.CurrentDrawingShape.Dispose();
                        this.CurrentDrawingShape = null;
                        this.CurrentDrawingShapeMouseClicks = 0;
                    }
                }
                else if (this.CurrentDrawingShape instanceof Shapes.R2Line && this.CurrentMouseDrawingShape == MouseDrawingShape.PolyLine) {
                    this.CurrentDrawingShape.IsDrawing = false;
                    var drawnLine = this.CurrentDrawingShape;
                    var cancellastline = false;
                    if (this.CurrentDrawingShapeMouseClicks < 1) {
                        if (drawnLine.Length < 1) {
                            this.EndDrawing(self);
                        }
                        else {
                            if (this.HafezDrawingMode) {
                                cancellastline = this.SpecifyDrawnLine(drawnLine);
                            }
                        }
                    }
                    if (cancellastline) {
                        this.EndDrawing(self);
                    }
                    else {
                        if (drawnLine.Length >= 1) {
                            this.AddShape(drawnLine);
                        }
                        var line = new Shapes.R2Line();
                        line.FirstPoint = drawnLine.LastPoint;
                        line.LastPointLocation = this.CurrentSnappedMouseLocation;
                        this.CurrentDrawingShape = line;
                        this.CurrentDrawingShapeMouseClicks++;
                    }
                }
                else if (this.CurrentDrawingShape instanceof Shapes.R2Line &&
                    (this.CurrentMouseDrawingShape == MouseDrawingShape.Lines ||
                        this.CurrentMouseDrawingShape == MouseDrawingShape.DimensionLines)) {
                    var drawnLine = this.CurrentDrawingShape;
                    if (drawnLine.Length >= 1) {
                        this.AddShape(drawnLine);
                        if (drawnLine.IsDimension)
                            drawnLine.ShapeColor = WebGL.Vector4Colors.DarkGreen;
                    }
                    this.EndDrawing(self);
                }
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.RectanglesWithLines) {
                    var rec = this.CurrentDrawingShape;
                    if (rec.IsZero) {
                    }
                    else if (rec.TopLine.IsHavingSameTerminals(rec.BottomLine)) {
                        var drawnLine = rec.TopLine;
                        this.SpecifyDrawnLine(drawnLine);
                    }
                    else if (rec.RightLine.IsHavingSameTerminals(rec.LeftLine)) {
                        var drawnLine = rec.LeftLine;
                        this.SpecifyDrawnLine(drawnLine);
                    }
                    else {
                        this.AddLineAndBlend(rec.TopLine);
                        this.AddLineAndBlend(rec.BottomLine);
                        this.AddLineAndBlend(rec.RightLine);
                        this.AddLineAndBlend(rec.LeftLine);
                        this.CurrentDrawingLayer.RemoveZeroLengthLines();
                        this.CurrentDrawingLayer.RemoveDuplicateLines();
                    }
                    this.CurrentDrawingShape.IsDrawing = false;
                    this.CurrentDrawingShape.Dispose();
                    this.CurrentDrawingShape = null;
                    this.CurrentDrawingShapeMouseClicks = 0;
                }
                else {
                    this.AddShape(self.CurrentDrawingShape);
                    this.EndDrawing(self);
                }
            }
            else {
                if (this.CurrentMouseDrawingShape == MouseDrawingShape.Selection) {
                    if (this.ShapeWhenPointerUp != null) {
                        this.ShapeWhenPointerUp.IsSelected = true;
                        this.SelectedShape = this.ShapeWhenPointerUp;
                    }
                }
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.Lines || this.CurrentMouseDrawingShape == MouseDrawingShape.DimensionLines) {
                    var line = new Shapes.R2Line();
                    line.FirstPointLocation = this.CurrentSnappedMouseLocation;
                    line.LastPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShapeMouseClicks = 2;
                    this.CurrentDrawingShape = line;
                    if (this.CurrentMouseDrawingShape == MouseDrawingShape.DimensionLines) {
                        line.IsDimension = true;
                    }
                }
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.PolyLine) {
                    var line = new Shapes.R2Line();
                    line.FirstPointLocation = this.CurrentSnappedMouseLocation;
                    line.LastPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShapeMouseClicks = 0;
                    this.CurrentDrawingShape = line;
                }
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.Rectangles || this.CurrentMouseDrawingShape == MouseDrawingShape.RectanglesWithLines) {
                    var rectangle = new Shapes.R2Rectangle();
                    rectangle.TopLeftPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.TopRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomLeftPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShape = rectangle;
                }
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.Arcs) {
                    var arc = new Shapes.R2Arc();
                    arc.FirstPointLocation = this.CurrentSnappedMouseLocation;
                    arc.MiddlePointLocation = this.CurrentSnappedMouseLocation;
                    arc.LastPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShapeMouseClicks = 2;
                    this.CurrentDrawingShape = arc;
                }
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.Circles) {
                    var circle = new Shapes.R2Circle();
                    circle.CenterPointLocation = this.CurrentSnappedMouseLocation;
                    circle.RadiusPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShape = circle;
                }
                else if (this.CurrentMouseDrawingShape == MouseDrawingShape.Circles3P) {
                    var circle = new Shapes.R2Circle();
                    circle.DrawingMode = Shapes.CircleDrawingMode.ThreePoints;
                    circle.FirstPointLocation = this.CurrentSnappedMouseLocation;
                    circle.MiddlePointLocation = this.CurrentSnappedMouseLocation;
                    circle.LastPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShapeMouseClicks = 2;
                    circle.DrawingStep = 2;
                    this.CurrentDrawingShape = circle;
                }
                if (this.CurrentDrawingShape != null)
                    this.CurrentDrawingShape.IsDrawing = true;
            }
        }
        if (e.button == 2) {
            if (this.CurrentDrawingShape != null) {
                if (this.CurrentDrawingShape instanceof Shapes.R2Line) {
                    this.EndDrawing(this);
                }
                if (this.CurrentDrawingShape instanceof Shapes.R2Rectangle) {
                    this.EndDrawing(this);
                }
            }
        }
    };
    R2Editor.prototype.ProcessMouseClickForCommand = function (self, e) {
        if (e.button == 0) {
            if (this.CurrentCommand == EditorCommands.RegionSelection) {
                if (this.CommandClicks == 0) {
                    var rectangle = new Shapes.R2Rectangle();
                    rectangle.ActAsSelector = true;
                    rectangle.TopLeftPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.TopRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomLeftPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShape = rectangle;
                    this.CommandClicksStack.push(this.CurrentMouseLocation);
                }
                else {
                    var rec = this.CurrentDrawingShape;
                    var topleft = rec.TopLeftPointLocation;
                    var bottomright = rec.BottomRightPointLocation;
                    this.EndDrawing(self);
                    this.CommandClicksStack.pop();
                    if (this.RegionSelected != null) {
                        this.RegionSelected(self, topleft.Xy, bottomright.Xy);
                    }
                }
            }
            else if (this.CurrentCommand == EditorCommands.RegionDelete) {
                if (this.CommandClicks == 0) {
                    var rectangle = new Shapes.R2Rectangle();
                    rectangle.ActAsSelector = true;
                    rectangle.TopLeftPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.TopRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomLeftPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShape = rectangle;
                    this.CommandClicksStack.push(this.CurrentProjectedMouseLocation.Xy);
                }
                else {
                    var rec = this.CurrentDrawingShape;
                    var topleft = rec.TopLeftPointLocation;
                    var bottomright = rec.BottomRightPointLocation;
                    this.EndDrawing(self);
                    this.CurrentCommand = EditorCommands.Drawing;
                    var dl = this.SelectShapesByRegion(topleft.Xy, bottomright.Xy);
                    dl.DrawingShapes.forEach(function (sh) {
                        var ix = self.CurrentDrawingLayer.GetShapeIndex(sh);
                        self.CurrentDrawingLayer.RemoveShape(ix);
                    });
                    this.CommandClicksStack.pop();
                    this.StateChanged();
                    this.CurrentDrawingLayer.ShouldRefreshLayers = true;
                }
            }
            else if (this.CurrentCommand == EditorCommands.RegionCrop) {
                if (this.CommandClicks == 0) {
                    var rectangle = new Shapes.R2Rectangle();
                    rectangle.ActAsSelector = true;
                    rectangle.TopLeftPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.TopRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomRightPointLocation = this.CurrentSnappedMouseLocation;
                    rectangle.BottomLeftPointLocation = this.CurrentSnappedMouseLocation;
                    this.CurrentDrawingShape = rectangle;
                    this.CommandClicksStack.push(this.CurrentProjectedMouseLocation.Xy);
                }
                else {
                    var rec = this.CurrentDrawingShape;
                    var topleft = rec.TopLeftPointLocation;
                    var bottomright = rec.BottomRightPointLocation;
                    this.EndDrawing(self);
                    this.CurrentCommand = EditorCommands.Drawing;
                    var dl = this.SelectShapesByRegion(topleft.Xy, bottomright.Xy);
                    var currshapes = self.CurrentDrawingLayer.VisibleShapes;
                    currshapes.forEach(function (sh) {
                        var shix = dl.GetShapeIndex(sh);
                        if (shix == -1) {
                            self.CurrentDrawingLayer.RemoveShape(self.CurrentDrawingLayer.GetShapeIndex(sh));
                        }
                    });
                    this.CommandClicksStack.pop();
                    this.StateChanged();
                    this.PickingAreaChanged = true;
                }
            }
            else if (this.CurrentCommand == EditorCommands.TotalTranslation || this.CurrentCommand == EditorCommands.TotalRotation) {
                if (this.CommandClicks == 0) {
                    var line = new Shapes.R2Line();
                    line.FirstPointLocation = this.CurrentProjectedMouseLocation;
                    line.LastPointLocation = this.CurrentProjectedMouseLocation;
                    line.LineType = Shapes.ShapeLineType.Dotted;
                    this.CurrentDrawingShape = line;
                    var points = self.CurrentDrawingLayer.GetAllShapesPoints();
                    points.forEach(function (p) {
                        p.UpdateLastLocation();
                    });
                    this.CommandClicksStack.push(this.CurrentProjectedMouseLocation.Xy);
                }
                else {
                    this.EndDrawing(self);
                    this.CurrentCommand = EditorCommands.Drawing;
                    var points = self.CurrentDrawingLayer.GetAllShapesPoints();
                    points.forEach(function (p) {
                        p.UpdateLastLocation();
                    });
                    this.CommandClicksStack.pop();
                    this.CurrentDrawingLayer.ClearCache();
                    this.PickingAreaChanged = true;
                }
            }
        }
        if (e.button == 2) {
            if (this.CommandClicks >= 0) {
                if (this.CurrentCommand == EditorCommands.RegionSelection || this.CurrentCommand == EditorCommands.RegionDelete || this.CurrentCommand == EditorCommands.RegionCrop) {
                    this.EndDrawing(self);
                    this.CurrentCommand = EditorCommands.Drawing;
                    this.CommandClicksStack.pop();
                    if (this.RegionSelectionCancelled != null) {
                        this.RegionSelectionCancelled(self);
                    }
                }
                if (this.CurrentCommand == EditorCommands.RegionDelete) {
                    this.EndDrawing(self);
                    this.CurrentCommand = EditorCommands.Drawing;
                    this.CommandClicksStack.pop();
                }
                if (this.CurrentCommand == EditorCommands.TotalTranslation || this.CurrentCommand == EditorCommands.TotalRotation) {
                    this.EndDrawing(self);
                    this.CurrentCommand = EditorCommands.Drawing;
                    var points = self.CurrentDrawingLayer.GetAllShapesPoints();
                    points.forEach(function (p) {
                        p.RevertToLastLocation();
                    });
                    this.CommandClicksStack.pop();
                }
            }
        }
    };
    R2Editor.prototype.MouseClick = function (self, e) {
        if (this.CancelMouseClick) {
            this.CancelMouseClick = false;
            return;
        }
        if (this.CurrentCommand == EditorCommands.Drawing) {
            if (this.CurrentDeviceInputMode == DeviceInputMode.Mouse) {
                this.ProcessMouseClickForDrawing(self, e);
            }
        }
        else {
            this.ProcessMouseClickForCommand(self, e);
        }
    };
    R2Editor.prototype.MouseDoubleClick = function (self, e) {
        if (self.MouseDoubleClicked != null) {
            self.MouseDoubleClicked(self, {
                OriginalEvent: e,
                MouseLocation: self.CurrentMouseLocation,
                ProjectedMouseLocation: self.CurrentProjectedMouseLocation,
                ProjectedSnappedMouseLocation: self.CurrentSnappedMouseLocation
            });
        }
        if (this.CurrentDeviceInputMode == DeviceInputMode.Mouse) {
            if (this.HafezDrawingMode) {
                var xmouse = this.CurrentProjectedMouseLocation.X;
                var ymouse = this.CurrentProjectedMouseLocation.Y;
                this.EndDrawing(this);
                if (this.ShapeWhenPointerUp == null) {
                    if (this.CurrentMouseDrawingShape == MouseDrawingShape.PolyLine) {
                        this.CurrentMouseDrawingShape = MouseDrawingShape.RectanglesWithLines;
                        this.Notify("Rectangles Mode");
                    }
                    else if (this.CurrentMouseDrawingShape == MouseDrawingShape.RectanglesWithLines) {
                        this.CurrentMouseDrawingShape = MouseDrawingShape.DimensionLines;
                        this.Notify("Dimension Lines");
                    }
                    else if (this.CurrentMouseDrawingShape == MouseDrawingShape.DimensionLines) {
                        this.CurrentMouseDrawingShape = MouseDrawingShape.PolyLine;
                        this.Notify("Poly Line");
                    }
                }
                else {
                    this.ShapeWhenPointerUp.IsSelected = true;
                    this._SelectedShape = this.ShapeWhenPointerUp;
                    this.PickingAreaChanged = true;
                    this.Notify("Shape Selected");
                }
            }
        }
        if (this.CurrentDeviceInputMode == DeviceInputMode.Tablet) {
            var shape = this.GetShapeUnderPointer(this._CurrentMouseLocation);
            if (shape != null) {
                if (shape instanceof Shapes.R2Line) {
                    var line = shape;
                    var arc = new Shapes.R2Arc();
                    arc.FirstPointLocation = line.FirstPointLocation;
                    arc.MiddlePointLocation = line.MiddlePointLocation;
                    arc.LastPointLocation = line.LastPointLocation;
                    var selectedIndex = self.CurrentDrawingLayer.GetShapeIndex(shape);
                    if (selectedIndex > -1)
                        self.ReplaceShape(selectedIndex, arc);
                    arc.IsSelected = true;
                }
            }
        }
        self.Paint();
    };
    R2Editor.prototype.KeyDown = function (self, e) {
        var x = e.which || e.keyCode;
        var y = String.fromCharCode(x);
        if (x == "119") {
            this.HideDrawingGrid = !this.HideDrawingGrid;
        }
        if (x == "120") {
            this.DisplayPickingBuffer = !this.DisplayPickingBuffer;
        }
        if (x == "32") {
            this.CurrentCommand = this._LastCommand;
        }
        self.Paint();
    };
    R2Editor.prototype.PrepareMeshingGridBasedOnShapes = function () {
        this.CurrentDrawingLayer.DetermineShapesIntersections();
        var ish;
        for (ish = 0; ish < this.CurrentDrawingLayer.ShapesCount; ish++) {
            this.MeshingGrid.Shapes.push(this.CurrentDrawingLayer.DrawingShapes[ish]);
        }
        this.MeshingGrid.PrepareConstantIntensityGridFromShapes();
    };
    R2Editor.prototype.PrepareMeshingGrid = function (VerticalFrom, VerticalTo, VerticalCount, HorizontalFrom, HorizontalTo, HorizontalCount) {
        this.CurrentDrawingLayer.DetermineShapesIntersections();
        this.MeshingGrid.VerticalFrom = VerticalFrom;
        this.MeshingGrid.VerticalTo = VerticalTo;
        this.MeshingGrid.VerticalSpacesCount = VerticalCount;
        this.MeshingGrid.HorizontalFrom = HorizontalFrom;
        this.MeshingGrid.HorizontalTo = HorizontalTo;
        this.MeshingGrid.HorizontalSpacesCount = HorizontalCount;
        var ish;
        for (ish = 0; ish < this.CurrentDrawingLayer.ShapesCount; ish++) {
            this.MeshingGrid.Shapes.push(this.CurrentDrawingLayer.DrawingShapes[ish]);
        }
    };
    R2Editor.prototype.ShowSegmentsLayer = function () {
        var slayer = new DrawingLayer();
        this.CurrentDrawingLayer.DetermineShapesIntersections();
        this.AdditionalLayers.push(this.CurrentDrawingLayer);
        this.CurrentDrawingLayer.DrawingShapes.forEach(function (shape) {
            shape.ApplySegmentation();
            shape.InnerSegments.forEach(function (seg) { return slayer.AddShape(seg); });
        });
        this.CurrentDrawingLayer = slayer;
    };
    R2Editor.prototype.ShowDrawingLayer = function () {
        this.CurrentDrawingLayer = this.AdditionalLayers.pop();
    };
    R2Editor.prototype.SelectShapesByRegion = function (topLeft, bottomRight) {
        var dl = new DrawingLayer();
        var maxX = bottomRight.X;
        if (maxX < topLeft.X)
            maxX = topLeft.X;
        var maxY = topLeft.Y;
        if (maxY < bottomRight.Y)
            maxY = bottomRight.Y;
        var minX = topLeft.X;
        if (minX > bottomRight.X)
            minX = bottomRight.X;
        var minY = bottomRight.Y;
        if (minY > topLeft.Y)
            minY = topLeft.Y;
        this.CurrentDrawingLayer.VisibleShapes.forEach(function (sh) {
            if (sh.MinimumX >= minX && sh.MaximumX <= maxX) {
                if (sh.MinimumY >= minY && sh.MaximumY <= maxY) {
                    dl.AddShapeWithoutProcessing(sh);
                }
            }
        });
        return dl;
    };
    R2Editor.prototype.GetShapesForSave = function () {
        var ix;
        var string_lines = "";
        for (ix = 0; ix < this.CurrentDrawingLayer.ShapesCount; ix++) {
            var sh = this.CurrentDrawingLayer.DrawingShapes[ix];
            var command = "";
            var btype = "Undefined";
            if (sh.BuildingType == Shapes.BuildingType.Door)
                btype = "Door";
            if (sh.BuildingType == Shapes.BuildingType.Opening)
                btype = "Opening";
            if (sh.BuildingType == Shapes.BuildingType.Wall)
                btype = "Wall";
            if (sh.BuildingType == Shapes.BuildingType.Window)
                btype = "Window";
            if (sh instanceof Shapes.R2Line) {
                var line = sh;
                command = "R2Line[" + line.FirstPointLocation.Xy.ToTupleString()
                    + "," + line.LastPointLocation.Xy.ToTupleString()
                    + "]{BuildingType!" + btype + "}";
            }
            if (sh instanceof Shapes.R2Arc) {
                var arc = sh;
                command = "R2Arc[" + arc.FirstPointLocation.Xy.ToTupleString()
                    + "," + arc.MiddlePointLocation.Xy.ToTupleString()
                    + "," + arc.LastPointLocation.Xy.ToTupleString()
                    + "]{BuildingType!" + btype + "}";
            }
            string_lines = string_lines.concat(command, "\n");
        }
        return string_lines;
    };
    R2Editor.prototype.GetCdtXmlFormat = function () {
        var layer = this.CurrentDrawingLayer.ToCdtXElement();
        var cdt = "<CADTrans>";
        cdt = cdt.concat("\n", this.CurrentDrawingLayer.ToCdtXElement());
        cdt = cdt.concat("\n</CADTrans>");
        return cdt;
    };
    R2Editor.prototype.GetSliceArrayXmlFormat = function () {
        return this.CurrentDrawingLayer.ToSliceArrayXElement();
    };
    return R2Editor;
}());
var VirtualTrackBall = (function () {
    function VirtualTrackBall() {
        this._ZoomFactor = 1;
        this._ZoomRadiusUnit = 100;
        this.FirstDragVector = WebGL.Vector3.Zero;
        this.LastDragVector = WebGL.Vector3.Zero;
        this.Reset();
    }
    Object.defineProperty(VirtualTrackBall.prototype, "IsPanning", {
        get: function () {
            return !this.IsRotating;
        },
        set: function (value) {
            this.IsRotating = !value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "ZoomFactor", {
        get: function () {
            return this._ZoomFactor;
        },
        set: function (value) {
            this._ZoomFactor = value;
            if (this._ZoomFactor < 0.1)
                this._ZoomFactor = 0.1;
            var ee = WebGL.Vector3.Subtract(this._Eye, this._Target);
            var s = ee.ToSpherical();
            s.Radial = this._ZoomFactor * this._ZoomRadiusUnit;
            this._Eye = WebGL.Vector3.Add(this._Target, s.ToCartesian());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "ZoomRadiusUnit", {
        get: function () {
            return this._ZoomRadiusUnit;
        },
        set: function (value) {
            this._ZoomRadiusUnit = value;
            var ee = WebGL.Vector3.Subtract(this._Eye, this._Target);
            var s = ee.ToSpherical();
            s.Radial = this._ZoomFactor * this._ZoomRadiusUnit;
            this._Eye = WebGL.Vector3.Add(this._Target, s.ToCartesian());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "PanFactor", {
        get: function () {
            return this.ZoomRadiusUnit * this.ZoomFactor / 2;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "PanTemporaryTranslationX", {
        get: function () {
            if (this.IsPanning) {
                return ((this.LastDragPoint.X - this.FirstDragPoint.X) * this.PanFactor);
            }
            else {
                return 0;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "PanTemporaryTranslationY", {
        get: function () {
            if (this.IsPanning) {
                return ((this.LastDragPoint.Y - this.FirstDragPoint.Y) * this.PanFactor);
            }
            else {
                return 0;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "PerspectiveMatrix", {
        get: function () {
            return WebGL.Matrix4.CreatePerspectiveFieldOfView(Angle.Pi.Radians / 4, this.ViewportWidth / this.ViewportHeight, 1, 10000);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "Eye", {
        get: function () {
            return this._Eye;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "Target", {
        get: function () {
            return this._Target;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "LookUp", {
        get: function () {
            return this._LookUp;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "LookAtMatrix", {
        get: function () {
            var look;
            if (this.IsDragging) {
                if (this.IsRotating) {
                    var ee = WebGL.Vector3.Subtract(this._Eye, this._Target);
                    var invertedRotation = this.Rotation.Inverted();
                    var eeRotated = WebGL.Vector3.TransformByQuaternion(ee, this.Rotation.Inverted());
                    this.temp_Eye = WebGL.Vector3.Add(this._Target, eeRotated);
                    this.temp_LookUp = WebGL.Vector3.TransformByQuaternion(this._LookUp, this.Rotation.Inverted());
                    this.temp_Target = this._Target;
                    look = WebGL.Matrix4.LookAt(this.temp_Eye, this.temp_Target, this.temp_LookUp);
                }
                else {
                    var PanXLocus = WebGL.Vector3.Cross(this._Eye, this._LookUp).Normalized;
                    var PanX = WebGL.Vector3.Multiply(PanXLocus, this.PanTemporaryTranslationX);
                    var PanYLocus = this._LookUp.Normalized;
                    var PanY = WebGL.Vector3.Multiply(PanYLocus, -this.PanTemporaryTranslationY);
                    this.temp_Eye = WebGL.Vector3.Add(WebGL.Vector3.Add(this._Eye, PanX), PanY);
                    this.temp_Target = WebGL.Vector3.Add(WebGL.Vector3.Add(this._Target, PanX), PanY);
                    this.temp_LookUp = this._LookUp;
                    look = WebGL.Matrix4.LookAt(this.temp_Eye, this.temp_Target, this.temp_LookUp);
                }
            }
            else {
                look = WebGL.Matrix4.LookAt(this._Eye, this._Target, this._LookUp);
            }
            return look;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "FinalMatrix", {
        get: function () {
            return WebGL.Matrix4.Multiply(this.LookAtMatrix, this.PerspectiveMatrix);
        },
        enumerable: true,
        configurable: true
    });
    VirtualTrackBall.prototype.MousePointToScaledViewportPoint = function (xp, yp) {
        var x = ((2.0 * xp) / this.ViewportWidth) - 1.0;
        var y = 1.0 - ((2.0 * yp) / this.ViewportHeight);
        var pf = new WebGL.Vector2(x, y);
        return pf;
    };
    VirtualTrackBall.prototype.MouseOnHemiSphere = function (scaledPoint) {
        var finalPoint = scaledPoint;
        var d = Math.sqrt(finalPoint.X * finalPoint.X + finalPoint.Y * finalPoint.Y);
        var v2 = Math.cos((Math.PI / 2.0) * ((d < 1.0) ? d : 1.0));
        var a = 1.0 / Math.sqrt(finalPoint.X * finalPoint.X + finalPoint.Y * finalPoint.Y + v2 * v2);
        var vv = new WebGL.Vector3(finalPoint.X * a, finalPoint.Y * a, v2 * a);
        var nv = vv;
        return nv;
    };
    VirtualTrackBall.prototype.FromMouseHemiSphereToWorld = function (mouseOnHemiSphere) {
        var actualEye = WebGL.Vector3.Subtract(this._Eye, this._Target);
        var normalizedUp = this._LookUp.Normalized;
        var projection = WebGL.Vector3.Multiply(normalizedUp, mouseOnHemiSphere.Y);
        var cross = WebGL.Vector3.Cross(this._LookUp, actualEye).Normalized;
        cross = WebGL.Vector3.Multiply(cross, mouseOnHemiSphere.X);
        projection = WebGL.Vector3.Add(projection, cross);
        var actualNormalizedEye = actualEye.Normalized;
        projection = WebGL.Vector3.Add(projection, (WebGL.Vector3.Multiply(actualNormalizedEye, mouseOnHemiSphere.Z)));
        return projection;
    };
    Object.defineProperty(VirtualTrackBall.prototype, "DeltaVectorDrag", {
        get: function () {
            return WebGL.Vector3.Subtract(this.LastDragVector, this.FirstDragVector);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "DeltaPointDrag", {
        get: function () {
            return WebGL.Vector2.Subtract(this.LastDragPoint, this.FirstDragPoint);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "IsDragging", {
        get: function () {
            if (this.DeltaPointDrag.Length > 0)
                return true;
            return false;
        },
        enumerable: true,
        configurable: true
    });
    VirtualTrackBall.prototype.StartDragging = function (x, y) {
        this.FirstDragPoint = this.MousePointToScaledViewportPoint(x, y);
        if (this.IsRotating) {
            this.FirstDragVector = this.MouseOnHemiSphere(this.FirstDragPoint);
            this.FirstDragVector = this.FromMouseHemiSphereToWorld(this.FirstDragVector);
        }
        else {
        }
    };
    VirtualTrackBall.prototype.Drag = function (x, y) {
        this.LastDragPoint = this.MousePointToScaledViewportPoint(x, y);
        if (this.IsRotating) {
            this.LastDragVector = this.MouseOnHemiSphere(this.LastDragPoint);
            this.LastDragVector = this.FromMouseHemiSphereToWorld(this.LastDragVector);
        }
        else {
        }
    };
    Object.defineProperty(VirtualTrackBall.prototype, "RotationAxis", {
        get: function () {
            return WebGL.Vector3.Cross(this.FirstDragVector, this.LastDragVector);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "RotationAngle", {
        get: function () {
            var delta = this.DeltaVectorDrag;
            var angle = Math.PI / 2 * delta.Length;
            return new Angle(angle);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "Rotation", {
        get: function () {
            return WebGL.Quaternion.FromAxisAngle(this.RotationAxis, this.RotationAngle.Radians);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VirtualTrackBall.prototype, "RotationMatrix", {
        get: function () {
            var quatMatrix = WebGL.Matrix4.CreateFromQuaternion(this.Rotation);
            return quatMatrix;
        },
        enumerable: true,
        configurable: true
    });
    VirtualTrackBall.prototype.FinishDragging = function () {
        if (this.temp_Eye.Length > 0) {
            this._Eye = this.temp_Eye;
            this._Target = this.temp_Target;
            this._LookUp = this.temp_LookUp;
        }
        this.temp_Eye = WebGL.Vector3.Zero;
        this.temp_Target = WebGL.Vector3.Zero;
        this.temp_LookUp = WebGL.Vector3.Zero;
        this.FirstDragPoint = new WebGL.Vector2(0, 0);
        this.LastDragPoint = new WebGL.Vector2(0, 0);
        this.FirstDragVector = WebGL.Vector3.Zero;
        this.LastDragVector = WebGL.Vector3.Zero;
    };
    VirtualTrackBall.prototype.Reset = function () {
        this._ZoomFactor = 1;
        this._ZoomRadiusUnit = 100;
        this._Eye = new WebGL.Vector3(0, 0, this._ZoomFactor * this._ZoomRadiusUnit);
        this._Target = new WebGL.Vector3(0, 0, 0);
        this._LookUp = new WebGL.Vector3(0, 1, 0);
        this.temp_Eye = WebGL.Vector3.Zero;
        this.temp_Target = WebGL.Vector3.Zero;
        this.temp_LookUp = WebGL.Vector3.Zero;
        this.FirstDragPoint = new WebGL.Vector2(0, 0);
        this.LastDragPoint = new WebGL.Vector2(0, 0);
        this.FirstDragVector = WebGL.Vector3.Zero;
        this.LastDragVector = WebGL.Vector3.Zero;
    };
    return VirtualTrackBall;
}());
var R3Viewer = (function () {
    function R3Viewer(glcanvas) {
        var _this = this;
        this.MouseTouchMoving = null;
        this.MouseDoubleClicked = null;
        this.MouseClicked = null;
        this.HideDrawingGrid = false;
        this.DrawingBaseGrid = new DrawingTools.BaseGrid();
        this.DrawingLayers = new Array();
        this.CurrentDrawingLayer = new DrawingLayer();
        this.MainTrackBall = new VirtualTrackBall();
        this._CurrentMouseLocation = new WebGL.Vector2(0, 0);
        this.ClearColor = new WebGL.Vector4(1, 1, 1, 1);
        this.IsLeftMouseButtonDown = false;
        this.IsRightMouseButtonDown = false;
        this.IsMiddleMouseButtonDown = false;
        if (glcanvas == undefined) {
            alert("No valid canvas has been given");
            throw "No valid canvas";
        }
        this.Canvas = glcanvas;
        this.gl = null;
        try {
            this.gl = (this.Canvas.getContext("webgl") || this.Canvas.getContext("experimental-webgl"));
        }
        catch (e) { }
        if (!this.gl) {
            alert("Unable to initialize WebGL. Your browser may not support it.");
            this.gl = null;
        }
        this.Canvas.width = this.Canvas.parentElement.clientWidth;
        this.Canvas.height = this.Canvas.parentElement.clientHeight;
        var self = this;
        var this_Canvas = this.Canvas;
        window.addEventListener("resize", function (e) { return _this.CanvasResize(_this, e); }, false);
        this_Canvas.addEventListener("mousedown", function (e) { return _this.MouseDown(_this, e); }, false);
        this_Canvas.addEventListener("touchstart", function (e) { return _this.TouchStart(_this, e); }, false);
        this_Canvas.addEventListener("mouseup", function (e) { return _this.MouseUp(_this, e); }, false);
        this_Canvas.addEventListener("touchend", function (e) { return _this.TouchEnd(_this, e); }, false);
        this_Canvas.addEventListener("mousemove", function (e) { return _this.MouseMove(_this, e); }, false);
        this_Canvas.addEventListener("touchmove", function (e) { return _this.TouchMove(_this, e); }, false);
        this_Canvas.addEventListener("contextmenu", function (e) { return e.preventDefault(); }, false);
        this_Canvas.addEventListener("click", function (e) { return _this.MouseClick(_this, e); }, false);
        this_Canvas.addEventListener("dblclick", function (e) { return _this.MouseDoubleClick(_this, e); }, false);
        this_Canvas.addEventListener("DOMMouseScroll", function (e) { return _this.MouseScroll(_this, e); }, false);
        this_Canvas.addEventListener("mousewheel", function (e) { return _this.MouseScroll(_this, e); }, false);
        window.addEventListener("keydown", function (e) { return _this.KeyDown(_this, e); }, false);
    }
    Object.defineProperty(R3Viewer.prototype, "ZoomFactor", {
        get: function () {
            return this.MainTrackBall.ZoomFactor;
        },
        set: function (value) {
            this.MainTrackBall.ZoomFactor = value;
            this.Paint();
        },
        enumerable: true,
        configurable: true
    });
    R3Viewer.prototype.UnProject3D = function (scaledPoint) {
        var finalMatrix;
        var _in = WebGL.Vector4.Zero;
        var _out = WebGL.Vector4.Zero;
        finalMatrix = this.MainTrackBall.FinalMatrix;
        finalMatrix = WebGL.Matrix4.Invert(finalMatrix);
        _in.X = scaledPoint.X;
        _in.Y = scaledPoint.Y;
        _in.Z = 0.0;
        _in.W = 1.0;
        _out = WebGL.Vector4.Transform(_in, finalMatrix);
        if (_out.W == 0.0)
            return (_out.Xyz);
        _out.X /= _out.W;
        _out.Y /= _out.W;
        _out.Z /= _out.W;
        return (_out.Xyz);
    };
    R3Viewer.prototype.MousePointToScaledViewportPoint = function (p) {
        var x = ((2.0 * p.X) / this.Canvas.width) - 1.0;
        var y = 1.0 - ((2.0 * p.Y) / this.Canvas.height);
        var pf = new WebGL.Vector2(x, y);
        return pf;
    };
    Object.defineProperty(R3Viewer.prototype, "CurrentMouseLocation", {
        get: function () {
            return this._CurrentMouseLocation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R3Viewer.prototype, "CurrentScaledMouseLocation", {
        get: function () {
            return this.MousePointToScaledViewportPoint(this._CurrentMouseLocation);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(R3Viewer.prototype, "CurrentMouseProjectedLocation", {
        get: function () {
            var cmpl = this.UnProject3D(this.CurrentScaledMouseLocation);
            return cmpl;
        },
        enumerable: true,
        configurable: true
    });
    R3Viewer.prototype.InvalidateSize = function () {
        this.Canvas.style.width = this.Canvas.parentElement.clientWidth + 'px';
        this.Canvas.style.height = this.Canvas.parentElement.clientHeight + 'px';
        this.Canvas.width = this.Canvas.clientWidth;
        this.Canvas.height = this.Canvas.clientHeight;
        this.Paint();
    };
    R3Viewer.prototype.CanvasResize = function (self, e) {
        this.InvalidateSize();
    };
    R3Viewer.findPos = function (obj) {
        var curleft = 0, curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return { x: curleft, y: curtop };
        }
        return undefined;
    };
    Object.defineProperty(R3Viewer.prototype, "CanvasDomPosition", {
        get: function () {
            var pos = R3Viewer.findPos(this.Canvas);
            return pos;
        },
        enumerable: true,
        configurable: true
    });
    R3Viewer.prototype.MouseDown = function (self, e) {
        e.preventDefault();
        if (e.button == 0) {
            this.IsLeftMouseButtonDown = true;
        }
        if (e.button == 2) {
            this.IsRightMouseButtonDown = true;
        }
        if (e.button == 1) {
            this.IsMiddleMouseButtonDown = true;
        }
        if (this.IsMiddleMouseButtonDown) {
            self.MainTrackBall.IsPanning = true;
            self.MainTrackBall.StartDragging(self.CurrentMouseLocation.X, self.CurrentMouseLocation.Y);
        }
        if (this.IsLeftMouseButtonDown) {
            self.MainTrackBall.IsRotating = true;
            self.MainTrackBall.StartDragging(self.CurrentMouseLocation.X, self.CurrentMouseLocation.Y);
        }
    };
    R3Viewer.prototype.MouseUp = function (self, e) {
        if (e.button == 0) {
            this.IsLeftMouseButtonDown = false;
        }
        if (e.button == 2) {
            this.IsRightMouseButtonDown = false;
        }
        if (e.button == 1) {
            this.IsMiddleMouseButtonDown = false;
        }
        self.MainTrackBall.FinishDragging();
        self.Paint();
    };
    R3Viewer.prototype.MouseMove = function (self, e) {
        e.preventDefault();
        var mouseX = 0;
        var mouseY = 0;
        if (e.touches) {
            mouseX = e.touches[0].pageX - this.CanvasDomPosition.x;
            mouseY = e.touches[0].pageY - this.CanvasDomPosition.y;
        }
        else {
            mouseX = e.pageX - this.CanvasDomPosition.x;
            mouseY = e.pageY - this.CanvasDomPosition.y;
        }
        self._CurrentMouseLocation = new WebGL.Vector2(mouseX, mouseY);
        if (self.MouseTouchMoving != null) {
            self.MouseTouchMoving(self, {
                OriginalEvent: e,
                MouseLocation: self.CurrentMouseLocation,
                ProjectedMouseLocation: self.CurrentMouseProjectedLocation,
            });
        }
        if (this.IsLeftMouseButtonDown == true || this.IsMiddleMouseButtonDown == true) {
            self.MainTrackBall.Drag(mouseX, mouseY);
        }
        self.Paint();
    };
    R3Viewer.prototype.TouchStart = function (self, e) {
        e.button = 0;
        this.MouseDown(self, e);
    };
    R3Viewer.prototype.TouchEnd = function (self, e) {
        e.button = 0;
        this.MouseUp(self, e);
    };
    R3Viewer.prototype.TouchMove = function (self, e) {
        this.MouseMove(self, e);
    };
    R3Viewer.prototype.MouseClick = function (self, e) {
    };
    R3Viewer.prototype.MouseDoubleClick = function (self, e) {
    };
    R3Viewer.prototype.MouseScroll = function (self, e) {
        var evt = window.event || e;
        var delta = evt.detail ? evt.detail * (-120) : evt.wheelDelta;
        var BeforeMouseWheel = self.CurrentMouseProjectedLocation;
        if (delta < 0) {
            self.ZoomFactor += 0.1;
        }
        else {
            self.ZoomFactor -= 0.1;
        }
        var AfterMouseWheel = self.CurrentMouseProjectedLocation;
        var pdelta = WebGL.Vector3.Subtract(AfterMouseWheel, BeforeMouseWheel);
        self.Paint();
    };
    R3Viewer.prototype.KeyDown = function (self, e) {
        var x = e.which || e.keyCode;
        var y = String.fromCharCode(x);
        if (x == "119") {
            this.HideDrawingGrid = !this.HideDrawingGrid;
        }
        self.Paint();
    };
    R3Viewer.prototype.Paint = function () {
        this.MainTrackBall.ViewportWidth = this.Canvas.width;
        this.MainTrackBall.ViewportHeight = this.Canvas.height;
        if (this.gl) {
            this.gl.clearColor(this.ClearColor.X, this.ClearColor.Y, this.ClearColor.Z, this.ClearColor.W);
            this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT | this.gl.STENCIL_BUFFER_BIT);
            this.gl.viewport(0, 0, this.Canvas.width, this.Canvas.height);
            this.DrawingBaseGrid.Height = this.MainTrackBall.ViewportHeight / 2;
            this.DrawingBaseGrid.Width = this.MainTrackBall.ViewportWidth / 2;
            this.DrawingBaseGrid.ProjectionMatrix = this.MainTrackBall.FinalMatrix;
            this.DrawingBaseGrid.DeltaWidth = 0;
            this.DrawingBaseGrid.DeltaHeight = 0;
            this.gl.enable(WebGLRenderingContext.DEPTH_TEST);
            this.gl.depthFunc(WebGLRenderingContext.LESS);
            if (!this.HideDrawingGrid) {
                this.DrawingBaseGrid.Render(this.gl);
                this.DrawingBaseGrid.RenderCenterLines(this.gl);
            }
            this.RenderEverything();
            this.gl.disable(WebGLRenderingContext.DEPTH_TEST);
        }
    };
    R3Viewer.prototype.RenderEverything = function () {
        var _this = this;
        this.CurrentDrawingLayer.ProjectionMatrix = this.MainTrackBall.FinalMatrix;
        var lls = this.CurrentDrawingLayer.AllWorkingLines;
        var avp = this.CurrentDrawingLayer.AveragePoint.Inverted();
        lls.forEach(function (l) {
            l.ProjectionMatrix = _this.MainTrackBall.FinalMatrix;
            l.RenderWall(_this.gl, avp);
        });
    };
    return R3Viewer;
}());
var Shapes;
(function (Shapes) {
    var R2LineStrips = (function (_super) {
        __extends(R2LineStrips, _super);
        function R2LineStrips(firstPointLocation) {
            _super.call(this);
            this._Points = new Array();
            this._Lines = new Array();
            var firstPoint = new Shapes.R2Point();
            firstPoint.PointSize = 6;
            firstPoint.AddOwner(this);
            firstPoint.ShapeCenter = firstPointLocation.ToVector3();
            this._Points.push(firstPoint);
        }
        Object.defineProperty(R2LineStrips.prototype, "PointsCount", {
            get: function () {
                return this._Points.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "LinesCount", {
            get: function () {
                return this._Lines.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "FirstLine", {
            get: function () {
                return this._Lines[0];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "FirstPoint", {
            get: function () {
                return this.FirstLine.FirstPoint;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "LastLine", {
            get: function () {
                return this._Lines[this._Lines.length - 1];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "LastPoint", {
            get: function () {
                if (this._Points.length == 1)
                    return this._Points[0];
                return this.LastLine.LastPoint;
            },
            enumerable: true,
            configurable: true
        });
        R2LineStrips.prototype.AddPoint = function (location) {
            var point = new Shapes.R2Point();
            point.ShapeCenter = location.ToVector3();
            point.PointSize = 6;
            point.AddOwner(this);
            var line = new Shapes.R2Line();
            line.FirstPoint = this.LastPoint;
            line.LastPoint = point;
            line.AddOwner(this);
            this._Lines.push(line);
            this._Points.push(point);
        };
        R2LineStrips.prototype.RemoveLastLine = function () {
            this._Points.splice(this._Points.length - 1);
            this._Lines.splice(this._Lines.length - 1);
        };
        Object.defineProperty(R2LineStrips.prototype, "IsLoop", {
            get: function () {
                return this.FirstPoint.Equals(this.LastPoint);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "IsPolygon", {
            get: function () {
                return this.IsLoop;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "PointsLocations", {
            get: function () {
                var locs = new Array();
                this._Points.forEach(function (x) { return locs.push(x.ShapeCenter); });
                return locs;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2LineStrips.prototype, "Centroid", {
            get: function () {
                var TotalPoint = WebGL.Vector3.Zero;
                this._Points.forEach(function (p) {
                    TotalPoint = WebGL.Vector3.Add(TotalPoint, p.ShapeCenter);
                });
                return WebGL.Vector3.Divide(TotalPoint, this._Points.length);
            },
            enumerable: true,
            configurable: true
        });
        R2LineStrips.prototype.LinesOffset = function (length) {
            var cent = this.Centroid;
            this._Lines[0].Normal;
            var lines = new Array();
            this._Lines.forEach(function (l) {
                var lin = new Shapes.R2Line();
                lin.FirstPointLocation = l.FirstPointLocation;
                lin.LastPointLocation = l.LastPointLocation;
                lines.push(lin);
            });
            lines.forEach(function (l) {
                l.MoveLineAlongNormal(length);
            });
            for (var ix = 0; ix < lines.length; ix++) {
                var iy = ix + 1;
                if (ix == lines.length - 1)
                    iy = 0;
                var fl = lines[ix];
                var ll = lines[iy];
                var xx = fl.IntersectionWithRay(ll.FirstPointLocation.Xy, ll.LastPointLocation.Xy);
                if (xx != null) {
                    fl.LastPointLocation = xx.ToVector3();
                    ll.FirstPointLocation = xx.ToVector3();
                }
                else {
                }
            }
            return lines;
        };
        R2LineStrips.prototype.ArePointsEqualsTo = function (ls) {
            if (ls != null && ls._Points.length == this._Points.length) {
                for (var iy = 0; iy < ls._Points.length; iy++) {
                    var point = ls._Points[iy];
                    var count = 0;
                    for (var ix = 0; ix < this._Points.length; ix++) {
                        var op = this._Points[ix];
                        if (point.ShapeCenter.NearlyEqual(op.ShapeCenter))
                            count++;
                    }
                    if (count == 0)
                        return false;
                }
                return true;
            }
            return false;
        };
        R2LineStrips.prototype.IsEdgePoint = function (point) {
            for (var ix = 0; ix < this._Lines.length; ix++) {
                var line = this._Lines[ix];
                if (line.IsPointOnLineSegment(point))
                    return true;
            }
            return false;
        };
        R2LineStrips.prototype.IsPointInsidePolygon = function (point) {
            var i, j, c = false;
            var nvert = this.PointsCount;
            var vertx = new Array();
            var verty = new Array();
            for (var ix = 0; ix < this._Points.length; ix++) {
                vertx.push(this._Points[ix].ShapeCenter.X);
                verty.push(this._Points[ix].ShapeCenter.Y);
            }
            var testx = point.X;
            var testy = point.Y;
            for (i = 0, j = nvert - 1; i < nvert; j = i++) {
                if (((verty[i] > testy) != (verty[j] > testy)) &&
                    (testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
                    c = !c;
            }
            return c;
        };
        R2LineStrips.prototype.ContainsPolygon = function (polygon) {
            var _this = this;
            var xx = 0;
            polygon._Points.forEach(function (p) {
                if (_this.IsEdgePoint(p.ShapeCenter.Xy) || _this.IsPointInsidePolygon(p.ShapeCenter.Xy))
                    xx++;
            });
            if (polygon.PointsCount == xx) {
                return true;
            }
            return false;
        };
        R2LineStrips.prototype.OnRender = function (gl) {
            if (!R2LineStrips.Program.IsLinked(gl))
                R2LineStrips.Program.Link(gl);
            R2LineStrips.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            R2LineStrips.Program.Uniforms.ShapeColor = this.ShapeColor;
            R2LineStrips.Program.ArrayVertexSize = 3;
            R2LineStrips.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(this.PointsLocations));
            R2LineStrips.Program.BeginUse(gl);
            gl.drawArrays(WebGLRenderingContext.LINE_STRIP, 0, this._Points.length);
            R2LineStrips.Program.EndUse(gl);
        };
        R2LineStrips.VertexShader = new WebGL.Shaders.VertexShader("\
                attribute vec4 ShapeCenter;\
                uniform mat4 ProjectionMatrix;\
                void main()\
                {\
                    {\
                        gl_Position = ProjectionMatrix * ShapeCenter;\
                        gl_PointSize = 1.0;\
                    }\
                } ");
        R2LineStrips.FragmentShader = new WebGL.Shaders.FragmentShader("\
                precision mediump float;\
                uniform vec4 ShapeColor;\
			    void main()\
                {\
                    {\
                        gl_FragColor = ShapeColor;\
                    }\
                } \
            ");
        R2LineStrips.Program = new WebGL.Shaders.PipelineProgram("R2LineStripsProgram", Shapes.R2Line.VertexShader, Shapes.R2Line.FragmentShader);
        return R2LineStrips;
    }(Shapes.R2Shape));
    Shapes.R2LineStrips = R2LineStrips;
})(Shapes || (Shapes = {}));
var Shapes;
(function (Shapes) {
    var R2Rectangle = (function (_super) {
        __extends(R2Rectangle, _super);
        function R2Rectangle() {
            _super.call(this);
            this.TopLeftPoint = new Shapes.R2Point();
            this.TopRightPoint = new Shapes.R2Point();
            this.BottomRightPoint = new Shapes.R2Point();
            this.BottomLeftPoint = new Shapes.R2Point();
            this.CenterPoint = new Shapes.R2Point();
            this._TopLine = new Shapes.R2Line();
            this._RightLine = new Shapes.R2Line();
            this._BottomLine = new Shapes.R2Line();
            this._LeftLine = new Shapes.R2Line();
            this.ActAsSelector = false;
            this.EditingControls[this.BottomRightPoint.ColorKey] = this.BottomRightPoint;
            this.EditingControls[this.CenterPoint.ColorKey] = this.CenterPoint;
            this.AddChild(this.BottomRightPoint);
            this.AddChild(this.CenterPoint);
            this._TopLine.FirstPoint = this.TopLeftPoint;
            this._TopLine.LastPoint = this.TopRightPoint;
            this._RightLine.FirstPoint = this.TopRightPoint;
            this._RightLine.LastPoint = this.BottomRightPoint;
            this._BottomLine.FirstPoint = this.BottomRightPoint;
            this._BottomLine.LastPoint = this.BottomLeftPoint;
            this._LeftLine.FirstPoint = this.BottomLeftPoint;
            this._LeftLine.LastPoint = this.TopLeftPoint;
        }
        Object.defineProperty(R2Rectangle.prototype, "TopLine", {
            get: function () {
                return this._TopLine;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "RightLine", {
            get: function () {
                return this._RightLine;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "BottomLine", {
            get: function () {
                return this._BottomLine;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "LeftLine", {
            get: function () {
                return this._LeftLine;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "ShapeName", {
            get: function () {
                return "R2Rectangle";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "Width", {
            get: function () {
                return this.TopRightPointLocation.X - this.TopLeftPointLocation.X;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "Height", {
            get: function () {
                return this.TopRightPointLocation.Y - this.BottomRightPointLocation.Y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "TopLeftPointLocation", {
            get: function () {
                return this.TopLeftPoint.ShapeCenter;
            },
            set: function (value) {
                this.TopLeftPoint.ShapeCenter = value;
                this.CalcCenter();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "TopRightPointLocation", {
            get: function () {
                return this.TopRightPoint.ShapeCenter;
            },
            set: function (value) {
                this.TopRightPoint.ShapeCenter = value;
                this.CalcCenter();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "BottomRightPointLocation", {
            get: function () {
                return this.BottomRightPoint.ShapeCenter;
            },
            set: function (value) {
                this.BottomRightPoint.ShapeCenter = value;
                this.CalcCenter();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "BottomLeftPointLocation", {
            get: function () {
                return this.BottomLeftPoint.ShapeCenter;
            },
            set: function (value) {
                this.BottomLeftPoint.ShapeCenter = value;
                this.CalcCenter();
            },
            enumerable: true,
            configurable: true
        });
        R2Rectangle.prototype.CalcCenter = function () {
            var pc = new PointsCloud();
            pc.AddPoint("", this.TopLeftPointLocation);
            pc.AddPoint("", this.TopRightPointLocation);
            pc.AddPoint("", this.BottomRightPointLocation);
            pc.AddPoint("", this.BottomLeftPointLocation);
            this.CenterPoint.ShapeCenter = pc.AveragePoint;
        };
        R2Rectangle.prototype.OnEditingControlChange = function (childShape) {
            if (childShape.ColorKey == this.BottomRightPoint.ColorKey) {
                this.TopRightPointLocation = new WebGL.Vector3(this.BottomRightPoint.ShapeCenter.X, this.TopLeftPointLocation.Y, 0);
                this.BottomLeftPointLocation = new WebGL.Vector3(this.TopLeftPointLocation.X, this.BottomRightPoint.ShapeCenter.Y, 0);
            }
            else if (childShape.ColorKey == this.CenterPoint.ColorKey) {
                var delta = WebGL.Vector3.Subtract(this.CenterPoint.ShapeCenter, this.CenterPoint.PreviousShapeCenter);
                this.BottomLeftPoint.ShapeCenter = WebGL.Vector3.Add(this.BottomLeftPoint.ShapeCenter, delta);
                this.TopLeftPoint.ShapeCenter = WebGL.Vector3.Add(this.TopLeftPoint.ShapeCenter, delta);
                this.TopRightPoint.ShapeCenter = WebGL.Vector3.Add(this.TopRightPoint.ShapeCenter, delta);
                this.BottomRightPoint.ShapeCenter = WebGL.Vector3.Add(this.BottomRightPoint.ShapeCenter, delta);
            }
        };
        R2Rectangle.prototype.OnRender = function (gl) {
            if (!Shapes.R2Line.Program.IsLinked(gl))
                Shapes.R2Line.Program.Link(gl);
            if (this.InnerTextSprite == null) {
                if (this.ActAsSelector) {
                    this.TopLine.ProjectionMatrix = this.ProjectionMatrix;
                    this.BottomLine.ProjectionMatrix = this.ProjectionMatrix;
                    this.LeftLine.ProjectionMatrix = this.ProjectionMatrix;
                    this.RightLine.ProjectionMatrix = this.ProjectionMatrix;
                    this.TopLine.RenderDotted(gl);
                    this.BottomLine.RenderDotted(gl);
                    this.LeftLine.RenderDotted(gl);
                    this.RightLine.RenderDotted(gl);
                }
                else {
                    Shapes.R2Line.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
                    Shapes.R2Line.Program.Uniforms.ShapeColor = this.ShapeColor;
                    Shapes.R2Line.Program.ArrayVertexSize = 3;
                    var lines = new Array();
                    lines.push(this.TopLeftPointLocation);
                    lines.push(this.TopRightPointLocation);
                    lines.push(this.BottomRightPointLocation);
                    lines.push(this.BottomLeftPointLocation);
                    Shapes.R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
                    Shapes.R2Line.Program.BeginUse(gl);
                    gl.drawArrays(WebGLRenderingContext.LINE_LOOP, 0, lines.length);
                    Shapes.R2Line.Program.EndUse(gl);
                }
            }
            if (this.IsDrawing || this.IsSelected) {
                this.BottomRightPoint.ProjectionMatrix = this.ProjectionMatrix;
                this.BottomRightPoint.Render(gl);
                this.CenterPoint.ProjectionMatrix = this.ProjectionMatrix;
                this.CenterPoint.Render(gl);
            }
            if (this.InnerTextSprite != null) {
                this.InnerTextSprite.Top = this.TopLeftPointLocation.Y;
                this.InnerTextSprite.Left = this.TopLeftPointLocation.X;
                this.InnerTextSprite.Width = this.Width;
                this.InnerTextSprite.Height = this.Height;
                this.InnerTextSprite.ProjectionMatrix = this.ProjectionMatrix;
                this.InnerTextSprite.UpdateTexture = true;
                this.InnerTextSprite.Render(gl);
            }
        };
        R2Rectangle.prototype.OnRenderForPicking = function (gl) {
            if (!Shapes.R2Line.Program.IsLinked(gl))
                Shapes.R2Line.Program.Link(gl);
            Shapes.R2Line.Program.Uniforms.ProjectionMatrix = this.ProjectionMatrix;
            Shapes.R2Line.Program.Uniforms.ShapeColor = this.SelectionColorVector;
            Shapes.R2Line.Program.ArrayVertexSize = 3;
            if (this.InnerTextSprite != null) {
                var lines = new Array();
                lines.push(this.TopLeftPointLocation);
                lines.push(this.TopRightPointLocation);
                lines.push(this.BottomRightPointLocation);
                lines.push(this.TopLeftPointLocation);
                lines.push(this.BottomLeftPointLocation);
                lines.push(this.BottomRightPointLocation);
                Shapes.R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
                Shapes.R2Line.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, lines.length);
                Shapes.R2Line.Program.EndUse(gl);
            }
            else {
                var lines = new Array();
                lines.push(this.TopLeftPointLocation);
                lines.push(this.TopRightPointLocation);
                lines.push(this.BottomRightPointLocation);
                lines.push(this.BottomLeftPointLocation);
                Shapes.R2Line.Program.ArrayVertexAttributes.ShapeCenter = new Float32Array(WebGL.Vectors3ToArray(lines));
                Shapes.R2Line.Program.BeginUse(gl);
                gl.drawArrays(WebGLRenderingContext.LINE_LOOP, 0, lines.length);
                Shapes.R2Line.Program.EndUse(gl);
            }
        };
        R2Rectangle.prototype.GetIntersectionsWith = function (shape) {
            var intersections = new Array();
            if (shape instanceof Shapes.R2Line) {
                var targetLine = shape;
                var iL = targetLine.GetIntersectionsWith(this);
                iL.forEach(function (x) { return intersections.push(x); });
            }
            if (shape instanceof Shapes.R2Arc) {
                var targetArc = shape;
                var iA = targetArc.GetIntersectionsWith(this);
                iA.forEach(function (x) { return intersections.push(x); });
            }
            if (shape instanceof Shapes.R2Circle) {
                var targetCircle = shape;
                var iC = targetCircle.GetIntersectionsWith(this);
                iC.forEach(function (x) { return intersections.push(x); });
            }
            if (shape instanceof R2Rectangle) {
                var targetRectangle = shape;
                var iR = this.GetIntersectionsWith(targetRectangle.TopLine);
                iR.forEach(function (x) { return intersections.push(x); });
                iR = this.GetIntersectionsWith(targetRectangle.RightLine);
                iR.forEach(function (x) { return intersections.push(x); });
                iR = this.GetIntersectionsWith(targetRectangle.BottomLine);
                iR.forEach(function (x) { return intersections.push(x); });
                iR = this.GetIntersectionsWith(targetRectangle.LeftLine);
                iR.forEach(function (x) { return intersections.push(x); });
            }
            return intersections;
        };
        R2Rectangle.prototype.IntersectionWithRay = function (firstPointLocation, lastPointLocation) {
            var intersections = new Array();
            var line = new WebGL.Geometry.Line();
            line.FirstPoint = firstPointLocation;
            line.LastPoint = lastPointLocation;
            var ii = line.IntersectionWithRay(this.TopLine.FirstPointLocation.Xy, this.TopLine.LastPointLocation.Xy);
            if (ii != null) {
                if (this.TopLine.IsPointOnLineSegment(ii))
                    intersections.push(ii);
            }
            ii = line.IntersectionWithRay(this.RightLine.FirstPointLocation.Xy, this.RightLine.LastPointLocation.Xy);
            if (ii != null) {
                if (this.RightLine.IsPointOnLineSegment(ii))
                    intersections.push(ii);
            }
            ii = line.IntersectionWithRay(this.BottomLine.FirstPointLocation.Xy, this.BottomLine.LastPointLocation.Xy);
            if (ii != null) {
                if (this.BottomLine.IsPointOnLineSegment(ii))
                    intersections.push(ii);
            }
            ii = line.IntersectionWithRay(this.LeftLine.FirstPointLocation.Xy, this.LeftLine.LastPointLocation.Xy);
            if (ii != null) {
                if (this.LeftLine.IsPointOnLineSegment(ii))
                    intersections.push(ii);
            }
            return intersections;
        };
        R2Rectangle.prototype.IntersectionWithLineSegment = function (firstPointLocation, lastPointLocation) {
            var intersections = new Array();
            var line = new WebGL.Geometry.Line();
            line.FirstPoint = firstPointLocation;
            line.LastPoint = lastPointLocation;
            var ii = line.IntersectionWithLineSegment(this.TopLine.FirstPointLocation.Xy, this.TopLine.LastPointLocation.Xy);
            if (ii != null)
                intersections.push(ii);
            ii = line.IntersectionWithLineSegment(this.RightLine.FirstPointLocation.Xy, this.RightLine.LastPointLocation.Xy);
            if (ii != null)
                intersections.push(ii);
            ii = line.IntersectionWithLineSegment(this.BottomLine.FirstPointLocation.Xy, this.BottomLine.LastPointLocation.Xy);
            if (ii != null)
                intersections.push(ii);
            ii = line.IntersectionWithLineSegment(this.LeftLine.FirstPointLocation.Xy, this.LeftLine.LastPointLocation.Xy);
            if (ii != null)
                intersections.push(ii);
            return intersections;
        };
        Object.defineProperty(R2Rectangle.prototype, "IsComposite", {
            get: function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "InnerShapes", {
            get: function () {
                var composites = new Array();
                composites.push(this.TopLine, this.RightLine, this.BottomLine, this.LeftLine);
                return composites;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "MinimumX", {
            get: function () {
                return Math.min(this.TopLine.MinimumX, this.RightLine.MinimumX, this.BottomLine.MinimumX, this.LeftLine.MinimumX);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "MaximumX", {
            get: function () {
                return Math.max(this.TopLine.MinimumX, this.RightLine.MinimumX, this.BottomLine.MinimumX, this.LeftLine.MinimumX);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "MinimumY", {
            get: function () {
                return Math.min(this.TopLine.MinimumY, this.RightLine.MinimumY, this.BottomLine.MinimumY, this.LeftLine.MinimumY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "MaximumY", {
            get: function () {
                return Math.max(this.TopLine.MinimumY, this.RightLine.MinimumY, this.BottomLine.MinimumY, this.LeftLine.MinimumY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "TerminalPoints", {
            get: function () {
                var vv = new Array();
                vv.push(this.TopLeftPointLocation.Xy);
                vv.push(this.TopRightPointLocation.Xy);
                vv.push(this.BottomRightPointLocation.Xy);
                vv.push(this.BottomLeftPointLocation.Xy);
                return vv;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(R2Rectangle.prototype, "IsZero", {
            get: function () {
                return this.TopLine.IsZero && this.BottomLine.IsZero && this.RightLine.IsZero && this.LeftLine.IsZero;
            },
            enumerable: true,
            configurable: true
        });
        R2Rectangle.prototype.ApplySegmentation = function () {
            var _this = this;
            this.TopLine.ApplySegmentation();
            this.RightLine.ApplySegmentation();
            this.BottomLine.ApplySegmentation();
            this.LeftLine.ApplySegmentation();
            this._InnerSegments = new Array();
            this.TopLine.InnerSegments.forEach(function (p) { return _this._InnerSegments.push(p); });
            this.RightLine.InnerSegments.forEach(function (p) { return _this._InnerSegments.push(p); });
            this.BottomLine.InnerSegments.forEach(function (p) { return _this._InnerSegments.push(p); });
            this.LeftLine.InnerSegments.forEach(function (p) { return _this._InnerSegments.push(p); });
        };
        R2Rectangle.MakeRectangle = function (left, top, width, height) {
            var rc = new Shapes.R2Rectangle();
            rc.TopLeftPointLocation = new WebGL.Vector2(left, top).ToVector3();
            rc.TopRightPointLocation = new WebGL.Vector2(left + width, top).ToVector3();
            rc.BottomRightPointLocation = new WebGL.Vector2(left + width, top - height).ToVector3();
            rc.BottomLeftPointLocation = new WebGL.Vector2(left, top - height).ToVector3();
            return rc;
        };
        return R2Rectangle;
    }(Shapes.R2Shape));
    Shapes.R2Rectangle = R2Rectangle;
})(Shapes || (Shapes = {}));
var VolumeLayer = (function () {
    function VolumeLayer() {
    }
    return VolumeLayer;
}());
//# sourceMappingURL=HappyEngine.js.map